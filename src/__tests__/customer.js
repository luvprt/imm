import React from 'react';
import { shallow } from 'enzyme';
import { AddCustomerComponent } from '../app/view/screen/admin/customers/admin.addCustomers';

describe("Customer", () => {
    test("Check the user inputs for adding a customer.", () => {
        const component = shallow(<AddCustomerComponent testViaJest={true} />);
        component.setState({
            companyName: 'Test', department: 'Test', jobTitle: 'Test',
            phone: '1231231231', prefix: 'Mr', privateNotes: 'Test', publicNotes: 'Test',
            firstName: 'Test', lastName: 'Test', email: 'Test@Test.Test', testViaJest: true
        });
        component.find('#addCustomerButton').simulate('click');
        expect(component.state().companyNameErr).toEqual("");
        expect(component.state().prefixErr).toEqual("");
        expect(component.state().firstNameErr).toEqual("");
        expect(component.state().lastNameErr).toEqual("");
        expect(component.state().phoneErr).toEqual("");
        expect(component.state().emailErr).toEqual("");
        expect(component.state().departmentErr).toEqual("");
        expect(component.state().jobTitleErr).toEqual("");
        expect(component.state().privateNotesErr).toEqual("");
        expect(component.state().publicNotesErr).toEqual("");
    });
});