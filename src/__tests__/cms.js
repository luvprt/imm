import React from 'react';
import { shallow } from 'enzyme';
import { AddCmsPageComponent } from '../app/view/screen/admin/cms/admin.addCmsPage';

describe("Cms", () => {
    test("Check the user inputs for adding cms page.", () => {
        const component = shallow(<AddCmsPageComponent testViaJest={true} />);
        component.setState({
            pageTitle: 'Test Title', metaKeywords: 'Test Keywords',
            metaDescription: 'Test Description', pageContent: 'Test Content',
            testViaJest: true
        });
        component.find('#addCmsButtton').simulate('click');
        expect(component.state().pageTitleErr).toEqual("");
        expect(component.state().metaKeywordsErr).toEqual("");
        expect(component.state().metaDescriptionErr).toEqual("");
        expect(component.state().pageContentErr).toEqual("");
    });
});