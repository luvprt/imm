import React from 'react';
import { shallow } from 'enzyme';
import { AddAddOnsComponent } from '../app/view/screen/admin/add-ons/admin.addAddOns';

describe("Subscription Plans Add-ons", () => {
    test("Check the user inputs for adding Plan Add-ons.", () => {
        const component = shallow(<AddAddOnsComponent testViaJest={true} />);
        component.setState({
            name: 'Test', description: 'test', price: '12', testViaJest: true
        });
        component.find('#addAddOnButtton').simulate('click');
        expect(component.state().nameErr).toEqual("");
        expect(component.state().descriptionErr).toEqual("");
        expect(component.state().priceErr).toEqual("");
    });
});