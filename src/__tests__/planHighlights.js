import React from 'react';
import { shallow } from 'enzyme';
import { AddSubscriptionPlanHighlightComponent } from '../app/view/screen/admin/subscription-plans-highlights/admin.addSubscriptionPlanHighlight'

describe("Subscription Plan Highlights", () => {
    test("Check the user inputs for adding Subscription Plan Highlights.", () => {
        const component = shallow(<AddSubscriptionPlanHighlightComponent testViaJest={true} />);
        component.setState({
            planHighLights: 'Test', description: 'test description',
            testViaJest: true
        });
        component.find('#addPlanHighlights').simulate('click');
        expect(component.state().planHighLightsErr).toEqual("");
        expect(component.state().descriptionErr).toEqual("");
    });
});