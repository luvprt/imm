import React from 'react';
import { shallow } from 'enzyme';
import { LocationAddressPage } from '../app/view/screen/admin/location/admin.locationAddress'

describe("Location", () => {
  test("Check the user inputs for locaation page.", () => {
    const component = shallow(<LocationAddressPage testViaJest={true} />);
    component.setState({
      user: '1', streetAddress: 'test', apt: 'test', state: 'punjab',
      city: 'mohali', postalCode: '160055', testViaJest: true
    });
    component.find('#testApiAddLoction').simulate('click');
    expect(component.state().customerIdErr).toEqual("");
    expect(component.state().addressErr).toEqual("");
    expect(component.state().aptErr).toEqual("");
    expect(component.state().cityErr).toEqual("");
    expect(component.state().stateErr).toEqual("");
    expect(component.state().zipCodeErr).toEqual("");
  });
});