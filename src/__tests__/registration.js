import React from 'react';
import { shallow } from 'enzyme';
import { RegisterPage } from '../app/view/screen/website/register'

describe("Register", () => {
  test("Check the user inputs for register page.", () => {
    const component = shallow(<RegisterPage testViaJest={true} />);
    component.setState({
      firstName: 'Test', lastName: 'Test',
      email: 'test@yopmail.com', password: 'Test@123',
      confirmPassword: 'Test@123', phone: '1234567891',
      address: 'test', apt: 'test', state: 'punjab',
      city: 'mohali', zipcode: '160055', testViaJest: true
    });
    component.find('#addRegisterButton').simulate('click');
    expect(component.state().firstNameErr).toEqual("");
    expect(component.state().lastNameErr).toEqual("");
    expect(component.state().emailErr).toEqual("");
    expect(component.state().passwordErr).toEqual("");
    expect(component.state().confirmPasswordErr).toEqual("");
    expect(component.state().phoneErr).toEqual("");
    expect(component.state().addressErr).toEqual("");
    expect(component.state().aptErr).toEqual("");
    expect(component.state().cityErr).toEqual("");
    expect(component.state().stateErr).toEqual("");
    expect(component.state().zipcodeErr).toEqual("");
  });
});