import React from 'react';
import { shallow } from 'enzyme';
import { AdminUserLogin } from '../app/view/screen/admin/login/admin.login';

describe("Login", () => {
  test("Check the user inputs for login.", () => {
    const component = shallow(<AdminUserLogin />);
    component.setState({
      email: 'test@test.com', password: 'Test@1234', testViaJest: true
    })
    component.find('#signInButton').simulate('click');
    expect(component.state().emailErr).toEqual("");
    expect(component.state().passwordErr).toEqual("");
  });
});