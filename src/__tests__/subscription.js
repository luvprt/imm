import React from 'react';
import { shallow } from 'enzyme';
import { AddSubscriptionPlanComponent } from '../app/view/screen/admin/subscription-plans/admin.addSubscriptionPlan';

describe("Subscription", () => {
    test("Check the user inputs for adding Subscription Plan.", () => {
        const component = shallow(<AddSubscriptionPlanComponent testViaJest={true} />);
        component.setState({
            noOfPersons: 5, planPrice: 20,
            testViaJest: true
        });
        component.find('#addSubPlan').simulate('click');
        expect(component.state().noOfPersonsErr).toEqual("");
        expect(component.state().planPriceErr).toEqual("");
    });
});