import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { store } from './app/store/store.factory'
import allRouter from './app/routing/route'
import history from './app/routing/history'
import { Router } from 'react-router-dom';
import './app/assets/css/stylesheet-frontend.css'
import './app/assets/css/stylesheet-backend.css'
import './app/assets/css/bootstrap.css'
import './app/assets/css/fonts.css'
import './app/assets/website/vendor/font-awesome/css/all.min.css'
import './app/assets/css/custom.css'
import '../node_modules/bootstrap/dist/js/bootstrap'
import "react-datepicker/dist/react-datepicker.css";
import { StripeProvider, Elements } from 'react-stripe-elements';

history.listen(_ => {
    window.scrollTo(0, 0)  
})

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
        <StripeProvider apiKey="pk_test_u5xrQnc8nzgsYp3lXwRXbAMU00I6i0z97t">
            <Elements>
                {allRouter}
            </Elements>
        </StripeProvider>
        </Router>
    </Provider>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
