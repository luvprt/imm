const BASE_URL = process.env.REACT_APP_API_BASE_URL;
const URL = (uri) => `${BASE_URL}${uri}`;

/***** Auth routes*********/
export const AUTH_LOGIN = URL('/auth/login');
export const FORGOT_PASSWORD = URL('/auth/forgotPassword');
export const USER_REGISTRATION = URL('/auth/registration');
export const ADD_MEMBERSHIP = URL('/auth/createMembership');
export const GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT = URL('/auth/getSubscriptionPlanWithHighlight');
export const GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID = URL('/auth/getSubscriptionPlanWithHighlightById');
export const UPDATE_USER_PLAN = URL('/auth/updateUserPlan');
export const GET_CMS_PAGE_DATA = URL('/auth/getPageByTitle');
export const EMAIL_EXIST = URL('/auth/checkEmailExists');
export const GET_CMS_PAGE_BY_TYPE = URL('/auth/getPagesByPageType');
export const GET_CMS_PAGE_DETAIL_BY_SLUG = URL('/auth/getPageBySlug');
export const VIEW_CLEARBIT_DATA_CUSTOMER = URL('/auth/clearbit')


/***** User routes*********/
export const RESET_PASSWORD = URL('/users/resetPassword');
export const VERIFY_TOKEN = URL('/users/verifyToken');

/***** Cms routes*********/
export const ADD_CMS_PAGE = URL('/cms/addCmsPage');
export const EDIT_CMS_PAGE = URL('/cms/editCmsPage');
export const VIEW_CMS_PAGE = URL('/cms/getPageById');
export const LIST_CMS_PAGE = URL('/cms/getCmsPages');
export const DELETE_CMS_PAGE = URL('/cms/deleteCmsPage');
export const GET_ALL_CMS_PAGES = URL('/cms/getAllCmsPages');

/***** Subscription Plan**/
export const ADD_SUBSCRIPTION_PLAN = URL('/subscriptionPlans/addPlan');
export const LIST_SUBSCRIPTION_PLANS = URL('/subscriptionPlans/getPlans');
export const SHOW_SUBSCRIPTION_PLANS = URL('/subscriptionPlans/getPlanById');
export const EDIT_SUBSCRIPTION_PLANS = URL('/subscriptionPlans/editPlan');
export const DELETE_SUBSCRIPTION_PLAN = URL('/subscriptionPlans/deletePlan');

/***** Subscription Plan Highlight**/
export const ADD_SUBSCRIPTION_PLAN_HIGHLIGHT = URL('/planHighlight/addHighlight');
export const LIST_SUBSCRIPTION_PLANS_HIGHLIGHT = URL('/planHighlight/getHighlight');
export const SHOW_SUBSCRIPTION_PLANS_HIGHLIGHT = URL('/planHighlight/getPlanHighlightById');
export const EDIT_SUBSCRIPTION_PLANS_HIGHLIGHT = URL('/planHighlight/editHighlight');
export const GET_All_USER_PLAN_TYPES = URL('/planHighlight/getAllUserPlanTypes');
export const DELETE_SUBSCRIPTION_PLANS_HIGHLIGHT = URL('/planHighlight/deleteHighLight');

/***** Add Ons and other Stats **/
export const GET_ALL_ADD_ONS = URL('/addOns/getAllAddOnsAndStats');
export const ADD_ADD_ON = URL('/addOns/addAddOn')
export const DELETE_ADD_ON = URL('/addOns/deleteAddOn')
export const LIST_ADD_ON = URL('/addOns/getAddOns')
export const EDIT_ADD_ON = URL('/addOns/editAddOn')
export const VIEW_ADD_ON = URL('/addOns/getAddOnById')

/***** Website **/
export const CONTACT_US = URL('/auth/contactUs');

/***** Location **/
export const ADD_LOCATION = URL('/location/addLocation');
export const LIST_LOCATION = URL('/location/getLocation');
export const DELETE_LOCATION = URL('/location/deleteLocation');
export const GET_LOCATION_BY_ID = URL('/location/getLocationById');
export const EDIT_LOCATION = URL('/location/editLocation');
export const VIEW_LOCATION = URL('/location/getCustomersByLocation');
export const GET_CUSTOMERS_BY_ROLE = URL('/location/getCustomersByRole');
export const LOCATION_STATUS = URL('/location/changeLocationStatus');


/***** Customer **/
export const ADD_CUSTOMER = URL('/customer/addCustomer');

/***** Employees **/
export const ADD_EMPLOYEE = URL('/employee/addEmployeePersonal');
export const LIST_EMPLOYEES = URL('/employee/getEmployees');
