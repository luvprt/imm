import {
  CONTACT_US,
} from '../routing/route';
import {
  request
} from '../request/axios.request'

export async function contactUsApi(contactUs) {
  let contactUsData = contactUs.data;
  return request({
      url: CONTACT_US,
      method: 'post',
      data: contactUsData
  })
}