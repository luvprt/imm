import {
    AUTH_LOGIN,
    FORGOT_PASSWORD,
    RESET_PASSWORD,
    VERIFY_TOKEN,
    USER_REGISTRATION,
    EMAIL_EXIST
} from '../routing/route';
import {
    request
} from '../request/axios.request'

export async function loginUser(userData) {
    let loginData = userData.data;
    return request({
        url: AUTH_LOGIN,
        method: 'post',
        data: loginData
    })
}

export async function userForgotPassword(userData) {
    let loginData = userData.data;
    return request({
        url: FORGOT_PASSWORD,
        method: 'post',
        data: loginData
    })
}

export async function userResetPassword(resetData) {
    let resetPasswordData = resetData.data;
    return request({
        url: RESET_PASSWORD,
        method: 'post',
        data: resetPasswordData
    })
}

export async function userVerifyToken() {
    return request({
        url: VERIFY_TOKEN,
        method: 'get'
    })
}

export async function userRegistrationApi(data) {
    let userData = data.data;
    return request({
        url: USER_REGISTRATION,
        method: 'post',
        data: userData
    })
}

// Check Email Exist Api 
export async function emaiExistApi(locationData) {
    let email = locationData.data;
    return request({
        url: EMAIL_EXIST + '/' + email,
        method: 'get'    
    })
  }