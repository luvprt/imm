import {
    ADD_CMS_PAGE,
    EDIT_CMS_PAGE,
    VIEW_CMS_PAGE,
    LIST_CMS_PAGE,
    DELETE_CMS_PAGE,
    GET_ALL_CMS_PAGES,
    GET_CMS_PAGE_DATA,
    GET_CMS_PAGE_BY_TYPE,
    GET_CMS_PAGE_DETAIL_BY_SLUG
} from '../routing/route';
import {
    request
} from '../request/axios.request'

export async function addCmsPageApi(cmsData) {
    let cmsPageData = cmsData.data;
    return request({
        url: ADD_CMS_PAGE,
        method: 'post',
        data: cmsPageData
    })
}

export async function editCmsPageApi(cmsData) {
    let cmsPageData = cmsData.data;
    return request({
        url: EDIT_CMS_PAGE,
        method: 'put',
        data: cmsPageData
    })
}

export async function listCmsPagesApi(cmsData) {
    let cmsPageData = cmsData.data;
    return request({
        url: LIST_CMS_PAGE,
        method: 'post',
        data: cmsPageData
    })
}

export async function viewCmsPageApi(cmsData) {
    let pageId = cmsData.data;
    return request({
        url: VIEW_CMS_PAGE + '/' + pageId,
        method: 'get'
    })
}

export async function deleteCmsPagesApi(getPlanData) {
    let pageId = getPlanData.data;
    return request({
        url: DELETE_CMS_PAGE + '/' + pageId,
        method: 'delete'
    })
}

export async function getAllCmsPagesApi() {
    return request({
        url: GET_ALL_CMS_PAGES,
        method: 'get'
    })
}

export async function getCmsPageDataApi(cmsData) {
    let pageTitle = cmsData.data;
    return request({
        url: GET_CMS_PAGE_DATA + '/' + pageTitle,
        method: 'get'
    })
}

// Get Cms Pages By Types 
export async function cmsPageByTypeApi(cmsData) {
    let pageSlug = cmsData.data;
    return request({
        url: GET_CMS_PAGE_BY_TYPE + '/' + pageSlug,
        method: 'get'
    })
}

// Get Cms Pages By Types 
export async function cmsPageDetailBySlugApi(cmsData) {
    let pageSlug = cmsData.data;
    return request({
        url: GET_CMS_PAGE_DETAIL_BY_SLUG + '/' + pageSlug,
        method: 'get'
    })
}