import {
    ADD_SUBSCRIPTION_PLAN,
    LIST_SUBSCRIPTION_PLANS,
    SHOW_SUBSCRIPTION_PLANS,
    EDIT_SUBSCRIPTION_PLANS,
    GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
    GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
    ADD_MEMBERSHIP,
    DELETE_SUBSCRIPTION_PLAN,
    UPDATE_USER_PLAN
} from '../routing/route';

import {
    request
} from '../request/axios.request'

export async function addSubscriptionPlanApi(subscriptionData) {
    let addSubscription = subscriptionData.data;
    return request({
        url: ADD_SUBSCRIPTION_PLAN,
        method: 'post',
        data: addSubscription
    })
}

export async function listSubscriptionPlanApi(getPlans) {
    let listPlans = getPlans.data;
    return request({
        url: LIST_SUBSCRIPTION_PLANS,
        method: 'post',
        data: listPlans
    })
}

export async function showSubscriptionPlanApi(getPlanData) {
    let planId = getPlanData.data;
    return request({
        url: SHOW_SUBSCRIPTION_PLANS + '/' + planId,
        method: 'get'
    })
}

export async function editSubscriptionPlanApi(editPlan) {
    let planData = editPlan.data;
    return request({
        url: EDIT_SUBSCRIPTION_PLANS,
        method: 'put',
        data: planData
    })
}

export async function getSubscriptionPlanWithHighlightApi() {
    return request({
        url: GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
        method: 'get'
    })
}

export async function getSubscriptionPlanWithHighlightByIdApi(planData) {
    return request({
        url: GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
        method: 'post',
        data : planData.data
    })
}

export async function addMembershipApi(membershipData) {
    let addMembership = membershipData.data;
    return request({
        url: ADD_MEMBERSHIP,
        method: 'post',
        data: addMembership
    })
}

export async function deleteSubscriptionPlanApi(getPlanData) {
    let planId = getPlanData.data;
    return request({
        url: DELETE_SUBSCRIPTION_PLAN + '/' + planId,
        method: 'delete'
    })
}

export async function updateUserPlanApi(updateUserPlan) {
    let planData = updateUserPlan.data;
    return request({
        url: UPDATE_USER_PLAN,
        method: 'put',
        data: planData
    })
}