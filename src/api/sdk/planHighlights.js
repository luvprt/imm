import {
  ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
  LIST_SUBSCRIPTION_PLANS_HIGHLIGHT,
  SHOW_SUBSCRIPTION_PLANS_HIGHLIGHT,
  EDIT_SUBSCRIPTION_PLANS_HIGHLIGHT,
  GET_All_USER_PLAN_TYPES,
  DELETE_SUBSCRIPTION_PLANS_HIGHLIGHT
} from '../routing/route';
import {
  request
} from '../request/axios.request'

export async function addSubscriptionPlanHighlightApi(subscriptionHighlightData) {
  let addSubscriptionHighlight = subscriptionHighlightData.data;
  return request({
    url: ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
    method: 'post',
    data: addSubscriptionHighlight
  })
}

export async function listSubscriptionPlanHighlightApi(getPlansHighlight) {
  let listPlansHighlight = getPlansHighlight.data;
  return request({
    url: LIST_SUBSCRIPTION_PLANS_HIGHLIGHT,
    method: 'post',
    data: listPlansHighlight
  })
}

export async function showSubscriptionPlanHighlightApi(getPlanData) {
  let planHighlightId = getPlanData.data;
  return request({
    url: SHOW_SUBSCRIPTION_PLANS_HIGHLIGHT + '/' + planHighlightId,
    method: 'get'
  })
}

export async function editSubscriptionPlanHighlightApi(editPlanHighlight) {
  let planHighlightData = editPlanHighlight.data;
  return request({
    url: EDIT_SUBSCRIPTION_PLANS_HIGHLIGHT,
    method: 'put',
    data: planHighlightData
  })
}

export async function deleteSubscriptionPlanHighlightApi(getPlanData) {
  let planHighlightId = getPlanData.data;
  return request({
    url: DELETE_SUBSCRIPTION_PLANS_HIGHLIGHT + '/' + planHighlightId,
    method: 'delete'
  })
}

export async function getAllUserPlanTypesApi() {
  return request({
    url: GET_All_USER_PLAN_TYPES,
    method: 'get'
  })
}