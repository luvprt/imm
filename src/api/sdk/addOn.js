import {
    GET_ALL_ADD_ONS, ADD_ADD_ON, EDIT_ADD_ON,
    LIST_ADD_ON, VIEW_ADD_ON, DELETE_ADD_ON
} from '../routing/route';
import { request } from '../request/axios.request'

export async function getAddOnsApi() {
    return request({ url: GET_ALL_ADD_ONS, method: 'get' })
}

export async function addAddOnApi(addOnData) {
    let addAddOnData = addOnData.data;
    return request({ url: ADD_ADD_ON, method: 'post', data: addAddOnData })
}

export async function editAddOnApi(cmsData) {
    let cmsPageData = cmsData.data;
    return request({ url: EDIT_ADD_ON, method: 'put', data: cmsPageData })
}

export async function listAddOnsApi(cmsData) {
    let cmsPageData = cmsData.data;
    return request({ url: LIST_ADD_ON, method: 'post', data: cmsPageData })
}

export async function viewAddOnApi(cmsData) {
    let pageId = cmsData.data;
    return request({ url: VIEW_ADD_ON + '/' + pageId, method: 'get' })
}

export async function deleteAddOnApi(getPlanData) {
    let pageId = getPlanData.data;
    return request({ url: DELETE_ADD_ON + '/' + pageId, method: 'delete' })
}