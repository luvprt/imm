import { ADD_CUSTOMER, VIEW_CLEARBIT_DATA_CUSTOMER } from '../routing/route';
import { request } from '../request/axios.request'

export async function addCustomerApi(customerData) {
  let addCustomerData = customerData.data;
  return request({ url: ADD_CUSTOMER, method: 'post', data: addCustomerData })
}

export async function emailCheckClearBitApi(clearBitData) {
  let emailData = clearBitData.data;
  return request({ url: VIEW_CLEARBIT_DATA_CUSTOMER, method: 'post', data: emailData })
}

