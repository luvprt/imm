import {
    ADD_EMPLOYEE,
    LIST_EMPLOYEES
} from '../routing/route';

import {
    request
} from '../request/axios.request'

export async function addEmployeeApi(employeeData) {
    let addEmployee = employeeData.data;
    return request({
        url: ADD_EMPLOYEE,
        method: 'post',
        data: addEmployee
    })
}

export async function listEmployeesApi(employeeData) {
    let listEmployee = employeeData.data;
    return request({
        url: LIST_EMPLOYEES,
        method: 'post',
        data: listEmployee
    })
}