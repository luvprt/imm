import {
  ADD_LOCATION,
  LIST_LOCATION,
  DELETE_LOCATION,
  GET_LOCATION_BY_ID,
  EDIT_LOCATION,
  VIEW_LOCATION,
  GET_CUSTOMERS_BY_ROLE,
  LOCATION_STATUS
} from '../routing/route';
import {
  request
} from '../request/axios.request'

// Add Location Api
export async function addLocationApi(locationData) {
  let locationPageData = locationData.data;
  return request({
    url: ADD_LOCATION,
    method: 'post',
    data: locationPageData
  })
}
// List Location Api
export async function listLocationApi(locationData) {
  let listLocationData = locationData.data;
  return request({
    url: LIST_LOCATION,
    method: 'post',
    data: listLocationData
  })
}
// Delete Location Api
export async function deleteLocationApi(locationData) {
  let locationId = locationData.data;
  return request({
    url: DELETE_LOCATION + '/' + locationId,
    method: 'delete'
  })
}

// Get Location By Id Api 
export async function getLocationByIdApi(locationData) {
  let locationId = locationData.data;
  return request({
    url: GET_LOCATION_BY_ID + '/' + locationId,
    method: 'get'
  })
}

// Edit Location  Api 
export async function editLocationApi(locationData) {
  let locationPageData = locationData.data;
  return request({
    url: EDIT_LOCATION,
    method: 'put',
    data: locationPageData
  })
}
// View Location Api
export async function viewLocationApi(locationData) {
  let viewLocationData = locationData.data;
  return request({
    url: VIEW_LOCATION,
    method: 'post',
    data: viewLocationData
  })
}

// List All Customer By Role Api
export async function listCustomersByRoleApi() {
  return request({
    url: GET_CUSTOMERS_BY_ROLE,
    method: 'get'
  })
}

// Location Status Api
export async function locationStatusApi(locationData) {
  let locationStatusData = locationData.data;
  return request({
    url: LOCATION_STATUS,
    method: 'put',
    data: locationStatusData
  })
}