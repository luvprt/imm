import axios from "axios";
import { config as defaultConfig } from '../config/api.config';
import history from '../../app/routing/history';
import { Tokens } from '../../app/storage';
import { ADMIN_LOGIN } from '../../app/routing/routeContants';
import { infoNotification } from '../../app/common/notification-alert';

export const request = async (config) => {

  let requestData = {
    ...defaultConfig(),
    ...config
  };

  let response;
  try {
    response = await axios.request(requestData);
  } catch (error) {
    return createResponseFromAxiosError(error);
  }

  return createResponseFromAxiosResponse(response);
};

function createResponseFromAxiosError(error) {
  // handle  error
  let status, message, data;

  if (error.response) {
    if (error.response.status===401) {
      Tokens.removeLocalData();
      infoNotification('Token expired, please login.')
      history.push(ADMIN_LOGIN);
    }
    status = error.response.status;
    message = error.message;
    data = error.response.data;

  } else if (error.request) {
    status = 0;
    message = error.message;

  } else {
    status = -1;
    message = error.message;
  }

  return { success: false, data, error: { status, message } };
}

function createResponseFromAxiosResponse(response) {
  return { success: true, data: response.data };
}
