import React from "react";
import _ from 'lodash'

export class PaginationFooter extends React.Component {
    render() {
        const { totalRecords, limit, pageNo } = this.props
        let getReminder = totalRecords % limit;
        let totalPages = 0;
        if (getReminder > 0) {
            totalPages = ((totalRecords - getReminder) / limit) + 1;
        } else {
            totalPages = totalRecords / limit;
        }
        let pagesCount = [];
        if (totalPages > 0) {
            for (let ii = 1; ii <= totalPages; ii++) {
                if (pageNo === ii) {
                    pagesCount.push(<li key={ii} onClick={() => this.props.getPageData(ii)} className="page-item active"><a className="page-link">{ii}</a></li>);
                } else {
                    pagesCount.push(<li key={ii} onClick={() => this.props.getPageData(ii)} className="page-item"><a className="page-link">{ii}</a></li>);
                }
            }
        }
        let pagesCountHtml = _.map(pagesCount, (html) => html)

        const toRecords = totalRecords > pageNo * limit ? pageNo * limit : totalRecords
        const fromRecords = (pageNo * limit) - (limit - 1)

        return (
            <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                <div className="visible-entries">Showing {fromRecords} to {toRecords} of {totalRecords} entries</div>
                <nav aria-label="...">
                    <ul className="pagination mb-0">
                        <li onClick={() => pageNo > 1 ? this.props.getPageData(pageNo - 1) : ''} className={pageNo > 1 ? "page-item" : "page-item disabled"}><span className="page-link" href="">{'<'}</span></li>
                        {pagesCountHtml}
                        <li onClick={() => (pageNo < totalPages) ? this.props.getPageData(pageNo + 1) : ''} className={(pageNo < totalPages) ? "page-item" : "page-item disabled"}><span className="page-link" href="">{'>'}</span></li>
                    </ul>
                </nav>
            </div>
        );
    }
}