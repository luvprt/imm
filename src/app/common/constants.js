export const constants = {
    PAGE_LIMIT: 10
}

export const userType = {
    SUPER_ADMIN: 'Super Admin',
    CONTRACTOR: 'Contractor',
    BUILDING_REPRESENTATIVE: 'Building Representative'
}
