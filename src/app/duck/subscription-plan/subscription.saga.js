import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    addSubscriptionPlanResponse,
    ADD_SUBSCRIPTION_PLAN,
    SUCCESS_ADD_SUBSCRIPTION_PLAN,
    ERROR_ADD_SUBSCRIPTION_PLAN,
    LIST_SUBSCRIPTION_PLANS,
    listSubscriptionPlanResponse,
    SUCCESS_LIST_SUBSCRIPTION_PLANS,
    ERROR_LIST_SUBSCRIPTION_PLANS,
    showSubscriptionDataResponse,
    SHOW_SUBSCRIPTION_PLAN,
    SUCCESS_SHOW_SUBSCRIPTION_PLAN,
    ERROR_SHOW_SUBSCRIPTION_PLAN,
    editSubscriptionPlanResponse,
    EDIT_SUBSCRIPTION_PLAN,
    SUCCESS_EDIT_SUBSCRIPTION_PLAN,
    ERROR_EDIT_SUBSCRIPTION_PLAN,
    getSubscriptionPlanWithHighlightResponse,
    SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
    ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
    GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
    GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
    SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
    ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
    getSubscriptionPlanWithHighlightByIdResponse,
    ADD_MEMBERSHIP,
    SUCCESS_ADD_MEMBERSHIP,
    ERROR_ADD_MEMBERSHIP,
    addMembershipResponse,
    deleteSubscriptionPlanResponse,
    SUCCESS_DELETE_SUBSCRIPTION_PLAN,
    ERROR_DELETE_SUBSCRIPTION_PLAN,
    DELETE_SUBSCRIPTION_PLAN,
    SUCCESS_UPDATE_USER_PLAN,
    UPDATE_USER_PLAN,
    ERROR_UPDATE_USER_PLAN,
    updateUserPlanResponse
} from './subscription.action';
import {
    addSubscriptionPlanApi,
    listSubscriptionPlanApi,
    showSubscriptionPlanApi,
    editSubscriptionPlanApi,
    getSubscriptionPlanWithHighlightApi,
    getSubscriptionPlanWithHighlightByIdApi,
    addMembershipApi,
    deleteSubscriptionPlanApi,
    updateUserPlanApi
} from '../../../api/index';

import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';

function* addSubscriptionPlanRequest(subscriptionData) {
    let getData = yield addSubscriptionPlanApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Subscription plan added successfully !!')
        yield put(addSubscriptionPlanResponse(SUCCESS_ADD_SUBSCRIPTION_PLAN, 'Added'));
    } else {
        errorNotification(getData.data.message)
        yield put(addSubscriptionPlanResponse(ERROR_ADD_SUBSCRIPTION_PLAN, getData.data));
    }
}

export function* addSubscriptionWatcher() {
    yield takeLatest(ADD_SUBSCRIPTION_PLAN, addSubscriptionPlanRequest);
}

function* listSubscriptionPlanRequest(subscriptionData) {
    let getData = yield listSubscriptionPlanApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(listSubscriptionPlanResponse(SUCCESS_LIST_SUBSCRIPTION_PLANS, getData.data));
    } else {
        yield put(listSubscriptionPlanResponse(ERROR_LIST_SUBSCRIPTION_PLANS, getData.data));
    }
}

export function* listSubscriptionWatcher() {
    yield takeLatest(LIST_SUBSCRIPTION_PLANS, listSubscriptionPlanRequest);
}

function* showSubscriptionPlanRequest(subscriptionData) {
    let getData = yield showSubscriptionPlanApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(showSubscriptionDataResponse(SUCCESS_SHOW_SUBSCRIPTION_PLAN, getData.data));
    } else {
        yield put(showSubscriptionDataResponse(ERROR_SHOW_SUBSCRIPTION_PLAN, getData.data));
    }
}

export function* showSubscriptionWatcher() {
    yield takeLatest(SHOW_SUBSCRIPTION_PLAN, showSubscriptionPlanRequest);
}

function* editSubscriptionPlanRequest(subscriptionData) {
    let getData = yield editSubscriptionPlanApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Subscription plan edited sucessfully !!');
        yield put(editSubscriptionPlanResponse(SUCCESS_EDIT_SUBSCRIPTION_PLAN, 'Edited'));
    } else {
        errorNotification('Error !!')
        yield put(editSubscriptionPlanResponse(ERROR_EDIT_SUBSCRIPTION_PLAN, getData.data));
    }
}

export function* editSubscriptionWatcher() {
    yield takeLatest(EDIT_SUBSCRIPTION_PLAN, editSubscriptionPlanRequest);
}

function* getSubscriptionPlanWithHighlightRequest() {
    let getData = yield getSubscriptionPlanWithHighlightApi();
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getSubscriptionPlanWithHighlightResponse(SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT, getData.data));
    } else {
        yield put(getSubscriptionPlanWithHighlightResponse(ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT, getData.data));
    }
}

export function* getSubscriptionWithHighlightWatcher() {
    yield takeLatest(GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT, getSubscriptionPlanWithHighlightRequest);
}

function* getSubscriptionPlanWithHighlightByIdRequest(data) {
    let getData = yield getSubscriptionPlanWithHighlightByIdApi(data);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getSubscriptionPlanWithHighlightByIdResponse(SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID, getData.data));
    } else {
        yield put(getSubscriptionPlanWithHighlightByIdResponse(ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID, getData.data));
    }
}

export function* getSubscriptionWithHighlightByIdWatcher() {
    yield takeLatest(GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID, getSubscriptionPlanWithHighlightByIdRequest);
}

function* addMembershipRequest(membershipData) {
    let getData = yield addMembershipApi(membershipData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification(getData.data.message);
        yield put(addMembershipResponse(SUCCESS_ADD_MEMBERSHIP, getData.data));
    } else {
        errorNotification(getData.data.message)
        yield put(addMembershipResponse(ERROR_ADD_MEMBERSHIP, getData.data));
    }
}

export function* addMembershipWatcher() {
    yield takeLatest(ADD_MEMBERSHIP, addMembershipRequest);
}

function* deleteSubscriptionPlanRequest(subscriptionData) {
    let getData = yield deleteSubscriptionPlanApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Subscription plan deleted sucessfully !!');
        yield put(deleteSubscriptionPlanResponse(SUCCESS_DELETE_SUBSCRIPTION_PLAN, 'Deleted'));
    } else {
        errorNotification('Error !!')
        yield put(deleteSubscriptionPlanResponse(ERROR_DELETE_SUBSCRIPTION_PLAN, getData.data));
    }
}

export function* deleteSubscriptionWatcher() {
    yield takeLatest(DELETE_SUBSCRIPTION_PLAN, deleteSubscriptionPlanRequest);
}

function* updateUserPlanRequest(planData) {
    let getData = yield updateUserPlanApi(planData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(updateUserPlanResponse(SUCCESS_UPDATE_USER_PLAN, 'PlanUpdated'));
    } else {
        errorNotification(getData.data.message)
        yield put(updateUserPlanResponse(ERROR_UPDATE_USER_PLAN, getData.data));
    }
}

export function* updateUserWatcher() {
    yield takeLatest(UPDATE_USER_PLAN, updateUserPlanRequest);
}