// Add Subscription Plan
export const ADD_SUBSCRIPTION_PLAN = 'ADD_SUBSCRIPTION_PLAN';
export const addSubscriptionPlan = (data) => ({
  type: ADD_SUBSCRIPTION_PLAN,
  data
});
export const SUCCESS_ADD_SUBSCRIPTION_PLAN = 'SUCCESS_ADD_SUBSCRIPTION_PLAN';
export const ERROR_ADD_SUBSCRIPTION_PLAN = 'ERROR_ADD_SUBSCRIPTION_PLAN';
export const addSubscriptionPlanResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const LIST_SUBSCRIPTION_PLANS = 'LIST_SUBSCRIPTION_PLANS';
export const listSubscriptionPlan = (data) => ({
  type: LIST_SUBSCRIPTION_PLANS,
  data
});
export const SUCCESS_LIST_SUBSCRIPTION_PLANS = 'SUCCESS_LIST_SUBSCRIPTION_PLANS';
export const ERROR_LIST_SUBSCRIPTION_PLANS = 'ERROR_LIST_SUBSCRIPTION_PLANS';
export const listSubscriptionPlanResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const SHOW_SUBSCRIPTION_PLAN = 'SHOW_SUBSCRIPTION_PLAN';
export const showSubscriptionData = (data) => ({
  type: SHOW_SUBSCRIPTION_PLAN,
  data
});
export const SUCCESS_SHOW_SUBSCRIPTION_PLAN = 'SUCCESS_SHOW_SUBSCRIPTION_PLAN';
export const ERROR_SHOW_SUBSCRIPTION_PLAN = 'ERROR_SHOW_SUBSCRIPTION_PLAN';
export const showSubscriptionDataResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const EDIT_SUBSCRIPTION_PLAN = 'EDIT_SUBSCRIPTION_PLAN';
export const editSubscriptionPlan = (data) => ({
  type: EDIT_SUBSCRIPTION_PLAN,
  data
});
export const SUCCESS_EDIT_SUBSCRIPTION_PLAN = 'SUCCESS_EDIT_SUBSCRIPTION_PLAN';
export const ERROR_EDIT_SUBSCRIPTION_PLAN = 'ERROR_EDIT_SUBSCRIPTION_PLAN';
export const editSubscriptionPlanResponse = (type, data) => ({
  type,
  data
});

// Get Subscription Plan With Highlight
export const GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT = 'GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT';
export const getSubscriptionPlanWithHighlight = (data) => ({
  type: GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
  data
});
export const SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT = 'SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT';
export const ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT = 'ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT';
export const getSubscriptionPlanWithHighlightResponse = (type, data) => ({
  type,
  data
});

// Get Subscription Plans With Highlight By Id 
export const GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID = 'GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID';
export const getSubscriptionPlanWithHighlightById = (data) => ({
  type: GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
  data
});
export const SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID = 'SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID';
export const ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID = 'ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID';
export const getSubscriptionPlanWithHighlightByIdResponse = (type, data) => ({
  type,
  data
});

// Add Membership
export const ADD_MEMBERSHIP = 'ADD_MEMBERSHIP';
export const addMembership = (data) => ({
  type: ADD_MEMBERSHIP,
  data
});
export const SUCCESS_ADD_MEMBERSHIP = 'SUCCESS_ADD_MEMBERSHIP';
export const ERROR_ADD_MEMBERSHIP = 'ERROR_ADD_MEMBERSHIP';
export const addMembershipResponse = (type, data) => ({
  type,
  data
});

// Delete Subscription Plans
export const DELETE_SUBSCRIPTION_PLAN = 'DELETE_SUBSCRIPTION_PLAN';
export const deleteSubscriptionPlan = (data) => ({
  type: DELETE_SUBSCRIPTION_PLAN,
  data
});
export const SUCCESS_DELETE_SUBSCRIPTION_PLAN = 'SUCCESS_DELETE_SUBSCRIPTION_PLAN';
export const ERROR_DELETE_SUBSCRIPTION_PLAN = 'ERROR_DELETE_SUBSCRIPTION_PLAN';
export const deleteSubscriptionPlanResponse = (type, data) => ({
  type,
  data
});

// Update User Subscription Plan
export const UPDATE_USER_PLAN = 'UPDATE_USER_PLAN';
export const updateUserPlan = (data) => ({
  type: UPDATE_USER_PLAN,
  data
});
export const SUCCESS_UPDATE_USER_PLAN = 'SUCCESS_UPDATE_USER_PLAN';
export const ERROR_UPDATE_USER_PLAN = 'ERROR_UPDATE_USER_PLAN';
export const updateUserPlanResponse = (type, data) => ({
  type,
  data
});