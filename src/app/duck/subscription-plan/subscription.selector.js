// Selector
export const getSubscriptionPlans = (state) => {
  return state.subscription.subscriptionData;
};

export const getSubscriptionPlanWithHighlightData = (state) => {
  return state.subscription.getSubscriptionPlanWithHighlightData;
};

export const getPlanDataById = (state) => {
  return state.subscription.getSubscriptionPlanWithHighlightDataById;
};

export const getmembershipData = (state) => {
  return state.subscription.membershipData;
};

export const getUpdatedPlan = (state) => {
  return state.subscription.updatedPlanData;
};