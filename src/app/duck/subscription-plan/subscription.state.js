export const DEFAULT_STATE = {
  subscriptionData: {},
  getSubscriptionPlanWithHighlightData: {},
  getSubscriptionPlanWithHighlightDataById: {},
  membershipData: {},
  updatedPlanData:{}
};