import {
  SUCCESS_ADD_SUBSCRIPTION_PLAN,
  ERROR_ADD_SUBSCRIPTION_PLAN,
  SUCCESS_LIST_SUBSCRIPTION_PLANS,
  ERROR_LIST_SUBSCRIPTION_PLANS,
  SUCCESS_SHOW_SUBSCRIPTION_PLAN,
  ERROR_SHOW_SUBSCRIPTION_PLAN,
  SUCCESS_EDIT_SUBSCRIPTION_PLAN,
  ERROR_EDIT_SUBSCRIPTION_PLAN,
  SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
  ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT,
  SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
  ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID,
  SUCCESS_ADD_MEMBERSHIP,
  ERROR_ADD_MEMBERSHIP,
  SUCCESS_DELETE_SUBSCRIPTION_PLAN,
  ERROR_DELETE_SUBSCRIPTION_PLAN,
  SUCCESS_UPDATE_USER_PLAN,
  ERROR_UPDATE_USER_PLAN
} from './subscription.action';
import {
  DEFAULT_STATE
} from './subscription.state'

export const subscriptionReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_ADD_SUBSCRIPTION_PLAN:
      const subscriptionData = action.data;
      return {
        ...state, subscriptionData
      };
    case ERROR_ADD_SUBSCRIPTION_PLAN:
      const errorSubscriptionData = action.data;
      return {
        ...state, subscriptionData: errorSubscriptionData
      };
    case SUCCESS_LIST_SUBSCRIPTION_PLANS:
      const subscriptionPlansList = action.data;
      return {
        ...state, subscriptionData: subscriptionPlansList
      };
    case ERROR_LIST_SUBSCRIPTION_PLANS:
      const subscriptionPlansListError = action.data;
      return {
        ...state, subscriptionData: subscriptionPlansListError
      };
    case SUCCESS_SHOW_SUBSCRIPTION_PLAN:
      const showSubscriptionPlans = action.data;
      return {
        ...state, subscriptionData: showSubscriptionPlans
      };
    case ERROR_SHOW_SUBSCRIPTION_PLAN:
      const showSubscriptionPlansError = action.data;
      return {
        ...state, subscriptionData: showSubscriptionPlansError
      };
    case SUCCESS_EDIT_SUBSCRIPTION_PLAN:
      const editSubscriptionData = action.data;
      return {
        ...state, subscriptionData: editSubscriptionData
      };
    case ERROR_EDIT_SUBSCRIPTION_PLAN:
      const errorEditSubscriptionData = action.data;
      return {
        ...state, subscriptionData: errorEditSubscriptionData
      };
    case SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT:
      const getSubscriptionPlanWithHighlightData = action.data;
      return {
        ...state, getSubscriptionPlanWithHighlightData
      };
    case ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT:
      const errorGetSubscriptionPlanWithHighlightData = action.data;
      return {
        ...state, getSubscriptionPlanWithHighlightData: errorGetSubscriptionPlanWithHighlightData
      };
    case SUCCESS_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID:
      const getSubscriptionPlanWithHighlightDataById = action.data;
      return {
        ...state, getSubscriptionPlanWithHighlightDataById
      };
    case ERROR_GET_SUBSCRIPTION_PLAN_WITH_HIGHLIGHT_BY_ID:
      const errorGetSubscriptionPlanWithHighlightDataByID = action.data;
      return {
        ...state, getSubscriptionPlanWithHighlightDataById: errorGetSubscriptionPlanWithHighlightDataByID
      };
    case SUCCESS_ADD_MEMBERSHIP:
      const membershipData = action.data;
      return {
        ...state, membershipData
      };
    case ERROR_ADD_MEMBERSHIP:
      const errorMembershipData = action.data;
      return {
        ...state, membershipData: errorMembershipData
      };
    case SUCCESS_DELETE_SUBSCRIPTION_PLAN:
      const deleteSubscriptionData = action.data;
      return {
        ...state, subscriptionData: deleteSubscriptionData
      };
    case ERROR_DELETE_SUBSCRIPTION_PLAN:
      const errorDeleteSubscriptionData = action.data;
      return {
        ...state, subscriptionData: errorDeleteSubscriptionData
      };
    case SUCCESS_UPDATE_USER_PLAN:
      return {
        ...state, updatedPlanData: action.data
      };
    case ERROR_UPDATE_USER_PLAN:
      return {
        ...state, updatedPlanData: action.data
      };
    default:
      return state;
  }
};