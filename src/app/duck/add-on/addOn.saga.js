import { put, takeLatest } from 'redux-saga/effects';
import {
    getAddOnsResponse, GET_ADD_ONS, SUCCESS_GET_ADD_ONS, ERROR_GET_ADD_ONS,
    addAddOnResponse, ADD_ADD_ON, SUCCESS_ADD_ADD_ON, ERROR_ADD_ADD_ON,
    editAddonResponse, EDIT_ADD_ON, SUCCESS_EDIT_ADD_ON, ERROR_EDIT_ADD_ON,
    viewAddOnResponse, VIEW_ADD_ON, SUCCESS_VIEW_ADD_ON, ERROR_VIEW_ADD_ON,
    deleteAddOnResponse, DELETE_ADD_ON, SUCCESS_DELETE_ADD_ON, ERROR_DELETE_ADD_ON,
    listAddOnsResponse, LIST_ADD_ONS, SUCCESS_LIST_ADD_ONS, ERROR_LIST_ADD_ONS
} from './addOn.action';
import {
    getAddOnsApi, addAddOnApi, editAddOnApi,
    viewAddOnApi, deleteAddOnApi, listAddOnsApi
} from '../../../api/index';
import _ from 'lodash'
import { successNotification, errorNotification } from '../../common/notification-alert';

function* getAddOnsRequest(addOndata) {
    let getData = yield getAddOnsApi(addOndata);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getAddOnsResponse(SUCCESS_GET_ADD_ONS, getData.data));
    } else {
        errorNotification('Error!')
        yield put(getAddOnsResponse(ERROR_GET_ADD_ONS, getData.data));
    }
}

export function* getAddOnsWatcher() {
    yield takeLatest(GET_ADD_ONS, getAddOnsRequest);
}

function* listAddOnsRequest(addOndata) {
    let getData = yield listAddOnsApi(addOndata);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(listAddOnsResponse(SUCCESS_LIST_ADD_ONS, getData.data));
    } else {
        yield put(listAddOnsResponse(ERROR_LIST_ADD_ONS, getData.data));
    }
}

export function* listAddOnsWatcher() {
    yield takeLatest(LIST_ADD_ONS, listAddOnsRequest);
}

function* addAddOnRequest(addOnData) {
    let getData = yield addAddOnApi(addOnData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Add-ons added successfully!!')
        yield put(addAddOnResponse(SUCCESS_ADD_ADD_ON, 'Added'));
    } else {
        errorNotification(getData.data.message)
        yield put(addAddOnResponse(ERROR_ADD_ADD_ON, getData.data));
    }
}

export function* addAddOnWatcher() {
    yield takeLatest(ADD_ADD_ON, addAddOnRequest);
}


function* editAddOnRequest(addOnData) {
    let editiAddOnData = yield editAddOnApi(addOnData);
    if (editiAddOnData.success && _.has(editiAddOnData, 'data.data')) {
        successNotification('Add-on edited successfully!!')
        yield put(editAddonResponse(SUCCESS_EDIT_ADD_ON, 'Edited'));
    } else {
        errorNotification('Error !!')
        yield put(editAddonResponse(ERROR_EDIT_ADD_ON, editiAddOnData.data));
    }
}

export function* editAddOnWatcher() {
    yield takeLatest(EDIT_ADD_ON, editAddOnRequest);
}

function* viewAddOnRequest(addOnData) {
    let viewAddOnData = yield viewAddOnApi(addOnData);
    console.log(viewAddOnData)
    if (viewAddOnData.success && _.has(viewAddOnData, 'data.data')) {
        yield put(viewAddOnResponse(SUCCESS_VIEW_ADD_ON, viewAddOnData.data));
    } else {
        errorNotification('Error !!')
        yield put(viewAddOnResponse(ERROR_VIEW_ADD_ON, viewAddOnData.data));
    }
}

export function* viewAddOnWatcher() {
    yield takeLatest(VIEW_ADD_ON, viewAddOnRequest);
}

function* deleteAddOnRequest(addOnData) {
    let deleteAddOnData = yield deleteAddOnApi(addOnData);
    if (deleteAddOnData.success && _.has(deleteAddOnData, 'data.data')) {
        successNotification('Add-on deleted sucessfully.');
        yield put(deleteAddOnResponse(SUCCESS_DELETE_ADD_ON, 'Deleted'));
    } else {
        errorNotification('Error !!')
        yield put(deleteAddOnResponse(ERROR_DELETE_ADD_ON, deleteAddOnData.data));
    }
}

export function* deleteAddOnWatcher() {
    yield takeLatest(DELETE_ADD_ON, deleteAddOnRequest);
}
