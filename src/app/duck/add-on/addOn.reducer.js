import {
  SUCCESS_GET_ADD_ONS, ERROR_GET_ADD_ONS,
  SUCCESS_ADD_ADD_ON, ERROR_ADD_ADD_ON,
  SUCCESS_VIEW_ADD_ON, ERROR_VIEW_ADD_ON,
  SUCCESS_DELETE_ADD_ON, ERROR_DELETE_ADD_ON,
  SUCCESS_EDIT_ADD_ON, ERROR_EDIT_ADD_ON,
  SUCCESS_LIST_ADD_ONS, ERROR_LIST_ADD_ONS
} from './addOn.action';
import { DEFAULT_STATE } from './addOn.state'

export const addOnReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_GET_ADD_ONS:
      const addOnData = action.data;
      return { ...state, addOnData };
    case ERROR_GET_ADD_ONS:
      const errorAddOnData = action.data;
      return { ...state, addOnData: errorAddOnData };
    case SUCCESS_ADD_ADD_ON:
      const addAddOnData = action.data;
      return { ...state, addOnData: addAddOnData };
    case ERROR_ADD_ADD_ON:
      const errorAddAddOnData = action.data;
      return { ...state, addOnData: errorAddAddOnData };
    case SUCCESS_VIEW_ADD_ON:
      const viewAddOnData = action.data;
      return { ...state, addOnData: viewAddOnData };
    case ERROR_VIEW_ADD_ON:
      const errorViewAddOnData = action.data;
      return { ...state, addOnData: errorViewAddOnData };
    case SUCCESS_DELETE_ADD_ON:
      const deleteAddOnData = action.data;
      return { ...state, addOnData: deleteAddOnData };
    case ERROR_DELETE_ADD_ON:
      const errorDeleteAddOnData = action.data;
      return { ...state, addOnData: errorDeleteAddOnData };
    case SUCCESS_EDIT_ADD_ON:
      const editAddOnData = action.data;
      return { ...state, addOnData: editAddOnData };
    case ERROR_EDIT_ADD_ON:
      const errorEditAddOnData = action.data;
      return { ...state, addOnData: errorEditAddOnData };
    case SUCCESS_LIST_ADD_ONS:
      const listAddOnsData = action.data;
      return { ...state, addOnData: listAddOnsData };
    case ERROR_LIST_ADD_ONS:
      const errorListAddOnsData = action.data;
      return { ...state, addOnData: errorListAddOnsData };
    default:
      return state;
  }
};