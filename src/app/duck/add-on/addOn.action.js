// get Add ons
export const GET_ADD_ONS = 'GET_ADD_ONS';
export const getAddOns = (data) => ({ type: GET_ADD_ONS, data });
export const SUCCESS_GET_ADD_ONS = 'SUCCESS_GET_ADD_ONS';
export const ERROR_GET_ADD_ONS = 'ERROR_GET_ADD_ONS';
export const getAddOnsResponse = (type, data) => ({ type, data });

// get Add ons
export const LIST_ADD_ONS = 'LIST_ADD_ONS';
export const listAddOns = (data) => ({ type: LIST_ADD_ONS, data });
export const SUCCESS_LIST_ADD_ONS = 'SUCCESS_LIST_ADD_ONS';
export const ERROR_LIST_ADD_ONS = 'ERROR_LIST_ADD_ONS';
export const listAddOnsResponse = (type, data) => ({ type, data });

export const ADD_ADD_ON = 'ADD_ADD_ON';
export const addAddOn = (data) => ({ type: ADD_ADD_ON, data });
export const SUCCESS_ADD_ADD_ON = 'SUCCESS_ADD_ADD_ON';
export const ERROR_ADD_ADD_ON = 'ERROR_ADD_ADD_ON';
export const addAddOnResponse = (type, data) => ({ type, data });

export const VIEW_ADD_ON = 'VIEW_ADD_ON';
export const viewAddOn = (data) => ({ type: VIEW_ADD_ON, data });
export const SUCCESS_VIEW_ADD_ON = 'SUCCESS_VIEW_ADD_ON';
export const ERROR_VIEW_ADD_ON = 'ERROR_VIEW_ADD_ON';
export const viewAddOnResponse = (type, data) => ({ type, data });

export const EDIT_ADD_ON = 'EDIT_ADD_ON';
export const editAddon = (data) => ({ type: EDIT_ADD_ON, data });
export const SUCCESS_EDIT_ADD_ON = 'SUCCESS_EDIT_ADD_ON';
export const ERROR_EDIT_ADD_ON = 'ERROR_EDIT_ADD_ON';
export const editAddonResponse = (type, data) => ({ type, data });

export const DELETE_ADD_ON = 'DELETE_ADD_ON';
export const deleteAddOn = (data) => ({ type: DELETE_ADD_ON, data });
export const SUCCESS_DELETE_ADD_ON = 'SUCCESS_DELETE_ADD_ON';
export const ERROR_DELETE_ADD_ON = 'ERROR_DELETE_ADD_ON';
export const deleteAddOnResponse = (type, data) => ({ type, data });
