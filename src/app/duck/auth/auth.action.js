// user login 
export const ADMIN_LOGIN = 'ADMIN_LOGIN';
export const adminLogin = (data) => ({
  type: ADMIN_LOGIN,
  data
});
export const SUCCESS_ADMIN_LOGIN = 'SUCCESS_ADMIN_LOGIN';
export const ERROR_ADMIN_LOGIN = 'ERROR_ADMIN_LOGIN';
export const adminLoginResponse = (type, data) => ({
  type,
  data
});
// forgot password
export const FORGOT_PASSWORD = 'FORGOT_PASSWORD';
export const forgotPassword = (data) => ({
  type: FORGOT_PASSWORD,
  data
});
export const FORGOT_PASSWORD_SUCCESS = 'FORGOT_PASSWORD_SUCCESS';
export const FORGOT_PASSWORD_ERROR = 'FORGOT_PASSWORD_ERROR';
export const forgotPasswordresponse = (type, data) => ({
  type,
  data
});

export const FORGOT_PASSWORD_MESSAGE_RESET = 'FORGOT_PASSWORD_MESSAGE_RESET';

// reset password
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const resetPassword = (data) => ({
  type: RESET_PASSWORD,
  data
});
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS';
export const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR';
export const resetPasswordresponse = (type, data) => ({
  type,
  data
});
// verify password
export const VERIFY_TOKEN = 'VERIFY_TOKEN';
export const verifyToken = (data) => ({
  type: VERIFY_TOKEN,
  data
});
export const VERIFY_TOKEN_SUCCESS = 'VERIFY_TOKEN_SUCCESS';
export const VERIFY_TOKEN_ERROR = 'VERIFY_TOKEN_ERROR';
export const verifyTokenresponse = (type, data) => ({
  type,
  data
});
// registration
export const REGISTRATION = 'REGISTRATION';
export const registration = (data) => ({
  type: REGISTRATION,
  data
});
export const REGISTRATION_SUCCESS = 'REGISTRATION_SUCCESS';
export const REGISTRATION_ERROR = 'REGISTRATION_ERROR';
export const registrationResponse = (type, data) => ({
  type,
  data
});

// Check Email Exist 
export const EMAIL_EXIST = 'EMAIL_EXIST';
export const emailExist = (data) => ({
  type: EMAIL_EXIST,
  data
});
export const EMAIL_EXIST_SUCCESS = 'EMAIL_EXIST_SUCCESS';
export const EMAIL_EXIST_ERROR = 'EMAIL_EXIST_ERROR';
export const emailExistResponse = (type, data) => ({
  type,
  data
});