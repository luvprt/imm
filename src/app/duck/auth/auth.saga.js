import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    adminLoginResponse,
    ADMIN_LOGIN,
    ERROR_ADMIN_LOGIN,
    SUCCESS_ADMIN_LOGIN,
    forgotPasswordresponse,
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_ERROR,
    FORGOT_PASSWORD_SUCCESS,
    FORGOT_PASSWORD_MESSAGE_RESET,
    resetPasswordresponse,
    RESET_PASSWORD,
    RESET_PASSWORD_ERROR,
    RESET_PASSWORD_SUCCESS,
    verifyTokenresponse,
    VERIFY_TOKEN,
    VERIFY_TOKEN_ERROR,
    VERIFY_TOKEN_SUCCESS,
    registrationResponse,
    REGISTRATION,
    REGISTRATION_SUCCESS,
    REGISTRATION_ERROR,
    EMAIL_EXIST,
    EMAIL_EXIST_SUCCESS,
    EMAIL_EXIST_ERROR,
    emailExistResponse
} from './auth.action';
import {
    loginUser,
    userForgotPassword,
    userResetPassword,
    userVerifyToken,
    userRegistrationApi,
    emaiExistApi
} from '../../../api/index';
import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';

function* userLogin(userData) {
    let loginData = yield loginUser(userData);
    if (loginData.success && _.has(loginData, 'data.data')) {
        yield put(adminLoginResponse(SUCCESS_ADMIN_LOGIN, {
            isLoggedIn: true,
            user: loginData.data,
            message: loginData.message
        }));
    } else {
        yield put(adminLoginResponse(ERROR_ADMIN_LOGIN, {
            isLoggedIn: false,
            user: '',
            message: loginData.data.message
        }));
    }
}

export function* loginWatcher() {
    yield takeLatest(ADMIN_LOGIN, userLogin);
}

function* getForgotPassword(data) {
    yield put(forgotPasswordresponse(FORGOT_PASSWORD_MESSAGE_RESET, { message: '' }));
    let userData = yield userForgotPassword(data);
    if (userData.success) {
        successNotification('Please check your email.')
        yield put(forgotPasswordresponse(FORGOT_PASSWORD_SUCCESS, {
            success: true
        }));
    } else {
        yield put(forgotPasswordresponse(FORGOT_PASSWORD_ERROR, {
            success: false,
            message: userData.data.message
        }));
    }
}

export function* forgotPasswordWatcher() {
    yield takeLatest(FORGOT_PASSWORD, getForgotPassword);
}

function* getResetPassword(data) {
    let userData = yield userResetPassword(data);
    if (userData.success) {
        successNotification('Password Reset successfully!!')
        yield put(resetPasswordresponse(RESET_PASSWORD_SUCCESS, {
            success: true
        }));
    } else {
        errorNotification(userData.data.message)
        yield put(resetPasswordresponse(RESET_PASSWORD_ERROR, {
            success: false
        }));
    }
}

export function* resetPasswordWatcher() {
    yield takeLatest(RESET_PASSWORD, getResetPassword);
}

function* getVerifyToken(data) {
    let userData = yield userVerifyToken(data);
    console.log(userData)
    if (userData.success) {
        yield put(verifyTokenresponse(VERIFY_TOKEN_SUCCESS, {
            success: true
        }));
    } else {
        yield put(verifyTokenresponse(VERIFY_TOKEN_ERROR, {
            success: false
        }));
    }
}

export function* verifyTokenWatcher() {
    yield takeLatest(VERIFY_TOKEN, getVerifyToken);
}

function* registrationReq(data) {
    let getUserData = yield userRegistrationApi(data);
    if (getUserData.success) {
        yield put(registrationResponse(REGISTRATION_SUCCESS, {
            success: true,
            user: getUserData.data,
        }));
    } else {
        yield put(registrationResponse(REGISTRATION_ERROR, {
            success: false,
            user: getUserData.data,
            message: getUserData.data.message
        }));
    }
}

export function* registrationWatcher() {
    yield takeLatest(REGISTRATION, registrationReq);
}

function* emailExistReq(data) {
    let getUserData = yield emaiExistApi(data);
    if (getUserData.success) {
        yield put(emailExistResponse(EMAIL_EXIST_SUCCESS, {data:Date.now(), message:'Email Exist'}));
    } else {
        yield put(emailExistResponse(EMAIL_EXIST_ERROR,  {data:Date.now(), message:'Email Not Exist'}));
    }
}

export function* emailExistWatcher() {
    yield takeLatest(EMAIL_EXIST, emailExistReq);
}