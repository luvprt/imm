import {
  SUCCESS_ADMIN_LOGIN,
  ERROR_ADMIN_LOGIN,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_ERROR,
  RESET_PASSWORD_SUCCESS,
  VERIFY_TOKEN_SUCCESS,
  VERIFY_TOKEN_ERROR,
  REGISTRATION_SUCCESS,
  REGISTRATION_ERROR,
  EMAIL_EXIST_SUCCESS,
  EMAIL_EXIST_ERROR,
  FORGOT_PASSWORD_MESSAGE_RESET
} from './auth.action';
import {
  DEFAULT_STATE
} from "./auth.state";

export const authReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_ADMIN_LOGIN:
      const loginData = action.data;
      return {
        ...state, isLoggedIn: loginData.isLoggedIn, user: loginData.user
      };
    case ERROR_ADMIN_LOGIN:
      const errorloginData = action.data;
      return {
        ...state, isLoggedIn: errorloginData.isLoggedIn, user: errorloginData.user, message: errorloginData.message
      };
    case FORGOT_PASSWORD_SUCCESS:
      const forgotData = action.data;
      return {
        ...state, forgotPassword: forgotData, user: null, isLoggedIn: false
      };
    case FORGOT_PASSWORD_ERROR:
      const errforgotData = action.data;
      return {
        ...state, forgotPassword: errforgotData,
      }
    case FORGOT_PASSWORD_MESSAGE_RESET:
      const resetMessageForgot = action.data;
      return {
        ...state, forgotPassword: resetMessageForgot, user: null, isLoggedIn: false
      };
    case RESET_PASSWORD_SUCCESS:
      const resetPasswordData = action.data;
      return {
        ...state, resetPassword: resetPasswordData.success
      };
    case VERIFY_TOKEN_SUCCESS:
      const verifyTokenData = action.data;
      return {
        ...state, verifyToken: verifyTokenData.success
      }
    case VERIFY_TOKEN_ERROR:
      const tokenData = action.data;
      return {
        ...state, verifyToken: tokenData.success
      }
    case REGISTRATION_SUCCESS:
      const registerData = action.data;
      return {
        ...state, userData: registerData
      }
    case REGISTRATION_ERROR:
      const registerDataErr = action.data;
      return {
        ...state, userData: registerDataErr
      }
    case EMAIL_EXIST_SUCCESS:
      const email = action.data;
      return {
        ...state, emailExistData: email
      }
    case EMAIL_EXIST_ERROR:
      const emailErr = action.data;
      return {
        ...state, emailExistData: emailErr
      }
    default:
      return state;
  }
};