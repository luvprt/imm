// Selector
export const getState = (state) => {
  return state.auth;
};

export const getAuthState = (state) => {
  return state.auth;
};

export const isUserLoggedIn = (state) => {
  return getState(state).isLoggedIn;
};

export const getLoggedInUser = (state) => {
  return getState(state).user;
};

export const getverifyToken = (state) => {
  return getState(state).verifyToken;
}

export const getRegisteredUser = (state) => {
  return state.auth.userData;
}

export const emailExistData = (state) => {
  return state.auth.emailExistData;
}