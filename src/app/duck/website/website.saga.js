import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    CONTACT_US,
    SUCCESS_CONTACT_US,
    ERROR_CONTACT_US,
    contactUsResponse,
    GET_CMS_PAGE_DATA,
    ERROR_GET_CMS_PAGE_DATA,
    SUCCESS_GET_CMS_PAGE_DATA,
    getCmsPageDataResponse,
    CMS_PAGE_BY_TYPE,
    CMS_PAGE_BY_TYPE_SUCCESS,
    CMS_PAGE_BY_TYPE_ERROR,
    cmsPageByTypeResponse,
    CMS_PAGE_DETAIL_BY_SLUG,
    CMS_PAGE_DETAIL_BY_SLUG_SUCCESS,
    CMS_PAGE_DETAIL_BY_SLUG_ERROR,
    cmsPageDetailBySlugResponse
} from './website.action';
import {
    contactUsApi,
    getCmsPageDataApi,
    cmsPageByTypeApi,
    cmsPageDetailBySlugApi
} from '../../../api/index';

import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';

function* contactUsRequest(conatctData) {
    let getData = yield contactUsApi(conatctData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Contact us mail send successfully, We will contact you shortly')
        yield put(contactUsResponse(SUCCESS_CONTACT_US, getData.data));
    } else {
        errorNotification(getData.data.message)
        yield put(contactUsResponse(ERROR_CONTACT_US, getData.data));
    }
}

export function* contactUsWatcher() {
    yield takeLatest(CONTACT_US, contactUsRequest);
}

function* getCmsPageDataRequest(data) {
    let getData = yield getCmsPageDataApi(data);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getCmsPageDataResponse(SUCCESS_GET_CMS_PAGE_DATA, getData.data));
    } else {
        errorNotification(getData.data.message)
        yield put(getCmsPageDataResponse(ERROR_GET_CMS_PAGE_DATA, getData.data));
    }
}

export function* getCmsPageDataWatcher() {
    yield takeLatest(GET_CMS_PAGE_DATA, getCmsPageDataRequest);
}

// Get Cms Page By Type
function* cmsPageByTypeReq(data) {
    let cmsPagedata = yield cmsPageByTypeApi(data);
    if (cmsPagedata.success && _.has(cmsPagedata, 'data.data')) {
        yield put(cmsPageByTypeResponse(CMS_PAGE_BY_TYPE_SUCCESS, cmsPagedata.data));
    } else {
        // errorNotification(cmsPagedata.data.message)
        yield put(cmsPageByTypeResponse(CMS_PAGE_BY_TYPE_ERROR, 'CMS Not Exist'));
    }
}

export function* cmsPageBytypeWatcher() {
    yield takeLatest(CMS_PAGE_BY_TYPE, cmsPageByTypeReq);
}

// Cms Page Detail By Slug
function* cmsPageDetailBySlugReq(data) {
    let cmsPageDetail = yield cmsPageDetailBySlugApi(data);
    if (cmsPageDetail.success && _.has(cmsPageDetail, 'data.data')) {
        yield put(cmsPageDetailBySlugResponse(CMS_PAGE_DETAIL_BY_SLUG_SUCCESS, cmsPageDetail.data));
    } else {
        // errorNotification(cmsPageDetail.data.message)
        yield put(cmsPageDetailBySlugResponse(CMS_PAGE_DETAIL_BY_SLUG_ERROR, 'CMS Not Exist'));
    }
}

export function* cmsPageDetailBySlugWatcher() {
    yield takeLatest(CMS_PAGE_DETAIL_BY_SLUG, cmsPageDetailBySlugReq);
}