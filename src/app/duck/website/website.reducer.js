import {
  SUCCESS_CONTACT_US,
  ERROR_CONTACT_US,
  SUCCESS_GET_CMS_PAGE_DATA,
  ERROR_GET_CMS_PAGE_DATA,
  CMS_PAGE_BY_TYPE_SUCCESS,
  CMS_PAGE_BY_TYPE_ERROR,
  CMS_PAGE_DETAIL_BY_SLUG_SUCCESS,
  CMS_PAGE_DETAIL_BY_SLUG_ERROR
} from './website.action';
import {
  DEFAULT_STATE
} from './website.state'

export const websiteReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_CONTACT_US:
      const contactUsData = action.data;
      return {
        ...state, contactUsData
      };
    case ERROR_CONTACT_US:
      const errorcontactUsData = action.data;
      return {
        ...state, contactUsData: errorcontactUsData
      };
    case SUCCESS_GET_CMS_PAGE_DATA:
      const GetCmsPageData = action.data;
      return {
        ...state, GetCmsPageData
      };
    case ERROR_GET_CMS_PAGE_DATA:
      const errorGetCmsPageData = action.data;
      return {
        ...state, GetCmsPageData: errorGetCmsPageData
      };
      case CMS_PAGE_BY_TYPE_SUCCESS:
      const cmaPageByType = action.data;
      return {
        ...state, cmsPageDataByType: cmaPageByType
      }
      case CMS_PAGE_BY_TYPE_ERROR:
        const cmaPageByTypeErr = action.data;
        return {
          ...state, cmsPageDataByType: cmaPageByTypeErr
        }
      case CMS_PAGE_DETAIL_BY_SLUG_SUCCESS:
      const cmaPageDetailBySlug = action.data;
      return {
        ...state, cmsPageDataBySlug: cmaPageDetailBySlug
      }
      case CMS_PAGE_DETAIL_BY_SLUG_ERROR:
        const cmaPageDetailBySlugErr = action.data;
        return {
          ...state, cmsPageDataBySlug: cmaPageDetailBySlugErr
        }
    default:
      return state;
  }
};