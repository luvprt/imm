import _ from 'lodash'
// Selector
export const getContactUs = (state) => {
  return state.website.contactUsData;
};

export const getCmsPageData = (state) => {
  if (state.website.GetCmsPageData && _.has(state.website.GetCmsPageData, 'data')) {
    return state.website.GetCmsPageData;
  }
  return null;
};

// Get All Cms Page Data By Type
export const cmsPageByIdData = (state) => {
  return state.website.cmsPageDataByType;
}

// Get All Cms Page Detail By Slug
export const cmsPageDataBySlug = (state) => {
  return state.website.cmsPageDataBySlug;
}
