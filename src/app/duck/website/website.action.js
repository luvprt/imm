// Contact Us
export const CONTACT_US = 'CONTACT_US';
export const contactUs = (data) => ({
  type: CONTACT_US,
  data
});
export const SUCCESS_CONTACT_US = 'SUCCESS_CONTACT_US';
export const ERROR_CONTACT_US = 'ERROR_CONTACT_US';
export const contactUsResponse = (type, data) => ({
  type,
  data
});

// Get Dynamic Cms Page Data 
export const GET_CMS_PAGE_DATA = 'GET_CMS_PAGE_DATA';
export const getCmsPageDataAction = (data) => ({
  type: GET_CMS_PAGE_DATA,
  data
});
export const SUCCESS_GET_CMS_PAGE_DATA = 'SUCCESS_GET_CMS_PAGE_DATA';
export const ERROR_GET_CMS_PAGE_DATA = 'ERROR_GET_CMS_PAGE_DATA';
export const getCmsPageDataResponse = (type, data) => ({
  type,
  data
});

// get Cms Pages By Page Type
export const CMS_PAGE_BY_TYPE = 'CMS_PAGE_BY_TYPE';
export const cmsPageByType = (data) => ({
  type: CMS_PAGE_BY_TYPE,
  data
});
export const CMS_PAGE_BY_TYPE_SUCCESS = 'CMS_PAGE_BY_TYPE_SUCCESS';
export const CMS_PAGE_BY_TYPE_ERROR = 'CMS_PAGE_BY_TYPE_ERROR';
export const cmsPageByTypeResponse = (type, data) => ({
  type,
  data
});

// get Cms Pages detail Page slug
export const CMS_PAGE_DETAIL_BY_SLUG = 'CMS_PAGE_DETAIL_BY_SLUG';
export const cmsPageDetailBySlug = (data) => ({
  type: CMS_PAGE_DETAIL_BY_SLUG,
  data
});
export const CMS_PAGE_DETAIL_BY_SLUG_SUCCESS = 'CMS_PAGE_DETAIL_BY_SLUG_SUCCESS';
export const CMS_PAGE_DETAIL_BY_SLUG_ERROR = 'CMS_PAGE_DETAIL_BY_SLUG_ERROR';
export const cmsPageDetailBySlugResponse = (type, data) => ({
  type,
  data
});