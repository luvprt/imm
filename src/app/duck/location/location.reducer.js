import {
  SUCCESS_ADD_LOCATION,
  ERROR_ADD_LOCATION,
  SUCCESS_LIST_LOCATION,
  ERROR_LIST_LOCATION,
  SUCCESS_DELETE_LOCATION,
  ERROR_DELETE_LOCATION,
  SUCCESS_GET_LOCATION_BY_ID,
  ERROR_GET_LOCATION_BY_ID,
  SUCCESS_EDIT_LOCATION,
  ERROR_EDIT_LOCATION,
  SUCCESS_VIEW_LOCATION,
  ERROR_VIEW_LOCATION,
  SUCCESS_LIST_CUSTOMERS_BY_ROLE,
  ERROR_LIST_CUSTOMERS_BY_ROLE,
  SUCCESS_LOCATION_STATUS,
  ERROR_LOCATION_STATUS
} from './location.action';
import {
  DEFAULT_STATE
} from "./location.state";

export const locationReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_ADD_LOCATION:
      const addLocation = action.data;
      return {
        ...state, locationData: addLocation
      };
    case ERROR_ADD_LOCATION:
      const errorAddLocationData = action.data;
      return {
        ...state, locationData: errorAddLocationData
      };
    case SUCCESS_LIST_LOCATION:
      const locationList = action.data;
      return {
        ...state, locationListData: locationList
      };
    case ERROR_LIST_LOCATION:
      const locationListError = action.data;
      return {
        ...state, locationListData: locationListError
      };
    case SUCCESS_DELETE_LOCATION:
      const deleteLocaion = action.data;
      return {
        ...state, locationData: deleteLocaion
      };
    case ERROR_DELETE_LOCATION:
      const errorDeleteLocaion = action.data;
      return {
        ...state, locationData: errorDeleteLocaion
      };
    case SUCCESS_GET_LOCATION_BY_ID:
      const getLocationById = action.data;
      return {
        ...state, locationDataById: getLocationById
      };
    case ERROR_GET_LOCATION_BY_ID:
      const errorgetLocationById = action.data;
      return {
        ...state, locationDataById: errorgetLocationById
      };
    case SUCCESS_EDIT_LOCATION:
      const editLocation = action.data;
      return {
        ...state, locationData: editLocation
      };
    case ERROR_EDIT_LOCATION:
      const errorEditLocationData = action.data;
      return {
        ...state, locationData: errorEditLocationData
      };
    case SUCCESS_VIEW_LOCATION:
      const viewLocation = action.data;
      return {
        ...state, viewLocationData: viewLocation
      };
    case ERROR_VIEW_LOCATION:
      const viewLocationError = action.data;
      return {
        ...state, viewLocationData: viewLocationError
      };
    case SUCCESS_LIST_CUSTOMERS_BY_ROLE:
      const customersData = action.data;
      return {
        ...state, customersByRole: customersData
      };
    case ERROR_LIST_CUSTOMERS_BY_ROLE:
      const customersDataError = action.data;
      return {
        ...state, customersByRole: customersDataError
      };
    case SUCCESS_LOCATION_STATUS:
      const locationStatus = action.data;
      return {
        ...state, locationStatusData: locationStatus
      };
    case ERROR_LOCATION_STATUS:
      const locationStatusError = action.data;
      return {
        ...state, locationStatusData: locationStatusError
      };
    default:
      return state;
  }
};