import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    SUCCESS_ADD_LOCATION,
    ERROR_ADD_LOCATION,
    ADD_LOCATION,
    addLocationPageResponse,
    SUCCESS_LIST_LOCATION,
    ERROR_LIST_LOCATION,
    LIST_LOCATION,
    listLocationResponse,
    SUCCESS_DELETE_LOCATION,
    DELETE_LOCATION,
    ERROR_DELETE_LOCATION,
    deleteLocationPageResponse,
    GET_LOCATION_BY_ID,
    SUCCESS_GET_LOCATION_BY_ID,
    ERROR_GET_LOCATION_BY_ID,
    getLocationByIdResponse,
    EDIT_LOCATION,
    SUCCESS_EDIT_LOCATION,
    ERROR_EDIT_LOCATION,
    editLocationResponse,
    SUCCESS_VIEW_LOCATION,
    ERROR_VIEW_LOCATION,
    VIEW_LOCATION,
    viewLocationResponse,
    listCustomersByRoleResponse,
    LIST_CUSTOMERS_BY_ROLE,
    SUCCESS_LIST_CUSTOMERS_BY_ROLE,
    ERROR_LIST_CUSTOMERS_BY_ROLE,
    LOCATION_STATUS,
    SUCCESS_LOCATION_STATUS,
    ERROR_LOCATION_STATUS,
    locationStatusResponse
} from './location.action';
import {
    addLocationApi,
    listLocationApi,
    deleteLocationApi,
    getLocationByIdApi,
    editLocationApi,
    viewLocationApi,
    listCustomersByRoleApi,
    locationStatusApi
} from '../../../api/index';
import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';

// Add Location Request
function* addLocationPageRequest(pageData) {
    let locationPageData = yield addLocationApi(pageData);
    if (locationPageData.success && _.has(locationPageData, 'data.data')) {
        successNotification('Location added successfully!!')
        yield put(addLocationPageResponse(SUCCESS_ADD_LOCATION, locationPageData.data));
    } else {
        errorNotification(locationPageData.data.message)
        yield put(addLocationPageResponse(ERROR_ADD_LOCATION, locationPageData.data));
    }
}

export function* addLocationPageWatcher() {
    yield takeLatest(ADD_LOCATION, addLocationPageRequest);
}

// Location Listing Request
function* listLocationRequest(locationData) {
    let getLocationData = yield listLocationApi(locationData);
    if (getLocationData.success && _.has(getLocationData, 'data.data')) {
        yield put(listLocationResponse(SUCCESS_LIST_LOCATION, getLocationData.data));
    } else {
        errorNotification(getLocationData.data.message)
        yield put(listLocationResponse(ERROR_LIST_LOCATION, getLocationData.data));
    }
}

export function* listLocationWatcher() {
    yield takeLatest(LIST_LOCATION, listLocationRequest);
}

// Delete Location By Id Request
function* deleteLocationRequest(pageData) {
    let deleteLocationData = yield deleteLocationApi(pageData);
    if (deleteLocationData.success && _.has(deleteLocationData, 'data.data')) {
        successNotification('Location deleted sucessfully !!');
        yield put(deleteLocationPageResponse(SUCCESS_DELETE_LOCATION, deleteLocationData.data));
    } else {
        errorNotification('Error !!')
        yield put(deleteLocationPageResponse(ERROR_DELETE_LOCATION, deleteLocationData.data));
    }
}

export function* deleteLocationWatcher() {
    yield takeLatest(DELETE_LOCATION, deleteLocationRequest);
}

// Get Location By Id Request
function* getLocationByIdRequest(locationData) {
    let getData = yield getLocationByIdApi(locationData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getLocationByIdResponse(SUCCESS_GET_LOCATION_BY_ID, getData.data));
    } else {
        yield put(getLocationByIdResponse(ERROR_GET_LOCATION_BY_ID, getData.data));
    }
}

export function* getLocationByIdWatcher() {
    yield takeLatest(GET_LOCATION_BY_ID, getLocationByIdRequest);
}

// EDIT Location Request
function* ediLocationRequest(pageData) {
    let locationPageData = yield editLocationApi(pageData);
    if (locationPageData.success && _.has(locationPageData, 'data.data')) {
        successNotification('Location updated successfully!!')
        yield put(editLocationResponse(SUCCESS_EDIT_LOCATION, locationPageData.data));
    } else {
        errorNotification(locationPageData.data.message)
        yield put(editLocationResponse(ERROR_EDIT_LOCATION, locationPageData.data));
    }
}

export function* editLocationPageWatcher() {
    yield takeLatest(EDIT_LOCATION, ediLocationRequest);
}

// View Location Request
function* viewLocationRequest(locationData) {
    let getViewLocationData = yield viewLocationApi(locationData);
    if (getViewLocationData.success && _.has(getViewLocationData, 'data.data')) {
        yield put(viewLocationResponse(SUCCESS_VIEW_LOCATION, getViewLocationData.data));
    } else {
        yield put(viewLocationResponse(ERROR_VIEW_LOCATION, getViewLocationData.data));
    }
}

export function* viewLocationWatcher() {
    yield takeLatest(VIEW_LOCATION, viewLocationRequest);
}

// List Customers By Role Request
function* listCustomersByRoleRequest(data) {
    let getlistCustomersByRoleData = yield listCustomersByRoleApi(data);
    if (getlistCustomersByRoleData.success && _.has(getlistCustomersByRoleData, 'data.data')) {
        yield put(listCustomersByRoleResponse(SUCCESS_LIST_CUSTOMERS_BY_ROLE, getlistCustomersByRoleData.data));
    } else {
        yield put(listCustomersByRoleResponse(ERROR_LIST_CUSTOMERS_BY_ROLE, getlistCustomersByRoleData.data));
    }
}

export function* listCustomersByRoleWatcher() {
    yield takeLatest(LIST_CUSTOMERS_BY_ROLE, listCustomersByRoleRequest);
}

// Location Status Request
function* locationStatusRequest(locationData) {
    let locationStatusData = yield locationStatusApi(locationData);
    if (locationStatusData.success && _.has(locationStatusData, 'data.data')) {
        yield put(locationStatusResponse(SUCCESS_LOCATION_STATUS, {data: Date.now()}));
    } else {
        errorNotification(locationStatusData.data.message)
        yield put(locationStatusResponse(ERROR_LOCATION_STATUS, {data: Date.now()}));
    }
}

export function* locationStatusWatcher() {
    yield takeLatest(LOCATION_STATUS, locationStatusRequest);
}