export const DEFAULT_STATE = {
  locationData: {},
  locationListData: {},
  locationDataById: {},
  viewLocationData: {},
  customersByRole: {},
  locationStatusData: {}
};