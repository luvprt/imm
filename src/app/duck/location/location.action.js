// ADD LOATION PAGE
export const ADD_LOCATION = 'ADD_LOCATION';
export const addLocation = (data) => ({
  type: ADD_LOCATION,
  data
});
export const SUCCESS_ADD_LOCATION = 'SUCCESS_ADD_LOCATION';
export const ERROR_ADD_LOCATION = 'ERROR_ADD_LOCATION';
export const addLocationPageResponse = (type, data) => ({
  type,
  data
});

// LIST LOCATION 
export const LIST_LOCATION = 'LIST_LOCATION';
export const listLocation = (data) => ({
  type: LIST_LOCATION,
  data
});
export const SUCCESS_LIST_LOCATION = 'SUCCESS_LIST_LOCATION';
export const ERROR_LIST_LOCATION = 'ERROR_LIST_LOCATION';
export const listLocationResponse = (type, data) => ({
  type,
  data
});

// DELETE LOCATION
export const DELETE_LOCATION = 'DELETE_LOCATION';
export const deleteLocation = (data) => ({
  type: DELETE_LOCATION,
  data
});
export const SUCCESS_DELETE_LOCATION = 'SUCCESS_DELETE_LOCATION';
export const ERROR_DELETE_LOCATION = 'ERROR_DELETE_LOCATION';
export const deleteLocationPageResponse = (type, data) => ({
  type,
  data
});

// GET LOCATION BY ID
export const GET_LOCATION_BY_ID = 'GET_LOCATION_BY_ID';
export const getLocationById = (data) => ({
  type: GET_LOCATION_BY_ID,
  data
});
export const SUCCESS_GET_LOCATION_BY_ID = 'SUCCESS_GET_LOCATION_BY_ID';
export const ERROR_GET_LOCATION_BY_ID = 'ERROR_GET_LOCATION_BY_ID';
export const getLocationByIdResponse = (type, data) => ({
  type,
  data
});

// EDIT LOATION PAGE
export const EDIT_LOCATION = 'EDIT_LOCATION';
export const editLocation = (data) => ({
  type: EDIT_LOCATION,
  data
});
export const SUCCESS_EDIT_LOCATION = 'SUCCESS_EDIT_LOCATION';
export const ERROR_EDIT_LOCATION = 'ERROR_EDIT_LOCATION';
export const editLocationResponse = (type, data) => ({
  type,
  data
});

// VIEW LOCATION 
export const VIEW_LOCATION = 'VIEW_LOCATION';
export const viewLocation = (data) => ({
  type: VIEW_LOCATION,
  data
});
export const SUCCESS_VIEW_LOCATION = 'SUCCESS_VIEW_LOCATION';
export const ERROR_VIEW_LOCATION = 'ERROR_VIEW_LOCATION';
export const viewLocationResponse = (type, data) => ({
  type,
  data
});

// List Customers By Role
export const LIST_CUSTOMERS_BY_ROLE = 'LIST_CUSTOMERS_BY_ROLE';
export const listCustomersByRole = (data) => ({ type: LIST_CUSTOMERS_BY_ROLE, data });
export const SUCCESS_LIST_CUSTOMERS_BY_ROLE = 'SUCCESS_LIST_CUSTOMERS_BY_ROLE';
export const ERROR_LIST_CUSTOMERS_BY_ROLE = 'ERROR_LIST_CUSTOMERS_BY_ROLE';
export const listCustomersByRoleResponse = (type, data) => ({ type, data });

// Location Status Change
export const LOCATION_STATUS = 'LOCATION_STATUS';
export const locationStatus = (data) => ({
  type: LOCATION_STATUS,
  data
});
export const SUCCESS_LOCATION_STATUS = 'SUCCESS_LOCATION_STATUS';
export const ERROR_LOCATION_STATUS = 'ERROR_LOCATION_STATUS';
export const locationStatusResponse = (type, data) => ({
  type,
  data
});