import _ from 'lodash'

// Selector
export const getLocationData = (state) => {
  return state.location.locationData;
};

export const getAllLocationData = (state) => {
  if (state.location.locationListData && _.has(state.location.locationListData, 'data')) {
    return state.location.locationListData;
  }
  return null;
};

export const getLocationByIdData = (state) => {
  if (state.location.locationDataById && _.has(state.location.locationDataById, 'data')) {
    return state.location.locationDataById;
  }
  return null;
};

export const viewAllLocationData = (state) => {
  if (state.location.viewLocationData && _.has(state.location.viewLocationData, 'data')) {
    return state.location.viewLocationData;
  }
  return null;
};

export const customersByRole = (state) => {
  if (state.location.customersByRole && _.has(state.location.customersByRole, 'data')) {
    return state.location.customersByRole;
  }
  return null;
};

export const locationStatusData = (state) => {
  if (state.location.locationStatusData && _.has(state.location.locationStatusData, 'data')) {
    return state.location.locationStatusData;
  }
  return null;
};