// Add Subscription Plan
export const ADD_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ADD_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const addSubscriptionPlanHighlight = (data) => ({
  type: ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
  data
});
export const SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const addSubscriptionPlanHighlightResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const LIST_SUBSCRIPTION_PLAN_HIGHLIGHT = 'LIST_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const listSubscriptionPlanHighlight = (data) => ({
  type: LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
  data
});
export const SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const listSubscriptionPlanHighlightResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const showSubscriptionDataHighlight = (data) => ({
  type: SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
  data
});
export const SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const showSubscriptionDataHighlightResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT = 'EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const editSubscriptionPlanHighlight = (data) => ({
  type: EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
  data
});
export const SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const editSubscriptionPlanHighlightResponse = (type, data) => ({
  type,
  data
});

// List Subscription Plans
export const DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT = 'DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const deleteSubscriptionPlanHighLight = (data) => ({
  type: DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT,
  data
});
export const SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT = 'SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT = 'ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT';
export const deleteSubscriptionPlanHighLightResponse = (type, data) => ({
  type,
  data
});

// Get All User And Plan Type 
export const GET_All_USER_PLAN_TYPES = 'GET_All_USER_PLAN_TYPES';
export const getAllUserPlanTypes = (data) => ({
  type: GET_All_USER_PLAN_TYPES,
  data
});
export const SUCCESS_GET_All_USER_PLAN_TYPES = 'SUCCESS_GET_All_USER_PLAN_TYPES';
export const ERROR_GET_All_USER_PLAN_TYPES = 'ERROR_GET_All_USER_PLAN_TYPES';
export const getAllUserPlanTypesResponse = (type, data) => ({
  type,
  data
});