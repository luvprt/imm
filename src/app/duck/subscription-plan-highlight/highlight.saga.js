import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    addSubscriptionPlanHighlightResponse,
    ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
    SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
    ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
    LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
    listSubscriptionPlanHighlightResponse,
    SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
    ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
    showSubscriptionDataHighlightResponse,
    SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
    SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
    ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
    editSubscriptionPlanHighlightResponse,
    EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
    SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
    ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
    GET_All_USER_PLAN_TYPES,
    SUCCESS_GET_All_USER_PLAN_TYPES,
    ERROR_GET_All_USER_PLAN_TYPES,
    getAllUserPlanTypesResponse,
    deleteSubscriptionPlanHighLightResponse,
    DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT,
    SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT,
    ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT
} from './highlight.action';
import {
    addSubscriptionPlanHighlightApi,
    listSubscriptionPlanHighlightApi,
    showSubscriptionPlanHighlightApi,
    editSubscriptionPlanHighlightApi,
    getAllUserPlanTypesApi,
    deleteSubscriptionPlanHighlightApi
} from '../../../api/index';
import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';

function* addSubscriptionPlanHighlightRequest(subscriptionData) {
    let getData = yield addSubscriptionPlanHighlightApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification(getData.data.message)
        yield put(addSubscriptionPlanHighlightResponse(SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT, 'Added'));
    } else {
        errorNotification(getData.data.message)
        yield put(addSubscriptionPlanHighlightResponse(ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    }
}

export function* addSubscriptionHighlightWatcher() {
    yield takeLatest(ADD_SUBSCRIPTION_PLAN_HIGHLIGHT, addSubscriptionPlanHighlightRequest);
}

function* listSubscriptionHighlightPlanRequest(subscriptionData) {
    let getData = yield listSubscriptionPlanHighlightApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(listSubscriptionPlanHighlightResponse(SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    } else {
        yield put(listSubscriptionPlanHighlightResponse(ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    }
}

export function* listSubscriptionHighlightWatcher() {
    yield takeLatest(LIST_SUBSCRIPTION_PLAN_HIGHLIGHT, listSubscriptionHighlightPlanRequest);
}

function* showSubscriptionPlanRequest(subscriptionData) {
    let getData = yield showSubscriptionPlanHighlightApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(showSubscriptionDataHighlightResponse(SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    } else {
        yield put(showSubscriptionDataHighlightResponse(ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    }
}

export function* showSubscriptionHighlightWatcher() {
    yield takeLatest(SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT, showSubscriptionPlanRequest);
}

function* editSubscriptionPlanHighlightRequest(subscriptionData) {
    let getData = yield editSubscriptionPlanHighlightApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification(getData.data.message);
        yield put(editSubscriptionPlanHighlightResponse(SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT, 'Edited'));
    } else {
        errorNotification('Error !!')
        yield put(editSubscriptionPlanHighlightResponse(ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    }
}

export function* editSubscriptionHighlightWatcher() {
    yield takeLatest(EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT, editSubscriptionPlanHighlightRequest);
}

function* getAllUserPlanTypesRequest() {
    let getData = yield getAllUserPlanTypesApi();
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(getAllUserPlanTypesResponse(SUCCESS_GET_All_USER_PLAN_TYPES, getData.data));
    } else {
        yield put(getAllUserPlanTypesResponse(ERROR_GET_All_USER_PLAN_TYPES, getData.data));
    }
}

export function* AllUserPlanTypesWatcher() {
    yield takeLatest(GET_All_USER_PLAN_TYPES, getAllUserPlanTypesRequest);
}

function* deleteSubscriptionPlanHighlightRequest(subscriptionData) {
    let getData = yield deleteSubscriptionPlanHighlightApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification(getData.data.message);
        yield put(deleteSubscriptionPlanHighLightResponse(SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT, 'Deleted'));
    } else {
        errorNotification('Error !!')
        yield put(deleteSubscriptionPlanHighLightResponse(ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT, getData.data));
    }
}

export function* deleteSubscriptionHighlightWatcher() {
    yield takeLatest(DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT, deleteSubscriptionPlanHighlightRequest);
}