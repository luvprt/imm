import {
  SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
  ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT,
  SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
  ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT,
  SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
  ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT,
  SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
  ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT,
  SUCCESS_GET_All_USER_PLAN_TYPES,
  ERROR_GET_All_USER_PLAN_TYPES,
  SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT,
  ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT
} from './highlight.action';
import {
  DEFAULT_STATE
} from './highlight.state'

export const planHighlightReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const planHighlightData = action.data;
      return {
        ...state, planHighlightData
      };
    case ERROR_ADD_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const errorSubscriptionData = action.data;
      return {
        ...state, planHighlightData: errorSubscriptionData
      };
    case SUCCESS_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const subscriptionplanHighlightList = action.data;
      return {
        ...state, planHighlightData: subscriptionplanHighlightList
      };
    case ERROR_LIST_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const subscriptionplanHighlightListError = action.data;
      return {
        ...state, planHighlightData: subscriptionplanHighlightListError
      };
    case SUCCESS_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const showSubscriptionPlansHighlight = action.data;
      return {
        ...state, planHighlightData: showSubscriptionPlansHighlight
      };
    case ERROR_SHOW_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const showSubscriptionplanHighlightError = action.data;
      return {
        ...state, planHighlightData: showSubscriptionplanHighlightError
      };
    case SUCCESS_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const editSubscriptionplanHighlightData = action.data;
      return {
        ...state, planHighlightData: editSubscriptionplanHighlightData
      };
    case ERROR_EDIT_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const errorEditSubscriptionplanHighlightData = action.data;
      return {
        ...state, planHighlightData: errorEditSubscriptionplanHighlightData
      };
    case SUCCESS_GET_All_USER_PLAN_TYPES:
      const AllUserPlanTypesData = action.data;
      return {
        ...state, AllUserPlanTypesData
      };
    case ERROR_GET_All_USER_PLAN_TYPES:
      const errorAllUserPlanTypesData = action.data;
      return {
        ...state, AllUserPlanTypesData: errorAllUserPlanTypesData
      };
    case SUCCESS_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const deleteSubscriptionplanHighlightData = action.data;
      return {
        ...state, planHighlightData: deleteSubscriptionplanHighlightData
      };
    case ERROR_DELETE_SUBSCRIPTION_PLAN_HIGHLIGHT:
      const errorDeleteSubscriptionplanHighlightData = action.data;
      return {
        ...state, planHighlightData: errorDeleteSubscriptionplanHighlightData
      };
    default:
      return state;
  }
};