import _ from 'lodash'

// Selector
export const getSavedHighlight = (state) => {
  return state.planHighlight.planHighlightData;
};

export const getAllUserPlanTypesData = (state) => {
  if (state.planHighlight.AllUserPlanTypesData && _.has(state.planHighlight.AllUserPlanTypesData, 'data')) {
    return state.planHighlight.AllUserPlanTypesData.data;
  }
  return null;
};