import {
  SUCCESS_ADD_EMPLOYEE, ERROR_ADD_EMPLOYEE,
  SUCCESS_LIST_EMPLOYEES, ERROR_LIST_EMPLOYEES
} from './employees.action';
import { DEFAULT_STATE } from './employees.state'

export const employeeReducer = (state = DEFAULT_STATE, action = { type: {}, data: {} }) => {
  switch (action.type) {
    case SUCCESS_ADD_EMPLOYEE:
      const addEmployeeData = action.data;
      return { ...state, addEmployeeData };
    case ERROR_ADD_EMPLOYEE:
      const errorAddEmployeeData = action.data;
      return { ...state, addEmployeeData: errorAddEmployeeData };
    case SUCCESS_LIST_EMPLOYEES:
      const employeesList = action.data;
      return { ...state, employeeData: employeesList };
    case ERROR_LIST_EMPLOYEES:
      const employeesListError = action.data;
      return { ...state, employeeData: employeesListError };
    default:
      return state;
  }
};