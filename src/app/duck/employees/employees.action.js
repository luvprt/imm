// Add Employee
export const ADD_EMPLOYEE = 'ADD_EMPLOYEE';
export const addEmployee = (data) => ({ type: ADD_EMPLOYEE, data });
export const SUCCESS_ADD_EMPLOYEE = 'SUCCESS_ADD_EMPLOYEE';
export const ERROR_ADD_EMPLOYEE = 'ERROR_ADD_EMPLOYEE';
export const addEmployeeResponse = (type, data) => ({ type, data });

// List Employees
export const LIST_EMPLOYEES = 'LIST_EMPLOYEES';
export const listEmployees = (data) => ({ type: LIST_EMPLOYEES, data });
export const SUCCESS_LIST_EMPLOYEES = 'SUCCESS_LIST_EMPLOYEES';
export const ERROR_LIST_EMPLOYEES = 'ERROR_LIST_EMPLOYEES';
export const listEmployeesResponse = (type, data) => ({ type, data });
