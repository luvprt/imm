import { put, takeLatest } from 'redux-saga/effects';
import {
    ADD_EMPLOYEE, SUCCESS_ADD_EMPLOYEE, ERROR_ADD_EMPLOYEE,
    addEmployeeResponse, LIST_EMPLOYEES,
    SUCCESS_LIST_EMPLOYEES, ERROR_LIST_EMPLOYEES,
    listEmployeesResponse
} from './employees.action';
import { addEmployeeApi, listEmployeesApi } from '../../../api/index';
import _ from 'lodash'
import { successNotification, errorNotification } from '../../common/notification-alert';

function* addEmployeeRequest(subscriptionData) {
    let getData = yield addEmployeeApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Employee added successfully !!')
        yield put(addEmployeeResponse(SUCCESS_ADD_EMPLOYEE, getData.data));
    } else {
        errorNotification(getData.data.message)
        yield put(addEmployeeResponse(ERROR_ADD_EMPLOYEE, getData.data));
    }
}

export function* addEmployeeWatcher() {
    yield takeLatest(ADD_EMPLOYEE, addEmployeeRequest);
}

function* listEmployeesRequest(subscriptionData) {
    let getData = yield listEmployeesApi(subscriptionData);
    if (getData.success && _.has(getData, 'data.data')) {
        yield put(listEmployeesResponse(SUCCESS_LIST_EMPLOYEES, getData.data));
    } else {
        yield put(listEmployeesResponse(ERROR_LIST_EMPLOYEES, getData.data));
    }
}

export function* listEmployeesWatcher() {
    yield takeLatest(LIST_EMPLOYEES, listEmployeesRequest);
}
