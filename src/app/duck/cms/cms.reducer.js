import {
  SUCCESS_ADD_CMS_PAGE,
  ERROR_ADD_CMS_PAGE,
  SUCCESS_EDIT_CMS_PAGE,
  ERROR_EDIT_CMS_PAGE,
  SUCCESS_VIEW_CMS_PAGE,
  ERROR_VIEW_CMS_PAGE,
  SUCCESS_LIST_CMS_PAGES,
  ERROR_LIST_CMS_PAGES,
  SUCCESS_DELETE_CMS_PAGE,
  ERROR_DELETE_CMS_PAGE,
  SUCCESS_GET_ALL_CMS_PAGES,
  ERROR_GET_ALL_CMS_PAGES
} from './cms.action';
import {
  DEFAULT_STATE
} from "./cms.state";

export const cmsReducer = (state = DEFAULT_STATE, action = {
  type: {},
  data: {}
}) => {
  switch (action.type) {
    case SUCCESS_ADD_CMS_PAGE:
      const addCmsData = action.data;
      return {
        ...state, cmsData: addCmsData
      };
    case ERROR_ADD_CMS_PAGE:
      const errorAddCmsData = action.data;
      return {
        ...state, cmsData: errorAddCmsData
      };
    case SUCCESS_EDIT_CMS_PAGE:
      const editCmsData = action.data;
      return {
        ...state, cmsData: editCmsData
      };
    case ERROR_EDIT_CMS_PAGE:
      const errorEditCmsData = action.data;
      return {
        ...state, cmsData: errorEditCmsData
      };
    case SUCCESS_VIEW_CMS_PAGE:
      const editViewData = action.data;
      return {
        ...state, cmsData: editViewData
      };
    case ERROR_VIEW_CMS_PAGE:
      const errorViewCmsData = action.data;
      return {
        ...state, cmsData: errorViewCmsData
      };
    case SUCCESS_LIST_CMS_PAGES:
      const listCmsData = action.data;
      return {
        ...state, cmsData: listCmsData
      };
    case ERROR_LIST_CMS_PAGES:
      const errorListCmsData = action.data;
      return {
        ...state, cmsData: errorListCmsData
      };
    case SUCCESS_DELETE_CMS_PAGE:
      const deleteCmsData = action.data;
      return {
        ...state, cmsData: deleteCmsData
      };
    case ERROR_DELETE_CMS_PAGE:
      const errorDeleteCmsData = action.data;
      return {
        ...state, cmsData: errorDeleteCmsData
      };
    case SUCCESS_GET_ALL_CMS_PAGES:
      const getCmsData = action.data;
      return {
        ...state, getAllCmsPages: getCmsData
      };
    case ERROR_GET_ALL_CMS_PAGES:
      const errorGetCmsData = action.data;
      return {
        ...state, getAllCmsPages: errorGetCmsData
      };
    default:
      return state;
  }
};