// ADD CMS PAGE
export const ADD_CMS_PAGE = 'ADD_CMS_PAGE';
export const addCmsPage = (data) => ({
  type: ADD_CMS_PAGE,
  data
});
export const SUCCESS_ADD_CMS_PAGE = 'SUCCESS_ADD_CMS_PAGE';
export const ERROR_ADD_CMS_PAGE = 'ERROR_ADD_CMS_PAGE';
export const addCmsPageResponse = (type, data) => ({
  type,
  data
});

// EDIT CMS PAGE
export const EDIT_CMS_PAGE = 'EDIT_CMS_PAGE';
export const editCmsPage = (data) => ({
  type: EDIT_CMS_PAGE,
  data
});
export const SUCCESS_EDIT_CMS_PAGE = 'SUCCESS_EDIT_CMS_PAGE';
export const ERROR_EDIT_CMS_PAGE = 'ERROR_EDIT_CMS_PAGE';
export const editCmsPageResponse = (type, data) => ({
  type,
  data
});

// VIEW CMS PAGE
export const VIEW_CMS_PAGE = 'VIEW_CMS_PAGE';
export const viewCmsPage = (data) => ({
  type: VIEW_CMS_PAGE,
  data
});
export const SUCCESS_VIEW_CMS_PAGE = 'SUCCESS_VIEW_CMS_PAGE';
export const ERROR_VIEW_CMS_PAGE = 'ERROR_VIEW_CMS_PAGE';
export const viewCmsPageResponse = (type, data) => ({
  type,
  data
});

// LIST CMS PAGE
export const LIST_CMS_PAGES = 'LIST_CMS_PAGES';
export const listCmsPages = (data) => ({
  type: LIST_CMS_PAGES,
  data
});
export const SUCCESS_LIST_CMS_PAGES = 'SUCCESS_LIST_CMS_PAGES';
export const ERROR_LIST_CMS_PAGES = 'ERROR_LIST_CMS_PAGES';
export const listCmsPagesResponse = (type, data) => ({
  type,
  data
});

// LIST CMS PAGE
export const DELETE_CMS_PAGE = 'DELETE_CMS_PAGE';
export const deleteCmsPage = (data) => ({
  type: DELETE_CMS_PAGE,
  data
});
export const SUCCESS_DELETE_CMS_PAGE = 'SUCCESS_DELETE_CMS_PAGE';
export const ERROR_DELETE_CMS_PAGE = 'ERROR_DELETE_CMS_PAGE';
export const deleteCmsPageResponse = (type, data) => ({
  type,
  data
});

// LIST CMS PAGE
export const GET_ALL_CMS_PAGES = 'GET_ALL_CMS_PAGES';
export const getAllCmsPages = (data) => ({
  type: GET_ALL_CMS_PAGES,
  data
});
export const SUCCESS_GET_ALL_CMS_PAGES = 'SUCCESS_GET_ALL_CMS_PAGES';
export const ERROR_GET_ALL_CMS_PAGES = 'ERROR_GET_ALL_CMS_PAGES';
export const getAllCmsPagesResponse = (type, data) => ({
  type,
  data
});
