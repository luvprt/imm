import {
    put,
    takeLatest
} from 'redux-saga/effects';
import {
    addCmsPageResponse,
    ERROR_ADD_CMS_PAGE,
    SUCCESS_ADD_CMS_PAGE,
    ADD_CMS_PAGE,
    editCmsPageResponse,
    ERROR_EDIT_CMS_PAGE,
    SUCCESS_EDIT_CMS_PAGE,
    EDIT_CMS_PAGE,
    viewCmsPageResponse,
    SUCCESS_VIEW_CMS_PAGE,
    ERROR_VIEW_CMS_PAGE,
    VIEW_CMS_PAGE,
    listCmsPagesResponse,
    SUCCESS_LIST_CMS_PAGES,
    ERROR_LIST_CMS_PAGES,
    LIST_CMS_PAGES,
    DELETE_CMS_PAGE,
    SUCCESS_DELETE_CMS_PAGE,
    ERROR_DELETE_CMS_PAGE,
    deleteCmsPageResponse,
    GET_ALL_CMS_PAGES,
    SUCCESS_GET_ALL_CMS_PAGES,
    ERROR_GET_ALL_CMS_PAGES,
    getAllCmsPagesResponse

} from './cms.action';
import {
    addCmsPageApi,
    editCmsPageApi,
    viewCmsPageApi,
    listCmsPagesApi,
    deleteCmsPagesApi,
    getAllCmsPagesApi
} from '../../../api/index';
import _ from 'lodash'
import {
    successNotification,
    errorNotification
} from '../../common/notification-alert';


function* addCmsPageRequest(pageData) {
    let cmsPageData = yield addCmsPageApi(pageData);
    console.log(cmsPageData)
    if (cmsPageData.success && _.has(cmsPageData, 'data.data')) {
        successNotification('Cms page added successfully!!')
        yield put(addCmsPageResponse(SUCCESS_ADD_CMS_PAGE, 'Added'));
    } else {
        errorNotification(cmsPageData.data.message)
        yield put(addCmsPageResponse(ERROR_ADD_CMS_PAGE, cmsPageData.data));
    }
}

export function* addCmsPageWatcher() {
    yield takeLatest(ADD_CMS_PAGE, addCmsPageRequest);
}

function* editCmsPageRequest(pageData) {
    let editCmsPageData = yield editCmsPageApi(pageData);
    console.log(editCmsPageData)
    if (editCmsPageData.success && _.has(editCmsPageData, 'data.data')) {
        successNotification('Cms page edited successfully!!')
        yield put(editCmsPageResponse(SUCCESS_EDIT_CMS_PAGE, 'Edited'));
    } else {
        errorNotification('Error !!')
        yield put(editCmsPageResponse(ERROR_EDIT_CMS_PAGE, editCmsPageData.data));
    }
}

export function* editCmsPageWatcher() {
    yield takeLatest(EDIT_CMS_PAGE, editCmsPageRequest);
}

function* viewCmsPageRequest(pageData) {
    let editCmsPageData = yield viewCmsPageApi(pageData);
    console.log(editCmsPageData)
    if (editCmsPageData.success && _.has(editCmsPageData, 'data.data')) {
        yield put(viewCmsPageResponse(SUCCESS_VIEW_CMS_PAGE, editCmsPageData.data));
    } else {
        yield put(viewCmsPageResponse(ERROR_VIEW_CMS_PAGE, editCmsPageData.data));
    }
}

export function* viewCmsPageWatcher() {
    yield takeLatest(VIEW_CMS_PAGE, viewCmsPageRequest);
}

function* listCmsPagesRequest(pageData) {
    let editCmsPageData = yield listCmsPagesApi(pageData);
    if (editCmsPageData.success && _.has(editCmsPageData, 'data.data')) {
        yield put(listCmsPagesResponse(SUCCESS_LIST_CMS_PAGES, editCmsPageData.data));
    } else {
        yield put(listCmsPagesResponse(ERROR_LIST_CMS_PAGES, editCmsPageData.data));
    }
}

export function* listCmsPagesWatcher() {
    yield takeLatest(LIST_CMS_PAGES, listCmsPagesRequest);
}


function* deleteCmsPagesRequest(pageData) {
    let deleteCmsPageData = yield deleteCmsPagesApi(pageData);
    if (deleteCmsPageData.success && _.has(deleteCmsPageData, 'data.data')) {
        successNotification('CMS page deleted sucessfully !!');
        yield put(deleteCmsPageResponse(SUCCESS_DELETE_CMS_PAGE, 'Deleted'));
    } else {
        errorNotification('Error !!')
        yield put(deleteCmsPageResponse(ERROR_DELETE_CMS_PAGE, deleteCmsPageData.data));
    }
}

export function* deleteCmsPagesWatcher() {
    yield takeLatest(DELETE_CMS_PAGE, deleteCmsPagesRequest);
}

function* getAllCmsPagesRequest() {
    let getCmsPageData = yield getAllCmsPagesApi();
    if (getCmsPageData.success && _.has(getCmsPageData, 'data.data')) {
        yield put(getAllCmsPagesResponse(SUCCESS_GET_ALL_CMS_PAGES, getCmsPageData.data));
    } else {
        errorNotification('Error !!')
        yield put(getAllCmsPagesResponse(ERROR_GET_ALL_CMS_PAGES, getCmsPageData.data));
    }
}

export function* getAllCmsPagesWatcher() {
    yield takeLatest(GET_ALL_CMS_PAGES, getAllCmsPagesRequest);
}