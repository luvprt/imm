// Selector
export const getCmsPages = (state) => {
    return state.cms.cmsData;
};

export const getAllCmsPagesData = (state) => {
    return state.cms.getAllCmsPages;
};