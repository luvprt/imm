import {
  SUCCESS_ADD_CUSTOMER, ERROR_ADD_CUSTOMER,
  SUCCESS_LIST_CUSTOMERS, ERROR_LIST_CUSTOMERS,
  SUCCESS_VIEW_CUSTOMER, ERROR_VIEW_CUSTOMER,
  SUCCESS_EDIT_CUSTOMER, ERROR_EDIT_CUSTOMER,
  SUCCESS_EMAIL_CHECK_CLEAR_BIT, ERROR_EMAIL_CHECK_CLEAR_BIT,
  RESET_CLEARBIT_DATA, C
} from './customer.action';
import { DEFAULT_STATE } from './customer.state'

export const customerReducer = (state = DEFAULT_STATE, action = { type: {}, data: {} }) => {
  switch (action.type) {
    case SUCCESS_ADD_CUSTOMER:
      const addCustomerData = action.data;
      return { ...state, addCustomerData };
    case ERROR_ADD_CUSTOMER:
      const errorAddCustomerData = action.data;
      return { ...state, addCustomerData: errorAddCustomerData };
    case SUCCESS_EMAIL_CHECK_CLEAR_BIT:
      const clearBitData = action.data;
      return { ...state, clearBitData };
    case ERROR_EMAIL_CHECK_CLEAR_BIT:
      const errorClearBitData = action.data;
      return { ...state, viewCustomerData: errorClearBitData };
    case RESET_CLEARBIT_DATA:
      const resetClearBitData = action.data;
      return { ...state, viewCustomerData: resetClearBitData };
    default:
      return state;
  }
};