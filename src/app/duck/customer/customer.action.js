// Add Customer
export const ADD_CUSTOMER = 'ADD_CUSTOMER';
export const addCustomer = (data) => ({ type: ADD_CUSTOMER, data });
export const SUCCESS_ADD_CUSTOMER = 'SUCCESS_ADD_CUSTOMER';
export const ERROR_ADD_CUSTOMER = 'ERROR_ADD_CUSTOMER';
export const addCustomerResponse = (type, data) => ({ type, data });

// Get Customer data from clearbit if any
export const EMAIL_CHECK_CLEAR_BIT = 'EMAIL_CHECK_CLEAR_BIT';
export const emailCheckClearbit = (data) => ({ type: EMAIL_CHECK_CLEAR_BIT, data });
export const SUCCESS_EMAIL_CHECK_CLEAR_BIT = 'SUCCESS_EMAIL_CHECK_CLEAR_BIT';
export const ERROR_EMAIL_CHECK_CLEAR_BIT = 'ERROR_EMAIL_CHECK_CLEAR_BIT';
export const emailCheckClearbitResponse = (type, data) => ({ type, data });

export const RESET_CLEARBIT_DATA = 'RESET_CLEARBIT_DATA';

// List Customers
export const LIST_CUSTOMERS = 'LIST_CUSTOMERS';
export const listCustomers = (data) => ({ type: LIST_CUSTOMERS, data });
export const SUCCESS_LIST_CUSTOMERS = 'SUCCESS_LIST_CUSTOMERS';
export const ERROR_LIST_CUSTOMERS = 'ERROR_LIST_CUSTOMERS';
export const listCustomersResponse = (type, data) => ({ type, data });

// View Customer
export const VIEW_CUSTOMER = 'VIEW_CUSTOMER';
export const viewCustomer = (data) => ({ type: VIEW_CUSTOMER, data });
export const SUCCESS_VIEW_CUSTOMER = 'SUCCESS_VIEW_CUSTOMER';
export const ERROR_VIEW_CUSTOMER = 'ERROR_VIEW_CUSTOMER';
export const viewCustomerResponse = (type, data) => ({ type, data });

// Edit Customer
export const EDIT_CUSTOMER = 'EDIT_CUSTOMER';
export const editCustomer = (data) => ({ type: EDIT_CUSTOMER, data });
export const SUCCESS_EDIT_CUSTOMER = 'SUCCESS_EDIT_CUSTOMER';
export const ERROR_EDIT_CUSTOMER = 'ERROR_EDIT_CUSTOMER';
export const editCustomerResponse = (type, data) => ({ type, data });
