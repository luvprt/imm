import { put, takeLatest } from 'redux-saga/effects';
import {
    addCustomerResponse, ADD_CUSTOMER,
    SUCCESS_ADD_CUSTOMER, ERROR_ADD_CUSTOMER,
    emailCheckClearbitResponse, EMAIL_CHECK_CLEAR_BIT,
    SUCCESS_EMAIL_CHECK_CLEAR_BIT, ERROR_EMAIL_CHECK_CLEAR_BIT,
    RESET_CLEARBIT_DATA
} from './customer.action';
import { addCustomerApi, emailCheckClearBitApi } from '../../../api/index';
import _ from 'lodash'
import { successNotification } from '../../common/notification-alert';

function* addCustomerRequest(customerData) {
    let getData = yield addCustomerApi(customerData);
    if (getData.success && _.has(getData, 'data.data')) {
        successNotification('Customer added successfully !!')
        yield put(addCustomerResponse(SUCCESS_ADD_CUSTOMER, getData.data));
    } else {
        yield put(addCustomerResponse(ERROR_ADD_CUSTOMER, getData.data));
    }
}

export function* addCustomerWatcher() {
    yield takeLatest(ADD_CUSTOMER, addCustomerRequest);
}


function* emailCheckClearBitRequest(customerData) {
    yield put(emailCheckClearbitResponse(RESET_CLEARBIT_DATA, { data: '' }));
    let clearBitData = yield emailCheckClearBitApi(customerData);
    if (clearBitData.success && _.has(clearBitData, 'data.data')) {
        yield put(emailCheckClearbitResponse(SUCCESS_EMAIL_CHECK_CLEAR_BIT, clearBitData.data));
    } else {
        yield put(emailCheckClearbitResponse(ERROR_EMAIL_CHECK_CLEAR_BIT, clearBitData.data));
    }
}

export function* emailCheckClearBitWatcher() {
    yield takeLatest(EMAIL_CHECK_CLEAR_BIT, emailCheckClearBitRequest);
}
