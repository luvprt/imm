import React from "react";
import LOGO from '../../../../assets/images/profile-img.jpg'
import { onLogout } from '../../../../routing/authService'
import { Link } from "react-router-dom";
import { getUserDetails } from '../../../../storage/user';
import _ from 'lodash'

export class AuthNavbar extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      name: '',
      email: ''
    }
  }

  componentWillMount() {
    let userData = getUserDetails()
    if (userData && _.has(userData, 'name') && _.has(userData, 'email')) {
      this.setState({ name: userData.name, email: userData.email });
    }
  }

  userLogout = () => {
    onLogout();
  }

  render() {
    const { name, email } = this.state
    return (
      <>
        <nav className="navbar navbar-header navbar-expand-lg bg-primary-gradient">

          <div className="container-fluid">
            <div className="collapse" id="search-nav">
              <form className="navbar-left navbar-form nav-search mr-md-3">
                <div className="input-group">
                  <div className="input-group-prepend">
                    <button type="submit" className="btn btn-search pr-1">
                      <i className="fa fa-search search-icon"></i>
                    </button>
                  </div>
                  <input type="text" placeholder="Search ..." className="form-control" />
                </div>
              </form>
            </div>
            <ul className="navbar-nav topbar-nav ml-md-auto align-items-center">
              <li className="nav-item toggle-nav-search hidden-caret">
                <a className="nav-link" data-toggle="collapse" href="#search-nav" role="button" aria-expanded="false" aria-controls="search-nav">
                  <i className="fa fa-search"></i>
                </a>
              </li>
              <li className="nav-item dropdown hidden-caret">
                <Link to="" onClick={e => e.preventDefault()} className="nav-link dropdown-toggle" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-envelope"></i>
                </Link>
                <ul className="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
                  <li>
                    <div className="dropdown-title d-flex justify-content-between align-items-center">
                      Messages
										<Link to="" onClick={e => e.preventDefault()} className="small">Mark all as read</Link>
                    </div>
                  </li>
                  <li>
                    <div className="message-notif-scroll scrollbar-outer">
                      <div className="notif-center">
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-img">
                            <img src="https://via.placeholder.com/50x50" alt="Img Profile" />
                          </div>
                          <div className="notif-content">
                            <span className="subject">Jimmy Denis</span>
                            <span className="block">
                              How are you ?
													</span>
                            <span className="time">5 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-img">
                            <img src="https://via.placeholder.com/50x50" alt="Img Profile" />
                          </div>
                          <div className="notif-content">
                            <span className="subject">Chad</span>
                            <span className="block">
                              Ok, Thanks !
													</span>
                            <span className="time">12 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-img">
                            <img src="https://via.placeholder.com/50x50" alt="Img Profile" />
                          </div>
                          <div className="notif-content">
                            <span className="subject">Jhon Doe</span>
                            <span className="block">
                              Ready for the meeting today...
													</span>
                            <span className="time">12 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-img">
                            <img src="https://via.placeholder.com/50x50" alt="Img Profile" />
                          </div>
                          <div className="notif-content">
                            <span className="subject">Talha</span>
                            <span className="block">
                              Hi, Apa Kabar ?
													</span>
                            <span className="time">17 minutes ago</span>
                          </div>
                        </Link>
                      </div>
                    </div>
                  </li>
                  <li>
                    <Link to="" onClick={e => e.preventDefault()} className="see-all">See all messages<i className="fa fa-angle-right"></i> </Link>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown hidden-caret">
                <Link to="" onClick={e => e.preventDefault()} className="nav-link dropdown-toggle" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i className="fa fa-bell"></i>
                  <span className="notification">4</span>
                </Link>
                <ul className="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                  <li>
                    <div className="dropdown-title">You have 4 new notification</div>
                  </li>
                  <li>
                    <div className="notif-scroll scrollbar-outer">
                      <div className="notif-center">
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-icon notif-primary"> <i className="fa fa-user-plus"></i> </div>
                          <div className="notif-content">
                            <span className="block">
                              New user registered
													</span>
                            <span className="time">5 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-icon notif-success"> <i className="fa fa-comment"></i> </div>
                          <div className="notif-content">
                            <span className="block">
                              Rahmad commented on Admin
													</span>
                            <span className="time">12 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-img">
                            <img src="https://via.placeholder.com/50x50" alt="Img Profile" />
                          </div>
                          <div className="notif-content">
                            <span className="block">
                              Reza send messages to you
													</span>
                            <span className="time">12 minutes ago</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()}>
                          <div className="notif-icon notif-danger"> <i className="fa fa-heart"></i> </div>
                          <div className="notif-content">
                            <span className="block">
                              Farrah liked Admin
													</span>
                            <span className="time">17 minutes ago</span>
                          </div>
                        </Link>
                      </div>
                    </div>
                  </li>
                  <li>
                    <Link to="" onClick={e => e.preventDefault()} className="see-all">See all notifications<i className="fa fa-angle-right"></i> </Link>
                  </li>
                </ul>
              </li>
              <li className="nav-item dropdown hidden-caret">
                <Link to="" onClick={e => e.preventDefault()} data-toggle="dropdown" aria-expanded="false" className="nav-link dropdown-toggle">
                  <i className="fas fa-layer-group"></i>
                </Link>
                <div className="dropdown-menu quick-actions quick-actions-info animated fadeIn">
                  <div className="quick-actions-header bg-light">
                    <span className="title text-dark">Quick Actions</span>
                  </div>
                  <div className="quick-actions-scroll scrollbar-outer">
                    <div className="quick-actions-items">
                      <div className="row m-0">
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-file-signature"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">Create Work Order</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-list"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">Add Customer</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-file-signature"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">Create Report</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-list"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">Add Project</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-file-signature"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">Create a Quote</span>
                          </div>
                        </Link>
                        <Link to="" onClick={e => e.preventDefault()} className="col-6 col-md-4 p-0 mb-3">
                          <div className="quick-actions-item h-100 my-0">
                            <i className="fas fa-list"></i>
                            <span className="text-dark h6 mb-0 text-center mt-1">View Invoices</span>
                          </div>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li className="nav-item dropdown hidden-caret">
                <Link to="" onClick={e => e.preventDefault()} className="dropdown-toggle profile-pic" data-toggle="dropdown" aria-expanded="false">
                  <div className="avatar-sm">
                    <img src={LOGO} alt="..." className="avatar-img rounded-circle" />
                  </div>
                </Link>
                <ul className="dropdown-menu dropdown-user animated fadeIn">
                  <div className="dropdown-user-scroll scrollbar-outer">
                    <li>
                      <div className="user-box">
                        <div className="avatar-lg">
                          <img src={LOGO} alt="profile" className="avatar-img rounded" />
                        </div>
                        <div className="u-text">
                          <h4>{name || ''}</h4>
                          <p className="text-muted">{email || ''}</p><a href="profile.html" className="btn btn-xs btn-secondary btn-sm">View Profile</a>
                        </div>
                      </div>
                    </li>
                    <li>
                      <div className="dropdown-divider"></div>
                      <span className="cursorCls dropdown-item">My Profile</span>
                      <span className="cursorCls dropdown-item" >My Balance</span>
                      <span className="cursorCls dropdown-item">Inbox</span>
                      <div className="dropdown-divider"></div>
                      <span className="cursorCls dropdown-item">Account Setting</span>
                      <span className="cursorCls dropdown-item">Service Request</span>
                      <div className="dropdown-divider"></div>
                      <span onClick={() => this.userLogout()} className="cursorCls dropdown-item">Logout</span>
                    </li>
                  </div>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </>
    );
  }
}
