import React, { Component } from 'react';
import { AuthLogoHeader } from './auth.logoheader'
import { AuthNavbar } from '../navbar/auth.navbar'
import { Loader } from '../loader/loader'

export class AuthHeader extends Component {

  render() {
    return (
      <div className="main-header">
        <Loader loader={this.props.loader}/>
        <AuthLogoHeader toggleAdminSideBarMobile={this.props.sideBarTogglerMobile} toggleAdminSideBar={this.props.sideBarToggler} />
        <AuthNavbar />
      </div>
    );
  }
}
