import React, { Component } from 'react';
import LOGO from '../../../../assets/images/logo-itm.png'
import { HOME } from '../../../../routing/routeContants';
import { Link } from "react-router-dom";

export class AuthLogoHeader extends Component {

    render() {
        return (
            <div className="logo-header">				
				<Link to={HOME} className="logo">
					<img src={LOGO} alt="navbar brand" className="navbar-brand" />
				</Link>
				<div className="navbar-toggler sidenav-toggler ml-auto" onClick={this.props.toggleAdminSideBarMobile}>
					<button className="btn btn-toggle toggle-sidebar"><i className="fas fa-bars text-dark"></i></button>
				</div>				
				<button className="topbar-toggler more"><i className="fas fa-ellipsis-v text-color-gradient"></i></button>
				<div className="nav-toggle" onClick={this.props.toggleAdminSideBar}>
					<button className="btn btn-toggle toggle-sidebar"><i className="fas fa-bars text-dark"></i></button>
				</div>
			</div>
        );
    }
}
