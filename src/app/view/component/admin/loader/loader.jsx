import React, { Component } from 'react';

export class Loader extends Component {
  render() {
    return (
      <>
        {
          this.props.loader === true ?
            <div className="loader_itm"><div className="loader_123"></div></div>
            : ''
        }
      </>
    );
  }
}

