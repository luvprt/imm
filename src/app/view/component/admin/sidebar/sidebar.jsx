import React, { Component } from 'react';
import LOGO from '../../../../assets/images/profile-img.jpg'
import { NavLink } from "react-router-dom";
import { onLogout } from '../../../../routing/authService'
import {
  ADMIN_DASHBOARD, LIST_SUBSCRIPTION_PLANS,
  ADD_SUBSCRIPTION_PLAN, LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS,
  ADD_SUBSCRIPTION_PLAN_HIGHLIGHTS, WEBSITE_CMS_PAGE,
  CMS_PAGES_LIST, VIEW_CUSTOMERS, ADD_CUSTOMER,
  VIEW_EMPLOYEES, LOCATION_ADDRESS, BILL_TO_ADDRESS,
  ADD_ADD_ON, ADD_ONS_LIST, ADD_CMS_PAGE
} from "../../../../routing/routeContants";
import { userType } from '../../../../common/constants';
import { User } from '../../../../storage/index';
import SimpleCrypto from "simple-crypto-js";
import { getUserDetails } from '../../../../storage/user';
import _ from 'lodash'

export class Sidebar extends Component {
  constructor(props) {
    super(props)
    const decryptUserType = new SimpleCrypto("123654*/4514545454");
    let getUserType = decryptUserType.decrypt(User.getUserType())
    this.state = {
      getUserType,
      name: ''
    }
  }

  componentWillMount() {
    let userData = getUserDetails()
    if (userData && _.has(userData, 'name')) {
      this.setState({ name: userData.name })
    }
  }

  userLogout = () => {
    onLogout();
  }

  render() {
    const { getMainRoute, getInnerRoute } = this.props
    const { getUserType, name } = this.state

    return (
      <div className="sidebar sidebar-style-2">
        <div className="sidebar-wrapper scrollbar scrollbar-inner">
          <div className="sidebar-content">
            <div className="user">
              <div className="avatar-sm float-left mr-2">
                <img src={LOGO} alt="..." className="avatar-img rounded-circle" />
              </div>
              <div className="info">
                <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                  <span>
                    {name || ''}
                    <span className="user-level">{getUserType || ''}</span>
                    <span className="caret"></span>
                  </span>
                </a>
                <div className="clearfix"></div>

                <div className="collapse in" id="collapseExample">
                  <ul className="nav">
                    <li>
                      <span className="sideBarUpperMenu link-collapse">My Profile</span>
                    </li>
                    <li>
                      <span className="sideBarUpperMenu link-collapse">Edit Profile</span>
                    </li>
                    <li>
                      <span onClick={() => this.userLogout()} className="sideBarUpperMenu link-collapse">Logout</span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <ul className="nav nav-primary">
              <li className={getMainRoute === 'dashboard' ? 'nav-item active' : 'nav-item'}>
                <NavLink className="nav-link" to={ADMIN_DASHBOARD}>
                  <i className="fas fa-home"></i>
                  <p>Dashboard</p>
                </NavLink>
              </li>

              {getUserType === userType.SUPER_ADMIN ?
                <>
                  < li className={getMainRoute === 'subscription' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#subscription-plan">
                      <i className="fas fa-dollar-sign"></i>
                      <p>Subscription Plans</p>
                      <span className="caret"></span>
                    </a>
                    <div className={getMainRoute === 'subscription' ? 'collapse show' : 'collapse'} id="subscription-plan">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'list-subscription' ? 'active' : ''} >
                          <NavLink className="nav-link" to={LIST_SUBSCRIPTION_PLANS}>
                            <span className="sub-item">Subscription Plans</span>
                          </NavLink>
                        </li>
                        <li className={getInnerRoute === 'add-subscription' ? 'active' : ''}>
                          <NavLink className="nav-link" to={ADD_SUBSCRIPTION_PLAN}>
                            <span className="sub-item">Add Subscription Plan</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className={getMainRoute === 'highlights' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#highlights-plan">
                      <i className="fa fa-tasks"></i>
                      <p>Plan Highlights</p>
                      <span className="caret"></span>
                    </a>

                    <div className={getMainRoute === 'highlights' ? 'collapse show' : 'collapse'} id="highlights-plan">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'list-highlights' ? 'active' : ''} >
                          <NavLink className="nav-link" to={LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS}>
                            <span className="sub-item">Plan Highlights</span>
                          </NavLink>
                        </li>
                        <li className={(getInnerRoute === 'add-highlights' || getInnerRoute === 'edit-highlights') ? 'active' : ''}>
                          <NavLink className="nav-link" to={ADD_SUBSCRIPTION_PLAN_HIGHLIGHTS}>
                            <span className="sub-item">Add Plans Highlight </span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className={getMainRoute === 'add-on' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#add-on">
                      <i className="fa fa-plus-circle"></i>
                      <p>Subscription Add-Ons</p>
                      <span className="caret"></span>
                    </a>

                    <div className={getMainRoute === 'add-on' ? 'collapse show' : 'collapse'} id="add-on">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'list-add-ons' ? 'active' : ''} >
                          <NavLink className="nav-link" to={ADD_ONS_LIST}>
                            <span className="sub-item">List Add-Ons</span>
                          </NavLink>
                        </li>
                        <li className={getInnerRoute === 'add-add-on' ? 'active' : ''}>
                          <NavLink className="nav-link" to={ADD_ADD_ON}>
                            <span className="sub-item">Add Subscription Add-Ons</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className={getMainRoute === 'cms' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#page-manage">
                      <i className="fas fa-copy"></i>
                      <p>Page Management</p>
                      <span className="caret"></span>
                    </a>
                    <div className={getMainRoute === 'cms' ? 'collapse show' : 'collapse'} id="page-manage">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'website-cms-page' ? 'active' : ''}>
                          <NavLink to={WEBSITE_CMS_PAGE} >
                            <span className="sub-item">Default Website Page</span>
                          </NavLink>
                        </li>
                        <li className={(getInnerRoute === 'list-cms-pages' || getInnerRoute === "add-cms-page") ? 'active' : ''}>
                          <NavLink to={CMS_PAGES_LIST} >
                            <span className="sub-item">Manage Service Page</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                </>
                : ''}
              {getUserType === userType.SUPER_ADMIN || getUserType === userType.CONTRACTOR ?
                <>
                  <li className={getMainRoute === 'location' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#location">
                      <i className="fa fa-location-arrow"></i>
                      <p>Location</p>
                      <span className="caret"></span>
                    </a>
                    <div className={getMainRoute === 'location' ? 'collapse show' : 'collapse'} id="location">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'location-address' ? 'active' : ''}>
                          <NavLink to={LOCATION_ADDRESS}>
                            <span className="sub-item">Location Address</span>
                          </NavLink>
                        </li>
                        <li className={getInnerRoute === 'billing-address' ? 'active' : ''}>
                          <NavLink to={BILL_TO_ADDRESS} >
                            <span className="sub-item">Bill to Address</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                  <li className={getMainRoute === 'customers' ? 'nav-item active' : 'nav-item'}>
                    <a data-toggle="collapse" href="#customers">
                      <i className="fas fa-users"></i>
                      <p>Customers</p>
                      <span className="caret"></span>
                    </a>
                    <div className={getMainRoute === 'customers' ? 'collapse show' : 'collapse'} id="customers">
                      <ul className="nav nav-collapse">
                        <li className={getInnerRoute === 'add-customer' ? 'active' : ''}>
                          <NavLink to={ADD_CUSTOMER} >
                            <span className="sub-item">Add Customer</span>
                          </NavLink>
                        </li>
                        <li className={getInnerRoute === 'list-customers' ? 'active' : ''}>
                          <NavLink to={VIEW_CUSTOMERS} >
                            <span className="sub-item">View Customers</span>
                          </NavLink>
                        </li>
                      </ul>
                    </div>
                  </li>
                </>
                : ''}
              {/*
                <li className={getMainRoute === 'team' ? 'nav-item active' : 'nav-item'}>
                  <a data-toggle="collapse" href="#team">
                    <i className="fas fa-users"></i>
                    <p>Team Management</p>
                    <span className="caret"></span>
                  </a>
                  <div className={getMainRoute === 'team' ? 'collapse show' : 'collapse'} id="team">
                    <ul className="nav nav-collapse">
                      <li className={getInnerRoute === 'manage-customer' ? 'active' : ''}>
                      <NavLink to={ADD_CUSTOMER} >
                        <span className="sub-item">Manage em</span>
                      </NavLink>
                    </li> 
                      <li className={getInnerRoute === 'list-employees' ? 'active' : ''}>
                        <NavLink to={VIEW_EMPLOYEES} >
                          <span className="sub-item">Manage Employee</span>
                        </NavLink>
                      </li>
                    </ul>
                  </div>
                  </li> */}

              {/* <li className="nav-item">
                <a data-toggle="collapse" href="#location">
                  <i className="fas fa-th"></i>
                  <p>Location</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="location">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="service-location.html">
                        <span className="sub-item">Location Address</span>
                      </a>
                    </li>
                    <li>
                      <a href="bill-to-address.html">
                        <span className="sub-item">Bill to Address</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#contractor">
                  <i className="fas fa-ticket-alt"></i>
                  <p>Contractor</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="contractor">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="contractor-list.html">
                        <span className="sub-item">Contractor List</span>
                      </a>
                    </li>
                    <li>
                      <a href="contractors-sfmo.html">
                        <span className="sub-item">SFMO Contractors</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
               */}
              {/* <!-- <li className="nav-item"> -->
            <!-- <a href="building-representative.html"> -->
              <!-- <i className="fas fa-list-alt"></i> -->
              <!-- <p>Building Repersentative</p> -->
            <!-- </a> -->
          <!-- </li> --> */}
              {/* <li className="nav-item">
                <a href="vendor-management.html">
                  <i className="fas fa-file-alt"></i>
                  <p>Vendor Management</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="inspector.html">
                  <i className="fas fa-calendar-day"></i>
                  <p>Inspector</p>
                </a>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#fire-m">
                  <i className="fas fa-receipt"></i>
                  <p>Fire Marshals</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="fire-m">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="fire-marshal-list.html">
                        <span className="sub-item">Fire Marshal list</span>
                      </a>
                    </li>
                    <li>
                      <a href="marshal-pending-requests.html">
                        <span className="sub-item">Pending Request</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#reports">
                  <i className="fas fa-building"></i>
                  <p>Report</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="reports">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="report-pricing.html">
                        <span className="sub-item">Report List</span>
                      </a>
                    </li>
                    <li>
                      <a href="reports.html">
                        <span className="sub-item">Total Payment Report</span>
                      </a>
                    </li>
                    <li>
                      <a href="/">
                        <span className="sub-item">Global Report Designer</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
               */}
              {/* <!-- <li className="nav-item"> -->
            <!-- <a href="inquiry.html"> -->
              <!-- <i className="fas fa-users"></i> -->
              <!-- <p>Inquiry</p> -->
            <!-- </a> -->
          <!-- </li> --> */}
              {/* <li className="nav-item">
                <a href="quotes-list.html">
                  <i className="fas fa-file-invoice"></i>
                  <p>Quote</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="work-orders.html">
                  <i className="fas fa-envelope"></i>
                  <p>Work Order</p>
                </a>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#training-p">
                  <i className="fas fa-user-graduate"></i>
                  <p>Training Programs</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="training-p">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="training-categories.html">
                        <span className="sub-item">Categories</span>
                      </a>
                    </li>
                    <li>
                      <a href="training-program.html">
                        <span className="sub-item">Programs List</span>
                      </a>
                    </li>
                    <li>
                      <a href="manage-program.html">
                        <span className="sub-item">Manage Programs</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#projects">
                  <i className="fas fa-th"></i>
                  <p>Projects</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="projects">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="add-project.html">
                        <span className="sub-item">Add Project</span>
                      </a>
                    </li>
                    <li>
                      <a href="projects-view.html">
                        <span className="sub-item">View Project</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#invoice-account">
                  <i className="fas fa-file-invoice"></i>
                  <p>Accounting</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="invoice-account">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="invoice-dashboard.html">
                        <span className="sub-item">Invoices Dashboard</span>
                      </a>
                    </li>
                    <li>
                      <a href="invoice-payments.html">
                        <span className="sub-item">Invoice Payments</span>
                      </a>
                    </li>
                    <li>
                      <a href="receive-payments.html">
                        <span className="sub-item">Receive Payments</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a href="inventory-management.html">
                  <i className="fas fa-warehouse"></i>
                  <p>Inventory Management</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="purchase-orders.html">
                  <i className="fas fa-warehouse"></i>
                  <p>Purchase Orders</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="fleet-management.html">
                  <i className="fas fa-map-marked-alt"></i>
                  <p>Fleet Management</p>
                </a>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#team-management">
                  <i className="fas fa-users-cog"></i>
                  <p>Team Management</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="team-management">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="departments.html">
                        <span className="sub-item">Manage Departments</span>
                      </a>
                    </li>
                    <li>
                      <a href="employee.html">
                        <span className="sub-item">Manage Employee</span>
                      </a>
                    </li>
                    <li>
                      <a href="view-logs.html">
                        <span className="sub-item">View Logs</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a href="customers.html">
                  <i className="fas fa-users"></i>
                  <p>Customers</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="manage-permits.html">
                  <i className="fas fa-list-alt"></i>
                  <p>Manage Permits</p>
                </a>
              </li>
              <li className="nav-item">
                <a href="calendar.html">
                  <i className="fas fa-calendar-day"></i>
                  <p>Calendar</p>
                </a>
              </li>
              <li className="nav-item">
                <a data-toggle="collapse" href="#email">
                  <i className="fas fa-envelope"></i>
                  <p>Email</p>
                  <span className="caret"></span>
                </a>
                <div className="collapse" id="email">
                  <ul className="nav nav-collapse">
                    <li>
                      <a href="email-inbox.html">
                        <span className="sub-item">Inbox</span>
                      </a>
                    </li>
                    <li>
                      <a href="email-compose.html">
                        <span className="sub-item">Email Compose</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li className="nav-item">
                <a href="messages.html">
                  <i className="fas fa-comment-dots"></i>
                  <p>Message</p>
                </a>
              </li> */}
              {/* <!-- <li className="nav-item"> -->
            <!-- <a href="feedback.html"> -->
              <!-- <i className="fas fa-calendar-day"></i> -->
              <!-- <p>Feedback</p> -->
            <!-- </a> -->
          <!-- </li> -->
          <!-- <li className="nav-item"> -->
            <!-- <a href="miscellaneous.html"> -->
              <!-- <i className="fas fa-receipt"></i> -->
              <!-- <p>Miscellaneous</p> -->
            <!-- </a> -->
          <!-- </li> --> */}
            </ul>
          </div>
        </div>
      </div >
    );
  }
}