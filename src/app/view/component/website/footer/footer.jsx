import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import LOGO from '../../../../assets/images/brand/logo-itm.png';
import { Link } from "react-router-dom";
import { cmsPageByIdData } from "../../../../duck/website/website.selector"
import { cmsPageByType } from "../../../../duck/website/website.action"
import {SERVICE_BASE} from "../../../../routing/routeContants"
import _ from 'lodash';

export class FooterPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            cmsPageData : []
        }
    }

    componentDidMount(){
        this.props.cmsPageByType('Service')
    }

    componentWillReceiveProps(props) {
        if (props.cmsPageByIdData && _.has(props.cmsPageByIdData, 'data')) {
           this.setState({cmsPageData: props.cmsPageByIdData.data})
        }
    }

    render() {
        const {cmsPageData} = this.state;

        let allCmsTitle = _.map(cmsPageData, (sb, j) => {
            return <li key={j}><Link to={SERVICE_BASE+sb.slug} >{sb.pageTitle}</Link></li>
          })

        return (
            <footer className="footer bg-secondary">
                <div className="footer-divider">
                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="654 332.9 1920 128.1" style={{ 'enableBackground': 'new 654 332.9 1920 128.1' }} >
                        <image style={{ 'overflow': 'visible', 'enableBackground': 'new' }} width="1897" height="1695" id="XMLID_2_" transform="matrix(1.0139 0 0 1.0139 3128.2771 -574.5285)">
                        </image>
                        <polygon id="XMLID_3_" className="st0" points="812.8,1007.2 -500.2,1766.1 -1107.2,1670.1 -1107.2,1796.1 812.8,1796.1 " />
                        <polygon id="XMLID_18_" className="st0" points="579,-25.5 2146.6,102.6 2499,38.5 2499,-25.5 " />
                        <polygon id="XMLID_33_" className="st1" points="654,332.9 654,461 2574,461 2574,332.9 2185,456.1 " />
                    </svg>
                </div>
                <div className="container">
                    <div className="row row-grid align-items-center w-100">
                        <div className="col-lg-4">
                            <div className="footer-logo"><img src={LOGO} className="img-fluid" alt="" />
                                <p>© Copyright, 2019 ITM Tracking, LLC, All Rights Reserved.</p>
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <div className="form-row">
                                <div className="col-md-3">
                                    <div className="ftr-title">Website</div>
                                    <ul className="ftr-list list-unstyled mt-3">
                                        <li><Link to="/about-us" >About us</Link></li>
                                        <li><Link to="/benefits" >Benifits of ITM Tracking</Link></li>
                                        <li><Link to="how-it-works" >How does it Works?</Link></li>
                                        <li><Link to="life-safety" >Life Safety</Link></li>
                                        <li><Link to="/training-programs" >Training Programs</Link></li>
                                        <li><Link to="/contact-us" >Contact</Link></li>
                                    </ul>
                                </div>
                                <div className="col-md-4 pl-lg-3">
                                    <div className="ftr-title">Services</div>
                                    <ul className="ftr-list list-unstyled mt-3">
                                        {allCmsTitle}
                                    </ul>
                                </div>
                                <div className="col-md-5 p-md-0">
                                    <div className="ftr-title">Contact Us</div>
                                    <ul className="list-unstyled ftr-contact mt-3">
                                        <li className="py-2">
                                            <div className="d-flex align-items-center">
                                                <div className="mr-3">
                                                    <i className="fal fa-map-marker-alt"></i>
                                                </div>
                                                <Link to="" onClick={e => e.preventDefault()}>Houston, TX</Link>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-center">
                                                <div className="mr-3">
                                                    <i className="fal fa-phone"></i>
                                                </div>
                                                <Link to="" onClick={e => e.preventDefault()}>3704486 713</Link>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-center">
                                                <div className="mr-3">
                                                    <i className="fal fa-envelope"></i>
                                                </div>
                                                <Link to="" onClick={e => e.preventDefault()}>customerservice@itmtracking.com </Link>
                                            </div>
                                        </li>
                                        <li className="list-inline-item">
                                            <Link to="" onClick={e => e.preventDefault()} className="pr-3 border-right border-secondary py-0">FAQ</Link>
                                        </li>
                                        <li className="list-inline-item">
                                            <Link to="" onClick={e => e.preventDefault()}>Term & Conditions</Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

const mapStateToprops = (state) => ({
    cmsPageByIdData: cmsPageByIdData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    cmsPageByType: cmsPageByType
}, dispatch);

export const Footer = connect(mapStateToprops, mapDispatchToProps)(FooterPage); 