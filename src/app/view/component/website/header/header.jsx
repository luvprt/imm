import React, { Component } from 'react';
import LOGO from '../../../../assets/images/logo-itm.png';
import PROFILE_IMG from '../../../../assets/images/profile-img.jpg';
import { NavLink, Link } from "react-router-dom";
import { Loader } from '../../../component/admin/loader/loader';
import { HOME, HOW_IT_WORKS, BENEFITS, LIFE_SAFETY, TRAINING_PROGRAMS, CONTACT_US, ABOUT_US, ADMIN_LOGIN  } from '../../../../routing/routeContants'

export class Header extends Component {
	render() {
		return (
			<header className="header-global">
				<Loader loader={this.props.loader} />
				<nav id="navbar-main" className="navbar navbar-main navbar-expand-lg navbar-transparent navbar-light headroom">
					<div className="container">
						<Link to={HOME} className="navbar-brand order-2 order-lg-1">
							<img alt="..." src={LOGO} />
						</Link>
						<button className="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
							<span className="navbar-toggler-icon"></span>
						</button>
						<div className="navbar-collapse collapse order-lg-2" id="navbar_global">
							<div className="navbar-collapse-header">
								<div className="row">
									<div className="col-6 collapse-brand">
										<Link to={HOME}>
											<img alt="..." src={LOGO} />
										</Link>
									</div>
									<div className="col-6 collapse-close">
										<button type="button" className="navbar-toggler" data-toggle="collapse" data-target="#navbar_global" aria-controls="navbar_global" aria-expanded="false" aria-label="Toggle navigation">
											<span></span>
											<span></span>
										</button>
									</div>
								</div>
							</div>
							<ul className="navbar-nav navbar-nav-hover align-items-lg-center ml-lg-auto">
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={HOME}>
										<span className="nav-link-inner--text">Home</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={BENEFITS}>
										<span className="nav-link-inner--text">Benefits of ITM Tracking</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" role="button" to={HOW_IT_WORKS}>
										<span className="nav-link-inner--text">How it Works?</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={LIFE_SAFETY}>
										<span className="nav-link-inner--text">Life Safety</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={TRAINING_PROGRAMS}>
										<span className="nav-link-inner--text">Training program</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={ABOUT_US}>
										<span className="nav-link-inner--text">About Us</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={CONTACT_US}>
										<span className="nav-link-inner--text">Contact Us</span>
									</NavLink>
								</li>
								<li className="nav-item">
									<NavLink activeClassName="active" className="nav-link" to={HOME+'#sp'}>
										<span className="nav-link-inner--text">SIGNUP</span>
									</NavLink>
								</li>

								{/* <li className="nav-item dropdown profile d-none d-lg-block">
									<a href="#" className="nav-link" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<span className="nav-link-inner--text">
											<img className="img-fluid profile-img" src={PROFILE_IMG} alt="profile-img" />
										</span>
									</a>
									<div className="dropdown-menu">
										<small>Welcome</small>
										<h6 className="mb-1 main-color font-weight-bold">Ralph Broad</h6>
										<hr />
										<ul className="list-inline">
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"> <i className="fas fa-tachometer-alt-fast" /> Dashboard</a>
											</li>
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"> <i className="fas fa-user-circle" />Account Settings</a>
											</li>
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"><i className="fas fa-credit-card" />Payment Methods</a>
											</li>
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"><i className="fas fa-id-card" />Training Programs</a>
											</li>
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"><i className="fas fa-plus-square" />Upgrade Plan</a>
											</li>
											<li className="list-inline-item">
												<a href="#" className="nav-link" role="button"><i className="fas fa-sign-out-alt" />Log Out</a></li>
										</ul>
									</div>
								</li>
								 */}
								<li className="nav-item dropdown d-none d-lg-block">
									<Link to={ADMIN_LOGIN}  className="nav-link login-link" data-toggle="tooltip" data-placement="bottom" title="" data-container="body" data-animation="true" data-original-title="Log In">
										<span className="nav-link-inner--text"><i className="fas fa-sign-in-alt"></i></span>
									</Link>
								</li>
							</ul>
						</div>
						{/* <div className="nav-item dropdown profile d-block d-lg-none order-3">
							<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span className="nav-link-inner--text"> <img className="img-fluid profile-img" src={PROFILE_IMG} alt="profile-img" /></span>
							</Link>
							<div className="dropdown-menu">
								<small>Welcome</small>
								<h6 className="mb-1 main-color font-weight-bold">Ralph Broad</h6>
								<hr />
								<ul className="list-inline">
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"> <i className="fas fa-tachometer-alt-fast"></i> Dashboard</Link>
									</li>
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"> <i className="fas fa-user-circle"></i>Account Settings</Link>
									</li>
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"><i className="fas fa-credit-card"></i>Payment Methods</Link>
									</li>
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"><i className="fas fa-id-card"></i>Training Programs</Link>
									</li>
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"><i className="fas fa-plus-square"></i>Upgrade Plan</Link>
									</li>
									<li className="list-inline-item">
										<Link to="" onClick={e => e.preventDefault()} className="nav-link" role="button"><i className="fas fa-sign-out-alt"></i>Log Out</Link></li>
								</ul>
							</div>
						</div> */}
					</div>
				</nav>
			</header>
		);
	}
}