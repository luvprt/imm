import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { Link } from "react-router-dom";
import { Line } from 'react-chartjs-2';

export class BuildingRepresentativeDashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            lineChartData: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label: 'My First dataset',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(75,192,192,0.4)',
                        borderColor: 'rgba(75,192,192,1)',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: 'rgba(75,192,192,1)',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [65, 59, 80, 81, 56, 55, 40]
                    }
                ]
            }
        }

    }

    render() {
        const { wrapperCls, wrapperClsMobile, lineChartData } = this.state
        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'dashboard'} />

                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">Dashboard</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard</h5>
                                        </div>
                                        {/* <!-- <div className="ml-md-auto py-2 py-md-0"> -->
								<!-- <Link to="#" className="btn btn-white-border btn-round mr-2">Manage</Link> -->
								<!-- <Link to="#" className="btn btn-white-border btn-round">Add Customer</Link> -->
							<!-- </div> --> */}
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5 dashboard_panel">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card dashboard-sec">
                                            <div className="card-header">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">Report Statistics</div>
                                                    <div className="card-tools">
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm mr-2">
                                                            <span className="btn-label">
                                                                <i className="far fa-file-pdf"></i>
                                                            </span>
                                                            Export
											</Link>
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm">
                                                            <span className="btn-label">
                                                                <i className="fas fa-print"></i>
                                                            </span>
                                                            Print
											</Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <Line height={80} data={lineChartData} />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card full-height report-sec">
                                            <div className="card-header">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">Submitted Reports for Today</div>
                                                </div>
                                            </div>
                                            <div className="card-body overflow-hidden">
                                                <div className="pr-1">
                                                    <div className="overflow-scroll">
                                                        <table className="table w-100 report-list-info">
                                                            <tr className="report-row-block work-order shadow-sm">
                                                                <td>
                                                                    <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                                                    <div className="report-sec address pl-4 ml-2">
                                                                        <small className="d-block light-color">Building Address</small>
                                                                        <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection">
                                                                        <small className="d-block light-color">Type of Inspection</small>
                                                                        <p className="mb-0">Annual Fire Sprinkler</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection-date">
                                                                        <small className="d-block light-color">Inspection Date</small>
                                                                        <p className="mb-0">05-02-2019</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspector-name">
                                                                        <small className="d-block light-color">Inspection Name</small>
                                                                        <p className="mb-0">Chris Dyer</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec contractor-name">
                                                                        <small className="d-block light-color">Contractor Name</small>
                                                                        <p className="mb-0">Daniel Grey</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec authority">
                                                                        <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                                                        <p className="mb-0">Harris County</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec status">
                                                                        <small className="d-block light-color">Inspection Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">Current <span><i className="fas fa-file-pdf text-color-gradient"></i></span></p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec notification">
                                                                        <small className="d-block light-color">Impairment Notification Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">N/A </p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec action text-center">
                                                                        <small className="d-block light-color">Action</small>
                                                                        <div className="mb-0 dropdown">
                                                                            <Link to="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                                            </Link>
                                                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                                                <Link className="dropdown-item" to="#!"><i className="far fa-file-pdf"></i> Download PDF</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-file-signature"></i> Work Order</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-envelope"></i> Email</Link>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr className="report-row-block work-order shadow-sm">
                                                                <td>
                                                                    <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                                                    <div className="report-sec address pl-4 ml-2">
                                                                        <small className="d-block light-color">Building Address</small>
                                                                        <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection">
                                                                        <small className="d-block light-color">Type of Inspection</small>
                                                                        <p className="mb-0">Annual Fire Sprinkler</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection-date">
                                                                        <small className="d-block light-color">Inspection Date</small>
                                                                        <p className="mb-0">05-02-2019</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspector-name">
                                                                        <small className="d-block light-color">Inspection Name</small>
                                                                        <p className="mb-0">Chris Dyer</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec contractor-name">
                                                                        <small className="d-block light-color">Contractor Name</small>
                                                                        <p className="mb-0">Daniel Grey</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec authority">
                                                                        <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                                                        <p className="mb-0">Harris County</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec status">
                                                                        <small className="d-block light-color">Inspection Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">Current <span><i className="fas fa-file-pdf text-color-gradient"></i></span></p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec notification">
                                                                        <small className="d-block light-color">Impairment Notification Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">N/A </p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec action text-center">
                                                                        <small className="d-block light-color">Action</small>
                                                                        <div className="mb-0 dropdown">
                                                                            <Link to="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                                            </Link>
                                                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                                                <Link className="dropdown-item" to="#!"><i className="far fa-file-pdf"></i> Download PDF</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-file-signature"></i> Work Order</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-envelope"></i> Email</Link>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr className="report-row-block work-order shadow-sm">
                                                                <td>
                                                                    <div className="row-style bg-warning"><i className="fas fa-tag"></i></div>
                                                                    <div className="report-sec address pl-4 ml-2">
                                                                        <small className="d-block light-color">Building Address</small>
                                                                        <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection">
                                                                        <small className="d-block light-color">Type of Inspection</small>
                                                                        <p className="mb-0">Annual Fire Sprinkler</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection-date">
                                                                        <small className="d-block light-color">Inspection Date</small>
                                                                        <p className="mb-0">05-02-2019</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspector-name">
                                                                        <small className="d-block light-color">Inspection Name</small>
                                                                        <p className="mb-0">Chris Dyer</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec contractor-name">
                                                                        <small className="d-block light-color">Contractor Name</small>
                                                                        <p className="mb-0">Daniel Grey</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec authority">
                                                                        <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                                                        <p className="mb-0">Harris County</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec status">
                                                                        <small className="d-block light-color">Inspection Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">Current <span><i className="fas fa-file-pdf text-color-gradient"></i></span></p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec notification">
                                                                        <small className="d-block light-color">Impairment Notification Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">N/A </p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec action text-center">
                                                                        <small className="d-block light-color">Action</small>
                                                                        <div className="mb-0 dropdown">
                                                                            <Link to="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                                            </Link>
                                                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                                                <Link className="dropdown-item" to="#!"><i className="far fa-file-pdf"></i> Download PDF</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-file-signature"></i> Work Order</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-envelope"></i> Email</Link>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr className="report-row-block work-order shadow-sm">
                                                                <td>
                                                                    <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                                                    <div className="report-sec address pl-4 ml-2">
                                                                        <small className="d-block light-color">Building Address</small>
                                                                        <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection">
                                                                        <small className="d-block light-color">Type of Inspection</small>
                                                                        <p className="mb-0">Annual Fire Sprinkler</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection-date">
                                                                        <small className="d-block light-color">Inspection Date</small>
                                                                        <p className="mb-0">05-02-2019</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspector-name">
                                                                        <small className="d-block light-color">Inspection Name</small>
                                                                        <p className="mb-0">Chris Dyer</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec contractor-name">
                                                                        <small className="d-block light-color">Contractor Name</small>
                                                                        <p className="mb-0">Daniel Grey</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec authority">
                                                                        <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                                                        <p className="mb-0">Harris County</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec status">
                                                                        <small className="d-block light-color">Inspection Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">Current <span><i className="fas fa-file-pdf text-color-gradient"></i></span></p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec notification">
                                                                        <small className="d-block light-color">Impairment Notification Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">N/A </p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec action text-center">
                                                                        <small className="d-block light-color">Action</small>
                                                                        <div className="mb-0 dropdown">
                                                                            <Link to="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                                            </Link>
                                                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                                                <Link className="dropdown-item" to="#!"><i className="far fa-file-pdf"></i> Download PDF</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-file-signature"></i> Work Order</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-envelope"></i> Email</Link>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr className="report-row-block work-order shadow-sm">
                                                                <td>
                                                                    <div className="row-style bg-danger"><i className="fas fa-tag"></i></div>
                                                                    <div className="report-sec address pl-4 ml-2">
                                                                        <small className="d-block light-color">Building Address</small>
                                                                        <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection">
                                                                        <small className="d-block light-color">Type of Inspection</small>
                                                                        <p className="mb-0">Annual Fire Sprinkler</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspection-date">
                                                                        <small className="d-block light-color">Inspection Date</small>
                                                                        <p className="mb-0">05-02-2019</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec inspector-name">
                                                                        <small className="d-block light-color">Inspection Name</small>
                                                                        <p className="mb-0">Chris Dyer</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec contractor-name">
                                                                        <small className="d-block light-color">Contractor Name</small>
                                                                        <p className="mb-0">Daniel Grey</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec authority">
                                                                        <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                                                        <p className="mb-0">Harris County</p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec status">
                                                                        <small className="d-block light-color">Inspection Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">Current <span><i className="fas fa-file-pdf text-color-gradient"></i></span></p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec notification">
                                                                        <small className="d-block light-color">Impairment Notification Status</small>
                                                                        <p className="mb-0 d-flex justify-content-between">N/A </p>
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                    <div className="report-sec action text-center">
                                                                        <small className="d-block light-color">Action</small>
                                                                        <div className="mb-0 dropdown">
                                                                            <Link to="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                                            </Link>
                                                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                                                <Link className="dropdown-item" to="#!"><i className="far fa-file-pdf"></i> Download PDF</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-file-signature"></i> Work Order</Link>
                                                                                <Link className="dropdown-item" to="#!"><i className="fas fa-envelope"></i> Email</Link>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>

                                                        </table>


                                                    </div>
                                                </div>
                                                <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                                    <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                                    <nav aria-label="...">
                                                        <ul className="pagination mb-0">
                                                            <li className="page-item disabled"><Link className="page-link" to="#!" tabindex="-1">Previous</Link></li>
                                                            <li className="page-item"><Link className="page-link" to="#!">1</Link></li>
                                                            <li className="page-item active"><Link className="page-link" to="#!">2 <span className="sr-only">(current)</span></Link></li>
                                                            <li className="page-item"><Link className="page-link" to="#!">3</Link></li>
                                                            <li className="page-item"><Link className="page-link" to="#!">Next</Link></li>
                                                        </ul>
                                                    </nav>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </>
        );
    }
}
