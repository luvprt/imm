import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { Link } from "react-router-dom";
import { User } from '../../../../storage/index'
import SimpleCrypto from 'simple-crypto-js'
import { userType } from '../../../../common/constants'
import { ContractorDashboardComponent } from './admin.contractorDashboard'
import { BuildingRepresentativeDashboard } from './admin.buildingRepresentativeDashboard'
import { Line, Pie, Bar } from 'react-chartjs-2';


class AdminDashboard extends React.Component {
  constructor(props) {
    super(props);
    const decryptUserType = new SimpleCrypto("123654*/4514545454");
    let getUserType = decryptUserType.decrypt(User.getUserType())
    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
      getUserType,
      lineChartData: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            fill: false,
            lineTension: 0.1,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40]
          }
        ]
      },
      pieChartData: {
        labels: [
          'Red',
          'Blue',
          'Yellow'
        ],
        datasets: [{
          data: [300, 50, 100],
          backgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ],
          hoverBackgroundColor: [
            '#FF6384',
            '#36A2EB',
            '#FFCE56'
          ]
        }]
      },
      barChartData: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [
          {
            label: 'My First dataset',
            backgroundColor: 'rgba(255,99,132,0.2)',
            borderColor: 'rgba(255,99,132,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(255,99,132,0.4)',
            hoverBorderColor: 'rgba(255,99,132,1)',
            data: [65, 59, 80, 81, 56, 55, 40]
          }
        ]
      }
    }

  }

  render() {
    const { wrapperCls, wrapperClsMobile, getUserType, lineChartData, pieChartData, barChartData } = this.state
    return (
      <>
        <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
          <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
          <Sidebar getMainRoute={'dashboard'} />

          {getUserType === userType.SUPER_ADMIN ?
            <div className="main-panel">
              <div className="content">
                <div className="panel-header bg-primary-gradient">
                  <div className="page-inner py-5">
                    <div className="d-flex align-items-left align-items-md-center flex-column flex-sm-row">
                      <div className="pr-3">
                        <h2 className="text-white pb-2 fw-bold">Dashboard</h2>
                        <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</h5>
                      </div>
                      <div className="ml-md-auto py-2 py-md-0">
                        <a href="#setting-btn" className="btn-lg btn-white-border btn-round mr-2 dropdown-item" data-toggle="modal" data-target="#settings" ><i className="fas fa-cog mr-2"></i> Settings</a>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="page-inner mt--5 dashboard_panel">
                  <div className="row">
                    <div className="col-md-6 mb-4">
                      <div className="card h-100 mb-2">
                        <div className="card-header">
                          <div className="card-head-row">
                            <div className="card-title">Latest Added Customer(s)</div>
                            <div className="card-tools">
                              <Link to="" onClick={e => e.preventDefault()} className="btn btn-info btn-border btn-round btn-sm mr-2">
                                View All
											      </Link>
                            </div>
                          </div>
                        </div>
                        <div className="card-body management-list">
                          <div className="pr-2 overflow-scroll">
                            <table className="table mb-3">
                              <thead className="thead-light">
                                <tr>
                                  <th scope="col">Name</th>
                                  <th scope="col">Email ID</th>
                                  <th className="text-center" scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Building Repersentative</td>
                                  <td>abc99@xyz.com</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Customer Dummy 1</td>
                                  <td>abc99@xyz.com</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Customer Dummy 2</td>
                                  <td>abc99@xyz.com</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Customer Dummy 3</td>
                                  <td>abc99@xyz.com</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Customer Dummy 4</td>
                                  <td>abc99@xyz.com</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 mb-4">
                      <div className="card h-100 mb-2">
                        <div className="card-header">
                          <div className="card-head-row">
                            <div className="card-title">Recent Quotes</div>
                            <div className="card-tools">
                              <Link to="" onClick={e => e.preventDefault()} className="btn btn-info btn-border btn-round btn-sm mr-2">
                                View All
											</Link>
                            </div>
                          </div>
                        </div>
                        <div className="card-body management-list">
                          <div className="pr-2 overflow-scroll">
                            <table className="table mb-3">
                              <thead className="thead-light">
                                <tr>
                                  <th scope="col">Company Name</th>
                                  <th scope="col">Report Name</th>
                                  <th scope="col">Total Amount</th>
                                  <th className="text-center" scope="col">Action</th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                  <td>Firepro Tech, Lic</td>
                                  <td>Annual Fire Alarm</td>
                                  <td>$1,395.00</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Firepro Tech, Lic</td>
                                  <td>Annual Fire Alarm</td>
                                  <td>$1,395.00</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Firepro Tech, Lic</td>
                                  <td>Annual Fire Alarm</td>
                                  <td>$1,395.00</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Firepro Tech, Lic</td>
                                  <td>Annual Fire Alarm</td>
                                  <td>$1,395.00</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" ><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Firepro Tech, Lic</td>
                                  <td>Annual Fire Alarm</td>
                                  <td>$1,395.00</td>
                                  <td>
                                    <div className="mb-0 dropdown text-center">
                                      <Link to="" onClick={e => e.preventDefault()} className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i className="fas fa-ellipsis-v "></i>
                                      </Link>
                                      <div className="dropdown-menu" aria-labelledby="dropdown-invite-list" x-placement="bottom-start" >
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-edit"></i>View &amp; Edit</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item"><i className="far fa-comment"></i>Message</Link>
                                        <Link to="" onClick={e => e.preventDefault()} className="dropdown-item" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</Link>
                                      </div>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-lg-4 mb-4">
                      <div className="card h-100 mb-2">
                        <div className="card-header">
                          <div className="card-head-row justify-content-between align-items-center">
                            <div className="card-title">Inquiry</div>
                            <div className="nav-item dropdown bg-light">
                              <Link to="" onClick={e => e.preventDefault()} className="nav-link dropdown-toggle text-dark" id="invoice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                This Month
											</Link>
                              <div className="dropdown-menu" aria-labelledby="invoice">
                                <Link to="" className="dropdown-item">Action</Link>
                                <Link to="" className="dropdown-item">Another action</Link>
                                <Link to="" className="dropdown-item">Something else here</Link>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="pull-in sparkline-fix">
                            <div className="chart-container">
                              <Pie height={260} data={pieChartData} />

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 mb-4">
                      <div className="card h-100 mb-2">
                        <div className="card-header">
                          <div className="card-head-row">
                            <div className="card-title">Work Order Status</div>
                            <div className="card-tools">
                              <Link to="" onClick={e => e.preventDefault()} className="btn btn-info btn-border btn-round btn-sm mr-2">
                                <span className="btn-label"><i className="far fa-file-pdf"></i></span>
                                Export
											        </Link>
                              <Link to="" onClick={e => e.preventDefault()} className="btn btn-info btn-border btn-round btn-sm">
                                <span className="btn-label"><i className="fas fa-print"></i></span>
                                Print
											        </Link>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="chart-container">
                            <Line height={330} data={lineChartData} />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-4 mb-4">
                      <div className="card h-100 mb-2">
                        <div className="card-header">
                          <div className="card-head-row justify-content-between align-items-center">
                            <div className="card-title">Statistics</div>
                            <div className="nav-item dropdown bg-light">
                              <Link to="" onClick={e => e.preventDefault()} className="nav-link dropdown-toggle text-dark" id="invoice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                This Month
										        	</Link>
                              <div className="dropdown-menu" aria-labelledby="invoice">
                                <Link to="" onClick={e => e.preventDefault()} className="dropdown-item">Action</Link>
                                <Link to="" onClick={e => e.preventDefault()} className="dropdown-item">Another action</Link>
                                <Link to="" onClick={e => e.preventDefault()} className="dropdown-item">Something else here</Link>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="card-body">
                          <div className="pull-in sparkline-fix">
                            <div className="chart-container">
                              <Bar
                                data={barChartData}
                                // width={100}
                                height={300}
                              // options={{
                              //     maintainAspectRatio: false
                              // }}
                              />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
            : ''}
          {getUserType === userType.CONTRACTOR ?
            <ContractorDashboardComponent />
            : ''}
          {getUserType === userType.BUILDING_REPRESENTATIVE ?
            <BuildingRepresentativeDashboard />
            : ''}

        </div>
      </>
    );
  }
}
export default AdminDashboard