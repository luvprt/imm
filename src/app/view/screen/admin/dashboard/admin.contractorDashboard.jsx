import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { Link } from "react-router-dom";
import { Line, Pie, Bar, Doughnut, Radar } from 'react-chartjs-2';


export class ContractorDashboardComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            lineChartData: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label: 'My First dataset',
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: 'rgba(75,192,192,0.4)',
                        borderColor: 'rgba(75,192,192,1)',
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: 'rgba(75,192,192,1)',
                        pointBackgroundColor: '#fff',
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: 'rgba(75,192,192,1)',
                        pointHoverBorderColor: 'rgba(220,220,220,1)',
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        data: [65, 59, 80, 81, 56, 55, 40]
                    }
                ]
            },
            pieChartData: {
                labels: [
                    'Red',
                    'Blue',
                    'Yellow'
                ],
                datasets: [{
                    data: [300, 50, 100],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56'
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56'
                    ]
                }]
            },
            barChartData: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [
                    {
                        label: 'My First dataset',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: [65, 59, 80, 81, 56, 55, 40]
                    }
                ]
            },
            doughnutChartData: {
                labels: [
                    'Red',
                    'Green',
                    'Yellow'
                ],
                datasets: [{
                    data: [300, 50, 100],
                    backgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56'
                    ],
                    hoverBackgroundColor: [
                        '#FF6384',
                        '#36A2EB',
                        '#FFCE56'
                    ]
                }]
            },
            radarChartData: {
                labels: ['Eating', 'Drinking', 'Sleeping', 'Designing', 'Coding', 'Cycling', 'Running'],
                datasets: [
                    {
                        label: 'My First dataset',
                        backgroundColor: 'rgba(179,181,198,0.2)',
                        borderColor: 'rgba(179,181,198,1)',
                        pointBackgroundColor: 'rgba(179,181,198,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(179,181,198,1)',
                        data: [65, 59, 90, 81, 56, 55, 40]
                    },
                    {
                        label: 'My Second dataset',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        pointBackgroundColor: 'rgba(255,99,132,1)',
                        pointBorderColor: '#fff',
                        pointHoverBackgroundColor: '#fff',
                        pointHoverBorderColor: 'rgba(255,99,132,1)',
                        data: [28, 48, 40, 19, 96, 27, 100]
                    }
                ]
            }
        }

    }

    render() {
        const { wrapperCls, wrapperClsMobile, lineChartData,
            pieChartData, barChartData, doughnutChartData, radarChartData } = this.state
        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'dashboard'} />

                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">Dashboard</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</h5>
                                        </div>
                                        <div className="ml-md-auto py-2 py-md-0">
                                            <a to="#setting-btn" className="btn-lg btn-white-border btn-round mr-2 dropdown-item" data-toggle="modal" data-target="#settings" ><i className="fas fa-cog mr-2"></i> Settings</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5 dashboard_panel">
                                <div className="row">
                                    <div className="col-md-6 mb-4">
                                        <div className="card h-100 mb-2">
                                            <div className="card-header">
                                                <div className="card-head-row">
                                                    <div className="card-title">Report Statistics</div>
                                                    <div className="card-tools">
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm mr-2">
                                                            <span className="btn-label"><i className="far fa-file-pdf"></i></span>
                                                            Export
											            </Link>
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm">
                                                            <span className="btn-label"><i className="fas fa-print"></i></span>
                                                            Print
											            </Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <Line data={lineChartData} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 mb-4">
                                        <div className="card h-100 mb-2">
                                            <div className="card-header">
                                                <div className="card-head-row">
                                                    <div className="card-title">Sales Statistics</div>
                                                    <div className="card-tools">
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm mr-2">
                                                            <span className="btn-label"><i className="far fa-file-pdf"></i></span>
                                                            Export
											            </Link>
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm">
                                                            <span className="btn-label"><i className="fas fa-print"></i></span>
                                                            Print
											            </Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <Bar
                                                    data={barChartData}
                                                // width={100}
                                                // height={50}
                                                // options={{
                                                //     maintainAspectRatio: false
                                                // }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-lg-4 mb-4">
                                        <div className="card h-100 mb-2">
                                            <div className="card-header">
                                                <div className="card-head-row justify-content-between align-items-center">
                                                    <div className="card-title">Invoices</div>
                                                    <div className="nav-item dropdown bg-light">
                                                        <Link className="nav-link dropdown-toggle text-dark" to="#" id="invoice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            This Month
											            </Link>
                                                        <div className="dropdown-menu" aria-labelledby="invoice">
                                                            <Link className="dropdown-item" to="#">Action</Link>
                                                            <Link className="dropdown-item" to="#">Another action</Link>
                                                            <Link className="dropdown-item" to="#">Something else here</Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="pull-in sparkline-fix">
                                                    <div className="chart-container">
                                                        <Pie height={260} data={pieChartData} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 mb-4">
                                        <div className="card h-100 mb-2">
                                            <div className="card-header">
                                                <div className="card-head-row">
                                                    <div className="card-title">Work Status</div>
                                                    <div className="card-tools">
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm mr-2">
                                                            <span className="btn-label"><i className="far fa-file-pdf"></i></span>
                                                            Export
											            </Link>
                                                        <Link to="#" className="btn btn-info btn-border btn-round btn-sm">
                                                            <span className="btn-label"><i className="fas fa-print"></i></span>
                                                            Print
											            </Link>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="chart-container">
                                                    <Doughnut height={330} data={doughnutChartData} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-lg-4 mb-4">
                                        <div className="card h-100 mb-2">
                                            <div className="card-header">
                                                <div className="card-head-row justify-content-between align-items-center">
                                                    <div className="card-title">Statistics</div>
                                                    <div className="nav-item dropdown bg-light">
                                                        <Link className="nav-link dropdown-toggle text-dark" to="#" id="invoice" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            This Month
											            </Link>
                                                        <div className="dropdown-menu" aria-labelledby="invoice">
                                                            <Link className="dropdown-item" to="#">Action</Link>
                                                            <Link className="dropdown-item" to="#">Another action</Link>
                                                            <Link className="dropdown-item" to="#">Something else here</Link>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card-body">
                                                <div className="pull-in sparkline-fix">
                                                    <div className="chart-container">
                                                        <Radar height={280} data={radarChartData} />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col-sm-6 col-lg-3">
                                        <div className="card full-height">
                                            <div className="card-body progress-card">
                                                <div className="progress-status">
                                                    <span className="h3 text-muted font-weight-bold mb-0">New Orders</span>
                                                    <span className="h3 fw-bold text-primary mb-0"> $70</span>
                                                </div>
                                                <p>All Customers Value</p>
                                                <div className="progress" style={{height:'6px'}}>
                                                    <div className="progress-bar bg-primary" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="" data-original-title="70%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-lg-3">
                                        <div className="card full-height">
                                            <div className="card-body progress-card">
                                                <div className="progress-status">
                                                    <span className="h3 text-muted font-weight-bold mb-0">New Orders</span>
                                                    <span className="h3 fw-bold text-danger mb-0"> $70</span>
                                                </div>
                                                <p>All Customers Value</p>
                                                <div className="progress" style={{height:'6px'}}>
                                                    <div className="progress-bar bg-danger" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="" data-original-title="70%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-lg-3">
                                        <div className="card full-height">
                                            <div className="card-body progress-card">
                                                <div className="progress-status">
                                                    <span className="h3 text-muted font-weight-bold mb-0">New Orders</span>
                                                    <span className="h3 fw-bold text-info mb-0"> $70</span>
                                                </div>
                                                <p>All Customers Value</p>
                                                <div className="progress" style={{height:'6px'}}>
                                                    <div className="progress-bar bg-info" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="" data-original-title="70%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6 col-lg-3">
                                        <div className="card full-height">
                                            <div className="card-body progress-card">
                                                <div className="progress-status">
                                                    <span className="h3 text-muted font-weight-bold mb-0">New Orders</span>
                                                    <span className="h3 fw-bold text-warning mb-0"> $70</span>
                                                </div>
                                                <p>All Customers Value</p>
                                                <div className="progress" style={{height:'6px'}}>
                                                    <div className="progress-bar bg-warning" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="top" title="" data-original-title="70%"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
