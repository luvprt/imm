import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { listSubscriptionPlanHighlight, getAllUserPlanTypes, deleteSubscriptionPlanHighLight } from '../../../../duck/subscription-plan-highlight/highlight.action'
import { getSavedHighlight, getAllUserPlanTypesData } from '../../../../duck/subscription-plan-highlight/highlight.selector'
import { Link, NavLink } from "react-router-dom";
import { constants } from '../../../../common/constants'
import Swal from 'sweetalert2'
import _ from 'lodash';
import { EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS_BASE } from '../../../../routing/routeContants'

class ListSubscriptionPlansHighlights extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userType: [],
            planType: [],
            wrapperCls: false,
            wrapperClsMobile: false,
            planHighlight: [],
            userTypeId: '',
            planTypeId: '',
            pageNo: 1,
            limit: constants.PAGE_LIMIT,
            totalRecords: 0,
            loader: false,
            sortingField: '',
            sortingOrder: ''
        }
        this.props.getAllUserPlanTypes();
    }

    componentWillReceiveProps(props) {
        if (props.getAllUserPlanTypesData) {
            const getPlanTypes = _.map(props.getAllUserPlanTypesData.planTypes, (ad) => {
                return { id: ad._id, userTypeId: ad.userType._id, name: ad.name }
            });
            const getUserTypes = _.map(props.getAllUserPlanTypesData.userTypes, (ad) => {
                return { id: ad._id, name: ad.name }
            });

            this.setState({
                userType: getUserTypes,
                planType: (this.state.userTypeId !== '') ? _.filter(getPlanTypes, (dd) => ((this.state.userTypeId !== '') ? this.state.userTypeId : getUserTypes[0].id) === dd.userTypeId ? dd : null) : _.filter(getPlanTypes, (dd) => dd),
                setAllPlantypes: getPlanTypes,
                loader: false
            });
        }

        if (props.planHighlight && _.has(props.planHighlight, 'data')) {
            this.setState({
                planHighlight: props.planHighlight.data,
                totalRecords: (props.planHighlight && props.planHighlight.count ? props.planHighlight.count : 0),
                loader: false
            })
        } else if (props.planHighlight === 'Deleted') {
            this.setState({ pageNo: 1, userTypeId: '', planTypeId: '', loader: false })
            this.props.listSubscriptionPlanHighlight({ offset: 1, limit: constants.PAGE_LIMIT })
        }
    }

    setPlanTypeOptionsOnChange = (getVal) => {
        const getPlanTypes = _.filter(this.state.setAllPlantypes, (dd) => getVal === dd.userTypeId ? dd : null);
        this.setState({ planType: getPlanTypes, userTypeId: getVal, planTypeId: '' });
    }

    componentDidMount() {
        const { pageNo, limit } = this.state
        this.setState({ loader: true })
        this.props.listSubscriptionPlanHighlight({ offset: pageNo, limit })
    }

    getPageData(pageNo) {
        const { limit, planTypeId, userTypeId, sortingField, sortingOrder } = this.state
        this.setState({ pageNo, loader: true })
        this.props.listSubscriptionPlanHighlight({
            offset: pageNo, limit,
            planTypeId, userTypeId,
            sortingField, sortingOrder
        })
    }

    getSearchData = (e) => {
        e.preventDefault()
        // initialise the search for first page
        this.setState({ pageNo: 1, loader: true })
        this.props.listSubscriptionPlanHighlight({
            offset: 1,
            limit: constants.PAGE_LIMIT,
            planTypeId: this.state.planTypeId,
            userTypeId: this.state.userTypeId
        })
    }

    clearSearchData = (e) => {
        e.preventDefault()
        this.setState({
            pageNo: 1, userTypeId: '', sortingField: '',
            planTypeId: '', sortingOrder: '', loader: true
        })
        this.props.listSubscriptionPlanHighlight({ offset: 1, limit: constants.PAGE_LIMIT })
    }

    sortTheData = (field, order) => {
        this.setState({ pageNo: 1, loader: true, sortingOrder: order, sortingField: field })
        this.props.listSubscriptionPlanHighlight({
            offset: 1, limit: constants.PAGE_LIMIT,
            sortingField: field,
            sortingOrder: order
        })
    }

    deleteSubscriptionPlanHighLight = (e, id) => {
        e.preventDefault()
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this Highlights of the Subscription Plan!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.setState({ loader: true })
                if (id) {
                    this.setState({ loader: true })
                    this.props.deleteSubscriptionPlanHighLight(id)
                }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // console.log('cancel')
            }
        })

    }

    render() {
        const { loader, wrapperClsMobile, wrapperCls, planHighlight,
            userTypeId, planTypeId, sortingOrder,
            totalRecords, limit, pageNo, userType, planType } = this.state

        let userTypeOptions = _.map(userType, (ut, i) => {
            return <option key={i} value={ut.id}>{ut.name}</option>
        })

        let planTypeOptions = _.map(planType, (pt, j) => {
            return <option key={j} value={pt.id}>{pt.name}</option>
        })

        let getTableData = '';
        if (planHighlight && planHighlight.length > 0) {
            getTableData = _.map(planHighlight, (sp, i) => {
                return <tr key={i}>
                    <td>{sp.userData.name}</td>
                    <td>
                        {sp.planData.name}
                    </td>
                    {/* <td>
                        {sp.description}
                    </td> */}
                    <td>
                        <div className="mb-0 dropdown text-center">
                            <Link to="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="fas fa-ellipsis-v"></i>
                            </Link>
                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                <NavLink className="dropdown-item" to={EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS_BASE + sp._id}>
                                    <i className="fas fa-edit"></i> Edit
                                </NavLink>
                                <NavLink onClick={(e) => this.deleteSubscriptionPlanHighLight(e, sp._id)} className="dropdown-item" to={'#'}>
                                    <i className="fas fa-trash"></i> Delete
                                </NavLink>
                            </div>
                        </div>
                    </td>
                </tr>;
            });
        }

        return (
            <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                <Sidebar getMainRoute={'highlights'} getInnerRoute={'list-highlights'} />

                <div className="main-panel">
                    <div className="content">
                        <div className="panel-header bg-primary-gradient">
                            <div className="page-inner py-5">
                                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                    <div>
                                        <h2 className="text-white pb-2 fw-bold">Manage Subscription Plans Highlights</h2>
                                        <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="page-inner mt--5">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="card assets-management-sec full-height">
                                        <div className="card-header d-flex justify-content-between align-items-center py-4">
                                            <div className="card-head-row">
                                                <div className="card-title font-weight-bold">Manage Subscription Plans Highlights</div>
                                            </div>
                                        </div>

                                        <div className="card-body management-list">

                                            <div className="pb-3">
                                                <div className="mb-4 work-orders-sec management-list">

                                                    <div className="search-info">
                                                        <div className="d-md-flex flex-wrap justify-content-between align-items-center">
                                                            <div className="form-group flex-1  pl-md-0">
                                                                <select value={userTypeId} onChange={(e) => this.setPlanTypeOptionsOnChange(e.target.value)} className="form-control search-basic bg-light">
                                                                    <option value=''>Select..</option>
                                                                    {userTypeOptions}
                                                                </select>
                                                            </div>
                                                            <div className="form-group flex-1">
                                                                <select value={planTypeId} onChange={(e) => this.setState({ planTypeId: e.target.value })} className="form-control search-basic bg-light">
                                                                    <option value=''>Select..</option>
                                                                    {planTypeOptions}
                                                                </select>
                                                            </div>

                                                            <div className="form-group">
                                                                <Link to="#" onClick={(e) => this.getSearchData(e)} className="btn-lg btn-primary btn-square text-white d-block text-center"> Search </Link>
                                                            </div>
                                                            <div className="form-group">
                                                                <Link to="#" onClick={(e) => this.clearSearchData(e)} className="btn-lg btn-dark btn-square text-white d-block text-center"> Clear Search </Link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="pr-2 overflow-scroll">
                                                        <table className="table mb-3">
                                                            <thead className="thead-light">
                                                                <tr>
                                                                    <th scope="col">User Type <span className="sortingArrows"><span onClick={() => this.sortTheData('userData.name', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('userData.name', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                                                                    <th scope="col">Plan Type</th>
                                                                    {/* <th scope="col">Description</th> */}
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {getTableData ? getTableData : <tr><td colSpan='6'>No Records Found. </td></tr>}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    {totalRecords ? <PaginationFooter planTypeId={planTypeId} userTypeId={userTypeId} getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}


const mapStateToprops = (state) => ({
    planHighlight: getSavedHighlight(state),
    getAllUserPlanTypesData: getAllUserPlanTypesData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    listSubscriptionPlanHighlight: listSubscriptionPlanHighlight,
    getAllUserPlanTypes: getAllUserPlanTypes,
    deleteSubscriptionPlanHighLight: deleteSubscriptionPlanHighLight
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ListSubscriptionPlansHighlights); 
