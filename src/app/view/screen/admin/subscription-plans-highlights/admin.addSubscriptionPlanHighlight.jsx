import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { addSubscriptionPlanHighlight, showSubscriptionDataHighlight, editSubscriptionPlanHighlight, getAllUserPlanTypes } from '../../../../duck/subscription-plan-highlight/highlight.action'
import { getSavedHighlight, getAllUserPlanTypesData } from '../../../../duck/subscription-plan-highlight/highlight.selector'
import { EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS } from '../../../../routing/routeContants'
import history from '../../../../routing/history'
import CKEditor from "react-ckeditor-component";
import { Link } from "react-router-dom";
import _ from 'lodash';
import { LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS } from '../../../../routing/routeContants';
import { fieldValidator } from '../../../../common/custom';

export class AddSubscriptionPlanHighlightComponent extends React.Component {
  constructor(props) {
    super(props);
    let highlightId = ''
    if (props.match && _.has(props.match, 'params') && _.has(props.match.params, 'id')) {
      highlightId = props.match.params.id
    }

    this.state = {
      userType: [],
      planType: [],
      wrapperCls: false,
      wrapperClsMobile: false,
      userTypeId: '',
      planTypeId: '',
      setAllPlantypes: [],
      planHighLights: '',
      description: '',
      planHighLightsErr: '',
      descriptionErr: '',
      error: '',
      highlightId,
      loader: true,
      testViaJest: props.testViaJest ? props.testViaJest : false
    }
    if (this.state.testViaJest === false) {
      this.props.getAllUserPlanTypes();
    }

  }

  onSubmit = () => {
    const { highlightId, description, planTypeId, planHighLights, userTypeId, testViaJest } = this.state
    let planHighLightsErr = '', descriptionErr = '', getError = false;

    if (planHighLights === '') {
      planHighLightsErr = 'Please enter plan highlight .';
      getError = true;
    }

    if (description === '') {
      descriptionErr = 'Please enter description .';
      getError = true;
    } else if (description.length < 5) {
      descriptionErr = 'Please enter minimum 5 characters';
      getError = true;
    }

    this.setState({ planHighLightsErr, descriptionErr, error: getError });

    if (getError === false && planHighLightsErr === '' && descriptionErr === '' && testViaJest === false) {
      this.setState({ loader: true })
      if (highlightId) {
        this.props.editSubscriptionPlanHighlight({
          description, planTypeId, highlightId: highlightId, planHighLights, userTypeId,
        })
      } else {
        this.props.addSubscriptionPlanHighlight({
          description, planTypeId, planHighLights, userTypeId,
        })
      }
    }

  }

  componentWillReceiveProps(props) {
    if (props.getAllUserPlanTypesData) {
      const getPlanTypes = _.map(props.getAllUserPlanTypesData.planTypes, (ad) => {
        return { id: ad._id, userTypeId: ad.userType._id, name: ad.name }
      });
      const getUserTypes = _.map(props.getAllUserPlanTypesData.userTypes, (ad) => {
        return { id: ad._id, name: ad.name }
      });

      this.setState({
        userType: getUserTypes,
        planType: _.filter(getPlanTypes, (dd) => getUserTypes[0].id === dd.userTypeId ? dd : null),
        setAllPlantypes: getPlanTypes,
        userTypeId: getUserTypes[0].id,
        planTypeId: getPlanTypes[0].id
      });
      if (props.match.path !== EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS) {
        this.setState({ loader: false });
      }
    }

    if (props.planHighlight && _.has(props.planHighlight, 'data._id')) {
      if (props.match.path === EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS) {
        this.setState({
          userTypeId: props.planHighlight.data.userTypeId,
          planTypeId: props.planHighlight.data.planTypeId,
          planHighLights: props.planHighlight.data.planHighLights,
          description: props.planHighlight.data.description,
          planType: _.filter(this.state.setAllPlantypes, (dd) => props.planHighlight.data.userTypeId === dd.userTypeId ? dd : null),
          loader: false
        });

      } else {
        this.setState({
          planHighLights: '',
          description: '',
          highlightId: '',
          loader: false
        });
      }
    }
    if (props.planHighlight && (props.planHighlight === 'Edited' || props.planHighlight === 'Added')) {
      this.setState({ loader: false })
      history.push(LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS)
    }
  }

  componentDidMount() {
    if (this.state.highlightId) {
      this.setState({ loader: true })
      setTimeout(() => {
        this.props.showSubscriptionDataHighlight(this.state.highlightId);
      }, 1000);
    }
  }

  setPlanTypeOptionsOnChange = (getVal) => {
    const getPlanTypes = _.filter(this.state.setAllPlantypes, (dd) => getVal === dd.userTypeId ? dd : null);
    this.setState({ planType: getPlanTypes, userTypeId: getVal, planTypeId: getPlanTypes[0].id });
  }

  onEditorChange = (evt) => {
    var newContent = evt.editor.getData();
    this.setState({
      planHighLights: newContent
    })
    this.checkValidation("planHighLights", newContent, 'required');
  }

  checkValidation = (field, value, type) => {
    let error = fieldValidator(field, value, type)
    this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
  }

  render() {
    const { wrapperClsMobile, highlightId, userTypeId, planHighLights, wrapperCls, loader,
      planHighLightsErr, descriptionErr, planTypeId, description, userType, planType } = this.state

    let userTypeOptions = _.map(userType, (ut, i) => {
      return <option key={i} value={ut.id}>{ut.name}</option>
    })

    let planTypeOptions = _.map(planType, (pt, j) => {
      return <option key={j} value={pt.id}>{pt.name}</option>
    })

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'highlights'} getInnerRoute={highlightId ? 'edit-highlights' : 'add-highlights'} />
        <div className="main-panel">
          <div className="content">
            <div className="panel-header bg-primary-gradient">
              <div className="page-inner py-5">
                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                  <div>
                    <h2 className="text-white pb-2 fw-bold">{highlightId ? 'Edit' : 'Add'} Subscription Plans Highlight</h2>
                    <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="page-inner mt--5">
              <div className="row">
                <div className="col-md-12">
                  <div className="card building-representative-sec full-height">
                    <div className="card-header d-flex justify-content-between align-items-center py-4">
                      <div className="card-head-row">
                        <div className="card-title font-weight-bold">{highlightId ? 'Edit' : 'Add'} Subscription Plans Highlight</div>
                      </div>
                    </div>
                    <div className="card-body management-list pt-0">
                      <div className="row">
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="w-100 text-primary">User Type*</label>
                            <select value={userTypeId} onChange={(e) => this.setPlanTypeOptionsOnChange(e.target.value)} className="form-control search-basic bg-light">
                              {userTypeOptions}
                            </select>
                          </div>
                        </div>
                        <div className="col-sm-6">
                          <div className="form-group">
                            <label className="w-100 text-primary">Plan Type*</label>
                            <div className="select2-input">
                              <select value={planTypeId} onChange={(e) => this.setState({ planTypeId: e.target.value })} className="form-control search-basic bg-light">
                                {planTypeOptions}
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="form-group">
                            <div className="d-flex justify-content-between align-items-center mb-2">
                              <label className="text-primary">Plan Highlights*</label>
                            </div>
                            <CKEditor
                              activeClass="p10"
                              content={planHighLights}
                              events={{
                                "change": this.onEditorChange,
                              }}
                            />
                            <span className="errorCls">{planHighLightsErr ? planHighLightsErr : ''}</span>
                          </div>
                        </div>
                        <div className="col-12">
                          <div className="form-group">
                            <label className="w-100 text-primary">Description*</label>
                            <textarea value={description} name="description" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'required') }} onChange={(e) => this.setState({ description: e.target.value })} className="form-control search- bg-light" />
                            <span className="errorCls">{descriptionErr ? descriptionErr : ''}</span>
                          </div>
                        </div>
                      </div>
                      <div className="text-center text-md-right w-100 my-3 px-2">
                        <Link to={LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS} className="btn btn-dark" data-dismiss="modal">Cancel</Link>
                        <button id="addPlanHighlights" type="button" onClick={() => this.onSubmit()} className="btn btn-primary">Save Highlight</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToprops = (state) => ({
  planHighlight: getSavedHighlight(state),
  getAllUserPlanTypesData: getAllUserPlanTypesData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addSubscriptionPlanHighlight: addSubscriptionPlanHighlight,
  showSubscriptionDataHighlight: showSubscriptionDataHighlight,
  editSubscriptionPlanHighlight: editSubscriptionPlanHighlight,
  getAllUserPlanTypes: getAllUserPlanTypes
}, dispatch);

export const AddSubscriptionPlanHighlight = connect(mapStateToprops, mapDispatchToProps)(AddSubscriptionPlanHighlightComponent); 