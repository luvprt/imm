import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header';
import { Sidebar } from '../../../component/admin/sidebar/sidebar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addAddOn, viewAddOn, editAddon } from '../../../../duck/add-on/addOn.action';
import { addOnsData } from '../../../../duck/add-on/addOn.selector'
import { EDIT_ADD_ON, ADD_ONS_LIST } from '../../../../routing/routeContants'
import history from '../../../../routing/history';
import { Link } from "react-router-dom";
import _ from 'lodash'
import { validateInputs } from '../../../../common/validation';
import { fieldValidator } from '../../../../common/custom';


export class AddAddOnsComponent extends React.Component {
    constructor(props) {
        super(props);
        let addOnId = ''
        if (props.match && _.has(props.match, 'params') && _.has(props.match.params, 'id')) {
            addOnId = props.match.params.id
        }
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            name: '',
            description: '',
            price: '',
            nameErr: '',
            descriptionErr: '',
            priceErr: '',
            addOnId,
            loader: false,
            testViaJest: props.testViaJest ? props.testViaJest : false
        }
    }

    componentWillReceiveProps(props) {

        if (props.addOn && _.has(props.addOn, 'data._id')) {
            if (props.match.path === EDIT_ADD_ON) {
                this.setState({
                    name: props.addOn.data.name,
                    description: props.addOn.data.description,
                    price: props.addOn.data.price,
                    loader: false
                });

            } else {
                this.setState({
                    name: '',
                    description: '',
                    price: '',
                    addOnId: ''
                });
            }
        } else if (props.addOn && (props.addOn === 'Edited' || props.addOn === 'Added')) {
            this.setState({ loader: false });
            history.push(ADD_ONS_LIST)
        } else if (props.addOn && _.has(props.addOn, 'message')) {
            this.setState({ loader: false });
            history.push(ADD_ONS_LIST)
        }
    }

    componentDidMount() {
        if (this.state.addOnId) {
            this.setState({ loader: true })
            this.props.viewAddOn(this.state.addOnId)
        }
    }

    onSubmit = () => {

        const { addOnId, name, description, price, testViaJest } = this.state

        let nameErr = '', descriptionErr = '', priceErr = '', getError = false;

        if (validateInputs('string', name) === 'empty') {
            nameErr = 'Please enter add-on name.';
            getError = true;
        } else if (validateInputs('string', name) === false) {
            nameErr = 'Please enter valid name.';
            getError = true;
        } else if (name.length < 3) {
            nameErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        if (validateInputs('string', description) === 'empty') {
            descriptionErr = 'Please enter description.';
            getError = true;
        } else if (validateInputs('string', description) === false) {
            descriptionErr = 'Please enter valid description.';
            getError = true;
        } else if (description.length < 3) {
            descriptionErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        if (validateInputs('number', price) === 'empty') {
            priceErr = 'Please enter add-on price.';
            getError = true;
        } else if (validateInputs('number', price) === false || parseInt(price, 10) === 0) {
            priceErr = 'Please enter valid number and not 0.';
            getError = true;
        } else if (price.length > 4) {
            priceErr = 'Please enter maximum 4 digits';
            getError = true;
        }

        this.setState({ nameErr, descriptionErr, priceErr, error: getError });

        if (nameErr === '' && descriptionErr === '' && priceErr === '' && getError === false && testViaJest === false) {
            this.setState({ loader: true })
            if (addOnId) {

                this.props.editAddon({
                    addOnId, name, description, price
                })
            } else {
                this.props.addAddOn({
                    name, description, price
                })
            }
        }
    }

    checkValidation = (field, value, type, maxLength, minLength) => {
        let error = fieldValidator(field, value, type, null, maxLength, minLength)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
    }

    render() {
        const { wrapperCls, addOnId, wrapperClsMobile, name, nameErr,
            description, descriptionErr, price, priceErr, loader
        } = this.state

        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'add-on'} getInnerRoute={'add-add-on'} />
                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">{addOnId ? 'Edit' : 'Add'} Add-On</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card building-representative-sec full-height">
                                            <div className="card-header d-flex justify-content-between align-items-center py-4">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">{addOnId ? 'Edit' : 'Add'} Add-On</div>
                                                </div>
                                            </div>
                                            <div className="card-body management-list pt-0">
                                                <div className="row">
                                                    <div className="col-sm-4">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Add-On Name*</label>
                                                            <input name="name" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string', null, 3) }} value={name} onChange={(e) => this.setState({ name: e.target.value })} className="form-control bg-light" type="text" placeholder="Add-On Name" />
                                                            <span className="errorCls">{nameErr ? nameErr : ''}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-4">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Price*</label>
                                                            <input name="price" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'number', 4, null) }} value={price} onChange={(e) => this.setState({ price: e.target.value })} className="form-control bg-light" type="text" placeholder="Price" />
                                                            <span className="errorCls">{priceErr ? priceErr : ''}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Description*</label>
                                                            <textarea name="description" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string', null, 3) }} value={description} onChange={(e) => this.setState({ description: e.target.value })} className="form-control search-basic bg-light" />
                                                            <span className="errorCls">{descriptionErr ? descriptionErr : ''}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="text-center text-md-right w-100 my-3 px-2">
                                                    <Link to={ADD_ONS_LIST} className="btn btn-dark" data-dismiss="modal">Cancel</Link>
                                                    <button id='addAddOnButtton' type="button" className="btn btn-primary" onClick={(e) => this.onSubmit()}>{addOnId ? 'Edit' : 'Add'} Add-On</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToprops = (state) => ({
    addOn: addOnsData(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addAddOn, viewAddOn, editAddon
}, dispatch);

export const AddAddOns = connect(mapStateToprops, mapDispatchToProps)(AddAddOnsComponent);


