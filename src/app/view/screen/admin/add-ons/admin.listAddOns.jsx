import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header';
import { Sidebar } from '../../../component/admin/sidebar/sidebar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { listAddOns, deleteAddOn } from '../../../../duck/add-on/addOn.action';
import { addOnsData } from '../../../../duck/add-on/addOn.selector'
import { constants } from '../../../../common/constants'
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { Link, NavLink } from "react-router-dom";
import Swal from 'sweetalert2'
import _ from 'lodash';
import { EDIT_ADD_ON_BASE, ADD_ADD_ON } from '../../../../routing/routeContants'

class ListAddOns extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            addOns: [],
            name: '',
            pageNo: 1,
            limit: constants.PAGE_LIMIT,
            totalRecords: 0,
            loader: true,
            sortingField: '',
            sortingOrder: ''
        }
    }

    componentWillReceiveProps(props) {
        if (props.addOns && _.has(props.addOns, 'data')) {
            this.setState({
                addOns: props.addOns.data,
                totalRecords: props.addOns.count ? props.addOns.count : 0,
                loader: false
            })
        } else if (props.addOns === 'Deleted') {
            this.setState({ pageNo: 1, name: '', loader: false })
            this.props.listAddOns({ offset: 1, limit: constants.PAGE_LIMIT })
        }
    }

    componentDidMount() {
        const { pageNo, limit } = this.state
        this.props.listAddOns({ offset: pageNo, limit })
    }

    getPageData = (pageNo) => {
        const { limit, sortingField, sortingOrder, name } = this.state
        this.setState({ pageNo, loader: true })
        this.props.listAddOns({
            offset: pageNo, limit,
            sortingField, name,
            sortingOrder
        })
    }

    getSearchData = (e) => {
        e.preventDefault();
        const { sortingField, sortingOrder, name } = this.state
        // initialise the search for first page
        this.setState({ pageNo: 1, loader: true })
        this.props.listAddOns({
            offset: 1, limit: constants.PAGE_LIMIT, name, sortingField, sortingOrder
        })
    }

    sortTheData = (field, order) => {
        this.setState({
            pageNo: 1, name: '', loader: true, sortingOrder: order, sortingField: field
        })
        this.props.listAddOns({
            offset: 1, limit: constants.PAGE_LIMIT, sortingField: field, sortingOrder: order
        })
    }

    clearSearchData = (e) => {
        e.preventDefault();
        this.setState({
            pageNo: 1, sortingField: '', sortingOrder: '', name: '', loader: true
        })
        this.props.listAddOns({ offset: 1, limit: constants.PAGE_LIMIT })
    }

    deleteAddOn = (e, id) => {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this add-ons!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.setState({ loader: true })
                if (id) {
                    this.props.deleteAddOn(id)
                }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // console.log('cancel')
            }
        })

    }

    render() {
        const { wrapperCls,
            wrapperClsMobile, name,
            addOns, totalRecords, limit,
            pageNo, loader, sortingOrder } = this.state
        let getTableData = '';
        if (addOns.length > 0) {
            getTableData = _.map(addOns, (add, i) => {
                console.log(EDIT_ADD_ON_BASE + add._id)
                return <tr key={i}>
                    <td>{add.name}</td>
                    <td>{add.price}</td>
                    <td>
                        <div className="mb-0 dropdown text-center">
                            <Link to="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="fas fa-ellipsis-v"></i>
                            </Link>
                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                <NavLink className="dropdown-item" to={EDIT_ADD_ON_BASE + add._id}>
                                    <i className="fas fa-edit"></i> Edit
                                </NavLink>
                                <NavLink onClick={(e) => this.deleteAddOn(e, add._id)} className="dropdown-item" to={'#'}>
                                    <i className="fas fa-trash"></i> Delete
                                </NavLink>
                            </div>
                        </div>
                    </td>
                </tr>;
            });
        }
        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'add-on'} getInnerRoute={'list-add-ons'} />
                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">Add-Ons Management</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card building-representative-sec full-height">
                                            <div className="card-header d-flex justify-content-between align-items-center py-4">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">Add-Ons</div>
                                                </div>
                                                <div className="search">
                                                    <NavLink to={ADD_ADD_ON} className="btn-lg btn-border btn-primary btn-round "><i className="far fa-plus-square mr-1"></i> Add Add-Ons</NavLink>
                                                </div>
                                            </div>
                                            <div className="search-info collapse show px-3">
                                                <div className="d-md-flex flex-wrap justify-content-between align-items-center">
                                                    <div className="form-group flex-1  pl-md-0">
                                                        <input value={name} onChange={(e) => this.setState({ name: e.target.value })} placeholder="Search Add-on Name" className="form-control search-basic" />
                                                    </div>

                                                    <div className="form-group">
                                                        <Link to="#" onClick={(e) => this.getSearchData(e)} className="btn-lg btn-primary btn-square text-white d-block text-center"> Search </Link>
                                                    </div>
                                                    <div className="form-group">
                                                        <Link to="#" onClick={(e) => this.clearSearchData(e)} className="btn-lg btn-dark btn-square text-white d-block text-center"> Clear Search </Link>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className="card-body management-list pt-0">
                                                <div className="pr-2 overflow-scroll">
                                                    <table className="table mb-3">
                                                        <thead className="thead-light">
                                                            <tr>
                                                                <th scope="col">Name <span className="sortingArrows"><span onClick={() => this.sortTheData('name', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('name', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                                                                <th scope="col">Price</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {getTableData ? getTableData : <tr><td colSpan='6'>No Records Found. </td></tr>}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                {totalRecords ? <PaginationFooter getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}


const mapStateToprops = (state) => ({
    addOns: addOnsData(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    listAddOns, deleteAddOn
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ListAddOns); 
