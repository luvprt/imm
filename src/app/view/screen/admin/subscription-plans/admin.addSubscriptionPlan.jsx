import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { addSubscriptionPlan, showSubscriptionData, editSubscriptionPlan } from '../../../../duck/subscription-plan/subscription.action'
import { getAddOns } from '../../../../duck/add-on/addOn.action'
import { getAllAddOns, getSubscriptionPlan } from '../../../../duck/add-on/addOn.selector'
import { EDIT_SUBSCRIPTION_PLAN } from '../../../../routing/routeContants'
import history from '../../../../routing/history'
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { Link } from "react-router-dom";
import _ from 'lodash'
import { validateInputs } from '../../../../common/validation';
import { LIST_SUBSCRIPTION_PLANS } from '../../../../routing/routeContants';
import { fieldValidator } from '../../../../common/custom';

export class AddSubscriptionPlanComponent extends React.Component {
  constructor(props) {
    super(props);
    let subscriptionId = ''
    if (props.match && _.has(props.match, 'params') && _.has(props.match.params, 'id')) {
      subscriptionId = props.match.params.id
    }

    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
      userType: [],
      userTypeVal: '',
      planPrice: '',
      planType: [],
      planTypeVal: '',
      noOfPersons: '',
      planDurations: [],
      planDurationsVal: '',
      addOns: [],
      onlyToSetAddOns: [],
      getAllAddOns: [],
      setAllPlantypes: [],
      isUnlimited: false,
      userTypeErr: '',
      planPriceErr: '',
      planTypeErr: '',
      noOfPersonsErr: '',
      planDurationsErr: '',
      addOnsErr: '',
      animatedComponents: makeAnimated(),
      subscriptionId,
      loader: false,
      testViaJest: props.testViaJest ? props.testViaJest : false
    }
    if (this.state.testViaJest === false) {
      this.props.getAllAddOns()
    }

  }

  onSubmit = () => {
    const { subscriptionId, addOns,
      planTypeVal, noOfPersons, testViaJest,
      planDurationsVal, userTypeVal, planPrice, isUnlimited } = this.state

    let planPriceErr = '', noOfPersonsErr = '', getError = false;

    if (validateInputs('number', planPrice) === 'empty') {
      planPriceErr = 'Please enter plan price.';
      getError = true;
    } else if (validateInputs('number', planPrice) === false || parseInt(planPrice, 10) === 0) {
      planPriceErr = 'Please enter valid number and not 0.';
      getError = true;
    } else if (planPrice.length > 4) {
      planPriceErr = 'Please enter maximum 4 digits';
      getError = true;
    }

    if (isUnlimited === false) {
      if (validateInputs('number', noOfPersons) === 'empty') {
        noOfPersonsErr = 'Please enter number of persons.';
        getError = true;
      } else if (validateInputs('number', noOfPersons) === false || parseInt(noOfPersons, 10) === 0) {
        noOfPersonsErr = 'Please enter valid number and not 0.';
        getError = true;
      } else if (noOfPersons.length > 3) {
        noOfPersonsErr = 'Please enter maximum 3 digits';
        getError = true;
      }
    }

    this.setState({ planPriceErr, noOfPersonsErr, error: getError });

    if (getError === false && planPriceErr === '' && (noOfPersonsErr === '' || isUnlimited === true) && testViaJest === false) {
      this.setState({ loader: true })
      if (subscriptionId) {
        this.props.editSubscriptionPlan({
          addOns, planType: planTypeVal, planId: subscriptionId,
          numberOfPersons: noOfPersons, duration: planDurationsVal,
          userType: userTypeVal, price: planPrice, isUnlimited
        })
      } else {
        this.props.addSubscriptionPlan({
          addOns, planType: planTypeVal, isUnlimited, numberOfPersons: noOfPersons,
          duration: planDurationsVal, userType: userTypeVal, price: planPrice
        })
      }

    }
  }

  componentWillReceiveProps(props) {
    if (props.addOnsAndStats) {
      const getAddOns = _.map(props.addOnsAndStats.addOns, (ad) => {
        return { value: ad._id, label: ad.name }
      });
      const getDuration = _.map(props.addOnsAndStats.durations, (ad) => {
        return { id: ad._id, name: ad.name }
      });
      const getPlanTypes = _.map(props.addOnsAndStats.planTypes, (ad) => {
        return { id: ad._id, userTypeId: ad.userType._id, name: ad.name }
      });
      const getUserTypes = _.map(props.addOnsAndStats.userTypes, (ad) => {
        return { id: ad._id, name: ad.name }
      });
      this.setState({
        getAllAddOns: getAddOns,
        userType: getUserTypes,
        planType: _.filter(getPlanTypes, (dd) => getUserTypes[0].id === dd.userTypeId ? dd : null),
        planDurations: getDuration,
        setAllPlantypes: getPlanTypes,
        planDurationsVal: getDuration ? getDuration[0].id : '',
        userTypeVal: getUserTypes ? getUserTypes[0].id : '',
        planTypeVal: getPlanTypes ? getPlanTypes[0].id : ''
      });
    }
    if (props.subscription && _.has(props.subscription, 'data._id')) {
      if (props.match.path === EDIT_SUBSCRIPTION_PLAN) {
        this.setState({
          userTypeVal: props.subscription.data.userType,
          planPrice: props.subscription.data.price,
          planTypeVal: props.subscription.data.planType,
          noOfPersons: props.subscription.data.numberOfPersons === null ? '' : props.subscription.data.numberOfPersons,
          planDurationsVal: props.subscription.data.duration,
          onlyToSetAddOns: _.map(props.subscription.data.addOns, (ad) => { return { value: ad._id, label: ad.name } }),
          addOns: _.map(_.map(props.subscription.data.addOns, (ad) => { return { value: ad._id, label: ad.name } }), (w) => w.value),
          planType: _.filter(this.state.setAllPlantypes, (dd) => props.subscription.data.userType === dd.userTypeId ? dd : null),
          isUnlimited: props.subscription.data.isUnlimited,
          loader: false
        });

      } else {
        this.setState({
          planPrice: '',
          noOfPersons: '',
          onlyToSetAddOns: [],
          addOns: [],
          subscriptionId: '',
          isUnlimited: false,
          loader: false
        });
      }
    }
    if (props.subscription && (props.subscription === 'Edited' || props.subscription === 'Added')) {
      this.setState({ loader: false })
      history.push(LIST_SUBSCRIPTION_PLANS)
    }
  }

  componentDidMount() {
    if (this.state.subscriptionId) {
      this.setState({ loader: true })
      setTimeout(() => {
        this.props.showSubscriptionData(this.state.subscriptionId);
      }, 1000);
    }
  }

  handleAddOnChange = (value) => {
    this.setState({ addOns: _.map(value, (w) => w.value), onlyToSetAddOns: value });
  }

  setPlanTypeOptionsOnChange = (getVal) => {
    const getPlanTypes = _.filter(this.state.setAllPlantypes, (dd) => getVal === dd.userTypeId ? dd : null);
    this.setState({ planType: getPlanTypes, userTypeVal: getVal, planTypeVal: getPlanTypes[0].id });
  }

  checkValidation = (field, value, type, maxLength) => {
    let error = fieldValidator(field, value, type, null, maxLength)
    this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
  }

  render() {
    const { loader, wrapperClsMobile, subscriptionId, onlyToSetAddOns, userType, getAllAddOns, planDurations, noOfPersons, wrapperCls,
      userTypeErr, planPriceErr, planTypeErr, noOfPersonsErr, planDurationsErr, planPrice,
      addOnsErr, animatedComponents, planType, isUnlimited, userTypeVal, planTypeVal, planDurationsVal } = this.state

    let userTypeOptions = _.map(userType, (ut, i) => {
      return <option key={i} value={ut.id}>{ut.name}</option>
    })

    let planTypeOptions = _.map(planType, (pt, j) => {
      return <option key={j} value={pt.id}>{pt.name}</option>
    })

    let planDurationsOptions = _.map(planDurations, (dt, k) => {
      return <option key={k} value={dt.id}>{dt.name}</option>
    })

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'subscription'} getInnerRoute={subscriptionId ? 'edit-subscription' : 'add-subscription'} />
        <div className="main-panel">
          <div className="content">
            <div className="panel-header bg-primary-gradient">
              <div className="page-inner py-5">
                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                  <div>
                    <h2 className="text-white pb-2 fw-bold">{subscriptionId ? 'Edit' : 'Add'} Subscription Plan</h2>
                    <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="page-inner mt--5">
              <div className="row">
                <div className="col-md-12">
                  <div className="card building-representative-sec full-height">
                    <div className="card-header d-flex justify-content-between align-items-center py-4">
                      <div className="card-head-row">
                        <div className="card-title font-weight-bold">{subscriptionId ? 'Edit' : 'Add'} Subscription Plan</div>
                      </div>
                    </div>
                    <div className="card-body management-list pt-0">
                      <div className="row">
                        <div className="col-sm-4 col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">User Type*</label>
                            <select value={userTypeVal} onChange={(e) => this.setPlanTypeOptionsOnChange(e.target.value)} className="form-control search-basic bg-light">
                              {userTypeOptions}
                            </select>
                            <span>{userTypeErr ? userTypeErr : ''}</span>
                          </div>
                        </div>
                        <div className="col-sm-4 col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">Plan Type*</label>
                            <div className="select2-input">
                              <select value={planTypeVal} onChange={(e) => this.setState({ planTypeVal: e.target.value })} className="form-control search-basic bg-light">
                                {planTypeOptions}
                              </select>
                              <span>{planTypeErr ? planTypeErr : ''}</span>
                            </div>
                          </div>
                        </div>
                        <div className="col-sm-4  col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">Set Price*</label>
                            <input name="planPrice" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'number', 4) }} value={planPrice} onChange={(e) => this.setState({ planPrice: e.target.value })} className="form-control bg-light" type="text" placeholder="" />
                            <span className="errorCls">{planPriceErr ? planPriceErr : ''}</span>
                          </div>
                        </div>
                        <div className="col-sm-4  col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">Plan Duration*</label>
                            <div className="select2-input">
                              <select value={planDurationsVal} onChange={(e) => this.setState({ planDurationsVal: e.target.value })} name="basic" className="form-control search-basic bg-light">
                                {planDurationsOptions}
                              </select>
                              <span>{planDurationsErr ? planDurationsErr : ''}</span>

                            </div>
                          </div>
                        </div>
                        <div className="col-sm-4  col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">No. of Persons*</label>
                            <input name="noOfPersons" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'number', 3) }} value={noOfPersons} onChange={(e) => this.setState({ noOfPersons: e.target.value, isUnlimited: false })} className="form-control bg-light" type="text" placeholder="" />
                            <span className="errorCls">{noOfPersonsErr ? noOfPersonsErr : ''}</span>

                          </div>
                        </div>
                        <div className="col-sm-4 col-lg-4">
                          <div className="form-group">
                            <label className="w-100 text-primary">Select Add Ons</label>
                            <Select
                              className="addonsCls"
                              closeMenuOnSelect={false}
                              components={animatedComponents}
                              isMulti
                              value={onlyToSetAddOns}
                              defaultValue={onlyToSetAddOns}
                              options={getAllAddOns}
                              onChange={value => this.handleAddOnChange(value)}
                            />
                            <span>{addOnsErr ? addOnsErr : ''}</span>
                          </div>
                        </div>
                        <div className="col-sm-6 d-flex flex-wrap align-items-center">
                          <div className="custom-control custom-checkbox">
                            <input id="unlimitedCheck" checked={isUnlimited} onChange={(e) => this.setState({ isUnlimited: e.target.checked, noOfPersonsErr: '', noOfPersons: '' })} className="custom-control-input" type="checkbox" />
                            <label htmlFor="unlimitedCheck" className="w-100 text-primary custom-control-label">For Unlimited persons:</label>
                          </div>
                        </div>
                      </div>
                      <div className="text-center text-md-right w-100 my-3 px-2">
                        <Link to={LIST_SUBSCRIPTION_PLANS} className="btn btn-dark" data-dismiss="modal">Cancel</Link>
                        <button id="addSubPlan" type="button" onClick={() => this.onSubmit()} className="btn btn-primary">Save Plan</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToprops = (state) => ({
  subscription: getSubscriptionPlan(state),
  addOnsAndStats: getAllAddOns(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addSubscriptionPlan: addSubscriptionPlan,
  getAllAddOns: getAddOns,
  showSubscriptionData: showSubscriptionData,
  editSubscriptionPlan: editSubscriptionPlan
}, dispatch);

export const AddSubscriptionPlan = connect(mapStateToprops, mapDispatchToProps)(AddSubscriptionPlanComponent); 