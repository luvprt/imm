import React from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { listSubscriptionPlan, deleteSubscriptionPlan } from '../../../../duck/subscription-plan/subscription.action'
import { getSubscriptionPlans } from '../../../../duck/subscription-plan/subscription.selector'
import { Link, NavLink } from "react-router-dom";
import { constants } from '../../../../common/constants'
import Swal from 'sweetalert2'
import _ from 'lodash';
import { EDIT_SUBSCRIPTION_PLAN_BASE } from '../../../../routing/routeContants'

class ListSubscriptionPlans extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            subscriptionPlans: [],
            pageNo: 1,
            limit: constants.PAGE_LIMIT,
            totalRecords: 0,
            userType: [],
            userTypeVal: '',
            planType: [],
            planTypeVal: '',
            setAllPlantypes: [],
            loader: false,
            sortingField: '',
            sortingOrder: ''
        }
    }


    componentWillReceiveProps(props) {
        if (props.subscriptionPlans && _.has(props.subscriptionPlans, 'data')) {
            const getPlanTypes = _.map(props.subscriptionPlans.data.planTypes, (ad) => {
                return { id: ad._id, userTypeId: ad.userType._id, name: ad.name }
            });
            const getUserTypes = _.map(props.subscriptionPlans.data.userTypes, (ad) => {
                return { id: ad._id, name: ad.name }
            });
            this.setState({
                subscriptionPlans: props.subscriptionPlans.data.getPlans,
                userType: getUserTypes,
                planType: this.state.userTypeVal ? _.filter(getPlanTypes, (dd) => (this.state.userTypeVal ? this.state.userTypeVal : getUserTypes[0].id) === dd.userTypeId ? dd : null) : getPlanTypes,
                totalRecords: props.subscriptionPlans.count ? props.subscriptionPlans.count : 0,
                setAllPlantypes: getPlanTypes,
                loader: false
            })
        } else if (props.subscriptionPlans === 'Deleted') {
            this.setState({ pageNo: 1, userTypeVal: '', planTypeVal: '' })
            this.props.listSubscriptionPlan({ offset: 1, limit: constants.PAGE_LIMIT })
        }
    }

    setPlanTypeOptionsOnChange = (getVal) => {
        const getPlanTypes = _.filter(this.state.setAllPlantypes, (dd) => getVal === dd.userTypeId ? dd : null);
        this.setState({ planType: getPlanTypes, userTypeVal: getVal, planTypeVal: '' });
    }

    componentDidMount() {
        const { pageNo, limit } = this.state
        this.setState({ loader: true })
        this.props.listSubscriptionPlan({ offset: pageNo, limit })
    }

    deleteSubscriptionPlan = (e, id) => {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this Subscription Plan!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.setState({ loader: true })
                if (id) {
                    this.setState({ loader: true })
                    this.props.deleteSubscriptionPlan(id)
                }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // console.log('cancel')
            }
        })
    }

    getPageData = (pageNo) => {
        const { limit, planTypeVal, userTypeVal, sortingField, sortingOrder } = this.state
        this.setState({ pageNo, loader: true })
        this.props.listSubscriptionPlan({
            offset: pageNo,
            limit, planType: planTypeVal, userType: userTypeVal,
            sortingField, sortingOrder
        })
    }


    sortTheData = (field, order) => {
        this.setState({
            pageNo: 1, loader: true,
            sortingOrder: order, sortingField: field,
            planTypeVal : '', userTypeVal:''
        })
        this.props.listSubscriptionPlan({
            offset: 1, limit: constants.PAGE_LIMIT,
            sortingField: field,
            sortingOrder: order
        })
    }

    getSearchData = (e) => {
        e.preventDefault();
        // initialise the search for first page
        this.setState({ pageNo: 1, loader: true })
        this.props.listSubscriptionPlan({
            offset: 1,
            limit: constants.PAGE_LIMIT,
            planType: this.state.planTypeVal,
            userType: this.state.userTypeVal
        })
    }

    clearSearchData = (e) => {
        e.preventDefault();
        this.setState({ pageNo: 1, userTypeVal: '', planTypeVal: '', loader: true })
        this.props.listSubscriptionPlan({ offset: 1, limit: constants.PAGE_LIMIT })
    }

    render() {
        const { loader, wrapperClsMobile, sortingOrder,
            wrapperCls, subscriptionPlans, userType, planType,
            totalRecords, limit, pageNo, userTypeVal, planTypeVal } = this.state

        console.log(loader)
        let userTypeOptions = _.map(userType, (ut, i) => {
            return <option key={i} value={ut.id}>{ut.name}</option>
        })

        let planTypeOptions = _.map(planType, (pt, j) => {
            return <option key={j} value={pt.id}>{pt.name}</option>
        })

        let getTableData = '';
        if (subscriptionPlans.length > 0) {
            getTableData = _.map(subscriptionPlans, (sp, i) => {
                return <tr key={i}>
                    <td>{sp.userType.name}</td>
                    <td> {sp.planType ? sp.planType.name : ''} </td>
                    <td>{sp.duration ? sp.duration.name : ''}</td>
                    <td>{sp.numberOfPersons === null ? '' : sp.numberOfPersons}{sp.isUnlimited === true ? 'Unlimited' : ''}</td>
                    <td>{sp.price}</td>
                    <td>
                        <div className="mb-0 dropdown text-center">
                            <Link to="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="fas fa-ellipsis-v"></i>
                            </Link>
                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                <NavLink className="dropdown-item" to={EDIT_SUBSCRIPTION_PLAN_BASE + sp._id}>
                                    <i className="fas fa-edit"></i> Edit
                                </NavLink>
                                <NavLink onClick={(e) => this.deleteSubscriptionPlan(e, sp._id)} className="dropdown-item" to={'#'}>
                                    <i className="fas fa-trash"></i> Delete
                                </NavLink>
                            </div>
                        </div>
                    </td>
                </tr>;
            });
        }

        return (
            <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                <Sidebar getMainRoute={'subscription'} getInnerRoute={'list-subscription'} />

                <div className="main-panel">
                    <div className="content">
                        <div className="panel-header bg-primary-gradient">
                            <div className="page-inner py-5">
                                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                    <div>
                                        <h2 className="text-white pb-2 fw-bold">Manage Subscription Plans</h2>
                                        <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="page-inner mt--5">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="card assets-management-sec full-height">
                                        <div className="card-header d-flex justify-content-between align-items-center py-4">
                                            <div className="card-head-row">
                                                <div className="card-title font-weight-bold">Manage Subscription Plans</div>
                                            </div>
                                        </div>

                                        <div className="card-body management-list">

                                            <div className="pb-3">
                                                <div className="mb-4 work-orders-sec management-list">

                                                    <div className="search-info">
                                                        <div className="d-md-flex flex-wrap justify-content-between align-items-center">
                                                            <div className="form-group flex-1  pl-md-0">
                                                                <select value={userTypeVal} onChange={(e) => this.setPlanTypeOptionsOnChange(e.target.value)} className="form-control search-basic bg-light">
                                                                    <option value=''>Select..</option>
                                                                    {userTypeOptions}
                                                                </select>
                                                            </div>
                                                            <div className="form-group flex-1">
                                                                <select value={planTypeVal} onChange={(e) => this.setState({ planTypeVal: e.target.value })} className="form-control search-basic bg-light">
                                                                    <option value=''>Select..</option>
                                                                    {planTypeOptions}
                                                                </select>
                                                            </div>

                                                            <div className="form-group">
                                                                <Link to="#" onClick={(e) => this.getSearchData(e)} className="btn-lg btn-primary btn-square text-white d-block text-center"> Search </Link>
                                                            </div>
                                                            <div className="form-group">
                                                                <Link to="#" onClick={(e) => this.clearSearchData(e)} className="btn-lg btn-dark btn-square text-white d-block text-center"> Clear Search </Link>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="pr-2 overflow-scroll">
                                                        <table className="table mb-3">
                                                            <thead className="thead-light">
                                                                <tr>
                                                                    <th scope="col">User Type <span className="sortingArrows"><span onClick={() => this.sortTheData('userType.name', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('userType.name', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                                                                    <th scope="col">Plan Type</th>
                                                                    <th scope="col">Duration</th>
                                                                    <th scope="col">No. of Persons</th>
                                                                    <th scope="col">Price</th>
                                                                    <th scope="col">Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {getTableData ? getTableData : <tr><td colSpan='6'>No Records Found. </td></tr>}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    {totalRecords ? <PaginationFooter getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}


const mapStateToprops = (state) => ({
    subscriptionPlans: getSubscriptionPlans(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    listSubscriptionPlan: listSubscriptionPlan,
    deleteSubscriptionPlan: deleteSubscriptionPlan
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ListSubscriptionPlans); 
