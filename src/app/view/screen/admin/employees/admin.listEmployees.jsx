import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { Link } from "react-router-dom";
import { ADD_EMPLOYEE } from '../../../../routing/routeContants';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { listEmployees } from '../../../../duck/employees/employees.action';
import { constants } from '../../../../common/constants';
import { getEmployeeData } from '../../../../duck/employees/employees.selector';
import _ from 'lodash';
export class ListEmployeesComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
      pageNo: 1,
      limit: constants.PAGE_LIMIT,
      employees: [],
      loader: false,
      totalRecords: 0,
      sortingField: '',
      sortingOrder: ''
    }
  }

  componentDidMount() {
    const { pageNo, limit } = this.state
    this.setState({ loader: true })
    this.props.listEmployees({ offset: pageNo, limit });
  }

  componentWillReceiveProps(props) {
    console.log(props)
    if (props.employees  && _.has(props.employees, 'data')) {
      this.setState({
        employees: props.employees.data,
        loader: false,
        totalRecords: props.employees.count ? props.employees.count : 0
      })
    }else{
      this.setState({
        loader: false
      })
    }
  }

  getPageData = (pageNo) => {
    const { limit } = this.state
    this.setState({ pageNo, loader: true })
    this.props.listEmployees({
      offset: pageNo,
      limit
    })
  }

  sortTheData = (field, order) => {
    this.setState({
        pageNo: 1, loader: true,
        sortingOrder: order, sortingField: field,
    })
    this.props.listEmployees({
        offset: 1, limit: constants.PAGE_LIMIT,
        sortingField: field,
        sortingOrder: order
    })
  }

  render() {
    const { wrapperCls, wrapperClsMobile, loader, employees, totalRecords, limit, pageNo,  sortingOrder } = this.state;
    let getTableData = '';
    if (employees.length > 0) {
      getTableData = _.map(employees, (emp, i) => {
        return <tr key={i}>
          <td>{emp.firstName}</td>
          <td>{emp.jobTitle ? emp.jobTitle : ''}</td>
          <td>{emp.department ? emp.department : ''}</td>
          <td>{emp.primaryPhone ? emp.primaryPhone : ''}</td>
          <td>Consectetur asperioires</td>
          <td></td>
          <td>
            <div className="mb-0 dropdown text-center">
              <a href="#" className="mb-0" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-ellipsis-v"></i>
              </a>
              <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                <a className="dropdown-item" href="new-employee-view.html"><i className="far fa-eye"></i> View</a>
                <a className="dropdown-item" href="#!"><i className="far fa-edit"></i> Edit</a>
                <a className="dropdown-item" href="#!"><i className="fas fa-cog"></i> Manage Employee</a>
                <a className="dropdown-item" href="#!"><i className="fas fa-file-alt"></i> View Employee</a>
              </div>
            </div>
          </td>
        </tr>
      });
    }

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'team'} getInnerRoute={'list-employees'} />
        <div className="main-panel">
          <div className="content">
            <div className="panel-header bg-primary-gradient">
              <div className="page-inner py-5">
                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                  <div>
                    <h2 className="text-white pb-2 fw-bold">Manage Employee</h2>
                    <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="page-inner mt--5">
              <div className="row">
                <div className="col-md-12">
                  <div className="card assets-management-sec full-height">
                    <div className="card-header d-flex justify-content-between align-items-center py-4">
                      <div className="card-head-row">
                        <div className="card-title font-weight-bold">Employee List</div>
                      </div>
                      <div className="search d-none d-sm-inline-block">
                        <Link to={ADD_EMPLOYEE} className="btn-lg btn-border btn-primary btn-round"><i className="far fa-plus-square mr-1"></i> Add New Employee</Link>
                      </div>
                      <div className="search d-sm-none">
                        <a href="add-new-employee.html" className="h3 mb-0"><i className="fas fa-plus-square mr-1 text-color-gradient"></i> </a>
                      </div>
                    </div>
                    <div className="card-body management-list">
                      <div className="pr-2 overflow-scroll">
                        <table className="table mb-3">
                          <thead className="thead-light">
                            <tr>
                              <th scope="col">Full Name <span className="sortingArrows"><span onClick={() => this.sortTheData('firstName', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('userType.name', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                              <th scope="col">Title</th>
                              <th scope="col">Department</th>
                              <th scope="col">Phone</th>
                              <th scope="col">Location</th>
                              <th scope="col">Status</th>
                              <th className="text-center" scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            {getTableData ? getTableData : <tr><td colSpan='7'>No Records Found. </td></tr>}
                          </tbody>
                        </table>
                      </div>
                      {totalRecords ? <PaginationFooter getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToprops = (state) => ({
  employees: getEmployeeData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  listEmployees: listEmployees,
}, dispatch);

export const ListEmployees = connect(mapStateToprops, mapDispatchToProps)(ListEmployeesComponent);