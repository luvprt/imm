import React from "react";

export class AddEmployeeContact extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div id="contact-tab" className={activeIndex === 1 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Email Address</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Primary Phone Number</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Secondary Phone Number</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="form-group px-0">
                            <label>Street Address 1</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="form-group px-0">
                            <label>Street Address 2</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>City</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>State/Province</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Zip/Postal Code</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Emergency Contact Name</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Emergency Contact Phone</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Emergency Contact Relation</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-12 text-right">
                        <div className="text-right my-4">
                            <span className="btn-lg btn-dark text-white">Back</span>
                            <span className="btn-lg btn-primary text-white">Next</span>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

