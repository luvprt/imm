import React from "react";

export class AddEmployeeDocument extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div id="document-tab" className={activeIndex === 5 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
            <div className="row">
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label className="w-100">Type</label>
                  <div className="select2-input">
                    <select name="basic" className="form-control search-basic bg-light">
                      <option defaultValue="report1">Select</option>
                      ...
                                                            <option defaultValue="report2">Report Status</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label>Name</label>
                  <input type="text" className="form-control" placeholder="" />
                </div>
              </div>
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label>Description</label>
                  <input type="text" className="form-control" placeholder="" />
                </div>
              </div>
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label>Document Number</label>
                  <input type="text" className="form-control" placeholder="" />
                </div>
              </div>
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label>Expiration Date</label>
                  <input type="text" className="form-control" placeholder="" />
                </div>
              </div>
              <div className="col-sm-4">
                <div className="form-group px-0">
                  <label>Document</label>
                  <div className="custom-file">
                    <input type="file" className="custom-file-input" id="browse-file" />
                    <label className="custom-file-label form-control mb-0" htmlFor="browse-file">Choose file</label>
                  </div>
                </div>
              </div>
              <div className="col-12 text-right">
                <div className="text-right my-4">
                  <span className="btn-lg btn-dark text-white">Back</span>
                  <span className="btn-lg btn-primary text-white">Next</span>
                </div>
              </div>
            </div>
          </div>

        )
    }

}

