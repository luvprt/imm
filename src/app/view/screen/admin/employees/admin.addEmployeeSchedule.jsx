import React from "react";

export class AddEmployeeSchedule extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div id="schedule-tab" className={activeIndex === 4 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                <div className="table-responsive">
                    <table className="table w-100">
                        <tbody>
                            <tr className="bg-light">
                                <th></th>
                                <th>Sunday</th>
                                <th>Monday</th>
                                <th>Tuesday</th>
                                <th>Wednesday</th>
                                <th>Thursday</th>
                                <th>Friday</th>
                                <th>Saturday</th>
                            </tr>
                            <tr>
                                <td>Shift Start</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Shift End</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>First Break Start</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>First Break End</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Second Break Start</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Second Break End</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Third Break Start</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                            <tr>
                                <td>Third Break End</td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                                <td><input type="text" className="form-control" /></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }

}

