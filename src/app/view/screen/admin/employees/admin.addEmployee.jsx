import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addEmployee } from '../../../../duck/employees/employees.action'
import { addEmployeeData } from '../../../../duck/employees/employees.selector';
import _ from 'lodash';
import { AddEmployeePersonal } from './admin.addEmployeePersonal';
import { AddEmployeeContact } from './admin.addEmployeeContact';
import { AddEmployeeEmployment } from './admin.addEmployeeEmployement';
import { AddEmployeeAccess } from './admin.addEmployeeAccess';
import { AddEmployeeSchedule } from './admin.addEmployeeSchedule';
import { AddEmployeeDocument } from './admin.addEmployeeDocument';
export class AddEmployeeComponent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
      activeIndex: 0
    }
  }

  componentWillReceiveProps(props) {
    let { activeIndex } = this.state
    if (props.addEmployeeData && _.has(props.addEmployeeData, 'data._id')) {
      this.setState({
        activeIndex: activeIndex + 1
      })
    }
  }

  render() {
    const { wrapperCls, wrapperClsMobile, activeIndex } = this.state;

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'team'} getInnerRoute={'list-employees'} />
        <div className="main-panel">
          <div className="content">
            <div className="panel-header bg-primary-gradient">
              <div className="page-inner py-5">
                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                  <div>
                    <h2 className="text-white pb-2 fw-bold">Manage Employee</h2>
                    <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="page-inner mt--5">
              <div className="row">
                <div className="col-md-12">
                  <div className="card assets-management-sec full-height">
                    <div className="card-header d-flex justify-content-between align-items-center py-4">
                      <div className="card-head-row">
                        <div className="card-title font-weight-bold">New Employee</div>
                      </div>
                    </div>
                    <div id="new_employee" className="card-body management-list">
                      <div className="tab-panel" role="tabpanel" aria-labelledby="request-tab">
                        <div className="py-3">
                          <div className="d-flex overflow-scroll">
                            <div className="card-head-row nav nav-pills w-100" role="tablist">
                              <a className={activeIndex === 0 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#personal-tab">Personal</a>
                              <a className={activeIndex === 1 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#contact-tab">Contact</a>
                              <a className={activeIndex === 2 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#employment-tab">Employment</a>
                              <a className={activeIndex === 3 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#access-tab">Access</a>
                              <a className={activeIndex === 4 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#schedule-tab">Schedule</a>
                              <a className={activeIndex === 5 ? "nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center btn-rounded" : "nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center btn-rounded"} data-toggle="pill" href="#document-tab">Document</a>
                            </div>
                          </div>
                          <div className="px-3 pb-2 mt-2">
                            <div className="tab-content mt-2 mb-4 work-orders-sec management-list">
                              <AddEmployeePersonal activeIndex={activeIndex} />
                              <AddEmployeeContact activeIndex={activeIndex} />
                              <AddEmployeeEmployment activeIndex={activeIndex} />
                              <AddEmployeeAccess activeIndex={activeIndex} />
                              <AddEmployeeSchedule activeIndex={activeIndex} />
                              <AddEmployeeDocument activeIndex={activeIndex} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToprops = (state) => ({
  addEmployeeData: addEmployeeData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addEmployee
}, dispatch);

export const AddEmployee = connect(mapStateToprops, mapDispatchToProps)(AddEmployeeComponent);
