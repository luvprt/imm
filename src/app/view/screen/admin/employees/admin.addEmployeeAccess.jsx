import React from "react";

export class AddEmployeeAccess extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div id="access-tab" className={activeIndex === 3 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                <div className="row">
                    <div className="col-md-4">
                        <div className="form-group px-0">
                            <label>Username</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group px-0">
                            <label>Tech Support PIN</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="form-group px-0">
                            <label className="w-100">User Account Status</label>
                            <input type="checkbox" defaultChecked data-toggle="toggle" data-onstyle="info" data-style="btn-round" />
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

