import React from "react";
import PROFILE_IMG from '../../../../assets/images/profile-img.jpg';
import { PhotoshopPicker } from 'react-color'
import DatePicker from "react-datepicker";
import { validateInputs } from '../../../../common/validation';
import { fieldValidator } from '../../../../common/custom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addEmployee } from '../../../../duck/employees/employees.action'
import { addEmployeeData } from '../../../../duck/employees/employees.selector';

export class AddEmployeePersonalComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            error: '',
            firstName: '',
            firstNameErr: '',
            lastName: '',
            lastNameErr: '',
            colorCode: '',
            colorCodeErr: '',
            nameToDisplayOnWorkOrders: '',
            nameToDisplayOnWorkOrdersErr: '',
            nameToDisplayOnDispatch: '',
            nameToDisplayOnDispatchErr: '',
            gender: '',
            genderErr: '',
            dob: '',
            dobErr: '',
            shortBio: '',
            driversLicenseNumber: '',
            driversLicenseNumberErr: '',
            driversLicenseExpirationDate: '',
            driversLicenseExpirationDateErr: '',
            previousColorCode: '',
            isFieldWorker: true,
            fieldWorkerType: '',
            fieldWorkerTypeErr: '',
            showColorPicker: false,
            profileImage: PROFILE_IMG,
            profileImageData: '',
            activeIndex: 0
        }
    }

    onNext = () => {
        const { firstName, lastName, colorCode, nameToDisplayOnWorkOrders,
            nameToDisplayOnDispatch, gender, dob,
            driversLicenseNumber, driversLicenseExpirationDate, isFieldWorker,
            fieldWorkerType, profileImageData, shortBio } = this.state;

        let firstNameErr = '', lastNameErr = '', colorCodeErr = '', nameToDisplayOnWorkOrdersErr = '',
            nameToDisplayOnDispatchErr = '', genderErr = '', dobErr = '', driversLicenseNumberErr = '',
            driversLicenseExpirationDateErr = '', fieldWorkerTypeErr = '', getError = false;

        if (validateInputs('string', firstName) === 'empty') {
            firstNameErr = 'Please enter first name.';
            getError = true;
        } else if (validateInputs('string', firstName) === false) {
            firstNameErr = 'Please enter valid first name.';
            getError = true;
        }

        if (validateInputs('string', lastName) === 'empty') {
            lastNameErr = 'Please enter last name.';
            getError = true;
        } else if (validateInputs('string', lastName) === false) {
            lastNameErr = 'Please enter valid last name.';
            getError = true;
        }

        if (validateInputs('string', colorCode) === 'empty') {
            colorCodeErr = 'Please select color code.';
            getError = true;
        }

        if (validateInputs('string', nameToDisplayOnWorkOrders) === 'empty') {
            nameToDisplayOnWorkOrdersErr = 'Please enter name to display on work orders.';
            getError = true;
        } else if (validateInputs('string', nameToDisplayOnWorkOrders) === false) {
            lastNameErr = 'Please enter valid name to display on work orders.';
            getError = true;
        }

        if (validateInputs('string', nameToDisplayOnDispatch) === 'empty') {
            nameToDisplayOnDispatchErr = 'Please enter name to display on dispatch.';
            getError = true;
        } else if (validateInputs('string', nameToDisplayOnDispatch) === false) {
            nameToDisplayOnDispatchErr = 'Please enter valid name to display on dispatch.';
            getError = true;
        }

        if (validateInputs('string', gender) === 'empty') {
            genderErr = 'Please select gender.';
            getError = true;
        }

        if (validateInputs('required', dob) === 'empty') {
            dobErr = 'Please select dob.';
            getError = true;
        }

        if (validateInputs('number', driversLicenseNumber) === 'empty') {
            driversLicenseNumberErr = 'Please enter driver license number.';
            getError = true;
        } else if (validateInputs('number', driversLicenseNumber) === false) {
            driversLicenseNumberErr = 'Please enter valid driver license number.';
            getError = true;
        }

        if (validateInputs('required', driversLicenseExpirationDate) === 'empty') {
            driversLicenseExpirationDateErr = 'Please select expiry date.';
            getError = true;
        }

        if (isFieldWorker && validateInputs('string', fieldWorkerType) === 'empty') {
            fieldWorkerTypeErr = 'Please select field worker type.';
            getError = true;
        }

        this.setState({
            firstNameErr, lastNameErr, colorCodeErr, nameToDisplayOnWorkOrdersErr,
            nameToDisplayOnDispatchErr, genderErr, dobErr, driversLicenseNumberErr,
            driversLicenseExpirationDateErr, fieldWorkerTypeErr, error: getError
        });

        if (getError === false && firstNameErr === '' && lastNameErr === '' && colorCodeErr === '' &&
            nameToDisplayOnWorkOrdersErr === '' && nameToDisplayOnDispatchErr === '' &&
            genderErr === '' && dobErr === '' && driversLicenseNumberErr === '' &&
            driversLicenseExpirationDateErr === '' && fieldWorkerTypeErr === '') {
            this.setState({ loader: true })
            const data = new FormData()
            data.append('profileImage', profileImageData)
            data.append('firstName', firstName);
            data.append('lastName', lastName);
            data.append('colorCode', colorCode);
            data.append('nameToDisplayOnWorkOrders', nameToDisplayOnWorkOrders);
            data.append('nameToDisplayonDispatch', nameToDisplayOnDispatch);
            data.append('gender', gender);
            data.append('dob', dob);
            data.append('driversLisenceNumber', driversLicenseNumber);
            data.append('driversExpirationDate', driversLicenseExpirationDate);
            data.append('isFieldWorker', isFieldWorker);
            data.append('fieldWorkerType', fieldWorkerType);
            data.append('shortBio', shortBio)
            // let data = { profileImage:profileImageData,firstName, lastName, colorCode, nameToDisplayOnWorkOrders, 
            //   nameToDisplayonDispatch:nameToDisplayOnDispatch, gender, dob, driversLisenceNumber:driversLicenseNumber , driversExpirationDate:driversLicenseExpirationDate, isFieldWorker, fieldWorkerType,shortBio }
            this.props.addEmployee(data)
        }
    }

    handleColorChange = (color) => {
        this.setState({ colorCode: color.hex });
        this.checkValidation('colorCode', color.hex, 'required')
    }

    onColorChangeComplete = (color) => {
        this.setState({ previousColorCode: this.state.colorCode, showColorPicker: false })
    }

    onCancelColorPicker = () => {
        this.setState({ colorCode: this.state.previousColorCode, showColorPicker: false })
    }

    triggerInputFile = () => this.fileInput.click()

    handleImageChange = (event) => {
        if (event) {
            this.setState({
                profileImageData: event.target.files[0],
                profileImage: URL.createObjectURL(event.target.files[0])
            })
        }
    }

    setDate = (field, date) => {
        console.log(field, date)
        this.setState({
            [field]: date
        })
        this.checkValidation(field, date, 'required');
    }

    checkValidation = (field, value, type, maxLength, minLength) => {
        let error = fieldValidator(field, value, type, null, maxLength, minLength)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
    }

    render() {
        const { activeIndex } = this.props;

        const { firstName, firstNameErr, lastName, lastNameErr, showColorPicker, colorCode,
            colorCodeErr, nameToDisplayOnWorkOrders, nameToDisplayOnWorkOrdersErr, nameToDisplayOnDispatch,
            nameToDisplayOnDispatchErr, gender, genderErr, dob, dobErr, driversLicenseNumber,
            driversLicenseNumberErr, driversLicenseExpirationDate, driversLicenseExpirationDateErr,
            isFieldWorker, fieldWorkerType, profileImage, fieldWorkerTypeErr, shortBio } = this.state

        const styles = {
            colorPicker: {
                background: colorCode
            }
        };

        return (
            <div id="personal-tab" className={activeIndex === 0 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                <div className="row">
                    <div className="col-md-8">
                        <div className="row">
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>First Name*</label>
                                    <input name="firstName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" placeholder="" value={firstName} onChange={(e) => this.setState({ firstName: e.target.value })} />
                                    {firstNameErr ? <div className='errorCls'>{firstNameErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Last Name*</label>
                                    <input name="lastName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" placeholder="" value={lastName} onChange={(e) => this.setState({ lastName: e.target.value })} />
                                    {lastNameErr ? <div className='errorCls'>{lastNameErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label className="w-100">This Employee is a Field Worker</label>
                                    <input type="checkbox" defaultChecked={isFieldWorker} data-toggle="toggle" data-onstyle="info" data-style="btn-round" onChange={(e) => this.setState({ isFieldWorker: e.target.checked })} />
                                    {isFieldWorker ?
                                        <div className="d-block pt-3">
                                            <label className="form-radio-label mb-2 mb-sm-0">
                                                <input className="form-radio-input" type="radio" name="fieldWorkerType" value="Inspector" defaultChecked={fieldWorkerType} onChange={(e) => { this.setState({ fieldWorkerType: e.target.value }); this.checkValidation(e.target.name, e.target.value, 'required') }} />
                                                <span className="form-radio-sign">Inspector</span>
                                            </label>
                                            <label className="form-radio-label mb-2 mb-sm-0">
                                                <input className="form-radio-input" type="radio" name="fieldWorkerType" value="Apprentice" defaultChecked={fieldWorkerType} onChange={(e) => { this.setState({ fieldWorkerType: e.target.value }); this.checkValidation(e.target.name, e.target.value, 'required') }} />
                                                <span className="form-radio-sign">Apprentice</span>
                                            </label>
                                            <label className="form-radio-label mb-2 mb-sm-0">
                                                <input className="form-radio-input" type="radio" name="fieldWorkerType" value="Technician" defaultChecked={fieldWorkerType} onChange={(e) => { this.setState({ fieldWorkerType: e.target.value }); this.checkValidation(e.target.name, e.target.value, 'required') }} />
                                                <span className="form-radio-sign">Technician or Foreman</span>
                                            </label>
                                        </div> : ''
                                    }
                                    {fieldWorkerTypeErr ? <div className='errorCls'>{fieldWorkerTypeErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Color Code</label>
                                    <div className="d-flex justify-content-left align-items-center">
                                        <input name="colorCode" type="text" className="form-control  w-auto d-inline-block" placeholder="" value={colorCode} readOnly />
                                        <span className="d-inline-block color-code ml-2" onClick={() => this.setState({ showColorPicker: !showColorPicker })} >
                                            <span style={styles.colorPicker}></span>
                                        </span>
                                    </div>
                                    {colorCodeErr ? <div className='errorCls'>{colorCodeErr}</div> : ''}
                                </div>
                                {showColorPicker ?
                                    <div className="colorPicker">
                                        <PhotoshopPicker color={colorCode} onChange={this.handleColorChange} onAccept={(color) => this.onColorChangeComplete(color)} onCancel={this.onCancelColorPicker} />
                                    </div> : ''}
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Name To Display On Work Orders</label>
                                    <input name="nameToDisplayOnWorkOrders" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" placeholder="" value={nameToDisplayOnWorkOrders} onChange={(e) => this.setState({ nameToDisplayOnWorkOrders: e.target.value })} />
                                    {nameToDisplayOnWorkOrdersErr ? <div className='errorCls'>{nameToDisplayOnWorkOrdersErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Name To Display On Dispatch</label>
                                    <input name="nameToDisplayOnDispatch" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" placeholder="" value={nameToDisplayOnDispatch} onChange={(e) => this.setState({ nameToDisplayOnDispatch: e.target.value })} />
                                    {nameToDisplayOnDispatchErr ? <div className='errorCls'>{nameToDisplayOnDispatchErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Gender</label>
                                    <div className="d-block pt-3">
                                        <label className="form-radio-label mb-2 mb-sm-0">
                                            <input className="form-radio-input" type="radio" name="gender" value="Male" defaultChecked={gender} onChange={(e) => { this.setState({ gender: e.target.value }); this.checkValidation(e.target.name, e.target.value, 'required') }} />
                                            <span className="form-radio-sign">Male</span>
                                        </label>
                                        <label className="form-radio-label mb-2 mb-sm-0">
                                            <input className="form-radio-input" type="radio" name="gender" value="Female" defaultChecked={gender} onChange={(e) => { this.setState({ gender: e.target.value }); this.checkValidation(e.target.name, e.target.value, 'required') }} />
                                            <span className="form-radio-sign">Female</span>
                                        </label>
                                    </div>
                                    {genderErr ? <div className='errorCls'>{genderErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Date Of Birth</label>
                                    <DatePicker type="text" className="form-control" placeholder="" selected={dob} onChange={(date) => this.setDate('dob', date)} />
                                    {dobErr ? <div className='errorCls'>{dobErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Drivers License Number</label>
                                    <input name="driversLicenseNumber" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'number') }} type="text" className="form-control" placeholder="" value={driversLicenseNumber} onChange={(e) => this.setState({ driversLicenseNumber: e.target.value })} />
                                    {driversLicenseNumberErr ? <div className='errorCls'>{driversLicenseNumberErr}</div> : ''}
                                </div>
                            </div>
                            <div className="col-xl-6">
                                <div className="form-group px-0">
                                    <label>Drivers License Expiration Date</label>
                                    <DatePicker type="text" className="form-control" selected={driversLicenseExpirationDate} onChange={(date) => this.setDate('driversLicenseExpirationDate', date)} />
                                    {driversLicenseExpirationDateErr ? <div className='errorCls'>{driversLicenseExpirationDateErr}</div> : ''}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="text-center mb-2">
                            <h5 className="font-weight-bold">Please Update Profile Picture</h5>
                        </div>
                        <div className="avatar-xxl mx-auto">
                            <img src={profileImage} alt="..." className="avatar-img rounded-circle" />
                        </div>
                        <div className="mx-auto text-center my-4">
                            <input
                                ref={fileInput => this.fileInput = fileInput}
                                type="file"
                                className="hiddenInputTypeFile"
                                onChange={(e) => this.handleImageChange(e)}
                            />
                            <span className="btn-lg bg-gradient-primary btn-round text-white" onClick={this.triggerInputFile}>Update Picture</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="zipcode" className="placeholder">Short Bio(optional)</label>
                            <textarea rows="4" type="text" className="form-control" required="" value={shortBio} onChange={(e) => this.setState({ shortBio: e.target.value })}></textarea>
                        </div>
                        <p className="pl-2">Note: This bio can be included in email confirmations of customers tell them more about the tech before the visit.</p>
                    </div>
                    <div className="col-12 text-right">
                        <div className="text-right my-4">
                            <button className="btn-lg btn-dark text-white">Back</button>
                            <button className="btn-lg btn-primary text-white" onClick={(e) => this.onNext()}>Next</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

const mapStateToprops = (state) => ({
    addEmployeeData: addEmployeeData(state)
  });
  
  const mapDispatchToProps = (dispatch) => bindActionCreators({
    addEmployee
  }, dispatch);
  
  export const AddEmployeePersonal = connect(mapStateToprops, mapDispatchToProps)(AddEmployeePersonalComponent);