import React from "react";

export class AddEmployeeEmployment extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        const { activeIndex } = this.props;
        return (
            <div id="employment-tab" className={activeIndex === 2 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label className="w-100">Employee Type</label>
                            <div className="select2-input">
                                <select name="basic" className="form-control search-basic bg-light">
                                    <option defaultValue="report1">Select</option>
                                    ...
                                                            <option defaultValue="report2">Report Status</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label>Job Title</label>
                            <input type="text" className="form-control" placeholder="" />
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label className="w-100">Experience Level</label>
                            <div className="select2-input">
                                <select name="basic" className="form-control search-basic bg-light">
                                    <option defaultValue="report1">Select</option>
                                    ...
                                                            <option defaultValue="report2">Report Status</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="form-group px-0">
                            <label className="w-100">Department</label>
                            <div className="select2-input">
                                <select name="basic" className="form-control search-basic bg-light">
                                    <option defaultValue="report1">Select</option>
                                    ...
                                                            <option defaultValue="report2">Report Status</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-2 d-flex align-items-center">
                        <div className="form-group px-0">
                            <div className="custom-control custom-checkbox flex-1 mt-4">
                                <input type="checkbox" className="custom-control-input" id="parent-account" defaultChecked="" />
                                <label className="custom-control-label" htmlFor="parent-account">Billing Contact</label>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="form-group px-0">
                            <label className="w-100">Management Position</label>
                            <div className="select2-input">
                                <select name="basic" className="form-control search-basic bg-light">
                                    <option defaultValue="report1">Select</option>
                                    ...
                                                            <option defaultValue="report2">Report Status</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="form-group px-0 select-multiple">
                            <label className="w-100">Supervisor</label>
                            <div className="select2-input">
                                <select id="multiple" name="multiple[]" className="form-control" multiple="multiple">
                                    <option defaultValue="AL">Alabama</option>
                                    ...
                                                            <option defaultValue="WY">Wyoming</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 text-right">
                        <div className="text-right my-4">
                            <span className="btn-lg btn-dark text-white">Back</span>
                            <span className="btn-lg btn-primary text-white">Next</span>
                        </div>
                    </div>
                </div>
            </div>

        )
    }

}

