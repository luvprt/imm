import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { validateInputs } from '../../../../common/validation';
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { addCustomer, emailCheckClearbit } from '../../../../duck/customer/customer.action'
import _ from 'lodash'
import { addCustomerData, clearBitData } from '../../../../duck/customer/customer.selector'
import { fieldValidator } from '../../../../common/custom';

export class AddCustomerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
      companyName: '', prefix: 'Mr', firstName: '', lastName: '',
      phone: '', email: '', department: '', jobTitle: '',
      privateNotes: '', publicNotes: '',
      companyNameErr: '', prefixErr: '', firstNameErr: '',
      lastNameErr: '', phoneErr: '', emailErr: '', departmentErr: '', jobTitleErr: '',
      privateNotesErr: '', publicNotesErr: '',
      loader: false,
      error: '',
      serviceMessage: '',
      customerId: '',
      activeIndex: 1,
      testViaJest: props.testViaJest ? props.testViaJest : false,
      paymentTerms: [{
        defaultPaymentMethod: "",
        paymentTerms: "",
        discount: "",
        laborChargeType: ""
      }]
    }
  }


  onSubmit = () => {
    const { companyName, department, jobTitle,
      phone, prefix, privateNotes, publicNotes,
      firstName, lastName, email, testViaJest } = this.state

    let companyNameErr = '', prefixErr = '', firstNameErr = '',
      lastNameErr = '', phoneErr = '', emailErr = '',
      departmentErr = '', jobTitleErr = '', privateNotesErr = '',
      publicNotesErr = '', getError = false;

    if (validateInputs('email', email) === 'empty') {
      emailErr = 'Please enter valid email.';
      getError = true;
    } else if (validateInputs('email', email) === false) {
      emailErr = 'Invalid email.';
      getError = true;
    }

    if (validateInputs('string', companyName) === 'empty') {
      companyNameErr = 'Please enter company name.';
      getError = true;
    } else if (validateInputs('string', companyName) === false) {
      companyNameErr = 'Invalid company name.';
      getError = true;
    }

    if (validateInputs('string', prefix) === 'empty') {
      prefixErr = 'Please select prefix.';
      getError = true;
    } else if (validateInputs('string', prefix) === false || (prefix !== 'Mr' && prefix !== 'Mrs')) {
      prefixErr = 'Invalid prefix, either Mr or Mrs.';
      getError = true;
    }

    if (validateInputs('string', firstName) === 'empty') {
      firstNameErr = 'Please enter first name.';
      getError = true;
    } else if (validateInputs('string', firstName) === false) {
      firstNameErr = 'Invalid first name.';
      getError = true;
    }

    if (validateInputs('string', lastName) === 'empty') {
      lastNameErr = 'Please enter last name.';
      getError = true;
    } else if (validateInputs('string', lastName) === false) {
      lastNameErr = 'Invalid last name.';
      getError = true;
    }

    if (validateInputs('phone', phone) === 'empty') {
      phoneErr = 'Please enter phone number.';
      getError = true;
    } else if (validateInputs('phone', phone) === false) {
      phoneErr = 'Invalid phone number.';
      getError = true;
    }

    if (validateInputs('string', department) === 'empty') {
      departmentErr = 'Please enter department.';
      getError = true;
    } else if (validateInputs('string', department) === false) {
      departmentErr = 'Invalid department.';
      getError = true;
    }

    if (validateInputs('string', jobTitle) === 'empty') {
      jobTitleErr = 'Please enter job title.';
      getError = true;
    } else if (validateInputs('string', jobTitle) === false) {
      jobTitleErr = 'Invalid job title.';
      getError = true;
    }

    if (validateInputs('string', jobTitle) === 'empty') {
      jobTitleErr = 'Please enter job title.';
      getError = true;
    } else if (validateInputs('string', jobTitle) === false) {
      jobTitleErr = 'Invalid job title.';
      getError = true;
    }

    if (validateInputs('string', privateNotes) === 'empty') {
      privateNotesErr = 'Please enter private notes.';
      getError = true;
    } else if (validateInputs('string', privateNotes) === false) {
      privateNotesErr = 'Invalid private notes.';
      getError = true;
    }

    if (validateInputs('string', publicNotes) === 'empty') {
      publicNotesErr = 'Please enter public notes.';
      getError = true;
    } else if (validateInputs('string', publicNotes) === false) {
      publicNotesErr = 'Invalid public notes.';
      getError = true;
    }


    this.setState({
      emailErr, companyNameErr, prefixErr, departmentErr, jobTitleErr, publicNotesErr,
      privateNotesErr, firstNameErr, lastNameErr, phoneErr, error: getError, serviceMessage: ''
    });

    if (getError === false && emailErr === '' && companyNameErr === ''
      && testViaJest === false && prefixErr === '' && departmentErr === ''
      && jobTitleErr === '' && publicNotesErr === '' && privateNotesErr === ''
      && firstNameErr === '' && lastNameErr === '' && phoneErr === '') {
      this.setState({ loader: true })
      this.props.addCustomer({
        phone, prefix, firstName, lastName, email,
        companyDetails: { companyName, jobTitle, department },
        additionalInfo: { privateNotes, publicNotes }
      })
    }
  }

  componentWillReceiveProps(props) {
    if (props.addCustomerData && _.has(props.addCustomerData, 'data._id')) {
      this.setState({
        activeIndex: 2, customerId: props.addCustomerData.data._id
      })
    }
    if (props.addCustomerData && _.has(props.addCustomerData, 'message') && props.addCustomerData.success === false) {
      this.setState({
        serviceMessage: props.addCustomerData.message
      })
      document.getElementById('serviceMessage').scrollIntoView();
    }
    if (props.checkEmailClearbit && _.has(props.checkEmailClearbit, 'data.personalDetail.firstName')) {
      this.setState({
        companyName: props.checkEmailClearbit.data.companyDetail.name || '',
        firstName: props.checkEmailClearbit.data.personalDetail.firstName || '',
        lastName: props.checkEmailClearbit.data.personalDetail.lastName || '',
        department: props.checkEmailClearbit.data.companyDetail.department || '',
        jobTitle: props.checkEmailClearbit.data.companyDetail.title || '',
      })
    }
  }

  paymentTermsValues = (i, event) => {
    const { name, value } = event.target;
    let paymentTerms = [...this.state.paymentTerms];
    paymentTerms[i] = { ...paymentTerms[i], [name]: value };
    this.setState({ paymentTerms });
  }

  addPaymentTerms = () => {
    // this.setState(prevState =>
    //   ({
    //     paymentTerms: [...prevState.paymentTerms,
    //     { defaultPaymentMethod: "", paymentTerms: "", discount: "", laborChargeType: "" }]
    //   }))
  }

  removePaymentTerms = (i) => {
    let paymentTerms = [...this.state.paymentTerms];
    paymentTerms.splice(i, 1);
    this.setState({ paymentTerms });
  }

  componentWillUnmount() {
    this.setState({
      wrapperCls: false,
      wrapperClsMobile: false,
      companyName: '', prefix: 'Mr', firstName: '', lastName: '',
      phone: '', email: '', department: '', jobTitle: '',
      privateNotes: '', publicNotes: '',
      companyNameErr: '', prefixErr: '', firstNameErr: '',
      lastNameErr: '', phoneErr: '', emailErr: '', departmentErr: '', jobTitleErr: '',
      privateNotesErr: '', publicNotesErr: '',
      loader: false,
      error: '',
      serviceMessage: '',
      customerId: '',
      activeIndex: 1,
      paymentTerms: [{
        defaultPaymentMethod: "",
        paymentTerms: "",
        discount: "",
        laborChargeType: ""
      }]
    })
  }

  accountPaymentTerms = () => {
    return _.map(this.state.paymentTerms, (el, i) =>
      <div key={i} className="d-sm-flex justify-content-between align-items-center">
        <div className="form-group  flex-1">
          <label htmlFor="pay-method">Default Payment Method</label>
          <input type="text" onChange={(e) => this.paymentTermsValues(i, e)} value={el.defaultPaymentMethod || ''} className="form-control" name="defaultPaymentMethod" placeholder="" />
        </div>
        <div className="form-group  flex-1">
          <label htmlFor="pay-terms">Payment Terms</label>
          <input type="text" className="form-control" onChange={(e) => this.paymentTermsValues(i, e)} value={el.paymentTerms || ''} name="paymentTerms" placeholder="" />
        </div>
        <div className="form-group  flex-1">
          <label htmlFor="discount">Discount</label>
          <input type="text" className="form-control" onChange={(e) => this.paymentTermsValues(i, e)} value={el.discount || ''} name="discount" placeholder="" />
        </div>
        <div className="form-group  flex-1">
          <label htmlFor="charge">Labor Charge Type</label>
          <input type="text" className="form-control" onChange={(e) => this.paymentTermsValues(i, e)} value={el.laborChargeType || ''} name="laborChargeType" placeholder="" />
        </div>
        <div className="form-group">
          <label onClick={() => this.removePaymentTerms(i)} htmlFor="charge">X</label>
        </div>
      </div>
    )
  }

  checkValidation = (field, value, type, maxLength, minLength) => {
    let error = fieldValidator(field, value, type, null, maxLength, minLength)
    this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
  }

  checEmailOnClearBit = () => {
    const { email } = this.state
    if (validateInputs('email', email) === true) {
      this.props.emailCheckClearbit({ email })
    }

  }

  render() {
    const { wrapperCls, wrapperClsMobile, companyName, department, jobTitle,
      phone, prefix, privateNotes, publicNotes, firstName, lastName, email,
      companyNameErr, prefixErr, firstNameErr, lastNameErr, phoneErr, emailErr,
      departmentErr, jobTitleErr, privateNotesErr, publicNotesErr, customerId,
      activeIndex, paymentTerms, serviceMessage } = this.state;

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'customers'} getInnerRoute={'add-customer'} />
        <div className="main-panel">
          <div className="content">
            <div className="panel-header bg-primary-gradient">
              <div className="page-inner py-5">
                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                  <div>
                    <h2 className="text-white pb-2 fw-bold">Customers Management</h2>
                    <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                  </div>
                </div>
              </div>
            </div>
            <div className="page-inner mt--5">
              <div className="row">
                <div className="col-md-12">
                  <div className="card customer-account-sec full-height">
                    <div className="card-header d-flex justify-content-between align-items-center py-4">
                      <div className="card-head-row">
                        <div className="card-title font-weight-bold">Add Customer</div>
                      </div>
                    </div>

                    <div className="card-body management-list" id='serviceMessage'>
                      <div className="d-flex overflow-scroll mb-4" role="tablist">
                        <div className="card-head-row nav nav-pills w-100 flex-nowrap" role="tablist">
                          <a className={activeIndex === 1 ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#account" >Account Info</a>
                          <a className={activeIndex === 2 ? "nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active" : "nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center"} data-toggle="pill" href="#financial" >Financial Data</a>
                          <a className={activeIndex === 3 ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#service"  >Service Locations</a>
                          <a className={activeIndex === 4 ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#bill-to-loc" >Bill To Locations</a>
                          <a className={activeIndex === 5 ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#equipments"> Equipments</a>
                          <a className={activeIndex === 6 ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#doc" >Documents</a>
                          {/* <a className={accEl ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#history" >History</a>
                          <a className={accEl ? 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center active' : 'nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center'} data-toggle="pill" href="#logs" >Logs</a> */}
                        </div>
                      </div>
                      <div id="nav-tabContent" className="tab-content mt-2 px-2 mb-4">
                        <div id="account" className={activeIndex === 1 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="view-title px-2 mx-2">Primary Contact</div>
                          {serviceMessage ? <div className="errorCls px-2 mx-2">{serviceMessage}</div> : ''}
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="form-group  flex-1">
                              <label htmlFor="input_5">Email Address</label>
                              <input onBlur={() => this.checEmailOnClearBit()} name="email" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'email') }} value={email} type="email" onChange={(e) => this.setState({ email: e.target.value })} className="form-control" id="input_5" placeholder="" />
                              {emailErr ? <span className="errorCls">{emailErr}</span> : ''}
                            </div>
                            <div className="form-group flex-2">
                              <div className="select2-input">
                                <label htmlFor="report-type">Perfix</label>
                                <select value={prefix} onChange={(e) => this.setState({ prefix: e.target.value })} id="report-type" name="basic" className="form-control search-basic">
                                  <option value="Mr">Mr</option>
                                  <option value="Mrs">Mrs</option>
                                </select>
                                {prefixErr ? <span className="errorCls">{prefixErr}</span> : ''}
                              </div>
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="input_2">First Name</label>
                              <input name="firstName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={firstName} onChange={(e) => this.setState({ firstName: e.target.value })} type="text" className="form-control" id="input_2" placeholder="" />
                              {firstNameErr ? <span className="errorCls">{firstNameErr}</span> : ''}
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="input_3">Last Name</label>
                              <input name="lastName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={lastName} onChange={(e) => this.setState({ lastName: e.target.value })} type="text" className="form-control" id="input_3" placeholder="" />
                              {lastNameErr ? <span className="errorCls">{lastNameErr}</span> : ''}
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="input_4">Phone Number</label>
                              <input name="phone" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'phone') }} value={phone} onChange={(e) => this.setState({ phone: e.target.value })} type="text" className="form-control" id="input_4" placeholder="" />
                              {phoneErr ? <span className="errorCls">{phoneErr}</span> : ''}
                            </div>
                          </div>
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="form-group  flex-1">
                              <label htmlFor="input_1">Company Name</label>
                              <input name="companyName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={companyName} type="text" onChange={(e) => this.setState({ companyName: e.target.value })} className="form-control" id="input_1" placeholder="" />
                              {companyNameErr ? <span className="errorCls">{companyNameErr}</span> : ''}
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="input_6">Department</label>
                              <input name="department" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={department} type="text" onChange={(e) => this.setState({ department: e.target.value })} className="form-control" id="input_6" placeholder="" />
                              {departmentErr ? <span className="errorCls">{departmentErr}</span> : ''}

                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="input_7">Job Title</label>
                              <input name="jobTitle" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={jobTitle} type="text" onChange={(e) => this.setState({ jobTitle: e.target.value })} className="form-control" id="input_7" placeholder="" />
                              {jobTitleErr ? <span className="errorCls">{jobTitleErr}</span> : ''}

                            </div>
                            <div className="custom-control custom-checkbox flex-1 mt-4">
                              <input type="checkbox" className="custom-control-input" id="parent-account" />
                              <label className="custom-control-label" htmlFor="parent-account">Only Show Parent Accounts</label>
                            </div>
                          </div>

                          <div className="view-title px-2 mx-2 mt-4">Additional Information</div>
                          <div className="d-sm-flex justify-content-between align-items-center from-row">
                            <div className="form-group flex-1">
                              <label htmlFor="private-note">Internal/Private Notes</label>
                              <textarea name="privateNotes" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={privateNotes} onChange={(e) => this.setState({ privateNotes: e.target.value })} className="form-control" type="text" id="private-note" rows="4"></textarea>
                              {privateNotesErr ? <span className="errorCls">{privateNotesErr}</span> : ''}

                            </div>
                            <div className="form-group flex-1">
                              <label htmlFor="public-note">Public/Work Order Notes</label>
                              <textarea name="publicNotes" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={publicNotes} onChange={(e) => this.setState({ publicNotes: e.target.value })} className="form-control" type="text" id="public-note" rows="4"></textarea>
                              {publicNotesErr ? <span className="errorCls">{publicNotesErr}</span> : ''}

                            </div>
                          </div>
                          <div className="form-row w-100">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button id="addCustomerButton" onClick={(e) => this.onSubmit()} className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center"> Save Customer</button>
                            </div>
                          </div>
                        </div>
                        <div id="financial" className={activeIndex === 2 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="d-sm-flex justify-content-between">
                            <div className="mx-2 ">
                              <div className="view-title mb-0 px-2">Account Payment Terms</div>
                            </div>
                            <span onClick={() => this.addPaymentTerms()} className="btn bg-primary-gradient text-white btn-rounded">Add More</span>
                          </div>
                          {this.accountPaymentTerms()}
                          <div className="form-row w-100">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save Customer</button>
                            </div>
                          </div>
                        </div>
                        <div id="service" className={activeIndex === 3 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="mx-2 ">
                              <div className="view-title mb-0 px-2">Stored Service Locations</div>
                            </div>
                            <a href="#!" className="nav-item nav-link bg-primary-gradient btn-round my-2 my-sm-0 text-center text-white">Add Location</a>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="alias">Building Alias</label>
                                <input type="text" className="form-control" id="alias" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="str-addr">Street Address</label>
                                <input type="text" className="form-control" id="str-addr" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span ><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-method">Building Alias</label>
                                <input type="text" className="form-control" id="pay-method" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-terms">Street Address</label>
                                <input type="text" className="form-control" id="pay-terms" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span ><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-method">Building Alias</label>
                                <input type="text" className="form-control" id="pay-method" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-terms">Street Address</label>
                                <input type="text" className="form-control" id="pay-terms" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="form-row w-100">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                            </div>
                          </div>
                        </div>
                        <div id="bill-to-loc" className={activeIndex === 4 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="mx-2 ">
                              <div className="view-title mb-0 px-2">Bill to Alias</div>
                            </div>
                            <a href="#!" className="nav-item nav-link bg-primary-gradient btn-round my-2 my-sm-0 text-center text-white">Add Bill to Location</a>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span ><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="alias">Bill To Alias</label>
                                <input type="text" className="form-control" id="alias" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="str-addr">Street Address</label>
                                <input type="text" className="form-control" id="str-addr" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span ><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-method">Bill To Alias</label>
                                <input type="text" className="form-control" id="pay-method" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-terms">Street Address</label>
                                <input type="text" className="form-control" id="pay-terms" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="delete-option-sec">
                            <span className="delete-icon"><span ><i className="fas fa-trash-alt"></i></span></span>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-method">Bill To Alias</label>
                                <input type="text" className="form-control" id="pay-method" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="pay-terms">Street Address</label>
                                <input type="text" className="form-control" id="pay-terms" />
                              </div>
                            </div>
                            <div className="d-sm-flex justify-content-between align-items-center">
                              <div className="form-group  flex-1">
                                <label htmlFor="units">Apt/Suite/Unit#</label>
                                <input type="text" className="form-control" id="units" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="city">City</label>
                                <input type="text" className="form-control" id="city" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="state">State/Province</label>
                                <input type="text" className="form-control" id="state" />
                              </div>
                              <div className="form-group  flex-1">
                                <label htmlFor="postal-code">Zip/Postal Code</label>
                                <input type="number" className="form-control" id="postal-code" />
                              </div>
                            </div>
                          </div>
                          <div className="form-row w-100">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                            </div>
                          </div>
                        </div>
                        <div id="equipments" className={activeIndex === 5 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="mx-2 ">
                              <div className="view-title mb-0 px-2">Equipment</div>
                            </div>
                            <div className="d-flex justify-content-between align-items-center">
                              <a href="#!" className="nav-item nav-link bg-primary-gradient btn-round my-2 my-sm-0 text-center text-white mr-sm-2">Add Fields</a>
                              <a href="#!" className="nav-item nav-link bg-primary-gradient btn-round my-2 my-sm-0 text-center text-white">Add Equipment</a>
                            </div>
                          </div>
                          <div className="d-sm-flex justify-content-between align-items-center">
                            <div className="form-group  flex-1">
                              <label htmlFor="type">Type</label>
                              <input type="text" className="form-control" id="type" placeholder="" />
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="manufacturer">Manufacturer</label>
                              <input type="text" className="form-control" id="manufacturer" placeholder="" />
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="modal">Modal</label>
                              <input type="text" className="form-control" id="modal" placeholder="" />
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="sku">SKU</label>
                              <input type="number" className="form-control" id="sku" placeholder="" />
                            </div>
                            <div className="form-group  flex-1">
                              <label htmlFor="serial">Serial#</label>
                              <input type="number" className="form-control" id="serial" placeholder="" />
                            </div>
                          </div>
                          <div className="form-row w-100">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                            </div>
                          </div>
                        </div>
                        <div id="doc" className={activeIndex === 6 ? "tab-pane active" : "tab-pane"} role="tabpanel" aria-labelledby="request-tab">
                          <div className="mx-2 ">
                            <div className="view-title mb-0 px-2">Customer Document Manager</div>
                          </div>
                          <div className="row">
                            <div className="col-sm-7">
                              <div className="bg-light mt-3 h-100">
                                <div className="font-weight-bold h4 p-3 border-bottom mb-0"><i className="fas fa-file-download mr-2"></i> Stored Documents</div>
                                <div className="p-3">
                                  <div className="card-head-row nav nav-pills w-100 flex-nowrap" role="tablist">
                                    <button className="nav-item nav-link btn bg-light-gradient btn-round active my-2 my-sm-0 text-center" data-toggle="pill" href="#stored-docs" >Docs</button>
                                    <button className="nav-item nav-link btn bg-light-gradient btn-round my-2 my-sm-0 text-center" data-toggle="pill" href="#stored-imgs">Images</button>
                                  </div>
                                  <div className="tab-content mt-2 px-2">
                                    <div id="stored-docs" className="pr-2 tab-pane active" role="tabpanel" aria-labelledby="request-tab">
                                      <div className="form-group">
                                        <div className="row document-gallery">
                                          <a href="../assets/images/file-pdf.png" className="d-inline-block img-thumbnail alert-dark align-items-center d-flex justify-content-center border p-5 shadow m-3" target="_blank" download="myfile">
                                            <img className="img-fluid" src="../assets/images/file-pdf.png" alt="PDF" />
                                          </a>
                                          <a href="../assets/images/file-pdf.png" className="d-inline-block img-thumbnail alert-dark align-items-center d-flex justify-content-center border p-5 shadow m-3">
                                            <img className="img-fluid" src="../assets/images/file-pdf.png" alt="PDF" />
                                          </a>
                                          <a href="../assets/images/file-pdf.png" className="d-inline-block img-thumbnail alert-dark align-items-center d-flex justify-content-center border p-5 shadow m-3">
                                            <img className="img-fluid" src="../assets/images/file-pdf.png" alt="PDF" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <div id="stored-imgs" className="pr-2 tab-pane" role="tabpanel" aria-labelledby="request-tab">
                                      <div className="row image-gallery mt-4">
                                        <div className="col-6 col-md-3 mb-4">
                                          <a href="../assets/images/project-img1.png">
                                            <img alt='' src="../assets/images/project-img1.png" className="img-fluid shadow" />
                                          </a>
                                          <a href="#!" className="remove-img"><i className="fas fa-times-circle text-danger delete-img"></i></a>
                                        </div>
                                        <div className="col-6 col-md-3 mb-4">
                                          <a href="../assets/images/project-img1.png">
                                            <img alt='' src="../assets/images/project-img1.png" className="img-fluid shadow" />
                                          </a>
                                          <a href="#!" className="remove-img"><i className="fas fa-times-circle text-danger delete-img"></i></a>
                                        </div>
                                        <div className="col-6 col-md-3 mb-4">
                                          <a href="../assets/images/project-img1.png">
                                            <img alt='' src="../assets/images/project-img1.png" className="img-fluid shadow" />
                                          </a>
                                          <a href="#!" className="remove-img"><i className="fas fa-times-circle text-danger delete-img"></i></a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                </div>
                              </div>
                            </div>
                            <div className="col-sm-5">
                              <div className="bg-light mt-3 h-100">
                                <div className="font-weight-bold h4 p-3 border-bottom mb-0">
                                  <i className="fas fa-upload"></i> Add More Documents To This Customer's profile
                                                    </div>
                                <div className="form-group">
                                  <form action="upload.php" className="dropzone">
                                    <div className="dz-message" data-dz-message>
                                      <div className="icon">
                                        <i className="flaticon-file"></i>
                                      </div>
                                      <h4 className="message">Drag and Drop files here</h4>
                                      <div className="note">(This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)</div>
                                    </div>
                                    <div className="fallback">
                                      <input name="file" type="file" multiple />
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="form-row w-100 mt-3">
                            <div className="form-group create-quote-sec text-right my-4 w-100">
                              <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                              <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center"> Save Customer</button>
                            </div>
                          </div>
                        </div>

                        {/* <div id="history" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                          <div className="grid-container mt-3">
                            <div className="grid-item bg-light">
                              <div className="media">
                                <div className="d-flex  mr-3 bg-primary-gradient h-100 p-3 rounded" alt="icon_img">
                                  <i className="far fa-chart-bar display-4 text-white mb-0"></i>
                                </div>
                                <div className="media-body align-self-center">
                                  <div className="h5 mb-0 text-uppercase">Last 12 MONTHS</div>
                                  <div className="h1 font-weight-bold mb-0">$7849.00</div>
                                </div>
                              </div>
                            </div>
                            <div className="grid-item bg-light">
                              <div className="media">
                                <div className="d-flex  mr-3 bg-primary-gradient h-100 p-3 rounded" alt="icon_img">
                                  <i className="far fa-chart-bar display-4 text-white mb-0"></i>
                                </div>
                                <div className="media-body align-self-center">
                                  <div className="h5 mb-0 text-uppercase">Last 12 MONTHS</div>
                                  <div className="h1 font-weight-bold mb-0">$7849.00</div>
                                </div>
                              </div>
                            </div>
                            <div className="grid-item bg-light">
                              <div className="media">
                                <div className="d-flex  mr-3 bg-primary-gradient h-100 p-3 rounded" alt="icon_img">
                                  <i className="far fa-chart-bar display-4 text-white mb-0"></i>
                                </div>
                                <div className="media-body align-self-center">
                                  <div className="h5 mb-0 text-uppercase">Last 12 MONTHS</div>
                                  <div className="h1 font-weight-bold mb-0">$7849.00</div>
                                </div>
                              </div>
                            </div>
                            <div className="grid-item bg-light">
                              <div className="media">
                                <div className="d-flex  mr-3 bg-primary-gradient h-100 p-3 rounded" alt="icon_img">
                                  <i className="far fa-chart-bar display-4 text-white mb-0"></i>
                                </div>
                                <div className="media-body align-self-center">
                                  <div className="h5 mb-0 text-uppercase">Last 12 MONTHS</div>
                                  <div className="h1 font-weight-bold mb-0">$7849.00</div>
                                </div>
                              </div>
                            </div>
                            <div className="grid-item bg-light">
                              <div className="media">
                                <div className="d-flex  mr-3 bg-primary-gradient h-100 p-3 rounded" alt="icon_img">
                                  <i className="far fa-chart-bar display-4 text-white mb-0"></i>
                                </div>
                                <div className="media-body align-self-center">
                                  <div className="h5 mb-0 text-uppercase">Last 12 MONTHS</div>
                                  <div className="h1 font-weight-bold mb-0">$7849.00</div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <hr />
                          <div className="py-3">
                            <div className="card-head-row nav nav-pills w-100 flex-nowrap" role="tablist">
                              <a className="nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center" data-toggle="pill" href="#work-order" >Work Order</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#quotes" >Quotes</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#projects_tab" >projects</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#reports-tab" >Reports</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#invoices" >Invoices</a>
                            </div>
                            <div className="tab-content mt-2 mb-4 work-orders-sec management-list">
                              <div id="work-order" className="tab-pane active" role="tabpanel" aria-labelledby="request-tab">
                                <div className="overflow-hidden">
                                  <div className="pr-1">
                                    <div className="overflow-scroll">
                                      <table className="table w-100 work-orders-table">
                                        <tbody>
                                          <tr className="report-row-block work-order shadow-sm">
                                            <td>
                                              <div className="row-style bg-info"><span><i className="fas fa-tag text-white"></i></span></div>
                                              <div className="report-sec order-work pl-4 ml-2">
                                                <small className="d-block light-color">Work Order #</small>
                                                <p className="mb-0">EM 223-2019-300</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-name">
                                                <small className="d-block light-color">Bill to Name</small>
                                                <p className="mb-0">Centerpoint Property</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services-name">
                                                <small className="d-block light-color">Service Location Name</small>
                                                <p className="mb-0">Centerpoint Properties</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services">
                                                <small className="d-block light-color">Service Location</small>
                                                <p className="mb-0">*786 Wallisville Rd., Houston,Texas, 77029</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec contractor-name">
                                                <small className="d-block light-color">Date Completed</small>
                                                <p className="mb-0">11-02-2019 </p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec tags">
                                                <small className="d-block light-color">Tags Left on System</small>
                                                <p className="mb-0">N/A</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-date">
                                                <small className="d-block light-color">Billed Date</small>
                                                <p className="mb-0">11/02/2019 <span className="bg-success-gradient bill-pay">Paid</span></p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec action text-center">
                                                <small className="d-block light-color">Action</small>
                                                <div className="mb-0 dropdown">
                                                  <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i className="fas fa-ellipsis-v"></i>
                                                  </a>
                                                  <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="bottom-start" style={{ "position": "absolute", "transform": "translate3d(18px, 18px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                                    <a className="dropdown-item" href="#!"><i className="far fa-eye"></i> View Work Order</a>
                                                    <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr className="report-row-block work-order shadow-sm">
                                            <td>
                                              <div className="row-style bg-warning"><a href="#"><i className="fas fa-tag text-white"></i></a></div>
                                              <div className="report-sec order-work pl-4 ml-2">
                                                <small className="d-block light-color">Work Order #</small>
                                                <p className="mb-0">EM 223-2019-300</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-name">
                                                <small className="d-block light-color">Bill to Name</small>
                                                <p className="mb-0">Centerpoint Property</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services-name">
                                                <small className="d-block light-color">Service Location Name</small>
                                                <p className="mb-0">Centerpoint Properties</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services">
                                                <small className="d-block light-color">Service Location</small>
                                                <p className="mb-0">*786 Wallisville Rd., Houston,Texas, 77029</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec contractor-name">
                                                <small className="d-block light-color">Date Completed</small>
                                                <p className="mb-0">11-02-2019 </p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec tags">
                                                <small className="d-block light-color">Tags Left on System</small>
                                                <p className="mb-0">N/A</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-date">
                                                <small className="d-block light-color">Billed Date</small>
                                                <p className="mb-0">11/02/2019 <span className="bg-danger-gradient bill-pay">Past Due</span></p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec action text-center">
                                                <small className="d-block light-color">Action</small>
                                                <div className="mb-0 dropdown">
                                                  <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i className="fas fa-ellipsis-v"></i>
                                                  </a>
                                                  <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                    <a className="dropdown-item" href="#!"><i className="far fa-eye"></i> View Work Order</a>
                                                    <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr className="report-row-block work-order shadow-sm">
                                            <td>
                                              <div className="row-style bg-danger"><a href="#"><i className="fas fa-tag text-white"></i></a></div>
                                              <div className="report-sec order-work pl-4 ml-2">
                                                <small className="d-block light-color">Work Order #</small>
                                                <p className="mb-0">EM 223-2019-300</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-name">
                                                <small className="d-block light-color">Bill to Name</small>
                                                <p className="mb-0">Centerpoint Property</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services-name">
                                                <small className="d-block light-color">Service Location Name</small>
                                                <p className="mb-0">Centerpoint Properties</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec services">
                                                <small className="d-block light-color">Service Location</small>
                                                <p className="mb-0">*786 Wallisville Rd., Houston,Texas, 77029</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec contractor-name">
                                                <small className="d-block light-color">Date Completed</small>
                                                <p className="mb-0">11-02-2019 </p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec tags">
                                                <small className="d-block light-color">Tags Left on System</small>
                                                <p className="mb-0">N/A</p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec bill-date">
                                                <small className="d-block light-color">Billed Date</small>
                                                <p className="mb-0">11/02/2019 <span className="bg-danger-gradient bill-pay">Past Due</span></p>
                                              </div>
                                            </td>
                                            <td>
                                              <div className="report-sec action text-center">
                                                <small className="d-block light-color">Action</small>
                                                <div className="mb-0 dropdown">
                                                  <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i className="fas fa-ellipsis-v"></i>
                                                  </a>
                                                  <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                    <a className="dropdown-item" href="#!"><i className="far fa-eye"></i> View Work Order</a>
                                                    <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                    <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                  </div>
                                                </div>
                                              </div>
                                            </td>
                                          </tr>

                                        </tbody>
                                      </table>
                                    </div>
                                  </div>
                                  <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                    <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                    <nav aria-label="...">
                                      <ul className="pagination mb-0">
                                        <li className="page-item  "><a className="page-link" href="#!" tabIndex="-1">Previous</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">1</a></li>
                                        <li className="page-item active"><a className="page-link" href="#!">2 <span className="sr-only">(current)</span></a></li>
                                        <li className="page-item"><a className="page-link" href="#!">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">Next</a></li>
                                      </ul>
                                    </nav>
                                  </div>
                                </div>

                              </div>
                              <div id="quotes" className="tab-pane list-quotes-info" role="tabpanel" aria-labelledby="request-tab">
                                <div className="pr-2 overflow-scroll">
                                  <table className="table w-100">
                                    <tbody>
                                      <tr className="list-row-block quotes-list shadow-sm hover">
                                        <td>
                                          <div className="date-tag"><i className="fas fa-calendar-alt"></i> <span>09/02/2019</span></div>
                                          <div className="list-sec list-1 border-success address">
                                            <div className="d-block h6 mt-2 mb-0 light-color">Amount Total</div>
                                            <div className="mb-0 h1">$9,326.00</div>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection">
                                            <small className="d-block light-color">Report/Quote Name</small>
                                            <p className="mb-0">Annual Fire Alarm</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection-date">
                                            <small className="d-block light-color">Bill to</small>
                                            <p className="mb-0">Century Fire Protection</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspector-name">
                                            <small className="d-block light-color">Company Name</small>
                                            <p className="mb-0">Firepro Tech, lic</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec contractor-name">
                                            <small className="d-block light-color">PO#</small>
                                            <p className="mb-0">-</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec authority">
                                            <small className="d-block light-color">Signature Name</small>
                                            <p className="mb-0">Carlos Meledez</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec status">
                                            <small className="d-block light-color">Quote Date</small>
                                            <p className="mb-0 d-flex justify-content-between">04/06/2019 </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 dropdown-list-items" id="dropdown-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="view-quote.html"><i className="far fa-eye"></i> View Detail</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="list-row-block quotes-list shadow-sm">
                                        <td>
                                          <div className="date-tag"><i className="fas fa-calendar-alt"></i> <span>09/02/2019</span></div>
                                          <div className="list-sec list-1 border-success address">
                                            <div className="d-block h6 mt-2 mb-0 light-color">Amount Total</div>
                                            <div className="mb-0 h1">$9,326.00</div>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection">
                                            <small className="d-block light-color">Report/Quote Name</small>
                                            <p className="mb-0">Annual Fire Alarm</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection-date">
                                            <small className="d-block light-color">Bill to</small>
                                            <p className="mb-0">Century Fire Protection</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspector-name">
                                            <small className="d-block light-color">Company Name</small>
                                            <p className="mb-0">Firepro Tech, lic</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec contractor-name">
                                            <small className="d-block light-color">PO#</small>
                                            <p className="mb-0">-</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec authority">
                                            <small className="d-block light-color">Signature Name</small>
                                            <p className="mb-0">Carlos Meledez</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec status">
                                            <small className="d-block light-color">Quote Date</small>
                                            <p className="mb-0 d-flex justify-content-between">04/06/2019 </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 dropdown-list-items" id="dropdown-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="view-quote.html"><i className="far fa-eye"></i> View Detail</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="list-row-block quotes-list shadow-sm">
                                        <td>
                                          <div className="date-tag"><i className="fas fa-calendar-alt"></i> <span>09/02/2019</span></div>
                                          <div className="list-sec list-1 border-success address">
                                            <div className="d-block h6 mt-2 mb-0 light-color">Amount Total</div>
                                            <div className="mb-0 h1">$9,326.00</div>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection">
                                            <small className="d-block light-color">Report/Quote Name</small>
                                            <p className="mb-0">Annual Fire Alarm</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection-date">
                                            <small className="d-block light-color">Bill to</small>
                                            <p className="mb-0">Century Fire Protection</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspector-name">
                                            <small className="d-block light-color">Company Name</small>
                                            <p className="mb-0">Firepro Tech, lic</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec contractor-name">
                                            <small className="d-block light-color">PO#</small>
                                            <p className="mb-0">-</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec authority">
                                            <small className="d-block light-color">Signature Name</small>
                                            <p className="mb-0">Carlos Meledez</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec status">
                                            <small className="d-block light-color">Quote Date</small>
                                            <p className="mb-0 d-flex justify-content-between">04/06/2019 </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 dropdown-list-items" id="dropdown-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="view-quote.html"><i className="far fa-eye"></i> View Detail</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="list-row-block quotes-list shadow-sm">
                                        <td>
                                          <div className="date-tag"><i className="fas fa-calendar-alt"></i> <span>09/02/2019</span></div>
                                          <div className="list-sec list-1 border-success address">
                                            <div className="d-block h6 mt-2 mb-0 light-color">Amount Total</div>
                                            <div className="mb-0 h1">$9,326.00</div>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection">
                                            <small className="d-block light-color">Report/Quote Name</small>
                                            <p className="mb-0">Annual Fire Alarm</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection-date">
                                            <small className="d-block light-color">Bill to</small>
                                            <p className="mb-0">Century Fire Protection</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspector-name">
                                            <small className="d-block light-color">Company Name</small>
                                            <p className="mb-0">Firepro Tech, lic</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec contractor-name">
                                            <small className="d-block light-color">PO#</small>
                                            <p className="mb-0">-</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec authority">
                                            <small className="d-block light-color">Signature Name</small>
                                            <p className="mb-0">Carlos Meledez</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec status">
                                            <small className="d-block light-color">Quote Date</small>
                                            <p className="mb-0 d-flex justify-content-between">04/06/2019 </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 dropdown-list-items" id="dropdown-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="view-quote.html"><i className="far fa-eye"></i> View Detail</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="list-row-block quotes-list shadow-sm">
                                        <td>
                                          <div className="date-tag"><i className="fas fa-calendar-alt"></i> <span>09/02/2019</span></div>
                                          <div className="list-sec list-1 border-success address">
                                            <div className="d-block h6 mt-2 mb-0 light-color">Amount Total</div>
                                            <div className="mb-0 h1">$9,326.00</div>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection">
                                            <small className="d-block light-color">Report/Quote Name</small>
                                            <p className="mb-0">Annual Fire Alarm</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspection-date">
                                            <small className="d-block light-color">Bill to</small>
                                            <p className="mb-0">Century Fire Protection</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec inspector-name">
                                            <small className="d-block light-color">Company Name</small>
                                            <p className="mb-0">Firepro Tech, lic</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec contractor-name">
                                            <small className="d-block light-color">PO#</small>
                                            <p className="mb-0">-</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec authority">
                                            <small className="d-block light-color">Signature Name</small>
                                            <p className="mb-0">Carlos Meledez</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec status">
                                            <small className="d-block light-color">Quote Date</small>
                                            <p className="mb-0 d-flex justify-content-between">04/06/2019 </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="list-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 dropdown-list-items" id="dropdown-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="view-quote.html"><i className="far fa-eye"></i> View Detail</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>

                                  <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                    <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                    <nav aria-label="...">
                                      <ul className="pagination mb-0">
                                        <li className="page-item disabled"><a className="page-link" href="#!" tabIndex="-1">Previous</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">1</a></li>
                                        <li className="page-item active"><a className="page-link" href="#!">2 <span className="sr-only">(current)</span></a></li>
                                        <li className="page-item"><a className="page-link" href="#!">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">Next</a></li>
                                      </ul>
                                    </nav>
                                  </div>
                                </div>
                              </div>
                              <div id="projects_tab" className="tab-pane list-quotes-info" role="tabpanel" aria-labelledby="request-tab">
                                <div className="row row-projects">
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 1" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Play Golf</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_customer"><i className="fas fa-users"></i> Assign Customer</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_tech"><i className="fas fa-user-cog"></i> Assign Technician</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_project"><i className="fas fa-user-tie"></i> Assign Project Manager</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Yesterday 3:12 AM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 7" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Brainstorming</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Monday 1:32 AM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 3" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Wisata Alam</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Monday 3:30 AM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 4" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Analysis</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Sunday 3:12 PM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 5" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Meeting</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Friday 1:12 PM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 8" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Analysis</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Yesterday 3:12 AM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div className="col-sm-6 col-lg-3">
                                    <div className="card">
                                      <div className="p-2">
                                        <img className="card-img-top rounded" src="../assets/images/project-img1.png" alt="Product 2" />
                                      </div>
                                      <div className="card-body pt-2">
                                        <div className="d-sm-flex flex-wrap w-100 justify-content-between align-items-center">
                                          <h4 className="mb-1 fw-bold">Analysis</h4>
                                          <div className="mb-0 dropdown">
                                            <a href="#" className="mb-0" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-report-list" x-placement="top-start" style={{ "position": "absolute", "transform": "translate3d(30px, -1px, 0px)", "top": "0px", "left": "0px", "willChange": "transform" }}>
                                              <a className="dropdown-item" href="projects-view.html"><i className="far fa-eye"></i> View</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                              <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                              <a className="dropdown-item" href="#!" data-toggle="modal" data-target="#assign_user"><i className="fas fa-users"></i> Assign Users</a>
                                            </div>
                                          </div>
                                        </div>
                                        <p className="text-muted small mb-2">Last updated: Friday 7:45 AM</p>
                                        <div className="avatar-group">
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <img src="https://via.placeholder.com/50x50" alt="..." className="avatar-img rounded-circle border border-white" />
                                          </div>
                                          <div className="avatar avatar-sm">
                                            <a className="avatar-title rounded-circle border border-white bg-primary-gradient text-white" href="#!" data-toggle="modal" data-target="#assign_user">+</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div className="form-row w-100">
                                  <div className="form-group create-quote-sec text-right my-4 w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button  className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                              <div id="reports-tab" className="tab-pane list-quotes-info report-sec" role="tabpanel" aria-labelledby="request-tab">
                                <div className="pr-2 overflow-scroll">
                                  <table className="table w-100 report-list-info">
                                    <tbody>
                                      <tr className="report-row-block work-order shadow-sm">
                                        <td>
                                          <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                          <div className="report-sec address pl-4 ml-2">
                                            <small className="d-block light-color">Building Address</small>
                                            <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection">
                                            <small className="d-block light-color">Type of Inspection</small>
                                            <p className="mb-0">Annual Fire Sprinkler</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection-date">
                                            <small className="d-block light-color">Inspection Date</small>
                                            <p className="mb-0">05-02-2019</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspector-name">
                                            <small className="d-block light-color">Inspection Name</small>
                                            <p className="mb-0">Chris Dyer</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec contractor-name">
                                            <small className="d-block light-color">Contractor Name</small>
                                            <p className="mb-0">Daniel Grey</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec authority">
                                            <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                            <p className="mb-0">Harris County</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec status">
                                            <small className="d-block light-color">Inspection Status</small>
                                            <p className="mb-0 d-flex justify-content-between">Current </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec notification">
                                            <small className="d-block light-color">Impairment Notification Status</small>
                                            <p className="mb-0 d-flex justify-content-between">N/A </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-signature"></i> Work Order</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-contract"></i> Create Quote</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-invoice"></i> Inspection Status</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="report-row-block work-order shadow-sm">
                                        <td>
                                          <div className="row-style bg-warning"><i className="fas fa-tag"></i></div>
                                          <div className="report-sec address pl-4 ml-2">
                                            <small className="d-block light-color">Building Address</small>
                                            <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection">
                                            <small className="d-block light-color">Type of Inspection</small>
                                            <p className="mb-0">Annual Fire Sprinkler</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection-date">
                                            <small className="d-block light-color">Inspection Date</small>
                                            <p className="mb-0">05-02-2019</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspector-name">
                                            <small className="d-block light-color">Inspection Name</small>
                                            <p className="mb-0">Chris Dyer</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec contractor-name">
                                            <small className="d-block light-color">Contractor Name</small>
                                            <p className="mb-0">Daniel Grey</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec authority">
                                            <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                            <p className="mb-0">Harris County</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec status">
                                            <small className="d-block light-color">Inspection Status</small>
                                            <p className="mb-0 d-flex justify-content-between">Current </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec notification">
                                            <small className="d-block light-color">Impairment Notification Status</small>
                                            <p className="mb-0 d-flex justify-content-between">N/A </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-signature"></i> Work Order</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-contract"></i> Create Quote</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-invoice"></i> Inspection Status</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="report-row-block work-order shadow-sm">
                                        <td>
                                          <div className="row-style bg-danger"><i className="fas fa-tag"></i></div>
                                          <div className="report-sec address pl-4 ml-2">
                                            <small className="d-block light-color">Building Address</small>
                                            <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection">
                                            <small className="d-block light-color">Type of Inspection</small>
                                            <p className="mb-0">Annual Fire Sprinkler</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection-date">
                                            <small className="d-block light-color">Inspection Date</small>
                                            <p className="mb-0">05-02-2019</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspector-name">
                                            <small className="d-block light-color">Inspection Name</small>
                                            <p className="mb-0">Chris Dyer</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec contractor-name">
                                            <small className="d-block light-color">Contractor Name</small>
                                            <p className="mb-0">Daniel Grey</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec authority">
                                            <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                            <p className="mb-0">Harris County</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec status">
                                            <small className="d-block light-color">Inspection Status</small>
                                            <p className="mb-0 d-flex justify-content-between">Current </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec notification">
                                            <small className="d-block light-color">Impairment Notification Status</small>
                                            <p className="mb-0 d-flex justify-content-between">N/A </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-signature"></i> Work Order</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-contract"></i> Create Quote</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-invoice"></i> Inspection Status</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="report-row-block work-order shadow-sm">
                                        <td>
                                          <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                          <div className="report-sec address pl-4 ml-2">
                                            <small className="d-block light-color">Building Address</small>
                                            <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection">
                                            <small className="d-block light-color">Type of Inspection</small>
                                            <p className="mb-0">Annual Fire Sprinkler</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection-date">
                                            <small className="d-block light-color">Inspection Date</small>
                                            <p className="mb-0">05-02-2019</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspector-name">
                                            <small className="d-block light-color">Inspection Name</small>
                                            <p className="mb-0">Chris Dyer</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec contractor-name">
                                            <small className="d-block light-color">Contractor Name</small>
                                            <p className="mb-0">Daniel Grey</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec authority">
                                            <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                            <p className="mb-0">Harris County</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec status">
                                            <small className="d-block light-color">Inspection Status</small>
                                            <p className="mb-0 d-flex justify-content-between">Current </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec notification">
                                            <small className="d-block light-color">Impairment Notification Status</small>
                                            <p className="mb-0 d-flex justify-content-between">N/A </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-signature"></i> Work Order</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-contract"></i> Create Quote</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-invoice"></i> Inspection Status</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr className="report-row-block work-order shadow-sm">
                                        <td>
                                          <div className="row-style bg-info"><i className="fas fa-tag"></i></div>
                                          <div className="report-sec address pl-4 ml-2">
                                            <small className="d-block light-color">Building Address</small>
                                            <p className="mb-0">1410 Williams Way Blvd (West Tower)</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection">
                                            <small className="d-block light-color">Type of Inspection</small>
                                            <p className="mb-0">Annual Fire Sprinkler</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspection-date">
                                            <small className="d-block light-color">Inspection Date</small>
                                            <p className="mb-0">05-02-2019</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec inspector-name">
                                            <small className="d-block light-color">Inspection Name</small>
                                            <p className="mb-0">Chris Dyer</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec contractor-name">
                                            <small className="d-block light-color">Contractor Name</small>
                                            <p className="mb-0">Daniel Grey</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec authority">
                                            <small className="d-block light-color">Anthority Having Jurisdiction</small>
                                            <p className="mb-0">Harris County</p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec status">
                                            <small className="d-block light-color">Inspection Status</small>
                                            <p className="mb-0 d-flex justify-content-between">Current </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec notification">
                                            <small className="d-block light-color">Impairment Notification Status</small>
                                            <p className="mb-0 d-flex justify-content-between">N/A </p>
                                          </div>
                                        </td>
                                        <td>
                                          <div className="report-sec action text-center">
                                            <small className="d-block light-color">Action</small>
                                            <div className="mb-0 dropdown">
                                              <a href="#" className="mb-0 rounded-circle" id="dropdown-report-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i className="fas fa-ellipsis-v"></i>
                                              </a>
                                              <div className="dropdown-menu" aria-labelledby="dropdown-report-list">
                                                <a className="dropdown-item" href="#!"><i className="far fa-file-pdf"></i> Download PDF</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-edit"></i> Edit</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-trash-alt"></i> Delete</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-signature"></i> Work Order</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-contract"></i> Create Quote</a>
                                                <a className="dropdown-item" href="#!"><i className="fas fa-file-invoice"></i> Inspection Status</a>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>

                                  <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                    <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                    <nav aria-label="...">
                                      <ul className="pagination mb-0">
                                        <li className="page-item disabled"><a className="page-link" href="#!" tabIndex="-1">Previous</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">1</a></li>
                                        <li className="page-item active"><a className="page-link" href="#!">2 <span className="sr-only">(current)</span></a></li>
                                        <li className="page-item"><a className="page-link" href="#!">3</a></li>
                                        <li className="page-item"><a className="page-link" href="#!">Next</a></li>
                                      </ul>
                                    </nav>
                                  </div>
                                </div>
                              </div>
                              <div id="invoices" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                                <div className="pr-2 overflow-scroll">
                                  <table className="table mb-3">
                                    <thead className="thead-light">
                                      <tr>
                                        <th scope="col">Date</th>
                                        <th scope="col">Customer</th>
                                        <th scope="col">Invoices#</th>
                                        <th scope="col">WO#</th>
                                        <th scope="col">PO</th>
                                        <th scope="col">Terms</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Total</th>
                                        <th scope="col">Total Due</th>
                                        <th scope="col">Action</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>09-07-2019</td>
                                        <td>Morbi leo risus</td>
                                        <td>1000</td>
                                        <td>EM 222-232-4332</td>
                                        <td></td>
                                        <td>DUR</td>
                                        <td><span className="bg-success-gradient bill-pay">Paid</span></td>
                                        <td>$343.99</td>
                                        <td>$343.88</td>
                                        <td>
                                          <div className="mb-0 dropdown text-center">
                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                              <a className="dropdown-item" href="#"><i className="far fa-eye"></i> View </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-edit"></i> Edit </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-file-pdf"></i> PDF </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-print"></i> Print </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>09-07-2019</td>
                                        <td>Morbi leo risus</td>
                                        <td>1000</td>
                                        <td>EM 222-232-4332</td>
                                        <td></td>
                                        <td>DUR</td>
                                        <td><span className="bg-danger-gradient bill-pay">Past Due</span></td>
                                        <td>$343.99</td>
                                        <td>$343.88</td>
                                        <td>
                                          <div className="mb-0 dropdown text-center">
                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                              <a className="dropdown-item" href="#"><i className="far fa-eye"></i> View </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-edit"></i> Edit </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-file-pdf"></i> PDF </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-print"></i> Print </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>09-07-2019</td>
                                        <td>Morbi leo risus</td>
                                        <td>1000</td>
                                        <td>EM 222-232-4332</td>
                                        <td></td>
                                        <td>DUR</td>
                                        <td><span className="bg-success-gradient bill-pay">Paid</span></td>
                                        <td>$343.99</td>
                                        <td>$343.88</td>
                                        <td>
                                          <div className="mb-0 dropdown text-center">
                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                              <a className="dropdown-item" href="#"><i className="far fa-eye"></i> View </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-edit"></i> Edit </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-file-pdf"></i> PDF </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-print"></i> Print </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>09-07-2019</td>
                                        <td>Morbi leo risus</td>
                                        <td>1000</td>
                                        <td>EM 222-232-4332</td>
                                        <td></td>
                                        <td>DUR</td>
                                        <td><span className="bg-success-gradient bill-pay">Paid</span></td>
                                        <td>$343.99</td>
                                        <td>$343.88</td>
                                        <td>
                                          <div className="mb-0 dropdown text-center">
                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                              <a className="dropdown-item" href="#"><i className="far fa-eye"></i> View </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-edit"></i> Edit </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-file-pdf"></i> PDF </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-print"></i> Print </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>09-07-2019</td>
                                        <td>Morbi leo risus</td>
                                        <td>1000</td>
                                        <td>EM 222-232-4332</td>
                                        <td></td>
                                        <td>DUR</td>
                                        <td><span className="bg-success-gradient bill-pay">Paid</span></td>
                                        <td>$343.99</td>
                                        <td>$343.88</td>
                                        <td>
                                          <div className="mb-0 dropdown text-center">
                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              <i className="fas fa-ellipsis-v"></i>
                                            </a>
                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                              <a className="dropdown-item" href="#"><i className="far fa-eye"></i> View </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-edit"></i> Edit </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-file-pdf"></i> PDF </a>
                                              <a className="dropdown-item" href="#"><i className="fas fa-print"></i> Print </a>
                                            </div>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </div>
                                <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                  <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                  <nav aria-label="...">
                                    <ul className="pagination mb-0">
                                      <li className="page-item disabled"><a className="page-link" href="#!" tabIndex="-1">Previous</a></li>
                                      <li className="page-item"><a className="page-link" href="#!">1</a></li>
                                      <li className="page-item active"><a className="page-link" href="#!">2 <span className="sr-only">(current)</span></a></li>
                                      <li className="page-item"><a className="page-link" href="#!">3</a></li>
                                      <li className="page-item"><a className="page-link" href="#!">Next</a></li>
                                    </ul>
                                  </nav>
                                </div>
                                <div className="form-row w-100">
                                  <div className="form-group create-quote-sec text-right my-4 w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div id="logs" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                          <div className="mx-2 ">
                            <div className="view-title mb-0 px-2">Customer Logs</div>
                          </div>
                          <div className="py-3">
                            <div className="card-head-row nav nav-pills w-100 flex-nowrap" role="tablist">
                              <a className="nav-item nav-link btn bg-light-gradient active my-2 my-sm-0 text-center" data-toggle="pill" href="#email-msg" >Email Messages</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#text-msg" >Text Messages</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#call-logs" >Call Logs</a>
                              <a className="nav-item nav-link btn bg-light-gradient my-2 my-sm-0 text-center" data-toggle="pill" href="#chat" >Chat</a>
                            </div>
                            <div className="tab-content mt-2 mb-4">
                              <div id="email-msg" className="tab-pane active" role="tabpanel" aria-labelledby="request-tab" >
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="assign-to" placeholder="Email Address" />
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="work-order" placeholder="Enter Order no." />
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date" name="datepicker" placeholder="Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control timepicker" id="time" name="timepicker" placeholder="Time" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-clock"></i>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <input type="text" className="form-control" id="email-status" placeholder="Status" />
                                  </div>
                                  <div className="form-group flex-1">
                                    <textarea type="text" className="form-control" id="email-message" placeholder="Message" rows="3"></textarea>
                                  </div>
                                </div>
                                <div className="form-row w-100 mt-3">
                                  <div className="form-group create-quote-sec text-right w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                              <div id="text-msg" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date" name="datepicker" placeholder="Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control timepicker" id="time" name="timepicker" placeholder="Time" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-clock"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-check flex-1">
                                    <div className="d-flex justify-content-around align-items-center h-100">
                                      <label className="form-radio-label mb-2 mb-sm-0">
                                        <input className="form-radio-input" type="radio" name="optionsRadios" defaultChecked="" />
                                        <span className="form-radio-sign">Inbound</span>
                                      </label>
                                      <label className="form-radio-label ml-3 mb-0">
                                        <input className="form-radio-input" type="radio" name="optionsRadios" />
                                        <span className="form-radio-sign">Outbound</span>
                                      </label>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="assign-to" placeholder="From Date" />
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="work-order" placeholder="To Date" />
                                    </div>
                                  </div>
                                </div>
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <input type="text" className="form-control" id="email-status" placeholder="Status" />
                                  </div>
                                  <div className="form-group flex-1">
                                    <textarea type="text" className="form-control" id="email-message" placeholder="Message" rows="3"></textarea>
                                  </div>
                                </div>
                                <div className="form-row w-100 mt-3">
                                  <div className="form-group create-quote-sec text-right w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                              <div id="call-logs" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date-from" name="datepicker" placeholder="From Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date-to" name="datepicker" placeholder="To Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date" name="datepicker" placeholder="Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control timepicker" id="time" name="timepicker" placeholder="Time" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-clock"></i>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <input type="text" className="form-control" id="call-duration" placeholder="Call Duration" />
                                  </div>
                                  <div className="form-group flex-1">
                                    <input type="text" className="form-control" id="status" placeholder="Outgoing" />
                                  </div>
                                  <div className="form-group flex-1">
                                    <textarea type="text" className="form-control" id="details" placeholder="Comments" rows="3"></textarea>
                                  </div>
                                </div>
                                <div className="form-row w-100 mt-3">
                                  <div className="form-group create-quote-sec text-right w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                              <div id="chat" className="tab-pane" role="tabpanel" aria-labelledby="request-tab">
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control datepicker" id="date" name="datepicker" placeholder="Date" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-calendar-alt"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control timepicker" id="time" name="timepicker" placeholder="Time" />
                                      <span className="input-icon-addon">
                                        <i className="far fa-clock"></i>
                                      </span>
                                    </div>
                                  </div>
                                  <div className="form-check flex-1">
                                    <div className="d-flex justify-content-around align-items-center h-100">
                                      <label className="form-radio-label mb-2 mb-sm-0">
                                        <input className="form-radio-input" type="radio" name="optionsRadios" defaultChecked="" />
                                        <span className="form-radio-sign">Inbound</span>
                                      </label>
                                      <label className="form-radio-label ml-3 mb-0">
                                        <input className="form-radio-input" type="radio" name="optionsRadios" />
                                        <span className="form-radio-sign">Outbound</span>
                                      </label>
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="assign-to" placeholder="From Date" />
                                    </div>
                                  </div>
                                  <div className="form-group flex-1">
                                    <div className="input-icon">
                                      <input type="text" className="form-control" id="work-order" placeholder="To Date" />
                                    </div>
                                  </div>
                                </div>
                                <div className="d-sm-flex justify-content-between form-row">
                                  <div className="form-group flex-1">
                                    <input type="text" className="form-control" id="email-status" placeholder="Status" />
                                  </div>
                                  <div className="form-group flex-1">
                                    <textarea type="text" className="form-control" id="email-message" placeholder="Message" rows="3"></textarea>
                                  </div>
                                </div>
                                <div className="form-row w-100 mt-3">
                                  <div className="form-group create-quote-sec text-right w-100">
                                    <button className="btn-cost btn-lg btn-dark btn-square text-white text-center"> Cancel </button>
                                    <button className="btn-cost btn-lg bg-primary-gradient btn-square text-white text-center">Save</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      */}

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const mapStateToprops = (state) => ({
  addCustomerData: addCustomerData(state),
  checkEmailClearbit: clearBitData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addCustomer,
  emailCheckClearbit
}, dispatch);

export const AddCustomer = connect(mapStateToprops, mapDispatchToProps)(AddCustomerComponent)
