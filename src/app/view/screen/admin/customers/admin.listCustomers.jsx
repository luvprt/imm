import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'

export class ListCustomers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wrapperCls: false,
      wrapperClsMobile: false,
    }
  }

  render() {
    const { wrapperCls, wrapperClsMobile } = this.state;

    return (
      <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
        <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
        <Sidebar getMainRoute={'customers'} getInnerRoute={'list-customers'} />
        <div className="main-panel">
            <div className="content">
                <div className="panel-header bg-primary-gradient">
                    <div className="page-inner py-5">
                        <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                            <div>
                                <h2 className="text-white pb-2 fw-bold">Customers Management</h2>
                                <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="page-inner mt--5">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="card assets-management-sec full-height">
                                <div className="card-header d-flex justify-content-between align-items-center py-4">
                                    <div className="card-head-row">
                                        <div className="card-title font-weight-bold">Customers List</div>
                                    </div>
                                    <div className="search d-none d-sm-inline-block">
                                        <a href="#" className="btn-lg btn-border btn-primary btn-round"><i className="far fa-plus-square mr-1"></i> Add Customer</a>
                                    </div>

                                    <div className="search d-sm-none">
                                        <a data-toggle="collapse" href="#request-btn" aria-expanded="false" aria-controls="search-block-sec" className="h3 mb-0"><i className="fas fa-plus-square mr-1 text-color-gradient"></i> </a>
                                    </div>
                                </div>
                                <div id="request-btn" className="search-info collapse fade text-center">
                                    <a href="#" className="d-inline-block my-3 mx-auto btn-lg btn-border btn-primary btn-round"><i className="far fa-plus-square mr-1"></i> Add Customer</a>
                                </div>

                                <div className="card-body management-list">
                                    <div className="d-sm-flex flex-wrap justify-content-end align-items-center mb-3">
                                        <div className="d-flex justify-content-between align-items-center px-2">
                                            <span className="mr-2">Search:</span>
                                            <input type="text" className="form-control w-auto flex-1" id="squareInput" placeholder="Search By make/Model,State,City." />
                                        </div>
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="parent-account" />
                                            <label className="custom-control-label" htmlFor="parent-account">Only Show Parent Accounts</label>
                                        </div>
                                        <div className="custom-control custom-checkbox">
                                            <input type="checkbox" className="custom-control-input" id="inactive-account" />
                                            <label className="custom-control-label" htmlFor="inactive-account">Only Show inactive Customers</label>
                                        </div>
                                    </div>
                                    <div className="pr-2 overflow-scroll">
                                        <table className="table mb-3">
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col">Customer Name</th>
                                                    <th scope="col">Primary Contact</th>
                                                    <th scope="col">Phone Number &amp; Email</th>
                                                    <th scope="col">Primary Location</th>
                                                    <th scope="col">Last Serviced</th>
                                                    <th className="text-center" scope="col">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Dummy Customer1</td>
                                                    <td>Morbi leo risus</td>
                                                    <td>85749874</td>
                                                    <td>Morbi leo risus #38493-8473</td>
                                                    <td>-</td>
                                                    <td>
                                                        <div className="mb-0 dropdown text-center">
                                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                            </a>
                                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                                                <a className="dropdown-item" href="customers-account-view.html"><i className="far fa-edit"></i>View &amp; Edit</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-comment"></i>Message</a>
                                                                <a className="dropdown-item" href="#" data-toggle="modal" data-target="#email-Customer"><i className="far fa-envelope"></i>Email</a>
                                                                <a className="dropdown-item" href="#"><i className="fas fa-phone"></i>Phone</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-trash-alt"></i>Delete</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dummy Customer1</td>
                                                    <td>Morbi leo risus</td>
                                                    <td>85749874</td>
                                                    <td>Morbi leo risus #38493-8473</td>
                                                    <td>-</td>
                                                    <td>
                                                        <div className="mb-0 dropdown text-center">
                                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                            </a>
                                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                                                <a className="dropdown-item" href="customers-account-view.html"><i className="far fa-edit"></i>View &amp; Edit</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-comment"></i>Message</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-envelope"></i>Email</a>
                                                                <a className="dropdown-item" href="#"><i className="fas fa-phone"></i>Phone</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-trash-alt"></i>Delete</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Dummy Customer1</td>
                                                    <td>Morbi leo risus</td>
                                                    <td>85749874</td>
                                                    <td>Morbi leo risus #38493-8473</td>
                                                    <td>-</td>
                                                    <td>
                                                        <div className="mb-0 dropdown text-center">
                                                            <a href="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i className="fas fa-ellipsis-v text-color-gradient"></i>
                                                            </a>
                                                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                                                <a className="dropdown-item" href="customers-account-view.html"><i className="far fa-edit"></i>View &amp; Edit</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-comment"></i>Message</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-envelope"></i>Email</a>
                                                                <a className="dropdown-item" href="#"><i className="fas fa-phone"></i>Phone</a>
                                                                <a className="dropdown-item" href="#"><i className="far fa-trash-alt"></i>Delete</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <div className="d-sm-flex justify-content-between align-items-center mt-4 mb-2">
                                        <div className="visible-entries">Showing 1 to 5 of 26 entries</div>
                                        <nav aria-label="...">
                                            <ul className="pagination mb-0">
                                                <li className="page-item disabled"><a className="page-link" href="#!" tabIndex="-1">Previous</a></li>
                                                <li className="page-item"><a className="page-link" href="#!">1</a></li>
                                                <li className="page-item active"><a className="page-link" href="#!">2 <span className="sr-only">(current)</span></a></li>
                                                <li className="page-item"><a className="page-link" href="#!">3</a></li>
                                                <li className="page-item"><a className="page-link" href="#!">Next</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    )
  }
}
