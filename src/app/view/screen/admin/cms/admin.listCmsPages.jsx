import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header';
import { Sidebar } from '../../../component/admin/sidebar/sidebar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { listCmsPages, deleteCmsPage } from '../../../../duck/cms/cms.action';
import { getCmsPages } from '../../../../duck/cms/cms.selector'
import { constants } from '../../../../common/constants'
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { Link, NavLink } from "react-router-dom";
import Swal from 'sweetalert2'
import _ from 'lodash';
import { EDIT_CMS_PAGE_BASE, ADD_CMS_PAGE } from '../../../../routing/routeContants'

class ListCmsPages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            cmsPages: [],
            pageTitle: '',
            pageNo: 1,
            limit: constants.PAGE_LIMIT,
            totalRecords: 0,
            loader: true,
            sortingField: '',
            sortingOrder: '',
            pageType: 'Service',
        }
    }

    componentWillReceiveProps(props) {
        if (props.cmsPages && _.has(props.cmsPages, 'data')) {
            this.setState({
                cmsPages: props.cmsPages.data,
                totalRecords: props.cmsPages.count ? props.cmsPages.count : 0,
                loader: false
            })
        } else if (props.cmsPages === 'Deleted') {
            this.setState({ pageNo: 1, pageTitle: '' })
            this.props.getListCmsPages({ offset: 1, limit: constants.PAGE_LIMIT, pageType: this.state.pageType })
        }
    }

    componentDidMount() {
        const { pageNo, limit, pageType } = this.state
        this.props.getListCmsPages({ offset: pageNo, limit, pageType })
    }

    getPageData = (pageNo) => {
        const { limit, sortingField, sortingOrder, pageTitle, pageType } = this.state
        this.setState({ pageNo, loader: true })
        this.props.getListCmsPages({
            offset: pageNo, limit,
            sortingField, pageTitle,
            sortingOrder,
            pageType
        })
    }

    getSearchData = (e) => {
        e.preventDefault();
        const { sortingField, sortingOrder, pageTitle, pageType } = this.state
        // initialise the search for first page
        this.setState({ pageNo: 1, loader: true })
        this.props.getListCmsPages({
            offset: 1, limit: constants.PAGE_LIMIT, pageTitle, sortingField, sortingOrder, pageType
        })
    }

    sortTheData = (field, order) => {
        this.setState({
            pageNo: 1, pageTitle: '', loader: true, sortingOrder: order, sortingField: field
        })
        this.props.getListCmsPages({
            offset: 1, limit: constants.PAGE_LIMIT, sortingField: field, sortingOrder: order, pageType: this.state.pageType
        })
    }

    clearSearchData = (e) => {
        e.preventDefault();
        this.setState({
            pageNo: 1, sortingField: '', sortingOrder: '', pageTitle: '', loader: true
        })
        this.props.getListCmsPages({ offset: 1, limit: constants.PAGE_LIMIT, pageType: this.state.pageType })
    }

    deleteCmsPage = (e, id) => {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this cms page!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.setState({ loader: true })
                if (id) {
                    this.props.deleteCmsPage(id)
                }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // console.log('cancel')
            }
        })

    }

    render() {
        const { wrapperCls,
            wrapperClsMobile, pageTitle,
            cmsPages, totalRecords, limit,
            pageNo, loader, sortingOrder } = this.state
        let getTableData = '';
        if (cmsPages.length > 0) {
            getTableData = _.map(cmsPages, (cp, i) => {
                return <tr key={i}>
                    <td>{cp.pageTitle}</td>
                    <td>{cp.metaKeywords}</td>
                    <td>
                        <div className="mb-0 dropdown text-center">
                            <Link to="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i className="fas fa-ellipsis-v"></i>
                            </Link>
                            <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                                <NavLink className="dropdown-item" to={ EDIT_CMS_PAGE_BASE + cp._id}>
                                    <i className="fas fa-edit"></i> Edit
                                </NavLink>
                                <NavLink onClick={(e) => this.deleteCmsPage(e, cp._id)} className="dropdown-item" to={'#'}>
                                    <i className="fas fa-trash"></i> Delete
                                </NavLink>
                            </div>
                        </div>
                    </td>
                </tr>;
            });
        }
        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'cms'} getInnerRoute={'list-cms-pages'} />
                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">Service Page Management</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card building-representative-sec full-height">
                                            <div className="card-header d-flex justify-content-between align-items-center py-4">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">Page</div>
                                                </div>
                                                <div className="search">
                                                    {/* <a href="#" className="btn-lg btn-border btn-primary btn-round "><i className="fas fa-globe-asia mr-1"></i> Visit Website</a> */}
                                                    <NavLink to={ADD_CMS_PAGE} className="btn-lg btn-border btn-primary btn-round "><i className="far fa-plus-square mr-1"></i> Add Page</NavLink>
                                                </div>
                                            </div>
                                            <div className="search-info collapse show px-3">
                                                <div className="d-md-flex flex-wrap justify-content-between align-items-center">
                                                    <div className="form-group flex-1  pl-md-0">
                                                        <input value={pageTitle} onChange={(e) => this.setState({ pageTitle: e.target.value })} placeholder="Search page title" className="form-control search-basic" />
                                                    </div>

                                                    <div className="form-group">
                                                        <Link to="#" onClick={(e) => this.getSearchData(e)} className="btn-lg btn-primary btn-square text-white d-block text-center"> Search </Link>
                                                    </div>
                                                    <div className="form-group">
                                                        <Link to="#" onClick={(e) => this.clearSearchData(e)} className="btn-lg btn-dark btn-square text-white d-block text-center"> Clear Search </Link>
                                                    </div>
                                                </div>

                                            </div>
                                            <div className="card-body management-list pt-0">
                                                <div className="pr-2 overflow-scroll">
                                                    <table className="table mb-3">
                                                        <thead className="thead-light">
                                                            <tr>
                                                                <th scope="col">Page Title <span className="sortingArrows"><span onClick={() => this.sortTheData('pageTitle', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('pageTitle', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                                                                <th scope="col">Meta Keywords</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {getTableData ? getTableData : <tr><td colSpan='6'>No Records Found. </td></tr>}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                {totalRecords ? <PaginationFooter getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}


const mapStateToprops = (state) => ({
    cmsPages: getCmsPages(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getListCmsPages: listCmsPages,
    deleteCmsPage: deleteCmsPage
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ListCmsPages); 
