import React from "react";
import { AuthHeader } from '../../../component/admin/header/auth.header';
import { Sidebar } from '../../../component/admin/sidebar/sidebar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { addCmsPage, viewCmsPage, editCmsPage, getAllCmsPages } from '../../../../duck/cms/cms.action';
import { getCmsPages, getAllCmsPagesData } from '../../../../duck/cms/cms.selector'
import { EDIT_CMS_PAGE, SERVICE_BASE } from '../../../../routing/routeContants'
import history from '../../../../routing/history';
import { Link } from "react-router-dom";
import CKEditor from "react-ckeditor-component";
import _ from 'lodash'
import { validateInputs } from '../../../../common/validation';
import { CMS_PAGES_LIST, WEBSITE_CMS_PAGE } from '../../../../routing/routeContants';
import { fieldValidator } from '../../../../common/custom';

export class AddCmsPageComponent extends React.Component {
    constructor(props) {
        super(props);
        let pageId = ''
        if (props.match && _.has(props.match, 'params') && _.has(props.match.params, 'id')) {
            pageId = props.match.params.id
        }
        this.state = {
            wrapperCls: false,
            wrapperClsMobile: false,
            pageTitle: '',
            parentPage: '',
            metaKeywords: '',
            metaDescription: '',
            pageContent: '',
            pageType: 'Service',
            pageTitleErr: '',
            parentPageErr: '',
            metaKeywordsErr: '',
            metaDescriptionErr: '',
            pageContentErr: '',
            pageId,
            loader: false,
            parentPages: [],
            testViaJest: props.testViaJest ? props.testViaJest : false,
            slug: ''
        }
        if (this.state.testViaJest === false) {
            this.props.getAllCmsPages()
        }
    }

    componentWillReceiveProps(props) {
        if (props.getAllCmsPagesData && _.has(props.getAllCmsPagesData, 'data')) {
            this.setState({
                parentPages: props.getAllCmsPagesData.data,
                loader: false
            });
        }
        if (props.cmsPage && _.has(props.cmsPage, 'data._id')) {
            console.log(props.cmsPage.data.pageType)
            if (props.match.path === EDIT_CMS_PAGE) {
                this.setState({
                    metaDescription: props.cmsPage.data.metaDescription,
                    metaKeywords: props.cmsPage.data.metaKeywords,
                    pageContent: props.cmsPage.data.pageContent,
                    pageTitle: props.cmsPage.data.pageTitle,
                    pageType: props.cmsPage.data.pageType,
                    slug: props.cmsPage.data.slug,
                    parentPage: props.cmsPage.data.parentPage === null || props.cmsPage.data.parentPage === '' ? '' : props.cmsPage.data.parentPage,
                    loader: false
                });

            } else {
                this.setState({
                    pageTitle: '',
                    parentPage: '',
                    metaKeywords: '',
                    metaDescription: '',
                    pageContent: '',
                    pageType: 'Service',
                    loader: false,
                    pageId: '',
                    slug: ''
                });
            }
        } else if (props.cmsPage && (props.cmsPage === 'Edited' || props.cmsPage === 'Added')) {
            this.setState({ loader: false });
            if (this.state.pageType === 'Service') {
                history.push(CMS_PAGES_LIST)
            } else {
                history.push(WEBSITE_CMS_PAGE)
            }
        } else if (props.cmsPage && _.has(props.cmsPage, 'message')) {
            this.setState({ loader: false });
            if (this.state.pageType === 'Service') {
                history.push(CMS_PAGES_LIST)
            } else {
                history.push(WEBSITE_CMS_PAGE)
            }
        }
    }

    componentDidMount() {
        if (this.state.pageId) {
            this.setState({ loader: true })
            this.props.viewCmsPage(this.state.pageId)
        }
    }

    onEditorChange = (evt) => {
        var newContent = evt.editor.getData();
        this.setState({
            pageContent: newContent
        })
        this.checkValidation('pageContent', newContent, 'required')
    }

    // Create slug by title change
    createSlugByTitle = (e) => {
        const slugVal = (e.target.value.toLowerCase()).replace(/ /g, '-');;
        this.setState({ pageTitle: e.target.value, slug: slugVal })

    }

    // Create slug by type change
    createSlugByType = (e) => {
        this.setState({ pageType: e.target.value })
    }

    onSubmit = () => {

        const { pageId, pageTitle,
            parentPage, metaKeywords,
            metaDescription, pageContent, testViaJest, pageType, slug } = this.state

        let pageTitleErr = '', metaKeywordsErr = '', metaDescriptionErr = '',
            pageContentErr = '', getError = false;

        if (validateInputs('Alphanumeric', pageTitle) === false) {
            pageTitleErr = 'Please enter alphanumeric characters and space  in page title.';
            getError = true;
        } else if (pageTitle.length < 3) {
            pageTitleErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        if (validateInputs('string', metaKeywords) === 'empty') {
            metaKeywordsErr = 'Please enter meta keyword.';
            getError = true;
        } else if (validateInputs('string', metaKeywords) === false) {
            metaKeywordsErr = 'Please enter valid meta keyword.';
            getError = true;
        } else if (metaKeywords.length < 3) {
            metaKeywordsErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        if (validateInputs('string', metaDescription) === 'empty') {
            metaDescriptionErr = 'Please enter meta description.';
            getError = true;
        } else if (validateInputs('string', metaDescription) === false) {
            metaDescriptionErr = 'Please enter valid meta description.';
            getError = true;
        } else if (metaDescription.length < 3) {
            metaDescriptionErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        if (pageContent === '') {
            pageContentErr = 'Please enter page content.';
            getError = true;
        } else if (pageContent.length < 3) {
            pageContentErr = 'Please enter minimum 3 characters';
            getError = true;
        }

        this.setState({ pageTitleErr, metaKeywordsErr, metaDescriptionErr, pageContentErr, error: getError });

        if (pageTitleErr === '' && metaKeywordsErr === '' && metaDescriptionErr === '' && pageContentErr === '' && getError === false && testViaJest === false) {

            this.setState({ loader: true })
            if (pageId) {
                this.props.editCmsPage({
                    pageId, pageTitle, parentPage, metaKeywords, metaDescription, pageContent, pageType, slug
                })
            } else {
                this.props.addCmsPage({
                    pageTitle, parentPage, metaKeywords, metaDescription, pageContent, pageType, slug
                })
            }
        }
    }

    checkValidation = (field, value, type, maxLength, minLength) => {
        let error = fieldValidator(field, value, type, null, maxLength, minLength)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
    }

    render() {
        const { wrapperCls, pageId, wrapperClsMobile, parentPages,
            pageTitle, parentPage, metaKeywords, metaDescription, pageContent,
            pageTitleErr, loader, metaKeywordsErr, metaDescriptionErr, pageContentErr, pageType, slug
        } = this.state
        let baseUrl;
        if (pageType === 'Website') {
            baseUrl = window.location.origin;
        } else {
            baseUrl = window.location.origin + '/service';
        }

        let parentPagesOptions = ''
        if (parentPages.length > 0) {
            if (pageId) {
                parentPagesOptions = _.map(_.filter(parentPages, (pp) => pp._id !== pageId), (pt, j) => {
                    return <option key={j} value={pt._id}>{pt.pageTitle}</option>
                })
            } else {
                parentPagesOptions = _.map(parentPages, (pt, j) => {
                    return <option key={j} value={pt._id}>{pt.pageTitle}</option>
                })
            }

        }


        return (
            <>
                <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                    <AuthHeader loader={loader} sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} />
                    <Sidebar getMainRoute={'cms'} getInnerRoute={(pageType === 'Service') ? 'add-cms-page' : 'website-cms-page'} />
                    <div className="main-panel">
                        <div className="content">
                            <div className="panel-header bg-primary-gradient">
                                <div className="page-inner py-5">
                                    <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                        <div>
                                            <h2 className="text-white pb-2 fw-bold">{pageId ? 'Edit' : 'Add'} Cms Page</h2>
                                            <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="page-inner mt--5">
                                <div className="row">
                                    <div className="col-md-12">
                                        <div className="card building-representative-sec full-height">
                                            <div className="card-header d-flex justify-content-between align-items-center py-4">
                                                <div className="card-head-row">
                                                    <div className="card-title font-weight-bold">{pageId ? 'Edit' : 'Add'} Page Content</div>
                                                </div>
                                            </div>
                                            <div className="card-body management-list pt-0">
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Page Title*</label>
                                                            <input name="pageTitle" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'Alphanumeric', null, 3) }} value={pageTitle} onChange={(e) => this.createSlugByTitle(e)} className="form-control bg-light" type="text" placeholder="Page title" />
                                                            <span className="errorCls">{pageTitleErr ? pageTitleErr : ''}</span>
                                                        </div>
                                                    </div>
                                                    {/* <div className="col-sm-4">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Page Type</label>
                                                            <div className="select2-input">
                                                                <select value={pageType} onChange={(e) => this.createSlugByType(e)} name="basic" className="form-control search-basic bg-light">
                                                                    <option value='Service'>Service</option>
                                                                    <option value='Website'>Website</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div> */}
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Parent Page</label>
                                                            <div className="select2-input">
                                                                <select value={parentPage} onChange={(e) => this.setState({ parentPage: e.target.value })} name="basic" className="form-control search-basic bg-light">
                                                                    <option value=''>Select</option>
                                                                    {parentPagesOptions}
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {pageType === 'Service' ?
                                                        <div className="col-12">
                                                            <div className="form-group">
                                                                <Link to={SERVICE_BASE + slug} target="_blank" className="mb-1 d-block d-sm-inline">Link : {baseUrl + '/' + slug}</Link>
                                                            </div>
                                                        </div>
                                                        : ''
                                                    }
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Meta Keywords*</label>
                                                            <textarea name="metaKeywords" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string', null, 3) }} value={metaKeywords} onChange={(e) => this.setState({ metaKeywords: e.target.value })} className="form-control bg-light" rows="3" type="text" placeholder="..."></textarea>
                                                            <span className="errorCls">{metaKeywordsErr ? metaKeywordsErr : ''}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6">
                                                        <div className="form-group">
                                                            <label className="w-100 text-primary">Meta Description*</label>
                                                            <textarea name="metaDescription" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string', null, 3) }} value={metaDescription} onChange={(e) => this.setState({ metaDescription: e.target.value })} className="form-control bg-light" rows="3" type="text" placeholder="..."></textarea>
                                                            <span className="errorCls">{metaDescriptionErr ? metaDescriptionErr : ''}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <div className="d-flex justify-content-between align-items-center mb-2">
                                                                <label className="text-primary">Page Content*</label>
                                                            </div>
                                                            {(pageContent || _.has(this.props.match, 'path')) !== EDIT_CMS_PAGE ? <CKEditor
                                                                activeClass="p10"
                                                                content={pageContent}
                                                                events={{
                                                                    "change": this.onEditorChange
                                                                }}
                                                                config={{
                                                                    allowedContent: true
                                                                }}
                                                            /> : ''}

                                                            <span className="errorCls">{pageContentErr ? pageContentErr : ''}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="text-center text-md-right w-100 my-3 px-2">
                                                    <Link to={(pageType === 'Service') ? CMS_PAGES_LIST : WEBSITE_CMS_PAGE} className="btn btn-dark" data-dismiss="modal">Cancel</Link>
                                                    <button id='addCmsButtton' type="button" className="btn btn-primary" onClick={(e) => this.onSubmit()}>{pageId ? 'Edit' : 'Add'} Page</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const mapStateToprops = (state) => ({
    cmsPage: getCmsPages(state),
    getAllCmsPagesData: getAllCmsPagesData(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addCmsPage: addCmsPage,
    editCmsPage: editCmsPage,
    viewCmsPage: viewCmsPage,
    getAllCmsPages: getAllCmsPages
}, dispatch);

export const AddCmsPage = connect(mapStateToprops, mapDispatchToProps)(AddCmsPageComponent);


