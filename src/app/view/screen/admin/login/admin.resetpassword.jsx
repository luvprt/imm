import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AdminHeader } from '../../../component/admin/header/admin.header'
import { ADMIN_LOGIN, HOME } from '../../../../routing/routeContants';
import history from '../../../../routing/history'
import _ from 'lodash'
import { resetPassword, verifyToken } from '../../../../duck/auth/auth.action'
import { Tokens } from '../../../../storage/index'
import LOGO from '../../../../assets/images/logo-itm.png'
import { validateInputs } from '../../../../common/validation';
import { Link } from 'react-router-dom'
import { fieldValidator } from '../../../../common/custom';

class ResetPassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showPasswordField: false,
            confirmShowPasswordField: false,
            confirmPassword: '',
            newPassword: '',
            passwordErr: '',
            confirmPasswordErr: '',
            error: '',
            loader: false
        }
        Tokens.setVerifyToken(props.location.search.split('=')[1])
    }

    componentDidMount() {
        this.props.verifyToken();
    }

    resetPasswordEvent = () => {
        this.setState({ confirmPasswordErr: '', passwordErr: '' });
        const { confirmPassword, newPassword } = this.state;

        let passwordErr = '', confirmPasswordErr = '', getError = false;


        if (validateInputs('password', newPassword) === 'empty') {
            passwordErr = 'Please enter password.';
            getError = true;
        } else if (validateInputs('password', newPassword) === false) {
            passwordErr = 'A special character, an upper case, a lower case, a number & minimum 8 character are required';
            getError = true;
        }

        if (validateInputs('password', confirmPassword) === 'empty') {
            confirmPasswordErr = 'Please enter confirm password.';
            getError = true;
        } else if (!(newPassword === confirmPassword)) {
            confirmPasswordErr = 'Confirm password doesn\'t match.';
            getError = true;
        }

        this.setState({ passwordErr, confirmPasswordErr, error: getError });

        if (getError === false && passwordErr === '' && confirmPasswordErr === '') {
            this.setState({ loader: true })
            this.props.resetPassword({ password: newPassword, confirmPassword })
        }
    }

    componentWillReceiveProps(props) {
        if (props.userResetPassword && _.has(props.userResetPassword, 'auth.resetPassword')) {
            this.setState({ loader: false });
            history.push(ADMIN_LOGIN);
        }

        if (props.getVerifyToken && _.has(props.getVerifyToken, 'auth.verifyToken')) {
            if (props.getVerifyToken.auth.verifyToken === false) {
                props.history.push('/');
            }
        }
    }


    showPassword = () => this.setState({ showPasswordField: !this.state.showPasswordField })
    consfirmShowPassword = () => this.setState({ confirmShowPasswordField: !this.state.confirmShowPasswordField })

    checkValidation = (field, value, type, password) => {
        let error = fieldValidator(field, value, type, password)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg, messageFromService: '' })
    }

    render() {

        const { loader, showPasswordField, confirmShowPasswordField, newPassword, confirmPassword, passwordErr, confirmPasswordErr } = this.state

        return (
            <div className="login">
                <AdminHeader loader={loader} />
                <div className="wrapper wrapper-login">
                    <div className="signup-form animated fadeIn">
                        <div className="bg-card card_one"></div>
                        <div className="bg-card card_two"></div>
                        <div className="container-login shadow">
                            <div className="d-block text-center">
                                <Link to={HOME} className="logo">
                                    <img src={LOGO} alt="navbar brand" className="navbar-brand" />
                                </Link>
                            </div>
                            <h3 className="text-center">Reset Password</h3>
                            <div className="login-form">
                                <div className="form-group form-floating-label">
                                    <input required value={newPassword} name= "password" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'password') }} onChange={(e) => this.setState({ newPassword: e.target.value })} id="passwordsignin" type={showPasswordField ? 'text' : 'password'} className="form-control input-border-bottom" />
                                    <label htmlFor="passwordsignin" className="placeholder">New Password</label>
                                    <div onClick={() => this.showPassword()} className="show-password">
                                        <i className={showPasswordField ? 'far fa-eye-slash' : 'far fa-eye'}></i>
                                    </div>
                                    {passwordErr ? <span className="errorCls">{passwordErr}</span> : ''}
                                </div>
                                <div className="form-group form-floating-label">
                                    <input required value={confirmPassword} name= "confirmPassword" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, null, newPassword) }} onChange={(e) => this.setState({ confirmPassword: e.target.value })} id="confirmpassword" type={confirmShowPasswordField ? 'text' : 'password'} className="form-control input-border-bottom" />
                                    <label htmlFor="confirmpassword" className="placeholder">Confirm Password</label>
                                    <div onClick={() => this.consfirmShowPassword()} className="show-password">
                                        <i className={confirmShowPasswordField ? 'far fa-eye-slash' : 'far fa-eye'}></i>
                                    </div>
                                    {confirmPasswordErr ? <span className="errorCls">{confirmPasswordErr}</span> : ''}
                                </div>
                                <div className="form-action">
                                    <span onClick={() => this.resetPasswordEvent()} className="btn btn-primary btn-rounded btn-sm">Reset My Password</span>
                                </div>
                                <div className="login-account">
                                    <span onClick={() => history.push(ADMIN_LOGIN)} className="linkCls msg">Back to Login!! </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


const mapStateToprops = (state) => ({
    userResetPassword: state,
    getVerifyToken: state
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    resetPassword: resetPassword,
    verifyToken: verifyToken
}, dispatch);

export const UserResetPassword = connect(mapStateToprops, mapDispatchToProps)(ResetPassword); 