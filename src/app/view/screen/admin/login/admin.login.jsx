import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AdminHeader } from '../../../component/admin/header/admin.header'
import ForgotPassword from './admin.forgotpassword'
import { adminLogin } from '../../../../duck/auth/auth.action'
import { isUserLoggedIn, getLoggedInUser, getAuthState } from '../../../../duck/auth/auth.selector'
import { Tokens, User } from '../../../../storage/index'
import _ from 'lodash'
import { ADMIN_DASHBOARD, HOME } from '../../../../routing/routeContants';
import history from '../../../../routing/history'
import { validateInputs } from '../../../../common/validation';
import LOGO from '../../../../assets/images/logo-itm.png';
import SimpleCrypto from "simple-crypto-js"
import { NavLink, Link } from 'react-router-dom';
import { fieldValidator } from '../../../../common/custom';


export class AdminUserLogin extends Component {
  constructor(props) {
    super(props)
    this.state = {
      forgotPasswordDiv: false,
      loginDiv: true,
      showPasswordField: false,
      email: '',
      password: '',
      rememberMe: '',
      emailErr: '',
      passwordErr: '',
      messageFromService: '',
      error: '',
      loader: false,
      testViaJest: false
    }
  }

  componentDidMount() {
    const email = this.getCookie("email");
    const pass = this.getCookie("pass");
    if (email !== '' && pass !== '') {
      var simpleCrypto = new SimpleCrypto("123654*/4514545454");
      var email_decrypted = simpleCrypto.decrypt(email);
      var password_decrypted = simpleCrypto.decrypt(pass);
      this.setState({ email: email_decrypted, password: password_decrypted, rememberMe: true })
    }
  }

  setCookie = (cname, cvalue, exdays) => {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie = (cname) => {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  deleteCookie = (cname) => {
    document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/';
  };

  componentWillReceiveProps(props) {
    if (props.user && _.has(props.user, 'token')) {
      Tokens.setToken(props.user.token);
      const decryptUserType = new SimpleCrypto("123654*/4514545454");
      let decryptedUserType = decryptUserType.encrypt(props.user.data.userTypeId.name);
      let userData = { name: props.user.data.firstName, email: props.user.data.email }
      User.setUserType(decryptedUserType);
      User.setUserDetails(userData);
      this.setState({ loader: false })
      //Remeber Me 
      if (this.state.rememberMe === true) {
        var simpleCrypto = new SimpleCrypto("123654*/4514545454");
        var email_encrypted = simpleCrypto.encrypt(this.state.email);
        var password_encrypted = simpleCrypto.encrypt(this.state.password);
        this.setCookie("email", email_encrypted, 7);
        this.setCookie("pass", password_encrypted, 7);
      } else {
        this.deleteCookie("email")
        this.deleteCookie("pass")
      }

      history.push(ADMIN_DASHBOARD);
    } else if (props.auth && _.has(props.auth, 'message')) {
      this.setState({ messageFromService: props.auth.message, loader: false });
    } else if (props.auth && _.has(props.auth, 'forgotPassword')) {
      if (props.auth.forgotPassword.success === true) {
        this.setState({ loader: false, forgotPasswordDiv: false, loginDiv: true });
      } else if (props.auth.forgotPassword.success === false) {
        this.setState({ loader: false })
      }
    }
  }

  adminLogin = () => {
    const { email, password, testViaJest } = this.state;
    let emailErr = '', passwordErr = '', getError = false;
    if (validateInputs('email', email) === 'empty') {
      emailErr = 'Please enter email.';
      getError = true;
    } else if (validateInputs('email', email) === false) {
      emailErr = 'Please enter valid email.';
      getError = true;
    }

    if (validateInputs('password', password) === 'empty') {
      passwordErr = 'Please enter password.';
      getError = true;
    } else if (validateInputs('password', password) === false) {
      passwordErr = 'A special character, an upper case, a lower case, a number & minimum 8 character are required';
      getError = true;
    }

    this.setState({ emailErr, passwordErr, error: getError });

    if (getError === false && emailErr === '' && passwordErr === '' && testViaJest === false) {
      this.setState({ loader: true })
      this.props.adminLogin({ email, password })
    }
  }

  forgotPasswordDivStatus = (statusOfDiv) => {
    if (statusOfDiv === 'open') {
      this.setState({
        forgotPasswordDiv: true,
        loginDiv: false
      })
    } else if (statusOfDiv === 'close') {
      this.setState({
        forgotPasswordDiv: false,
        loginDiv: true,
        emailErr: '',
        passwordErr: '',
        messageFromService: ''
      })
    }

  }

  keyPressDownEvent = (e) => {
    if (e.key === 'Enter') {
      this.adminLogin();
    }
  }

  showPassword = () => this.setState({ showPasswordField: !this.state.showPasswordField })

  checkValidation = (field, value, type, maxLength, minLength) => {
    let error = fieldValidator(field, value, type, null, maxLength, minLength)
    this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg, messageFromService: '' })
  }

  render() {
    const { emailErr, loader,
      messageFromService, passwordErr,
      forgotPasswordDiv, loginDiv, showPasswordField,
      email, password, rememberMe } = this.state
    return (
      <div className="login">
        <AdminHeader loader={loader} />
        <div className="wrapper wrapper-login p-0">
          {loginDiv ?
            <div className="signup-form animated fadeIn">
              <div className="bg-card card_one"></div>
              <div className="bg-card card_two"></div>
              <div className="container-login shadow">
                <div className="d-block text-center">
                  <Link to={HOME} className="logo">
                    <img src={LOGO} alt="navbar brand" className="navbar-brand" />
                  </Link>
                </div>
                <h3 className="text-center">Sign In</h3>
                <div className="login-form">
                  {messageFromService ? <div className='form-group text-center errorCls'>{messageFromService}</div> : ''}
                  <div className="form-group form-floating-label">
                    <input onKeyDown={(e) => this.keyPressDownEvent(e)} id="username" name="email" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'email') }} required value={email} onChange={(e) => this.setState({ email: e.target.value })} type="text" className="form-control input-border-bottom" />
                    <label htmlFor="username" className="placeholder">Email</label>
                    {emailErr ? <div className='errorCls'>{emailErr}</div> : ''}
                  </div>
                  <div className="form-group form-floating-label">
                    <input onKeyDown={(e) => this.keyPressDownEvent(e)} id="password" name="password" required onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'password') }} value={password} onChange={(e) => this.setState({ password: e.target.value })} type={showPasswordField ? 'text' : 'password'} className="form-control input-border-bottom" />
                    <label htmlFor="password" className="placeholder">Password</label>
                    <div onClick={() => this.showPassword()} className="show-password">
                      <i className={showPasswordField ? 'far fa-eye-slash' : 'far fa-eye'}></i>
                    </div>
                    {passwordErr ? <div className='errorCls'>{passwordErr}</div> : ''}
                  </div>
                  <div className="row form-sub m-0">
                    <div className="custom-control custom-checkbox">
                      <input checked={rememberMe} onChange={(e) => this.setState({ rememberMe: e.target.checked })} type="checkbox" className="custom-control-input" id="rememberme" />
                      <label className="custom-control-label" htmlFor="rememberme">Remember Me</label>
                    </div>
                    <span onClick={(e) => this.forgotPasswordDivStatus('open')} className="linkCls link float-right">Forgot Password ?</span>

                  </div>
                  <div className="form-action mb-3">
                    <span id="signInButton" onClick={(e) => this.adminLogin()} className="btn bg-primary-gradient text-white btn-rounded btn-login">Sign In</span>
                  </div>
                  <div className="login-account">
                    <span className="msg">Don't have an account yet ? </span>
                    <NavLink to={HOME + "#sp"} className="link"> Sign Up</NavLink>
                  </div>
                </div>
              </div> </div> : ''}
          <ForgotPassword setloader={(setLoader) => this.setState({ loader: setLoader })} updateForgot={this} show={forgotPasswordDiv} changeTologinView={(data) => this.forgotPasswordDivStatus(data)} />
        </div>
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  login: isUserLoggedIn(state),
  user: getLoggedInUser(state),
  auth: getAuthState(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  adminLogin: adminLogin
}, dispatch);

export const AdminLogin = connect(mapStateToprops, mapDispatchToProps)(AdminUserLogin); 