import React, { Component } from 'react';
import { forgotPassword } from '../../../../duck/auth/auth.action'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import { validateInputs } from '../../../../common/validation';
import { fieldValidator } from '../../../../common/custom';

class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            emailErr: '',
            error: '',
            messageFromService: ''
        }
    }

    forgotPasswordEvent = () => {
        const { email } = this.state;
        let emailErr = '', getError = false;

        if (validateInputs('email', email) === 'empty') {
            emailErr = 'Please enter valid email.';
            getError = true;
        } else if (validateInputs('email', email) === false) {
            emailErr = 'Invalid email.';
            getError = true;
        }

        this.setState({ emailErr, error: getError });

        if (getError === false && emailErr === '') {
            this.props.setloader(true)
            this.props.forgotPassword({ email })
        }

    }

    componentWillReceiveProps(props) {
        if (props.userforgotPassword && _.has(props.userforgotPassword, 'auth.forgotPassword.message')) {
            this.setState({ messageFromService: props.userforgotPassword.auth.forgotPassword.message })
        }
        if (props.show !== this.props.show) {
            this.setState({ emailErr: '', email: '' })
        }
    }

    checkValidation = (field, value, type, maxLength, minLength) => {
        let error = fieldValidator(field, value, type, null, maxLength, minLength)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg, messageFromService: '' })
    }

    render() {

        const { emailErr, messageFromService } = this.state
        const { show } = this.props

        return (
            <>
                {show ?
                    <div className="container-forgetpaswd animated fadeIn">
                        <div className="bg-card card_one"></div>
                        <div className="bg-card card_two"></div>
                        <div className="container container-signup animated fadeIn">
                            <span onClick={() => this.props.changeTologinView('close')} className="text-primary float-left linkCls" href="#"><i className="fas fa-arrow-left"></i></span>
                            <h3 className="text-center">Forgot your password?</h3>
                            <p className="text-center">Don't worry, it happens. Let us know the email address you signed up with and we'll send you an email with instructions. </p>
                            {messageFromService ? <div className="form-group text-center errorCls">{messageFromService}</div> : ''}
                            <div className="login-form">
                                <div className="form-group form-floating-label">
                                    <input name="email" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'email') }} required onChange={(e) => this.setState({ email: e.target.value })} id="email" type="text" className="form-control input-border-bottom" />
                                    <label htmlFor="email" className="placeholder">Enter Your Email</label>
                                    {emailErr ? <span className="errorCls">{emailErr}</span> : ''}
                                </div>

                                <div className="form-action">
                                    <span onClick={() => this.forgotPasswordEvent()} href="#" className="btn btn-primary btn-rounded text-white btn-login">Send Email</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    : ''}
            </>
        );
    }
}
const mapStateToprops = (state) => ({
    userforgotPassword: state,
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    forgotPassword: forgotPassword
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ForgotPassword); 