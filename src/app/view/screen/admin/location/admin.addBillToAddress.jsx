import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { constants } from '../../../../common/constants'
import { Link, NavLink } from "react-router-dom";
import { PaginationFooter } from '../../../../common/admin.paginationFooter'
import { AuthHeader } from '../../../component/admin/header/auth.header'
import { Sidebar } from '../../../component/admin/sidebar/sidebar'
import { AddLocation } from './admin.addLocationAddress'
import { ViewLocation } from './admin.viewLocationAddress'
import Swal from 'sweetalert2'
import { validateInputs } from '../../../../common/validation';
import { addLocation, listLocation, deleteLocation, getLocationById, editLocation, viewLocation, listCustomersByRole, locationStatus } from '../../../../duck/location/location.action';
import { getLocationData, getAllLocationData, getLocationByIdData, viewAllLocationData, customersByRole, locationStatusData } from '../../../../duck/location/location.selector';
import _ from 'lodash';
import { fieldValidator } from '../../../../common/custom';


export class BillToAddressPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            wrapperCls: false,
            loader: false,
            wrapperClsMobile: false,
            modalValue: false,
            modalEdit: '',
            user: '',
            streetAddress: '',
            apt: '',
            state: '',
            city: '',
            postalCode: '',
            customerIdErr: '',
            addressErr: '',
            aptErr: '',
            stateErr: '',
            cityErr: '',
            zipCodeErr: '',
            error: '',
            locationType: 'Billing',
            allLocation: [],
            pageNo: 1,
            limit: constants.PAGE_LIMIT,
            totalRecords: 0,
            searchValue: '',
            sortingOrder: '',
            sortingField: '',
            modalButton: 'Add',
            modalViewValue: false,
            viewLocationData: [],
            customerLabel:''
        }
    }

    componentDidMount() {
        const { pageNo, limit, locationType } = this.state
        this.props.listLocation({ offset: pageNo, limit, locationType });
        this.props.listCustomersByRole();
        this.setState({loader: true});
    }

    componentWillReceiveProps(props) {
        const { pageNo, limit, locationType } = this.state

        // Get Location Data After Add and Delete Location 
        if (props.getLocationData && props.getLocationData.success && (props.getLocationData !== this.props.getLocationData)) {
            this.setState({ modalValue: false, loader: false })
            this.props.listLocation({ offset: pageNo, limit, locationType });
        } else if (props.getLocationData && _.has(props.getLocationData, 'message')) {
            this.setState({ loader: false });
        }

        // Set Location Data In Modal After Click On Edit 
        if (props.getLocationByIdData && props.getLocationByIdData.success && (props.getLocationByIdData !== this.props.getLocationByIdData)) {
            let locationDataById = props.getLocationByIdData.data;
            this.setState({
                modalEdit: locationDataById._id,
                user: locationDataById.user.id,
                customerLabel: {value: locationDataById.user.id, label: locationDataById.user.firstName+' '+locationDataById.user.lastName},
                streetAddress: locationDataById.streetAddress,
                apt: locationDataById.apt,
                state: locationDataById.state,
                city: locationDataById.city,
                postalCode: locationDataById.postalCode,
                modalValue: true,
                customerIdErr: '',
                addressErr: '',
                aptErr: '',
                stateErr: '',
                cityErr: '',
                zipCodeErr: '',
                modalButton: 'Update'
            })
        }

        if (props.getAllLocationData && _.has(props.getAllLocationData, 'data')) {
            this.setState({
                allLocation: props.getAllLocationData.data,
                totalRecords: props.getAllLocationData.count ? props.getAllLocationData.count : 0,
                loader: false
            })
        }else if((props.getAllLocationData && _.has(props.getAllLocationData, 'message'))){
            this.setState({loader:false})
        }

        if (props.viewAllLocationData && _.has(props.getAllLocationData, 'data')) {
            this.setState({ viewLocationData: props.viewAllLocationData.data })
        }

         // Check Location Status
         if ((props.locationStatusData !== this.props.locationStatusData)){
            const {pageNo, limit, locationType, searchValue } = this.state
            this.props.listLocation({ offset: pageNo, limit, locationType, search: searchValue });
            this.setState({loader: false})
        }
    }

    // View Location Modal 
    viewLocationModal = (address, type) => {
        this.props.viewLocation({ 'location': address, 'locationType': type })
        this.setState({ modalViewValue: true })
    }


    //Get Location Data According To Pagination
    getPageData = (pageNo) => {
        const { limit, sortingField, sortingOrder, locationType } = this.state
        this.setState({ pageNo, })
        this.props.listLocation({
            offset: pageNo, limit, locationType,
            sortingField, sortingOrder
        })
    }

    //Get Location Data By Search
    getDataBysearch = (e) => {
        e.preventDefault();
        let pageNo = 1;
        const { limit, locationType, searchValue } = this.state
        this.setState({ pageNo: pageNo, sortingField: '', sortingOrder: '', loader: true });
        this.props.listLocation({
            offset: pageNo, search: searchValue, limit, locationType
        })
    }

    //Clear Location Data By Search
    clearSearchData = (e) => {
        e.preventDefault();
        let pageNo = 1;
        const { limit, locationType } = this.state
        this.setState({ pageNo: pageNo, sortingField: '', sortingOrder: '', searchValue:'', loader:true });
        this.props.listLocation({
            offset: pageNo,  limit, locationType
        })
    }

    // Delete Location 
    deleteLocation = (e, id) => {
        e.preventDefault();
        Swal.fire({
            title: 'Are you sure?',
            text: 'You want to delete this location!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                //this.setState({ loader: true })
                if (id) {
                    this.props.deleteLocation(id)
                }
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                // console.log('cancel')
            }
        })
    }

    // Sort The Location Data
    sortTheData = (field, order) => {
        const { locationType } = this.state
        this.setState({
            pageNo: 1, searchValue: '', sortingOrder: order, sortingField: field
        })
        this.props.listLocation({
            offset: 1, limit: constants.PAGE_LIMIT, sortingField: field, sortingOrder: order, locationType
        })
    }

    // Save Location data 
    submitLocation = () => {
        const { user, streetAddress, apt, city, state, postalCode, locationType, modalEdit } = this.state;

        let customerIdErr = '', addressErr = '', aptErr = '', stateErr = '', cityErr = '', zipCodeErr = '', getError = false;

        if (user === '') {
            customerIdErr = 'Please select customer.';
            getError = true;
        }

        if (validateInputs('string', streetAddress) === 'empty') {
            addressErr = 'Please enter address.';
            getError = true;
        } else if (validateInputs('string', streetAddress) === false) {
            addressErr = 'Please enter valid address.';
            getError = true;
        }

        if (validateInputs('string', apt) === 'empty') {
            aptErr = 'Please enter apt.';
            getError = true;
        } else if (validateInputs('string', apt) === false) {
            aptErr = 'Please enter valid apt.';
            getError = true;
        }

        if (validateInputs('string', city) === 'empty') {
            cityErr = 'Please enter city.';
            getError = true;
        } else if (validateInputs('string', city) === false) {
            cityErr = 'Please enter valid city.';
            getError = true;
        }

        if (validateInputs('string', state) === 'empty') {
            stateErr = 'Please enter state.';
            getError = true;
        } else if (validateInputs('string', state) === false) {
            stateErr = 'Please enter valid state.';
            getError = true;
        }

        if (validateInputs('string', postalCode) === 'empty') {
            zipCodeErr = 'Please enter zip code.';
            getError = true;
        } else if (validateInputs('string', postalCode) === false) {
            zipCodeErr = 'Please enter valid zip code.';
            getError = true;
        } else if (postalCode.length > 6) {
            zipCodeErr = 'Please enter upto 6 characters.';
            getError = true;
        }

        this.setState({ addressErr, aptErr, cityErr, customerIdErr, stateErr, zipCodeErr, error: getError });

        if (getError === false && addressErr === '' && customerIdErr === '' && cityErr === '' && aptErr === '' && zipCodeErr === '' && stateErr === '') {
            if (modalEdit === '') {
                this.setState({ loader: true })
                this.props.addLocation({ user, streetAddress, apt, city, state, postalCode, locationType });
            } else {
                this.setState({ loader: true })
                this.props.editLocation({ user, streetAddress, apt, city, state, postalCode, locationType, locationId: modalEdit });
            }

        }

    }

    checkValidation = (field, value, type, maxLength, minLength) => {
        let error = fieldValidator(field, value, type, null, maxLength, minLength)
        this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
    }

    // Change Status
    changeStatus = (id,status) => {
        this.setState({loader: true})
        this.props.locationStatus({locationId:id , status:status})
    }

    render() {
        const { locationType, wrapperCls, wrapperClsMobile, modalValue, user, streetAddress, apt, state, city, postalCode, addressErr, cityErr, aptErr, customerIdErr, stateErr, zipCodeErr, allLocation, totalRecords, pageNo, limit, searchValue, sortingOrder, loader, modalButton, modalViewValue, viewLocationData, customerLabel } = this.state;
        const {customersByRole} = this.props;

        let getTableData = _.map(allLocation, (al, i) => {
            return <tr key={i}>
                <td>{al.streetAddress}</td>
                <td>{al.apt}</td>
                <td>{al.city}</td>
                <td>{al.state}</td>
                <td>{al.postalCode}</td>
                <td>{al.user && al.user.firstName + ' ' + al.user.lastName}</td>
                <td>
                    <div className="custom-control custom-switch">
                        <input type="checkbox" checked={al.isActive} className="custom-control-input" id={al._id}  onChange={(e) => this.changeStatus(al._id ,e.target.checked)} />
                        <label className="custom-control-label" htmlFor={al._id}></label>
                    </div>
                </td>
                <td>
                    <div className="mb-0 dropdown text-center">
                        <Link to="#" className="mb-0 rounded-circle" id="dropdown-invite-list" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-ellipsis-v"></i>
                        </Link>
                        <div className="dropdown-menu" aria-labelledby="dropdown-invite-list">
                            <NavLink className="dropdown-item" onClick={() => this.viewLocationModal(al.streetAddress, locationType)} to="#">
                                <i className="far fa-eye"></i> View
                            </NavLink>
                            <NavLink onClick={() => this.props.getLocationById(al._id)} className="dropdown-item" to={"#"}>
                                <i className="fas fa-edit"></i> Edit
                            </NavLink>
                            <NavLink onClick={(e) => this.deleteLocation(e, al._id)} className="dropdown-item" to={'#'}>
                                <i className="fas fa-trash"></i> Delete
                            </NavLink>
                        </div>
                    </div>
                </td>
            </tr>;
        });

        return (
            <div className={wrapperCls ? 'wrapper sidebar_minimize' : wrapperClsMobile ? 'wrapper nav_open' : 'wrapper'} >
                <AuthHeader sideBarTogglerMobile={() => this.setState({ wrapperClsMobile: !this.state.wrapperClsMobile })} sideBarToggler={() => this.setState({ wrapperCls: !this.state.wrapperCls })} loader={loader} />
                <Sidebar getMainRoute={'location'} getInnerRoute={'billing-address'} />
                <div className="main-panel">
                    <div className="content">
                        <div className="panel-header bg-primary-gradient">
                            <div className="page-inner py-5">
                                <div className="d-flex align-items-left align-items-md-center flex-column flex-md-row">
                                    <div>
                                        <h2 className="text-white pb-2 fw-bold">Bill to Address</h2>
                                        <h5 className="text-white op-7 mb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="page-inner mt--5">
                            <div className="row">
                                <div className="col-md-12">
                                    <div className="card building-representative-sec full-height">
                                        <div className="card-header d-flex justify-content-between align-items-center py-4">
                                            <div className="card-head-row">
                                                <div className="card-title font-weight-bold">Bill to Address List</div>
                                            </div>
                                            <div className="search">
                                                <span onClick={() => this.setState({ modalValue: true, user: '', streetAddress: '', apt: '', state: '', city: '', postalCode: '', customerIdErr: '', addressErr: '', aptErr: '', stateErr: '', cityErr: '', zipCodeErr: '', modalEdit: '', modalButton: 'Add', customerLabel: '', })} className="btn-lg btn-border btn-primary btn-round"><i className="far fa-plus-square mr-1"></i> Add Bill to Location</span>
                                            </div>
                                        </div>
                                        <div className="search-info collapse show px-3">
                                            <div className="d-md-flex flex-wrap justify-content-between align-items-center">
                                                <div className="form-group flex-1  pl-md-0">
                                                    <input value={searchValue} onChange={(e) => this.setState({ searchValue: e.target.value })} placeholder="Search Physical Location" className="form-control search-basic" />
                                                </div>

                                                <div className="form-group">
                                                    <Link to="#" onClick={(e) => this.getDataBysearch(e)} className="btn-lg btn-primary btn-square text-white d-block text-center"> Search </Link>
                                                </div>
                                                <div className="form-group">
                                                    <Link to="#" onClick={(e) => this.clearSearchData(e)} className="btn-lg btn-dark btn-square text-white d-block text-center"> Clear Search </Link>
                                                </div>
                                            </div>

                                        </div>
                                       {/*  <div className="search-info collapse show px-3">
                                            <div className="form-group d-md-flex flex-wrap justify-content-end align-items-center">
                                                <span className="mr-2 font-weight-bold">Search</span>
                                                <input type="text" value={searchValue} onChange={(e) => this.getDataBysearch(e.target.value)} className="bg-light border p-2" placeholder="Search Location" />
                                            </div>
                                        </div> */}

                                        <div className="card-body management-list pt-0">
                                            <div className="pr-2 overflow-scroll">
                                                <table className="table mb-3">
                                                    <thead className="thead-light">
                                                        <tr>
                                                            <th scope="col">Physical Address <span className="sortingArrows"><span onClick={() => this.sortTheData('streetAddress', 1)} className={sortingOrder === 1 ? "fa fa-sort-up text-primary" : "fa fa-sort-up"}></span><span onClick={() => this.sortTheData('streetAddress', -1)} className={sortingOrder === -1 ? "fa fa-sort-down text-primary" : "fa fa-sort-down"}></span></span></th>
                                                            <th scope="col">Apt/Suite</th>
                                                            <th scope="col">City</th>
                                                            <th scope="col">State</th>
                                                            <th scope="col">Zip Code</th>
                                                            <th scope="col">Customer</th>
                                                            <th scope="col">Status</th>
                                                            <th className="text-center" scope="col">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {(getTableData && getTableData.length>0) ? getTableData : <tr><td colSpan='6'>No Records Found. </td></tr>}
                                                    </tbody>
                                                </table>
                                            </div>
                                            {totalRecords ? <PaginationFooter getPageData={(data) => this.getPageData(data)} pageNo={pageNo} totalRecords={totalRecords} limit={limit} /> : ''}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Add Location Modal Here */}
                <AddLocation
                    openModal={modalValue}
                    closeModal={() => this.setState({ modalValue: false })}
                    customerId={user}
                    address={streetAddress}
                    apt={apt}
                    state={state}
                    city={city}
                    zipCode={postalCode}
                    customerLabel={customerLabel}
                    changeCustomerId={(data) => { this.setState({ user: data.value, customerLabel: data }); this.checkValidation('customerId', data.value, 'required') }}
                    changeAddress={(data) => this.setState({ streetAddress: data })}
                    changeCity={(data) => this.setState({ city: data })}
                    changeApt={(data) => this.setState({ apt: data })}
                    changeState={(data) => this.setState({ state: data })}
                    changeZipCode={(data) => this.setState({ postalCode: data })}
                    submitLocation={() => this.submitLocation()}
                    addressErr={addressErr}
                    aptErr={aptErr}
                    cityErr={cityErr}
                    customerIdErr={customerIdErr}
                    stateErr={stateErr}
                    zipCodeErr={zipCodeErr}
                    modalButton={modalButton}
                    locationType={locationType}
                    customersByRoleData={customersByRole && customersByRole.data}
                    checkValidation={(target, value, type) => this.checkValidation(target, value, type)}
                />
                {/* View Location Modal */}
                <ViewLocation
                    openViewModal={modalViewValue}
                    closeViewModal={() => this.setState({ modalViewValue: false })}
                    viewLocationData={viewLocationData}
                />
            </div>
        )
    }
}

const mapStateToprops = (state) => ({
    getLocationData: getLocationData(state),
    getAllLocationData: getAllLocationData(state),
    getLocationByIdData: getLocationByIdData(state),
    viewAllLocationData: viewAllLocationData(state),
    customersByRole: customersByRole(state),
    locationStatusData: locationStatusData(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    addLocation: addLocation,
    listLocation: listLocation,
    deleteLocation: deleteLocation,
    getLocationById: getLocationById,
    editLocation: editLocation,
    viewLocation: viewLocation,
    listCustomersByRole: listCustomersByRole,
    locationStatus: locationStatus,
}, dispatch);

export const BillToAddress = connect(mapStateToprops, mapDispatchToProps)(BillToAddressPage); 
