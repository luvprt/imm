import React from "react";
import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";
import _ from 'lodash';
import { ADD_CUSTOMER } from "../../../../routing/routeContants";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';

export class AddLocation extends React.Component {

  render() {
    const { locationType, openModal, closeModal, customerId, address, apt, state, city, zipCode, changeCustomerId, changeAddress, changeCity, changeApt, changeState, changeZipCode, submitLocation, addressErr, cityErr, aptErr, customerIdErr, stateErr, zipCodeErr, modalButton, checkValidation, customersByRoleData, customerLabel } = this.props;
    
    const allCustomers =  _.map(customersByRoleData, (ac, i) => {
      return { value: ac._id, label: ac.firstName+' '+ac.lastName }
    });

    return (
      <div>
        <Modal show={openModal} onHide={() => closeModal()} className="modal-lg-w">
          <Modal.Header closeButton>
            <Modal.Title>
              {modalButton + (locationType === 'Service' ? ' Service Location' : ' Bill to Location')}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-12">
                <div className="form-group">
                  <label className="w-100">Add Customer   <span className="float-sm-right d-sm-inline-block d-block">Customer not in the system?<Link to={ADD_CUSTOMER}>Add them here!</Link></span></label>
                  <Select
                      className="addonsCls"
                      components={makeAnimated()}
                      value={customerLabel}
                      defaultValue={customerLabel}
                      options={allCustomers}
                      onChange={value => changeCustomerId(value)}
                    />
                  {customerIdErr ? <span className="errorClsRegister"> {customerIdErr}</span> : ''}
                </div>
              </div>
              <div className="col-12">
                <div className="form-group">
                  <label className="w-100">Physical Address</label>
                  <input name="address" onKeyUp={(e) => { checkValidation(e.target.name, e.target.value, 'string') }} type="text" value={address} onChange={(e) => changeAddress(e.target.value)} className="form-control mb-0" placeholder="Address" />
                  {addressErr ? <span className="errorClsRegister"> {addressErr}</span> : ''}
                </div>
              </div>
              <div className="col-12 col-sm-6">
                <div className="form-group">
                  <label className="w-100">Apt/Suite</label>
                  <input name="apt" onKeyUp={(e) => { checkValidation(e.target.name, e.target.value, 'string') }} type="text" value={apt} onChange={(e) => changeApt(e.target.value)} className="form-control mb-0" placeholder="" />
                  {aptErr ? <span className="errorClsRegister"> {aptErr}</span> : ''}
                </div>
              </div>
              <div className="col-12 col-sm-6">
                <div className="form-group">
                  <label className="w-100">Zip Code</label>
                  <input name="zipCode" onKeyUp={(e) => { checkValidation(e.target.name, e.target.value, 'string', 6) }} type="text" value={zipCode} onChange={(e) => changeZipCode(e.target.value)} className="form-control mb-0" placeholder="" />
                  {zipCodeErr ? <span className="errorClsRegister"> {zipCodeErr}</span> : ''}
                </div>
              </div>
              <div className="col-12 col-sm-6">
                <div className="form-group">
                  <label className="w-100">City</label>
                  <input name="city" onKeyUp={(e) => { checkValidation(e.target.name, e.target.value, 'string') }} type="text" value={city} onChange={(e) => changeCity(e.target.value)} className="form-control mb-0" placeholder="" />
                  {cityErr ? <span className="errorClsRegister"> {cityErr}</span> : ''}
                </div>
              </div>
              <div className="col-12 col-sm-6">
                <div className="form-group">
                  <label className="w-100">State</label>
                  <input name="state" onKeyUp={(e) => { checkValidation(e.target.name, e.target.value, 'string') }} type="text" value={state} onChange={(e) => changeState(e.target.value)} className="form-control mb-0" placeholder="" />
                  {stateErr ? <span className="errorClsRegister"> {stateErr}</span> : ''}
                </div>
              </div>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <div className="text-right">
              <button type="button" className="btn btn-primary" onClick={() => submitLocation()}>{modalButton}</button>
              <button type="button" className="btn btn-dark" onClick={() => closeModal()}>Cancel</button>
            </div>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }

}