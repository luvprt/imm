import React from "react";
import Modal from "react-bootstrap/Modal";
import { Link } from "react-router-dom";
import _ from 'lodash';
import { ADD_CUSTOMER } from "../../../../routing/routeContants";

export class ViewLocation extends React.Component {

  render() {
    const {openViewModal, closeViewModal, viewLocationData} = this.props;
    let locationData = _.map(viewLocationData, (vl, j) => {
      return <tr key={j}>
          <td>{vl.user.firstName + ' '+ vl.user.lastName}</td>
          <td>{vl.user.email}</td>
          <td>{vl.user.phone}</td>
      </tr>
    })

    return (
      <div>
        <Modal show={openViewModal} onHide={() => closeViewModal()} className="modal-lg-w"> 
          <Modal.Header closeButton>
            <Modal.Title>
              {'# '+ (viewLocationData[0] && viewLocationData[0].streetAddress)}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
          <div className="text-right font-weight-bold">Customer not in the system?<Link to={ADD_CUSTOMER}>Add them here!</Link></div>
              <div className="pr-2 overflow-scroll mt-2 view-location-table">
                  <table className="table mb-3 ">
                      <thead className="thead-light">
                          <tr>
                              <th>Building Repersentative</th>
                              <th>Email</th>
                              <th>Phone</th>
                          </tr>
                      </thead>
                      <tbody>
                          {locationData}
                      </tbody>
                  </table>
              </div>
          </Modal.Body>
        </Modal>
      </div>
    )
  }

}