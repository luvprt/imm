import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BENEFITS_BANNER from '../../../assets/images/theme/benifits-banner.jpg';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { cmsPageDetailBySlug } from '../../../duck/website/website.action';
import { cmsPageDataBySlug } from '../../../duck/website/website.selector';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';


class ServicePage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CmsPageData:'',
            loader: false,
            slug_id: '',
            pageTitle: 'ITM'
        }
    }

    componentDidMount(){
        const slug = this.props.match.params.slug;
        this.setState({loader:true});
        this.props.cmsPageDetailBySlug(slug);
    }

    componentWillReceiveProps(props) {
      if (props.cmsPageDataBySlug && _.has(props.cmsPageDataBySlug, 'data')) {
          this.setState({
            CmsPageData: (props.cmsPageDataBySlug.data && props.cmsPageDataBySlug.data.pageContent),
            pageTitle: (props.cmsPageDataBySlug.data && props.cmsPageDataBySlug.data.pageTitle),
            loader: false,
          })
          if(props.match.params.slug!== this.props.match.params.slug){
            const slug = props.match.params.slug;
            this.setState({loader:true});
            this.props.cmsPageDetailBySlug(slug);
          }
      }
    }

    render() {
        const {CmsPageData, loader, pageTitle} = this.state;

        return (
            <div> 
                <Header loader={loader}/>
                <main>
                    <div className="inner-banner">
                        <div className="inner-banner-image">
                            <img className="img-fluid" src={BENEFITS_BANNER} alt="banner img" />
                        </div>
                        <div className="inner-banner-content">
                            <div className="container">
                                <div className="row d-flex text-center align-items-center">
                                    <h1 className="col-sm-12 text-white">{pageTitle+' Services'}</h1>
                                    <h3 className="col-sm-12 text-white">ITM Tracking is building uniform expectations and requirements for all contractor</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    {ReactHtmlParser(CmsPageData)}
                </main>
                <Footer/>     
            </div>
        );
    }
}

const mapStateToprops = (state) => ({
    cmsPageDataBySlug: cmsPageDataBySlug(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    cmsPageDetailBySlug : cmsPageDetailBySlug
}, dispatch);

export const Service = connect(mapStateToprops, mapDispatchToProps)(ServicePage);