import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BANNER_IMG from '../../../assets/images/details_banner.jpg';
import { getSubscriptionPlanWithHighlightById, updateUserPlan } from '../../../duck/subscription-plan/subscription.action';
import { getPlanDataById, getUpdatedPlan } from '../../../duck/subscription-plan/subscription.selector';
import _ from 'lodash';
import history from '../../../routing/history'
import ReactHtmlParser from 'react-html-parser';
import { HOME, MEMBERSHIP_BASE } from '../../../routing/routeContants';

class CheckoutPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      checkoutPlanData: [],
      annualPlanDetail: [],
      monthlyPlanDetail: [],
      planPrice: [],
      planAddOn: [],
      selectedPlanDuration: 'Monthly',
      selectedPlan: '',
      errPlanMessage: '',
      stripePlanId: '',
      allStripePlan: [],
      checkoutButton: false,
      firstMonthlyPlan: '',
      firstAnnualPlan: '',
      AllAddOnWihPrice: []
    }
  }

  componentDidMount() {
    let userTypeId = this.props.match.params.userTypeId;
    let PlanId = this.props.match.params.planId;
    let userId = this.props.match.params.userId;
    let allStorage = JSON.parse(localStorage.getItem('planDetail'));
    let data = { userTypeId: userTypeId, planTypeId: PlanId, addOns: allStorage.addOn }
    this.props.getSubscriptionPlanWithHighlightById(data)
    if (allStorage && allStorage.remove) {
      this.props.updateUserPlan({ subscriptionPlan: PlanId, userId: userId, userTypeId: userTypeId, addOns: [{}] })
    } else {
      this.setState({ checkoutButton: true })
    }
  }

  componentWillReceiveProps(props) {
    if (props.planDataById && _.has(props.planDataById, 'data')) {
      let annualPlanDetail = {};
      let allStripePlan = {};
      let monthlyPlanDetail = {};
      let planPrice = {};
      let planAddOn = {};
      let m = 0;
      let l = 0;
      _.map(props.planDataById.data.highlightData.subscription, (pl, j) => {
        if (pl.duration && pl.duration.name === 'Monthly') {
          if (m === 0) {
            this.setState({ selectedPlan: pl._id, firstMonthlyPlan: pl._id, stripePlanId: pl.stripePlanId !== null ? pl.stripePlanId : '' })
            m = 1;
          }
          monthlyPlanDetail[pl._id] = pl;
          planPrice[pl._id] = pl.price;
          planAddOn[pl._id] = pl.addOns;
          allStripePlan[pl._id] = (pl.stripePlanId && pl.stripePlanId !== null ? pl.stripePlanId : '');
        } else {
          if (l === 0) {
            this.setState({ firstAnnualPlan: pl._id })
            l = 1;
          }
          annualPlanDetail[pl._id] = pl;
          planPrice[pl._id] = pl.price;
          planAddOn[pl._id] = pl.addOns;
          allStripePlan[pl._id] = (pl.stripePlanId && pl.stripePlanId !== null ? pl.stripePlanId : '');
        }
      })
      this.setState({ AllAddOnWihPrice: props.planDataById.data.addOnsData, checkoutPlanData: props.planDataById.data.highlightData, annualPlanDetail: annualPlanDetail, monthlyPlanDetail: monthlyPlanDetail, planPrice: planPrice, allStripePlan: allStripePlan, planAddOn })
    }
    if (props.getUpdatedPlan === 'PlanUpdated') {
      this.setState({ checkoutButton: true });
      let allStorage = JSON.parse(localStorage.getItem('planDetail'));
      delete allStorage['remove'];
      localStorage.setItem('planDetail', JSON.stringify(allStorage))

    }
  }

  // Set State On Remove Plan 
  removePlan = () => {
    let allStorage = JSON.parse(localStorage.getItem('planDetail'));
    allStorage.remove = this.props.match.params.userId;
    localStorage.setItem('planDetail', JSON.stringify(allStorage))
    history.push(HOME+'#sp')


  }

  // On Click Checkout Button Call Function
  nextCheckout() {
    let planId = this.state.selectedPlan;
    let errPlanMessage = '';
    if (planId !== '') {
      let stripePlanId = this.state.stripePlanId;
      if (stripePlanId !== '') {
        let subscriptionPlan = this.state.selectedPlan;
        let userId = this.props.match.params.userId;
        let planPrice = this.state.planPrice[this.state.selectedPlan];
        let planDuration = this.state.selectedPlanDuration;
        let planName = this.state.checkoutPlanData.name;
        let addonPrice = _.sumBy(this.state.AllAddOnWihPrice, 'price');
        let planData = JSON.parse(localStorage.getItem('planDetail'));
        planData.price = planPrice;
        planData.addonPrice = addonPrice;
        planData.duration = planDuration;
        planData.planName = planName;
        planData.AllAddOn = this.props.planDataById.data.addOnsData;
        localStorage.setItem('planDetail', JSON.stringify(planData))
        history.push(MEMBERSHIP_BASE + subscriptionPlan + '/' + userId + '/' + stripePlanId)
      } else {
        errPlanMessage = "Subscription Plan Not Valid";
      }
    } else {
      errPlanMessage = "Please Select Subscription Plan";
    }

    this.setState({ errPlanMessage: errPlanMessage })
  }

  changeSubscription = (type) => {
    const {firstMonthlyPlan, firstAnnualPlan} = this.state;
    if(type==='Monthly'){
      this.setState({ selectedPlanDuration: "Monthly", selectedPlan: firstMonthlyPlan })
      if(firstMonthlyPlan===''){
        this.setState({ errPlanMessage: "Please Select Subscription Plan" })
      }else{
        this.setState({ errPlanMessage: "" })
      }
    }else{
      this.setState({ selectedPlanDuration: "Annual", selectedPlan: firstAnnualPlan })
      if(firstAnnualPlan===''){
        this.setState({ errPlanMessage: "Please Select Subscription Plan" })
      }else{
        this.setState({ errPlanMessage: "" })
      }
    }
  }

  render() {
    const { checkoutPlanData, selectedPlanDuration, annualPlanDetail, monthlyPlanDetail, planPrice, selectedPlan, allStripePlan, errPlanMessage, checkoutButton, planAddOn, AllAddOnWihPrice } = this.state;

    let allPlan = _.map((selectedPlanDuration === 'Monthly' ? monthlyPlanDetail : annualPlanDetail), (sl, k) => {
      return <option key={k} value={sl._id}>{(sl.isUnlimited === true) ? 'Unlimited' : sl.numberOfPersons}</option>
    })

    let defaultaddOn = _.map((planAddOn[selectedPlan]), (al, uu) => {
      return <span key={uu} className="mr-2 liColor"><small className="fa fa-check mr-1"></small>{al.name}</span>
    })
    let addOnPrice = _.sumBy(AllAddOnWihPrice, 'price');
    let AllAddOnWithCheckbox = _.map(AllAddOnWihPrice, (alp, uup) => {
      return <span key={uup} className="mr-2 liColor"><small className="fa fa-check mr-1"></small>{alp.name + ' - $' + alp.price}</span>
    })

    return (
      <div>
        <Header />
        <main>
          <div className="inner-banner p-0">
            <div className="details_banner_image">
              <img className="img-fluid" src={BANNER_IMG} alt="" />
            </div>
            <div className="inner-banner-content banner-sec">
              <div className="container">
                <div className="row d-flex text-center align-items-center">
                  <h1 className="col-sm-12 text-white">Checkout Steps</h1>
                  <h3 className="col-sm-12 text-white">ITM Tracking is building uniform expectations and requirements for all contractor</h3>
                </div>
              </div>
            </div>
          </div>
          <section className="section profile-p mb-5 divider-height-btm">
            <div className="container mb-5">
              <h4 className="main-color font-weight-bold mb-3">Your Cart</h4>
              <div className="card">
                <div className="card-body profile-main-cnt mb-4">
                  <div className="tab-pane" id="upgrade">
                    <div className="table-upgrade">


                      <div className="row">
                        <div className="col-md-5 col-lg-3 list-content col-content">
                          <div className="title"><h5 className="main-color font-weight-bold mb-0">{checkoutPlanData.name}</h5></div>
                          {ReactHtmlParser(checkoutPlanData.highlights && checkoutPlanData.highlights.planHighLights)}
                        </div>
                        <div className="col-md-7 col-lg-9">
                          <div className="row">
                            <div className="col-lg-3">
                              <div className="title">No.of Person</div>
                              <div className="mb-2">
                                <select value={selectedPlan} onChange={(e) => this.setState({ selectedPlan: e.target.value, stripePlanId: (allStripePlan[e.target.value] ? allStripePlan[e.target.value] : '') })} className="form-control" id="select-no">
                                  <option value="">Select person</option>
                                  {allPlan}
                                </select>
                                {errPlanMessage ? <span className="errorCls"> {errPlanMessage}</span> : ''}
                              </div>
                            </div>
                            <div className="col-lg-3">
                              <div className="title">Subscription</div>
                              <div className="btn-group btn-group-toggle mb-2" >
                                <label className={selectedPlanDuration === "Monthly" ? "btn btn_secondary active" : "btn btn_secondary"}>
                                  <input type="radio" onClick={(e) => this.changeSubscription("Monthly") } /> Monthly
                                    </label>
                                <label className={selectedPlanDuration !== "Monthly" ? "btn btn_secondary active" : "btn btn_secondary"}>
                                  <input type="radio" onClick={(e) => this.changeSubscription("Annual") } /> Annual
                                    </label>
                              </div>
                            </div>
                            <div className="col-lg-3">
                              <div className="title">Plan Amount</div>
                              <h5 className="main-color mb-0">${planPrice && planPrice[selectedPlan] ? planPrice[selectedPlan] : 0}</h5>
                            </div>
                            <div className="col-lg-3 text-lg-right">
                              <div className="title">Sub Total</div>
                              <h5 className="main-color mb-0">${planPrice && planPrice[selectedPlan] ? planPrice[selectedPlan] : 0}</h5>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row align-items-end">
                        <div className="col-lg-10">
                          {defaultaddOn.length>0 ?
                            <div className="plans-tabs">
                              <ul className="list-content p-0">
                                <h5>AddOns - <span className="font-weight-normal"> Already included in plan</span></h5>
                                {defaultaddOn}
                              </ul>
                            </div> : ''
                          }

                          {AllAddOnWithCheckbox.length>0 ?
                            <div className="plans-tabs">
                              <ul className="list-content p-0">
                                <h5>AddOns - <span className="font-weight-normal">More</span></h5>
                                {AllAddOnWithCheckbox}
                              </ul>
                            </div> : ''
                          }
                        </div>
                        {AllAddOnWithCheckbox.length>0 ?
                          <div className="col-lg-2 text-lg-right">
                              <div className="title">AddOn Total</div>
                              <h5 className="main-color">${addOnPrice}</h5>
                          </div>  : ''
                        }
                      </div>
                      <div className="main-color col-content">
                        <div className="w-100 pt-3">
                          <a href="javascript:;" onClick={() => this.removePlan()}>
                            <i className="fa fa-times-circle mr-2"></i>
                            <span>Remove plan</span>
                          </a>
                        </div>
                      </div>
                      <hr />
                      <div className="d-sm-flex justify-content-end align-items-center">
                        <div className="col-content mr-3">
                          <h6 className="d-inline-block mb-0 mr-2">Final Amount :</h6>
                          <h4 className="main-color d-inline-block font-weight-bold mb-0">${planPrice && planPrice[selectedPlan] ? planPrice[selectedPlan] + addOnPrice : 0 + addOnPrice}</h4>
                        </div>
                        <div className="col-content">
                          {checkoutButton ?
                            <span onClick={() => this.nextCheckout()} className="d-block text-center default-rounded-btn btn-primary mb-3 mb-sm-0">
                              Checkout
                                  </span> : ''}
                        </div>
                      </div>


                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  planDataById: getPlanDataById(state),
  getUpdatedPlan: getUpdatedPlan(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getSubscriptionPlanWithHighlightById: getSubscriptionPlanWithHighlightById,
  updateUserPlan: updateUserPlan
}, dispatch);

export const Checkout = connect(mapStateToprops, mapDispatchToProps)(CheckoutPage); 