import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import { contactUs } from '../../../duck/website/website.action';
import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getContactUs } from '../../../duck/website/website.selector'
import _ from 'lodash';
import { validateInputs } from '../../../common/validation';

class ContactUs extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            comments: '',
            phone: '',
            subject: '',
            robot: false,
            errNameMessage: '',
            errEmailMessage: '',
            errCommentsMessage: '',
            errRobotMessage: '',
            errPhoneMessage:'',
            error: '',
            loader: false,
        }
    }

    componentWillReceiveProps(props){
        if (props.getContactUs && props.getContactUs.success) {
            this.setState({ loader: false })
            this.resetForm();
        } else if (props.getContactUs && _.has(props.getContactUs, 'message')) {
            this.setState({ loader: false });
        }
    }

    //Reset Contact Form 
    
    resetForm = () => {
        this.setState({name:'',email:'',phone:'',comments:'',subject:'',robot:''})
    }

    // Submit Contact Form 

    ContactNext = () => {

        const { name, email, comments, phone, subject, robot } = this.state;

        let errPhoneMessage ='', errNameMessage = '', errEmailMessage = '', errCommentsMessage = '', errRobotMessage = '', getError = false;

        if (validateInputs('string', name) === 'empty') {
            errNameMessage = 'Please enter name.';
            getError = true;
        } else if (validateInputs('string', name) === false) {
            errNameMessage = 'Please enter valid name.';
            getError = true;
        }

        if (validateInputs('email', email) === 'empty') {
            errEmailMessage = 'Please enter valid email.';
            getError = true;
        } else if (validateInputs('email', email) === false) {
            errEmailMessage = 'Invalid email.';
            getError = true;
        }

        if (comments === 'empty') {
            errCommentsMessage = 'Please enter comments.';
            getError = true;
        } else if (comments.length < 25) {
            errCommentsMessage = 'Please enter minimum 25 charcters.';
            getError = true;
        }

        if (validateInputs('phone', phone) === false) {
            errPhoneMessage = 'Invalid phone.';
            getError = true;
        }

        if (robot === false) {
            errRobotMessage = 'Please checked robot option.';
            getError = true;
        }

        this.setState({ errNameMessage, errCommentsMessage, errEmailMessage, errRobotMessage, errPhoneMessage, error: getError });

        if (getError === false && errPhoneMessage ==='' && errNameMessage === '' && errCommentsMessage === '' && errEmailMessage === '' && errRobotMessage === '') {
            this.setState({ loader: true }) 
            let contactData = {name, email, comments}
            if(phone !== ''){
                contactData.phone = phone
            }
            if(subject !== ''){
                contactData.subject = subject
            }
            this.props.contactUs(contactData)
        }
    }

    render() {
        const { name, email, comments, phone, subject, robot, errCommentsMessage, errNameMessage, errEmailMessage, errRobotMessage, errPhoneMessage, loader } = this.state

        return (
            <div>
                <Header loader={loader}/>
                <main>
                    <div className="inner-banner p-0">
                        <iframe title="Myframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d443090.19583745464!2d-95.68217666404344!3d29.816880924169766!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8640b8b4488d8501%3A0xca0d02def365053b!2sHouston%2C%20TX%2C%20USA!5e0!3m2!1sen!2sin!4v1566970298217!5m2!1sen!2sin" width="100%" height="450" frameBorder="0" style={{ border: 0 }} allowFullScreen></iframe>
                    </div>
                    <section className="section divider-height-btm pt-0 contactUsPageCls">
                        <div className="container mb-5">
                            <div className="row">
                                <div className="col-12 col-xl-10 contact-us offset-md-1">
                                    <div className="row m-md-0 shadow bg-white">
                                        <div className="col-12 col-md-4 p-md-0 bg-gradient-primary">
                                            <div className="px-4 py-5 h-100">
                                                <h3 className="text-white">Contact</h3>
                                                <h2 className="text-white font-weight-bold">Information</h2>
                                                <p className="text-white mt-3">Have a question, complaint or a compliment? Send it our way! Please be as detailed as possible so that we can better assist you.</p>
                                                <div className="icon-contact text-white my-4">
                                                    <p><i className="fas fa-map-marker-alt"></i> Houston, TX	</p>
                                                    <p><i className="fas fa-phone"></i>7133704486	</p>
                                                    <p className="text-break"><i className="fas fa-envelope"></i> customerservice@itmtracking.com</p>
                                                </div>
                                                <div className="social-icon">
                                                    <Link to="" onClick={e => e.preventDefault()}><i className="fab fa-facebook-f"></i></Link>
                                                    <Link to="" onClick={e => e.preventDefault()}><i className="fab fa-youtube"></i></Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-12 col-md-8 p-md-0">
                                            <div className="form_contact h-100">
                                                <h1 className="mb-4">Get in<span className="font-weight-bold text-primary">Touch</span></h1>
                                                <form className="contact-us-form">
                                                    <div className="row">
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" value={name} onChange={(e) => this.setState({ name: e.target.value })} className="form-control" id="user-name" placeholder="Name" />
                                                                {errNameMessage ? <span className="errorClsRegister"> {errNameMessage}</span> : ''}
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" value={phone} onChange={(e) => this.setState({ phone: e.target.value })} className="form-control" id="user-phone" placeholder="Phone" />
                                                                {errPhoneMessage ? <span className="errorClsRegister"> {errPhoneMessage}</span> : ''}
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="email" value={email} onChange={(e) => this.setState({ email: e.target.value })} className="form-control" id="user-email" placeholder="Email" />
                                                                {errEmailMessage ? <span className="errorClsRegister"> {errEmailMessage}</span> : ''}
                                                            </div>
                                                        </div>
                                                        <div className="col-md-6">
                                                            <div className="form-group">
                                                                <input type="text" value={subject} onChange={(e) => this.setState({ subject: e.target.value })} className="form-control" id="user-subject" placeholder="Subject" />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12">
                                                            <div className="form-group">
                                                                <textarea rows="4" className="comments" placeholder="Comments" onChange={(e) => this.setState({ comments: e.target.value })} value={comments}></textarea>
                                                                {errCommentsMessage ? <span className="errorClsRegister"> {errCommentsMessage}</span> : ''}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="d-sm-flex justify-content-between mt-3">
                                                        <div className="">
                                                            <div className="custom-control custom-checkbox">
                                                                <input type="checkbox" checked={robot} onChange={(e) => this.setState({ robot: e.target.checked })} className="custom-control-input" id="robo" />
                                                                <label className="custom-control-label font-weight-bold" htmlFor="robo">I am not a robot</label>
                                                                {errRobotMessage ? <span className="errorClsRegister"> {errRobotMessage}</span> : ''}
                                                            </div>
                                                        </div>
                                                        <div className="btn-wrapper">
                                                            <span onClick={() => this.resetForm()} className="default-rounded-btn btn-dark mr-2 mb-3 mb-sm-0">
                                                                <span className="btn-inner--text">Cancel</span>
                                                            </span>
                                                            <span onClick={() => this.ContactNext()} className="default-rounded-btn btn-primary mb-3 mb-sm-0">
                                                                <span className="btn-inner--text"> Send </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>
                <Footer />
            </div>
        );
    }
}

const mapStateToprops = (state) => ({
    getContactUs: getContactUs(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    contactUs : contactUs
}, dispatch);

export default connect(mapStateToprops, mapDispatchToProps)(ContactUs); 