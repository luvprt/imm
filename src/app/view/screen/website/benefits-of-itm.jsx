import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BENEFITS_BANNER from '../../../assets/images/theme/benifits-banner.jpg';
import BENEFITS_IMG1 from '../../../assets/images/theme/benifits-img1.png';
import BENEFITS_IMG2 from '../../../assets/images/theme/benifits-img2.png';
import BENEFITS_IMG3 from '../../../assets/images/theme/benifits-img3.png';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCmsPageDataAction } from '../../../duck/website/website.action';
import { getCmsPageData } from '../../../duck/website/website.selector';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';


class BenefitsPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CmsPageData:'',
            loader: false,
        }
    }

    componentDidMount(){
        this.setState({loader:true});
        let title = ((this.props.location.pathname).replace("/","")).replace(/[_\W]+/g, " ");
        this.props.getCmsPageDataAction(title);
    }

    componentWillReceiveProps(props) {
        if (props.getCmsPageData && _.has(props.getCmsPageData, 'data')) {
            this.setState({
              CmsPageData: (props.getCmsPageData.data && props.getCmsPageData.data.pageContent),
              loader:false
            })
        }
    }

      render() {
        const {CmsPageData, loader} = this.state;
        return (
            <div>
                <Header loader={loader}/>
                <main>
                    <div className="inner-banner">
                        <div className="inner-banner-image">
                            <img className="img-fluid" src={BENEFITS_BANNER} alt="" />
                        </div>
                        <div className="inner-banner-content">
                            <div className="container">
                                <div className="row d-flex text-center align-items-center">
                                    <h1 className="col-sm-12 text-white">Benefits of ITM Tracking</h1>
                                    <h3 className="col-sm-12 text-white">ITM Tracking is building uniform expectations and requirements for all contractor</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    {ReactHtmlParser(CmsPageData)}
                    {/* <section className="section divider-height-btm">
                        <div className="container">
                            <div className="row row-grid align-items-center  mb-5">
                                <div className="col-xl-6 col-lg-5 text-center">
                                    <img src={BENEFITS_IMG1} className="img-fluid floating" alt="" />
                                </div>
                                <div className="col-xl-6 col-lg-7">
                                    <div className="pr-md-5">
                                    <h3 className="mb-3">Benefits for <span className="font-weight-bold text-primary mt-5">City/AHJ</span></h3>
                                    <ul className="list-unstyled mt-3">
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Creates a means to communicate with Contractors & Building Owners.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Allows Fire Marshals to focus more on enforcement, while eliminating possible busy work.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">TM Tracking helps local AHJ with immediate notification of non-compliant buildings.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">ITM Tracking brings awareness to the building owners of the required quarterly, semi-annual, annual, 3rd yr. & 5th yr. NFPA & City standards. </p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Currently thousands of businesses are delinquent with all required inspections.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">ITM Tracking could possibly generate new jobs for fire marshals; due to more accurate reporting of compliant and non-compliant buildings.</p>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center">
                                <div className="col-xl-6 col-lg-5 order-lg-2 text-center">
                                    <img src={BENEFITS_IMG2} className="img-fluid floating" alt="" />
                                </div>
                                <div className="col-xl-6 col-lg-7 order-lg-1">
                                    <div className="pr-md-5">
                                    <h3 className="mb-3">Benefits for <span className="font-weight-bold text-primary mt-5">Contractor</span></h3>
                                    <ul className="list-unstyled mt-3">
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Creates a means to communicate with Contractors & Building Owners.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Creates a means to communicate with AHJ & Building Owners.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Reliability and peace of mind that all inspection reports are reported in timely and accurate manner to given AHJ’s “less admin work, which in return lowers your overhead”.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">24/7 online access to your submitted reports.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Capability to sort your compliant and non-compliant buildings “Now you know who needs a repair quote, by sorting your blue, yellow & red tagged buildings”.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Your customer’s will have access to their reports online 24/7. Less busy work for your admins.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">When you submit a building by address a username and password will be automatically sent to the building representative.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">No longer a need to save building reports to company server.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Building owners will be notified ahead of time when inspections are due, which in turn helps the contractor stay on top of inspection due dates.</p>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center  mb-5">
                                <div className="col-xl-6 col-lg-5 text-center">
                                    <img src={BENEFITS_IMG3} className="img-fluid floating" alt="" />
                                </div>
                                <div className="col-xl-6 col-lg-7">
                                    <div className="pr-md-5">
                                    <h3 className="mb-3">Benefits for <span className="font-weight-bold text-primary mt-5">Building Owners/Representatives</span></h3>
                                    <ul className="list-unstyled mt-3">
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Creates a means to communicate with Contractors & Building Owners.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Allows Fire Marshals to focus more on enforcement, while eliminating possible busy work.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">TM Tracking helps local AHJ with immediate notification of non-compliant buildings.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">ITM Tracking brings awareness to the building owners of the required quarterly, semi-annual, annual, 3rd yr. & 5th yr. NFPA & City standards.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">Currently thousands of businesses are delinquent with all required inspections.</p>
                                            </div>
                                        </li>
                                        <li className="py-2">
                                            <div className="d-flex align-items-top">
                                                <div className="badge badge-circle badge-primary mr-3">
                                                <i className="fa fa-check"></i>
                                                </div>
                                                <p className="mb-0">ITM Tracking could possibly generate new jobs for fire marshals due to more accurate reporting of compliant and non-compliant buildings.</p>
                                            </div>
                                        </li>
                                    </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </section>  */}
                </main>
                <Footer />
            </div>
        );
    }
}

const mapStateToprops = (state) => ({
    getCmsPageData: getCmsPageData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getCmsPageDataAction : getCmsPageDataAction
}, dispatch);

export const Benefits = connect(mapStateToprops, mapDispatchToProps)(BenefitsPage);