import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';


export class LocateContractors extends Component {

    render() {

        return (
            <div>
                <Header />

                <main>
                    <div className="inner-banner">
                        <div className="inner-banner-image">
                            <img className="img-fluid" src="assets/images/theme/benifits-banner.jpg" />
                        </div>
                        <div className="inner-banner-content">
                            <div className="container">
                                <div className="row d-flex text-center align-items-center">
                                    <h1 className="col-sm-12 text-white"> Locate a Contractor</h1>
                                    <h3 className="col-sm-12 text-white"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy  </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section className="contractor-category section divider-height-btm">
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-xl-9 col-lg-9 text-center">
                                    <div className="card">
                                        <nav className="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
                                            <a className="flex-sm-fill text-sm-center nav-link active" data-toggle="tab" href="#category-tab1" role="tab" >Sprinkler Contractor</a>
                                            <a className="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#category-tab2" role="tab" aria-selected="false">Extinguisher Contractor</a>
                                            <a className="flex-sm-fill text-sm-center nav-link" data-toggle="tab" href="#category-tab3" role="tab" aria-selected="false">Alarm Contractor</a>
                                        </nav>
                                        <div className="card-body">
                                            <div className="tab-content">
                                                <div className="tab-pane fade show active" id="category-tab1" role="tabpanel">
                                                    <div className="row">
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text new</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="tab-pane fade" id="category-tab2" role="tabpanel">
                                                    <div className="row">
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random test</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="tab-pane fade" id="category-tab3" role="tabpanel">
                                                    <div className="row">
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random test</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="col-sm-4 mb-3">
                                                            <div className="category-card">
                                                                <div className="card-title bg-primary-gradient text-white">
                                                                    Contractor lorem
													</div>
                                                                <div className="card_cnt_sec">
                                                                    <div className="infomation">
                                                                        <i className="fa fa-list text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-map-marker-alt text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        <i className="fa fa-phone text-primary"></i>
                                                                        <span>117 5 Lorem Ipsum is not simply random text</span>
                                                                    </div>
                                                                    <div className="infomation">
                                                                        TDI #ACR-1603362
														</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </main>

                <Footer />
            </div>
        );
    }
}