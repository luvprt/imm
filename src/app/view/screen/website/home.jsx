import React, { Component } from 'react';
import history from '../../../routing/history'
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import HERO_IMG1 from '../../../assets/images/hero-img1.png';
import HERO_IMG2 from '../../../assets/images/hero-img2.png';
import HERO_IMG3 from '../../../assets/images/hero-img3.png';
import PROJECT_MNGMNT_IMG from '../../../assets/images/project-mngment-img.png';
import COMPUTERIZET from '../../../assets/images/computrizet-mngment-img.png';
import CONTRACTOR_PRO_IMG from '../../../assets/images/contractor-pro-img.png';
import SLIDER1 from '../../../assets/images/sloder-logo-1.jpg';
import SLIDER2 from '../../../assets/images/sloder-logo-2.jpg';
import SLIDER3 from '../../../assets/images/sloder-logo-3.jpg';
import SLIDER4 from '../../../assets/images/sloder-logo-4.jpg';
import LOCATE from '../../../assets/images/locate-map-bg.png'
import Slider from "react-slick";
import '../../../assets/css/slick.css'
import '../../../assets/css/slick-theme.css'
import { REGISTRATION } from '../../../routing/routeContants';
import { getSubscriptionPlanWithHighlight } from '../../../duck/subscription-plan/subscription.action'
import {getSubscriptionPlanWithHighlightData } from '../../../duck/subscription-plan/subscription.selector'
import ReactHtmlParser from 'react-html-parser';
import _ from 'lodash';
import { Link, NavLink } from "react-router-dom";
import { CHECKOUT_BASE, LOCATE_CONTRATORS, HOW_IT_WORKS, CONTACT_US } from '../../../routing/routeContants';

class HomePage extends Component {

    constructor(props){
        super(props)
        this.myRef = React.createRef(); 
        this.state = {
            settings: {
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 2,
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 2,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 991,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2,
                      initialSlide: 2,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false 
                    }
                  }
                ]
            },
            settingsHomeBanner: {
                dots: false,
                arrows: false,
                infinite: true,
                autoplay: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 991,
                    settings: {
                      slidesToShow:1,
                      slidesToScroll: 1,
                      initialSlide: 1,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false 
                    }
                  }
                ]
            },
            settingsBottom: {
                dots: true,
                infinite: true,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 2,
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 2,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 991,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 2,
                      initialSlide: 2,
                      dots: true,
                      arrows: false 
                    }
                  },
                  {
                    breakpoint: 480,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      dots: true,
                      arrows: false 
                    }
                  }
                ]
          },
            flipped:'',
            subscriptionData:[],
            subscribtionTab:0,
            selected_addOn: [],
            homeData: '',
            addOnsData:[]
        }
    }

    componentDidMount(){
        this.props.getSubscriptionPlanWithHighlight();
    }

    componentWillReceiveProps(props) {
        if (props.getSubscriptionPlanWithHighlightData && _.has(props.getSubscriptionPlanWithHighlightData, 'data')) {
            this.setState({
              subscriptionData: props.getSubscriptionPlanWithHighlightData.data.planData,
              homeData: props.getSubscriptionPlanWithHighlightData.data.homeData && props.getSubscriptionPlanWithHighlightData.data.homeData.pageContent,
              addOnsData: props.getSubscriptionPlanWithHighlightData.data.addOnsData && props.getSubscriptionPlanWithHighlightData.data.addOnsData,
            })
        }
        if(props.history.location.hash==='#sp'){
          this.scrollToMyRef();
        }
    }

    onFlipped = (id) => {
        let selected_addOn = []
        this.setState({flipped: id, selected_addOn})
    }

    onchnageUserType = (j) =>{
      this.setState({subscribtionTab:j})
    }

    // Save Addon on Change Function 
    changeAddon(e){ 
      const selected_addOn = this.state.selected_addOn;
      if(e.target.checked===true){
        selected_addOn.push(e.target.name);
      }else{
        let index = selected_addOn.indexOf(e.target.name)
        selected_addOn.splice(index,1)
      };
      this.setState({ selected_addOn});
    }

    moveToRegister(planId,userType, addOn){
      let allStorage = JSON.parse(localStorage.getItem('planDetail'));
      if(allStorage && allStorage.remove){
        let userId = allStorage.remove;
        let setData = {subscriptionPlan:planId,user:userType,addOn:addOn, remove: userId}
        localStorage.setItem('planDetail',JSON.stringify(setData))
        history.push(CHECKOUT_BASE+planId+'/'+userType.id+'/'+userId);
      }else{
        let setData = {subscriptionPlan:planId,user:userType,addOn:addOn}
        localStorage.setItem('planDetail',JSON.stringify(setData))
        history.push(REGISTRATION);
      }
    }

    scrollToMyRef = () => {
      this.myRef.current.scrollIntoView()
    }

  render() {
    const { settingsBottom, settingsHomeBanner, settings,flipped, subscriptionData, subscribtionTab, selected_addOn, homeData, addOnsData} = this.state
    console.log(selected_addOn)
    let subscriptionPlan
    let allSubscription = _.map(subscriptionData, (sb, j) => {
      return <span className={subscribtionTab===j ? "linkCls active default-rounded-btn tab-btn-primary" : "linkCls default-rounded-btn tab-btn-primary"} onClick={() => this.onchnageUserType(j)} key={j}>{sb.name} Plans</span>
    })

    if(subscriptionData[subscribtionTab] && subscriptionData[subscribtionTab]['planData']){
      subscriptionPlan = _.map(subscriptionData[subscribtionTab]['planData'], (pl, j) => {
        let userTypeID = subscriptionData[subscribtionTab]._id;
        let userTypeName = subscriptionData[subscribtionTab].name;
        let userType = {id:userTypeID,name:userTypeName};
        let addOn = {};
        let addOnCheck = [];
        let allPlan = _.map(pl['subscription'], (sl, k) => {
                _.map(sl['addOns'], (ad, d) => {
                    addOn[ad._id] = ad;
                    addOnCheck.push(ad._id);
                })
                  if(sl.numberOfPersons===2 || sl.numberOfPersons===5 || sl.isUnlimited){
                    return <div className="grid-item" key={k}>
                      <p className="mb-1"><small>$</small><strong>{sl.price} </strong></p><sup>{ sl.isUnlimited ? 'for unlimited users' : 'for '+ sl.numberOfPersons+ ' users'} / {sl.duration.name}</sup>
                    </div>
                  }
              })
        let str = pl.name;
        let match = str.includes(" Pro");
        //with checkboxes Add-ON
        let AllAddOnWithCheckbox = _.map(addOnsData, (adon, l) => {
                return _.includes(addOnCheck, adon._id) || <div className="custom-control custom-checkbox" key={l}>
                  <input name={adon._id}  onChange={(e) => this.changeAddon(e)}  checked={_.includes(selected_addOn,adon._id)} type="checkbox" className="custom-control-input" id={adon._id} />
                  <label className="custom-control-label" htmlFor={adon._id}>{adon.name+' - $'+adon.price}</label>
               </div>
         })
        // without checkboxes
        let AllAddOn = _.map(addOn, (al,uu) => <span key={uu} className="mr-2 liColor"><small className="fa fa-check mr-1"></small>{al.name}</span>)

        return <div className="col-lg-4 scene p-lg-0 mb-4 mb-lg-0 mainClsForCms" key={j}>
        <div className={flipped===pl._id ? (match ? "pricing-active is-flipped" : "pricing_cardOne is-flipped") : (match ? "pricing-active" : "pricing_cardOne")} >
          <div id="cardOne-front" className="pricing-card bg-white front tab-pane active" role="tabpanel" aria-labelledby="request-tab">
            <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
            <h5 className="text-uppercase">{pl.name}</h5>
            <div className="grid-container">
              {  allPlan }
            </div>
            <div className="list-content p-0">{ ReactHtmlParser(pl.highlights && pl.highlights.planHighLights) }</div>
            
            <div className="d-block text-center">
              <span onClick={()=>this.onFlipped(pl._id)} className="btn btn-sm text-capitalize btn-outline-secondary tab-pane flip-btn1">View Plan</span>
            </div>
          </div>
          <div id="cardOne-back" className={match ?"pricing-card bg-white back tab-pane proCls":"pricing-card bg-white back tab-pane"} role="tabpanel" aria-labelledby="request-tab">
            <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
            <h5 className="text-uppercase">{pl.name}</h5>
            <div className="grid-container">
              {  allPlan }
            </div>
            <div className="plans-tabs overflow-scroll">
              { ReactHtmlParser(pl.highlights && pl.highlights.description) }
              {AllAddOn.length>0 ? 
                <ul className="list-content p-0">
                  <h5>AddOns - <span className="font-weight-normal"> Already included in plan</span></h5>
                  {AllAddOn}
                  {/* <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="permit-tracking" />
                    <label className="custom-control-label" htmlFor="permit-tracking">Permit Tracking (Depends on cost of permit)</label>
                  </div> */}
                </ul> : ''
              }
            </div>
            <div className="plans-tabs overflow-scroll">
                <ul className="list-content p-0">
                  <h5>AddOns - <span className="font-weight-normal">More</span></h5>
                  {AllAddOnWithCheckbox}
                </ul>
            </div>
            <div className="d-flex align-items-center">
              <span onClick={()=>this.onFlipped('')} className={match ? "cursorCls text-left tab-pane flip-btn2 text-white": "cursorCls text-left tab-pane flip-btn2 text-white bg-primary-gradient" }><i className="fas fa-arrow-left"></i></span>
              
              <button onClick={() => this.moveToRegister(pl._id,userType,selected_addOn)} className="display-block text-center mx-auto btn btn-sm text-capitalize btn-outline-secondary">Buy Now</button>
            </div>
            
          </div>
        
        </div>
        </div>
      })
    }

    return (
        <div> 
            <Header/>
            <main>                
                <div className="hero-header">
                    <section className="section section-shaped bg-white">
                        <div className="container py-lg-md d-flex">
                        <div className="col px-0">
                            <div className="row">
                            <div className="col-xl-5 col-lg-4 order-lg-2">
                                <div className="hero-img">
                                  <Slider {...settingsHomeBanner}>
                                          <img src={HERO_IMG1} className="img-fluid floating" alt=""/>
                                          <img src={HERO_IMG2} className="img-fluid floating" alt=""/>
                                          <img src={HERO_IMG3} className="img-fluid floating" alt=""/>
                                    </Slider>
                                </div>
                            </div>
                            <div className="col-xl-7 col-lg-8 order-lg-1">
                                <h1 className="display-3"><span className="hero-text1">Helping Establish an</span>
                                <span className="d-block font-weight-bold">All code Compliant City</span>
                                </h1>
                                <p className="lead">Built and designed by industry professionals. <br/> 
                                Providing an innovative approach to running your business.</p>
                                <div className="btn-wrapper">
                                <Link to={HOW_IT_WORKS} className="btn btn-dark btn-lg  mb-3 mb-sm-0">
                                    <span className="btn-inner--text text-uppercase">How it Works</span>
                                </Link>
                                <span className="btn btn-primary btn-lg mb-3 mb-sm-0" onClick={() => this.scrollToMyRef()}>
                                    <span className="btn-inner--text text-uppercase">Get Started</span>
                                </span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <div className="separator separator-bottom separator-hero">
                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 788.904" style={{"enableBackground":"new 0 0 1920 788.904"}} >
                                <image style={{'overflow':'visible'}} width="1903" height="1700" id="XMLID_2_" transform="matrix(1.0108 0 0 1.0108 4655.0688 893.8212)">
                                </image>
                                <polygon id="XMLID_3_" style={{'fill':'#EBEEF1'}} points="1920,0 607,758.904 0,662.904 0,788.904 1920,788.904 " />
                            </svg>
                        </div>
                    </section>
                </div>

                <section className="section divider-bottom bg-secondary-light pt-5">
                    <div className="container">
                        <div className="row justify-content-center m-0">
                            <div className="col-xl-9">
                                <h2 className="section--title text-center">Features of ITM Suite</h2>
                                <h4 className="section--sub-title text-center mb-5 mt-4">Features designed to help you run your business with the customer in mind. Stop jumping in between multiple softwares and use our all in one Suite.</h4>
                            </div>
                            <div className="col-lg-12">
                                <div id="home_slider" className="home-slider slider">
                                    <Slider {...settings}>
                                            <div className="home-slider-wrap ">
                                                <div className="card card-lift--hover shadow border-0">
                                                    <div className="card-body py-5">
                                                        <div className="icon icon-shape rounded-circle mb-4">
                                                            <i className="fal fa-list-alt"></i>
                                                        </div>
                                                        <h6><span>Customer Management</span> Feature</h6>
                                                        <p className="description mt-3">Managing your customer accounts is a breeze with Service Fusion. Easily access customer's contact information.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="home-slider-wrap">
                                                <div className="card card-lift--hover shadow border-0">
                                                    <div className="card-body py-5">
                                                        <div className="icon icon-shape rounded-circle mb-4">
                                                            <i className="fal fa-file-export"></i>
                                                        </div>
                                                        <h6><span>Dispatching</span> & <span>Scheduling</span></h6>
                                                        <p className="description mt-3">Schedule and dispatch jobs directly to field workers' smartphones through our mobile app or via text messages.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="home-slider-wrap">
                                                <div className="card card-lift--hover shadow border-0">
                                                    <div className="card-body py-5">
                                                        <div className="icon icon-shape rounded-circle mb-4">
                                                            <i className="fal fa-tasks"></i>
                                                        </div>
                                                        <h6><span>Asset Management</span><br/> Feature</h6>
                                                        <p className="description mt-3">Our GPS tracking solutions allow you to take full control of your service fleet without relying on the field workers' mobile phones.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="home-slider-wrap">
                                                <div className="card card-lift--hover shadow border-0">
                                                    <div className="card-body py-5">
                                                        <div className="icon icon-shape rounded-circle mb-4">
                                                            <i className="fal fa-mobile-android"></i>
                                                        </div>
                                                        <h6>Mobile & <br/><span>Web Apps</span></h6>
                                                        <p className="description mt-3">Win more repeat customers by putting your own personalized, branded mobile app on their smartphones.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="home-slider-wrap">
                                                <div className="card card-lift--hover shadow border-0">
                                                    <div className="card-body py-5">
                                                        <div className="icon icon-shape rounded-circle mb-4">
                                                            <i className="fa fa-phone"></i>
                                                        </div>
                                                        <h6>Customer Management <span>Feature</span></h6>
                                                        <p className="description mt-3">Managing your customer accounts is a breeze with Service Fusion. Easily access customer's contact information.</p>
                                                    </div>
                                                </div>
                                            </div>
                                        <div className="home-slider-wrap">
                                            <div className="card card-lift--hover shadow border-0">
                                                <div className="card-body py-5">
                                                    <div className="icon icon-shape rounded-circle mb-4">
                                                        <i className="fa fa-phone"></i>
                                                    </div>
                                                    <h6>Customer Management <span>Feature</span></h6>
                                                    <p className="description mt-3">Managing your customer accounts is a breeze with Service Fusion. Easily access customer's contact information.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="separator separator-bottom separator-hero">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 1920 128.058" style={{"enableBackground":"new 0 0 1920 128.058"}} >
                            <image style={{'overflow':'visible'}} width="1903" height="1700" id="XMLID_2_"  transform="matrix(1.0108 0 0 1.0108 2474.1865 -907.369)">
                            </image>
                            <polygon id="XMLID_3_" style={{"fill":"#ffffff"}} points="158.824,674.288 -1154.176,1433.191 -1761.176,1337.191 -1761.176,1463.191 
                    158.824,1463.191 " />
                            <polygon id="XMLID_18_" style={{"fill":"#ffffff"}} points="-529,-335.263 1038.56,-206.942 1391,-271.103 1391,-334.765 " />
                            <polygon id="XMLID_33_" style={{"fill":"#FFFFFF"}} points="0,0 0,128.058 1920,128.058 1920,0 1531,123.154 " />
                        </svg>
                    </div>
                </section>
                {ReactHtmlParser(homeData)}
                {/* <section className="section section-lg bg-white section-third-h">
                    <div className="container">
                        <div className="row row-grid align-items-center">
                        <div className="col-xl-6 col-lg-5 order-lg-2">
                            <img src={PROJECT_MNGMNT_IMG} className="img-fluid floating" alt="floating img1" />
                        </div>
                        <div className="col-xl-6 col-lg-7 order-lg-1">
                            <div className="pr-lg-5">
                            <h3 className="mb-3"><span className="font-weight-bold text-primary">Project Management</span> Suite</h3>
                            <p className="text-justify text-md-left default-text-style">Managing your clients doesn't have to be difficult. With ITM Suite we take the headache out of project management and allow you to concentrate on your customers. Our Project Management Suite allows you to communicate with everyone on your team while keeping the customer updated throughout the whole project scope. Assign techicians, schedule jobs, send updates through SMS to customers & techs, and much more. </p>
                            <ul className="list-unstyled mt-5">
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                    <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                    </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Premium customer management within ITM. Create customers with ease. </p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">With Fleet Management you can track everything from fuel, mileage, and GPS</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Employee Management is an easy process. Add, update and control your team. </p>
                                    </div>
                                </div>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                        
                        <div className="row row-grid align-items-center  mb-5">
                        <div className="col-xl-6 col-lg-6">
                            <img src={COMPUTERIZET} className="img-fluid floating" alt="floating img2" />
                        </div>
                        <div className="col-xl-6 col-lg-6">
                            <div className="pr-lg-5">
                            <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Service Management</span> Suite</h3>
                            <p className="text-justify text-md-left default-text-style" >Imagine a service company that is able to generate reports and work orders while keeping all essential personel in the loop at all times. Cool right? ITM Service Management Suite is a streamlined set of features allowing the Contractor to quickly generate work orders & reports all from the calendar during your booking process. All your technicians and customers stay informed allowing you to concentrate on the tasks ahead. </p>
                            <ul className="list-unstyled mt-5">
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                    <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                    </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Create multiple customer contacts, phone numbers</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Setup multiple service locations & credit cards on file</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Store equipment information, documents, images & view history </p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Create multiple customer contacts, phone numbers</p>
                                    </div>
                                </div>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                        
                        <div className="row row-grid align-items-center">
                        <div className="col-xl-6 col-lg-5 order-lg-2">
                            <img src={CONTRACTOR_PRO_IMG} className="img-fluid floating" alt="floating img3" />
                        </div>
                        <div className="col-xl-6 col-lg-7 order-lg-1">
                            <div className="pr-lg-5">
                            <h3 className="mb-3"><span className="font-weight-bold text-primary">Contractor</span> Pro</h3>
                            <p className="default-text-style text-justify text-md-left" >Contractor Pro is by far our most treasured possession on ITM Suite. We have included everything you need in order to run a successfull Service & Customer Management experience.</p>
                            <ul className="list-unstyled mt-5">
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                    <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                    </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Easily migrate your current customer & employee base with our Quickbooks API</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Communicate with your employees in the field with direct chat and texting</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">No need to use your phone. Call anyone from your browser easily.</p>
                                    </div>
                                </div>
                                </li>
                                <li className="py-2">
                                <div className="d-flex align-items-center">
                                    <div>
                                        <div className="badge badge-circle badge-primary mr-3">
                                        <i className="fa fa-check"></i>
                                        </div>
                                    </div>
                                    <div>
                                    <p className="mb-0">Know the status of your Jobs with our dispatch feature. See everything live.</p>
                                    </div>
                                </div>
                                </li>
                            </ul>
                            </div>
                        </div>
                        </div>
                    </div>
                </section> */}

               
                <section className="section bg-secondary separator-bottom divider-top">

      <div className="separator separator-top separator-hero">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 1920 128.058" style={{"enableBackground":"new 0 0 1920 128.058"}} >
                            <image style={{'overflow':'visible'}} width="1903" height="1700" id="XMLID_2_"  transform="matrix(1.0108 0 0 1.0108 2474.1865 -907.369)">
                            </image>
                            <polygon id="XMLID_3_" style={{"fill":"#ffffff"}} points="158.824,674.288 -1154.176,1433.191 -1761.176,1337.191 -1761.176,1463.191 
                    158.824,1463.191 " />
                            <polygon id="XMLID_18_" style={{"fill":"#ffffff"}} points="-529,-335.263 1038.56,-206.942 1391,-271.103 1391,-334.765 " />
                            <polygon id="XMLID_33_" style={{"fill":"#FFFFFF"}} points="1920,128.058 1920,0 0,0 0,128.058 389,4.904 " />
                        </svg>
                    </div>


      <div className="container">
        <div className="row row-grid align-items-center justify-content-center pricing-table">
			
          <div className="col-lg-8 text-center"  ref={this.myRef}>
            <h2 className="section--title">Pricing</h2>
            <h5 className="section--sub-title text-center">We believe we have created the most efficient platform for Contractors within almost any industry. No set-up fees and no contract to sign up. Go month-to-month and cancel anytime! </h5>
          </div>
          <div className="col-12 mb-sm-4">
            <div className="row align-items-center justify-content-center">
              {allSubscription}
            </div>
          </div>
          <div className="col-12 mb-4  mt-0 mt-sm-5 pricingTable_cols">
            <div className="row align-items-center">
              {subscriptionPlan} 
              {/*  <div className="col-lg-4 scene p-lg-0 mb-4 mb-lg-0">
              <div className="pricing-active">
                <div id="pricing-cd"  className="pricing-card bg-white front tab-pane active" role="tabpanel" aria-labelledby="request-tab" >
                  <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
                  <div className="pricing-cd" >
                    <h5 className="text-uppercase">Contactor Pro</h5>
                    <div className="grid-container">
                      <div className="grid-item">
                        <p className="mb-1"><small>$</small><strong>250 </strong></p><sup>for 2 users / per month</sup>
                      </div>
                      <div className="grid-item">
                        <p className="mb-1"><small>$</small><strong>642 </strong></p><sup>for 5 users / per month</sup>
                      </div>
                      <div className="grid-item">
                        <p className="mb-1"><small>$</small><strong>1235 </strong></p><sup>for 10 users / per month</sup>
                      </div>
                    </div>
                    <ul className="list-content p-0">
                      <li>Project Management Suite</li>
                      <li>Service Management Suite</li>
                      <li>Quickbooks Integration</li>
                      <li>Gmail Integration</li>
                      <li>Report & Quote Designer</li>
                      <li>Embedded Text Messaging</li>
                      <li>Embedded Voice & Call Recording</li>
                      <li>Document Storage</li>
                      <li>ITM NFPA Template Reports</li>
                      <li>ITM Verified Vendors</li>			
                    </ul>
                    <div className="d-block text-center nav nav-pills">
                      <a href="#view-plan-pro" data-toggle="pill" aria-selected="true" className="btn btn-sm text-capitalize btn-outline-secondary tab-pane flip-btn1">View Plan</a>
                    </div>
                  </div>
                </div>
                <div  id="view-plan-pro"  className="pricing-card bg-white back tab-pane" role="tabpanel" aria-labelledby="request-tab" >
                  <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
                  <h5 className="text-uppercase">Contactor Pro</h5>
                  <div className="grid-container">
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>250 </strong></p><sup>for 2 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>642 </strong></p><sup>for 5 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>1235 </strong></p><sup>for 10 users / per month</sup>
                    </div>
                  </div>
                  <div className="plans-tabs overflow-scroll">
                    <ul className="list-content p-0">
                      <h5>Service Suite - <span className="font-weight-normal">Includes</span></h5>
                      <li>Asset Management Software</li>
                      <li>Custom work Order and quotes</li>
                      <li>Job estimating & tracking</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Report Designer - <span className="font-weight-normal">Build Your Own Reports</span></h5>
                      <li>The user can create their own reports form our software instead of using ITMs standrads forms. The benifits contractors who may not be the part of...</li>
                      <li>Links to work Orders and Quotes</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Project Management Suite- <span className="font-weight-normal">Includes</span></h5>
                      <li>Team Management Software</li>
                      <li>Customer Management Software</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Life safety Reporting - <span className="font-weight-normal">5 free reports a month</span></h5>
                      <li>Access to all life Safety Report with work order and quote linking</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>White Label - <span className="font-weight-normal">Software</span></h5>
                      <li>Your Own Domain Like : <a className="text-white" href="#">https:yourdomain.itmsuite.com</a></li>
                      <li>Your Reports, Your designs, Your Website.</li>
                      <li>Send your customers to your domain to ensure they're at the right place.</li>
                      <li>All Building Owners who sign up at your domain are automatically added to your account.</li>
                      <li>Build Confidence with your Customers.</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Add - ons - <span className="font-weight-normal">Includes</span></h5>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="permit-tracking" />
                        <label className="custom-control-label" htmlFor="permit-tracking">Permit Tracking (Depends on cost of permit)</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="managed-doc" />
                        <label className="custom-control-label" htmlFor="managed-doc">Free | 15GB Managed Document Storage</label>
                      </div>
                    </ul>
                  </div>
                  <div className="d-flex align-items-center">
                    <a href="#pricing-cd" data-toggle="pill" aria-selected="false" className="text-left tab-pane flip-btn2 text-white"><i className="fas fa-arrow-left"></i></a>
                    
                    <button className="display-block text-center mx-auto btn btn-sm text-capitalize btn-outline-secondary">Buy Now</button>
                  </div>
                </div>
              </div>
              </div>
              <div className="col-lg-4 scene pl-lg-0 mb-4 mb-lg-0">
              <div className="pricing-active">
                <div id="cardThird-front" className="pricing-card bg-white">
                  <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
                  <h5 className="text-uppercase">Project Management Suite</h5>
                  <div className="grid-container">
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>58 </strong></p><sup>for 2 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>73 </strong></p><sup>for 5 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>125.99 </strong></p><sup>for unlimited users / per month</sup>
                    </div>
                  </div>
                  <ul className="list-content p-0">
                    <li>Customer Management</li>
                    <li>Employee Management</li>
                    <li>Invoicing & Time Keeping</li>
                    <li>Gmail Integration</li>
                    <li>Direct Chat</li>
                    <li>Sales Tracking</li>
                    <li>Premium Project Management</li>
                    <li>Basic Service Management</li>
                    <li>Per Report Pricing on ITM NFPA Inspections</li>
                    <li>ITM Verified Vendors</li>
                  </ul>
                  <div className="d-block text-center">
                    <a href="#cardThird-back" data-toggle="pill" aria-selected="false" className="btn btn-sm text-capitalize btn-outline-secondary tab-pane flip-btn1">View Plan</a>
                  </div>
                </div>
                <div id="cardThird-back" className="pricing-card bg-white">
                  <div className="ribbon ribbon-top-right"><span>30 Days Trial</span></div>
                  <h5 className="text-uppercase">Project Management Suite</h5>
                  <div className="grid-container">
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>58 </strong></p><sup>for 2 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>73 </strong></p><sup>for 5 users / per month</sup>
                    </div>
                    <div className="grid-item">
                      <p className="mb-1"><small>$</small><strong>125.99 </strong></p><sup>for unlimited users / per month</sup>
                    </div>
                  </div>
                  <div className="plans-tabs overflow-scroll">
                    <ul className="list-content p-0">
                      <h5>Service Suite - <span className="font-weight-normal">Includes</span></h5>
                      <li>Asset Management Software</li>
                      <li>Custom work Order and quotes</li>
                      <li>Job estimating & tracking</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Report Designer - <span className="font-weight-normal">Build Your Own Reports</span></h5>
                      <li>The user can create their own reports form our software instead of using ITMs standrads forms. The benifits contractors who may not be the part of...</li>
                      <li>Links to work Orders and Quotes</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Project Management Suite- <span className="font-weight-normal">Includes</span></h5>
                      <li>Team Management Software</li>
                      <li>Customer Management Software</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Life safety Reporting - <span className="font-weight-normal">5 free reports a month</span></h5>
                      <li>Access to all life Safety Report with work order and quote linking</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>White Label - <span className="font-weight-normal">Software</span></h5>
                      <li>Your Own Domain Like : <a className="text-primary" href="#">https:yourdomain.itmsuite.com</a></li>
                      <li>Your Reports, Your designs, Your Website.</li>
                      <li>Send your customers to your domain to ensure they're at the right place.</li>
                      <li>All Building Owners who sign up at your domain are automatically added to your account.</li>
                      <li>Build Confidence with your Customers.</li>
                    </ul>
                    <ul className="list-content p-0">
                      <h5>Add - ons - <span className="font-weight-normal">Includes</span></h5>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="permit-tracking" />
                        <label className="custom-control-label" htmlFor="permit-tracking">Permit Tracking (Depends on cost of permit)</label>
                      </div>
                      <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="managed-doc" />
                        <label className="custom-control-label" htmlFor="managed-doc">Free | 15GB Managed Document Storage</label>
                      </div>
                    </ul>
                  </div>
                  <div className="d-flex align-items-center">
                    <a href="#cardThird-front" data-toggle="pill" aria-selected="false" className="text-left tab-pane flip-btn2 text-white bg-primary-gradient"><i className="fas fa-arrow-left"></i></a>
                    
                    <button className="display-block text-center mx-auto btn btn-sm text-capitalize btn-outline-secondary">Buy Now</button>
                  </div>
                </div>
              </div>
              </div> */} 
            </div>
          </div>
        
          <div className="col-sm-9 text-center">
            <p className="default-text-style">Sign up email is classified as the first user and each additional user that's invited within your plan will only be charged if you exceed your user limits. All employee accounts you create count as a user and you must upgrade your account to compensate for the additional users required.</p>
          </div>
        
          <div className="col-12 col-xl-10 offset-xl-1 pt-1 pb-5 mb-2">
            <div className="row">
              <div className="col-md-6">
                <h4><span className="main-color">‘Add Ons’</span> to your Plans </h4>
                <p className="default-text-style">Beef up your abilities with some of our add ons. More <strong>Super Powers</strong> means less software you need to use. ITM is the only Life Safety Software you will ever need. </p>
              </div>
              <div className="col-md-6 mt-4">
                <ul className="list-content">
                  <li>15GB Document Storage - Shared with all your users & customers - </li>
                  <li>Additional Employees - Each addtional employee - <strong>$48</strong></li>
                  <li>Embedded SMS - Communicate to your customers via text</li>
                  <li>Embedded Voice & Call Recording from your browser</li>
                  <li>Quickbooks Integration</li> 
                  <li>Freshbooks Integration</li> 
                </ul>
              </div>
            </div>
          </div>
		   </div>
      </div>
      <div className="separator separator-bottom separator-hero">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 1920 128.058" style={{"enableBackground":"new 0 0 1920 128.058"}} >
                            <image style={{'overflow':'visible'}} width="1903" height="1700" id="XMLID_2_"  transform="matrix(1.0108 0 0 1.0108 2474.1865 -907.369)">
                            </image>
                            <polygon id="XMLID_3_" style={{'fill':'#fff'}} points="158.824,674.288 -1154.176,1433.191 -1761.176,1337.191 -1761.176,1463.191 
                    158.824,1463.191 " />
                            <polygon id="XMLID_18_" style={{'fill':'#fff'}} points="-529,-335.263 1038.56,-206.942 1391,-271.103 1391,-334.765 " />
                            <polygon id="XMLID_33_" style={{'fill':'#fff'}} points="0,0 0,128.058 1920,128.058 1920,0 1531,123.154 " />
                        </svg>
                    </div>
    </section>

               
               
                <section className="section bg-white">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-8 text-center">
                                <h2 className="section--title text-center">Proudly Support</h2>
                                <div className="logo-slider">
                                    <Slider {...settingsBottom}>
                                            <div className="logo-slider--box"><img src={SLIDER1} className="img-fluid" alt=""/></div>                                        
                                            <div className="logo-slider--box"><img src={SLIDER2} className="img-fluid" alt=""/></div>
                                            <div className="logo-slider--box"><img src={SLIDER3} className="img-fluid" alt=""/></div>
                                            <div className="logo-slider--box"><img src={SLIDER4} className="img-fluid" alt=""/></div>
                                            <div className="logo-slider--box"><img src={SLIDER1} className="img-fluid" alt=""/></div>
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                
                <section className="section section-lg map-animate-section">
                    <div className="bg-gradient-primary floating-gradient">
                        <img className="img-fluid map-location-bg" src={LOCATE} alt="Locate Map" />
                    </div>
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-8 text-center text-white">
                                <h2 className="section--title text-center text-white">Locate a Contractor</h2>
                                {/* <!-- Here they need to be able to search all of the Contractors within their Zip code. They should also be able to sign up for ITM after selecting a Contractor from here, this would autopopulate the Contractor information from the Contractor they chose. Understand?  --> */}
                                <p className="lead">
                                    Need some work done? Choose a Contractor from our list of verified and rated Contractors.
                                </p>
                            </div>
                            <div className="col-12 map-animate">
                              <NavLink to={LOCATE_CONTRATORS} className="cursorCls default-rounded-btn btn-light buttonStyle">Locate Contractors</NavLink>
                            </div>
                        </div>
                    </div>
                </section>
                
                <section className="pt-5 divider-height-btm">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8 text-center">
            <h2 className="section--title text-center">Get In Touch!</h2>
            <p className="lead">
                If you would like to get in touch with ITM Suite. Contact us below! 
            </p>
            <p className="text-center"><Link to={CONTACT_US} className="cursorCls default-rounded-btn btn-primary btn-rounded mt-3">Say Hi!</Link></p>
          </div>
        </div>
      </div>
    </section>
            </main>
            <Footer/>     
        </div>
    );
  }
}

const mapStateToprops = (state) => ({
  getSubscriptionPlanWithHighlightData : getSubscriptionPlanWithHighlightData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  getSubscriptionPlanWithHighlight: getSubscriptionPlanWithHighlight,
}, dispatch);

export const Home = connect(mapStateToprops, mapDispatchToProps)(HomePage);