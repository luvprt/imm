import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BENEFITS_BANNER from '../../../assets/images/theme/benifits-banner.jpg';
import HOW_IT_WORKS_IMG1 from '../../../assets/images/theme/howitworks-img1.png';
import HOW_IT_WORKS_IMG2 from '../../../assets/images/theme/howitworks-img2.png';
import HOW_IT_WORKS_IMG3 from '../../../assets/images/theme/howitworks-img3.png';
import HOW_IT_WORKS_IMG4 from '../../../assets/images/theme/howitworks-img4.png';
import HOW_IT_WORKS_IMG5 from '../../../assets/images/theme/howitworks-img5.png';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCmsPageDataAction } from '../../../duck/website/website.action';
import { getCmsPageData } from '../../../duck/website/website.selector';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';


class HowItWorksPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CmsPageData:'',
            loader: false,
        }
    }

    componentDidMount(){
        this.setState({loader:true});
        let title = ((this.props.location.pathname).replace("/","")).replace(/[_\W]+/g, " ");
        this.props.getCmsPageDataAction(title);
    }

    componentWillReceiveProps(props) {
        if (props.getCmsPageData && _.has(props.getCmsPageData, 'data')) {
            this.setState({
              CmsPageData: (props.getCmsPageData.data && props.getCmsPageData.data.pageContent),
              loader: false,
            })
        }
    }

    render() {
        const {CmsPageData, loader} = this.state;

        return (
            <div> 
                <Header loader={loader}/>
                <main>
                    <div className="inner-banner">
                        <div className="inner-banner-image">
                            <img className="img-fluid" src={BENEFITS_BANNER} alt="banner img" />
                        </div>
                        <div className="inner-banner-content">
                            <div className="container">
                                <div className="row d-flex text-center align-items-center">
                                    <h1 className="col-sm-12 text-white">How It Works?</h1>
                                    <h3 className="col-sm-12 text-white">ITM Tracking is building uniform expectations and requirements for all contractor</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    {ReactHtmlParser(CmsPageData)}
                    {/* <section className="section divider-height-btm">
                        <div className="container">
                            <div className="row row-grid align-items-center justify-content-center mb-5">
                                <div className="col-xl-4 col-lg-3 text-center">
                                    <img src={HOW_IT_WORKS_IMG1} className="img-fluid floating" alt=""/>
                                </div>
                                <div className="col-xl-6 col-lg-7">
                                    <div className="pr-md-5">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Unlimited</span> Access</h3>
                                        <p className="default-text-style">Don't let us decide when you get access. Control your company, communicate with your customers and access all your required documents 24/7.</p>
                                        <ul className="list-unstyled mt-3">
                                            <li className="py-2">
                                                <div className="d-flex align-items-center">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">City/County contracts with ITM Tracking, LLC</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-center">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">1-5 year contracts available</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-center">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Unlimited access to ITMTracking.com documents per Fire Marshal and AHJ request</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center justify-content-center">
                                <div className="col-xl-4 col-lg-3 order-lg-2 text-center">
                                    <img src={HOW_IT_WORKS_IMG2} className="img-fluid floating" alt=""/>
                                </div>
                                <div className="col-xl-6 col-lg-7 order-lg-1">
                                    <div className="pr-md-5">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Report</span> Inspections</h3>
                                        <p className="default-text-style">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
                                        <ul className="list-unstyled mt-3">
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Notifies all contractors to report all inspections to www.ITMTracking.com</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Sends test due date letters to business owners</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center justify-content-center mb-5">
                                <div className="col-xl-4 col-lg-3 text-center">
                                    <img src={HOW_IT_WORKS_IMG3} className="img-fluid floating" alt=""/>
                                </div>
                                <div className="col-xl-6 col-lg-7">
                                    <div className="pr-md-5">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Perform</span> Inspections</h3>
                                        <p className="default-text-style">Performing inspections on ITM Suite is easy! Just log into your account as a Contractor or Inspector, select the required report and enter all the information. Your reports will be emailed to all the valuable parties making your job easier!</p>
                                        <ul className="list-unstyled mt-3">
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Perform inspections per NFPA 10, 13, 20, 25, 72, etc.</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Enter reports online via www.ITMTracking.com</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Must pay a small fee per report submitted</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Does not pay fee to update yellow/red tag status to blue</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center justify-content-center">
                                <div className="col-xl-4 col-lg-3 order-lg-2 text-center">
                                    <img src={HOW_IT_WORKS_IMG4} className="img-fluid floating" alt=""/>
                                </div>
                                <div className="col-xl-6 col-lg-7 order-lg-1">
                                    <div className="pr-md-5">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Database</span> Records</h3>
                                        <p className="default-text-style">Never worry about losing your information on ITM Suite. ITM uses Amazon RDS ensuring that all your information is safe and secure. WIth daily snapshots of the database you can rest easy knowing it's all safe.</p>
                                        <ul className="list-unstyled mt-3">
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Database records & tracks all fire protection system inspections, testing & maintenance reports</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Processes all submitted reports to differentiate yellow, red, and blue tagged facilities</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Reports can be e-mailed or viewed by the AHJ at any given time to show how many days passed without corrective action, if necessary</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Sends non-compliance notices to the building owners</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Phone call will be made to business owner and contractor when applicable</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="row row-grid align-items-center justify-content-center mb-5">
                                <div className="col-xl-4 col-lg-3 text-center">
                                    <img src={HOW_IT_WORKS_IMG5} className="img-fluid floating" alt=""/>
                                </div>
                                <div className="col-xl-6 col-lg-7">
                                    <div className="pr-md-5">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Report</span> & <span className="font-weight-bold text-primary mt-5">Quote Designer</span></h3>
                                        <p className="default-text-style">Imagine being able to finally build something that works for you. Seamlessly allowing your team to adjust but also ensuring flawless integrations for your company.</p>
                                        <ul className="list-unstyled mt-3">
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Choose how your Quotes & Reports will look from every button to every checkbox.</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Decide what text goes where, what information will be required for the end user.</p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Control your company and make your reports work for you. </p>
                                                </div>
                                            </li>
                                            <li className="py-2">
                                                <div className="d-flex align-items-top">
                                                    <div className="badge badge-circle badge-primary mr-3">
                                                        <i className="fa fa-check"></i>
                                                    </div>
                                                    <p className="mb-0">Preview your Reports & Quotes and Build it when you're ready!</p>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> */}
                </main>
                <Footer/>     
            </div>
        );
    }
}

const mapStateToprops = (state) => ({
    getCmsPageData: getCmsPageData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getCmsPageDataAction : getCmsPageDataAction
}, dispatch);

export const HowItWorks = connect(mapStateToprops, mapDispatchToProps)(HowItWorksPage);