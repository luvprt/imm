import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BENEFITS_BANNER from '../../../assets/images/theme/benifits-banner.jpg';
import PROFILE_PAGE from '../../../assets/images/profile-page.jpg'
import CONTRACTOR_ICON from '../../../assets/images/contractor-icon.png'

import { Link } from "react-router-dom";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCmsPageDataAction } from '../../../duck/website/website.action';
import { getCmsPageData } from '../../../duck/website/website.selector';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';


class LifeSafetyPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CmsPageData:'',
            loader:false,
        }
    }

    componentDidMount(){
        this.setState({loader:true});
        let title = ((this.props.location.pathname).replace("/","")).replace(/[_\W]+/g, " ");
        this.props.getCmsPageDataAction(title);
    }

    componentWillReceiveProps(props) {
        if (props.getCmsPageData && _.has(props.getCmsPageData, 'data')) {
            this.setState({
              CmsPageData: (props.getCmsPageData.data && props.getCmsPageData.data.pageContent),
              loader: false
            })
        }
    }

  render() {
    const {CmsPageData, loader} = this.state;

    return (
        <div> 
            <Header loader={loader}/>
            <main>
                <div className="inner-banner">
                    <div className="inner-banner-image">
                        <img className="img-fluid" src={BENEFITS_BANNER} alt=""/>
                    </div>
                    <div className="inner-banner-content">
                        <div className="container">
                            <div className="row d-flex text-center align-items-center">
                                <h1 className="col-sm-12 text-white"> Life Safety </h1>
                                <h3 className="col-sm-12 text-white"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut </h3>
                            </div>
                        </div>
                    </div>
                </div>
                {ReactHtmlParser(CmsPageData)}
                {/* <section className="section divider-height-btm life-safety">
                    <div className="container">
                        <div className="row row-grid align-items-center">
                            <div className="col-xl-7 col-lg-7 order-md-1 text-center">
                                <img className="img-fluid" src={PROFILE_PAGE} alt=""/>
                            </div>
                            <div className="col-xl-5 col-lg-5 order-md-2 right-side-section">
                                <div className="px-4 py-5 lifeCls">
                                    <h3 className="mb-3"><span className="font-weight-bold text-primary">Lorem Ipsum </span></h3>
                                    <p>Managing your customer accounts is a breeze with Service Fusion. Easily access customer's contact information, set their personal communication preferences, billing terms, store private and public notes, track referral sources and a lot more.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="section divider-height-btm">
                    <div className="container">
                        <div className="row justify-content-center mb-5">
                            <div className="col-xl-4 col-lg-4">
                                <div className="card profile-card height100">
                                    <div className="card-body profile-sections">
                                        <img className="profile-img-icon" src={CONTRACTOR_ICON} alt="img" />
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5 h4 mb-0"> Contractor </span></h3>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
                                    </div>
                                    <p className="text-center sign-up-butn"><Link to="/home#sp" className="default-rounded-btn btn-primary"> SIGN UP</Link></p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4">
                                <div className="card profile-card height100">
                                    <div className="card-body profile-sections">
                                        <i className="far fa-fire-smoke"></i>
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5 h4 mb-0"> Fire Marshal </span></h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
                                    </div>
                                    <p className="text-center sign-up-butn"><Link to="/home#sp" className="default-rounded-btn btn-primary"> SIGN UP</Link></p>
                                </div>
                            </div>
                            <div className="col-xl-4 col-lg-4">
                                <div className="card profile-card height100">
                                    <div className="card-body profile-sections">
                                        <i className="fas fa-building"></i>
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5 h4 mb-0"> Building Representative </span></h3>
                                        <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p>
                                    </div>
                                    <p className="text-center sign-up-butn"><Link to="/home#sp" className="default-rounded-btn btn-primary"> SIGN UP</Link></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> */}
            </main>
            <Footer/>     
        </div>
    );
  }
}

const mapStateToprops = (state) => ({
    getCmsPageData: getCmsPageData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getCmsPageDataAction : getCmsPageDataAction
}, dispatch);

export const LifeSafety = connect(mapStateToprops, mapDispatchToProps)(LifeSafetyPage);