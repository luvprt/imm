import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { registration, emailExist } from '../../../duck/auth/auth.action';
import { getRegisteredUser, emailExistData } from '../../../duck/auth/auth.selector'
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BANNER_IMG from '../../../assets/images/theme/benifits-banner.jpg';
import HAT_IMG from '../../../assets/images/hard-hat.png';
import HAT_IMG_ACTIVE from '../../../assets/images/hard-hat-active.png';
import history from '../../../routing/history'
import { validateInputs } from '../../../common/validation';
import _ from 'lodash';
import { Link } from "react-router-dom";
import { CHECKOUT_BASE, HOME } from '../../../routing/routeContants';
import ReCAPTCHA from "react-google-recaptcha";
import { fieldValidator } from '../../../common/custom';
export class RegisterPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      userTypeName: '',
      userTypeId: '',
      subscriptionPlan: '',
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      phone: '',
      address: '',
      apt: '',
      city: '',
      state: '',
      zipcode: '',
      companyName: '',
      alarmContractor: '',
      sprinklerContractor: '',
      extinguisherContractor: '',
      addOns: [],
      emailErr: '',
      firstNameErr: '',
      lastNameErr: '',
      passwordErr: '',
      confirmPasswordErr: '',
      phoneErr: '',
      addressErr: '',
      aptErr: '',
      cityErr: '',
      stateErr: '',
      zipcodeErr: '',
      companyNameErr: '',
      alarmContractorErr: '',
      sprinklerContractorErr: '',
      extinguisherContractorErr: '',
      error: '',
      loader: false,
      testViaJest: props.testViaJest ? props.testViaJest : false,
      emailAvaibility: '',
      captchaResponse: '',
      captchaErr: '',
      serviceMessage: ''
    }
  }

  componentWillReceiveProps(props) {
    if (props.userData && (props.userData !== this.props.userData) && props.userData.success) {
      let userId = props.userData.user.data._id;
      let userTypeId = this.state.userTypeId;
      let subscriptionPlan = this.state.subscriptionPlan;
      this.setState({ loader: false })
      history.push(CHECKOUT_BASE + subscriptionPlan + '/' + userTypeId + '/' + userId);
    } else if (props.userData && (props.userData !== this.props.userData) && _.has(props.userData.user, 'message')) {
      this.setState({ loader: false, serviceMessage: props.userData.user.message });
    }
    if ((props.emailExistData !== this.props.emailExistData) && props.emailExistData.message === "Email Exist") {
      this.setState({ emailAvaibility: false, loader: false })
    } else if ((props.emailExistData !== this.props.emailExistData) && props.emailExistData.message === "Email Not Exist") {
      this.setState({ emailAvaibility: true, loader: false })
    }
  }

  componentDidMount() {
    if (this.state.testViaJest === false) {
      let planData = JSON.parse(localStorage.getItem('planDetail'));
      this.setState({ subscriptionPlan: planData.subscriptionPlan, userTypeId: planData.user.id, userTypeName: planData.user.name })
    }
  }

  // Validate Register Form and Save 
  register = () => {
    const { firstName, captchaResponse, lastName, email, password, phone, address, apt, city, state, zipcode, companyName, subscriptionPlan, userTypeId, confirmPassword, addOns, userTypeName, alarmContractor, sprinklerContractor, extinguisherContractor, testViaJest } = this.state;

    let emailErr = '', captchaErr = '', firstNameErr = '', lastNameErr = '', passwordErr = '', confirmPasswordErr = '', phoneErr = '', addressErr = '', aptErr = '', cityErr = '', stateErr = '', zipcodeErr = '', companyNameErr = '', alarmContractorErr = '', sprinklerContractorErr = '', extinguisherContractorErr = '', getError = false;

    if (captchaResponse === '') {
      captchaErr = 'Please select captcha.';
      getError = true;
    }

    if (validateInputs('string', firstName) === 'empty') {
      firstNameErr = 'Please enter first name.';
      getError = true;
    } else if (validateInputs('string', firstName) === false) {
      firstNameErr = 'Please enter valid first name.';
      getError = true;
    }

    if (validateInputs('string', lastName) === 'empty') {
      lastNameErr = 'Please enter last name.';
      getError = true;
    } else if (validateInputs('string', lastName) === false) {
      lastNameErr = 'Please enter valid last name.';
      getError = true;
    }

    if (validateInputs('email', email) === 'empty') {
      emailErr = 'Please enter valid email.';
      getError = true;
    } else if (validateInputs('email', email) === false) {
      emailErr = 'Invalid email.';
      getError = true;
    }

    if (validateInputs('password', password) === 'empty') {
      passwordErr = 'Please enter password.';
      getError = true;
    } else if (validateInputs('password', password) === false) {
      passwordErr = 'A special character, an upper case, a lower case, a number & minimum 8 character are required';
      getError = true;
    }

    if (validateInputs('phone', phone) === 'empty') {
      phoneErr = 'Please enter phone.';
      getError = true;
    } else if (validateInputs('phone', phone) === false) {
      phoneErr = 'Invalid phone.';
      getError = true;
    }

    if (validateInputs('string', address) === 'empty') {
      addressErr = 'Please enter address .';
      getError = true;
    } else if (validateInputs('string', address) === false) {
      addressErr = 'Please enter valid address.';
      getError = true;
    }

    if (validateInputs('string', apt) === 'empty') {
      aptErr = 'Please enter apt .';
      getError = true;
    } else if (validateInputs('string', apt) === false) {
      aptErr = 'Please enter valid apt.';
      getError = true;
    }

    if (validateInputs('string', state) === 'empty') {
      stateErr = 'Please enter state .';
      getError = true;
    } else if (validateInputs('string', state) === false) {
      stateErr = 'Please enter valid state.';
      getError = true;
    }

    if (validateInputs('string', city) === 'empty') {
      cityErr = 'Please enter city.';
      getError = true;
    } else if (validateInputs('string', city) === false) {
      cityErr = 'Please enter valid city.';
      getError = true;
    }

    if (validateInputs('string', zipcode) === 'empty') {
      zipcodeErr = 'Please enter zip code .';
      getError = true;
    } else if (validateInputs('string', zipcode) === false) {
      zipcodeErr = 'Please enter valid zip code.';
      getError = true;
    } else if (zipcode.length > 6) {
      zipcodeErr = 'Please enter upto 6 characters.';
      getError = true;
    }

    if (password !== confirmPassword) {
      confirmPasswordErr = 'Password and confirm password does not match.';
      getError = true;
    }

    if (userTypeName === 'Contractor') {
      if (validateInputs('string', companyName) === 'empty') {
        companyNameErr = 'Please enter company name .';
        getError = true;
      } else if (validateInputs('string', companyName) === false) {
        companyNameErr = 'Please enter valid company name.';
        getError = true;
      }
    } else {
      if (validateInputs('string', alarmContractor) === 'empty') {
        alarmContractorErr = 'Please enter alarm contractor name .';
        getError = true;
      } else if (validateInputs('string', alarmContractor) === false) {
        alarmContractorErr = 'Please enter valid alarm contractor name.';
        getError = true;
      }

      if (validateInputs('string', sprinklerContractor) === 'empty') {
        sprinklerContractorErr = 'Please enter sprinkler contractor name .';
        getError = true;
      } else if (validateInputs('string', sprinklerContractor) === false) {
        sprinklerContractorErr = 'Please enter valid sprinkler contractor name.';
        getError = true;
      }

      if (validateInputs('string', extinguisherContractor) === 'empty') {
        extinguisherContractorErr = 'Please enter extinguisher contractor name .';
        getError = true;
      } else if (validateInputs('string', extinguisherContractor) === false) {
        extinguisherContractorErr = 'Please enter valid extinguisher contractor name.';
        getError = true;
      }
    }

    this.setState({ emailErr, firstNameErr, lastNameErr, passwordErr, confirmPasswordErr, phoneErr, addressErr, aptErr, stateErr, cityErr, zipcodeErr, companyNameErr, alarmContractorErr, sprinklerContractorErr, extinguisherContractorErr, error: getError, captchaErr });
    if (userTypeName === 'Contractor') {
      if (getError === false && emailErr === '' && firstNameErr === '' && lastNameErr === '' && passwordErr === '' && phoneErr === '' && addressErr === '' && aptErr === '' && cityErr === '' && zipcodeErr === '' && companyNameErr === '' && confirmPasswordErr === '' && captchaErr === '' && testViaJest === false) {
        this.setState({ loader: true })
        this.props.registration({ firstName, lastName, email, password, phone, address, apt, city, state, zipcode, companyName, subscriptionPlan, userTypeId, confirmPassword, addOns, userTypeName, alarmContractor, sprinklerContractor, extinguisherContractor })
      }
    } else {
      if (getError === false && emailErr === '' && firstNameErr === '' && lastNameErr === '' && passwordErr === '' && phoneErr === '' && addressErr === '' && aptErr === '' && cityErr === '' && zipcodeErr === '' && confirmPasswordErr === '' && alarmContractorErr === '' && sprinklerContractorErr === '' && extinguisherContractorErr === '' && captchaErr === '' && testViaJest === false) {
        this.setState({ loader: true })
        this.props.registration({ firstName, lastName, email, password, phone, address, apt, city, state, zipcode, companyName, subscriptionPlan, userTypeId, confirmPassword, addOns, userTypeName, alarmContractor, sprinklerContractor, extinguisherContractor })
      }
    }
  }

  // Email Exist Check 
  checkEmailExist = () => {
    let email = this.state.email;
    let emailErr = '', getError = false;

    if (validateInputs('email', email) === 'empty') {
      emailErr = 'Please enter valid email.';
      getError = true;
    } else if (validateInputs('email', email) === false) {
      emailErr = 'Invalid email.';
      getError = true;
    }
    this.setState({ emailErr, error: getError });
    if (getError === false && emailErr === '') {
      this.setState({ loader: true })
      this.props.emailExist(email)
    }
  }



  // Captcha Function
  captachChange = (value) => {
    if (value === '') {

    } else {
      this.setState({ captchaResponse: value, captchaErr: '' })
    }
  }

  checkValidation = (field, value, type, maxLength = null) => {
    const { password } = this.state;
    let error = fieldValidator(field, value, type, password, maxLength)
    this.setState({ error: error.getError, [error.fieldNameErr]: error.errorMsg })
  }

  render() {
    const { serviceMessage, userTypeName, firstName, lastName, email, password, confirmPassword, phone, address, apt, city, state, zipcode, companyName, emailErr, firstNameErr, lastNameErr, passwordErr, confirmPasswordErr, phoneErr, addressErr, aptErr, stateErr, cityErr, zipcodeErr, companyNameErr, alarmContractor, sprinklerContractor, extinguisherContractor, alarmContractorErr, sprinklerContractorErr, extinguisherContractorErr, loader, emailAvaibility, captchaErr } = this.state;

    return (
      <div>
        <Header loader={loader} />
        <main>
          <div className="inner-banner">
            <div className="inner-banner-image">
              <img className="img-fluid" src={BANNER_IMG} alt="" />
            </div>
            <div className="inner-banner-content">
              <div className="container">
                <div className="row d-flex text-center align-items-center">
                  <h1 className="col-sm-12 text-white"> Sign Up As {userTypeName} </h1>
                  <h3 className="col-sm-12 text-white"> ITM Tracking is building uniform expectations and requirements for all contractors. </h3>
                </div>
              </div>
            </div>
          </div>
          <section className="section divider-height-btm userTypeTabs">
            <div className="container">
              <div className="row justify-content-center">
                <div className="col-xl-9 col-lg-9 text-center">
                  <div className="card signup-top">
                    <nav className="nav nav-pills flex-column flex-sm-row" id="nav-tab" role="tablist">
                      <a className={userTypeName === "Contractor" ? "flex-sm-fill text-sm-center nav-link active" : "flex-sm-fill text-sm-center nav-link"} >
                        <span className="hard-hat mr-2">
                          <img className="hat" src={HAT_IMG} alt="" />
                          <img className="active-hat" src={HAT_IMG_ACTIVE} alt="" />
                        </span> Contractor
                            </a>
                      <a className="flex-sm-fill text-sm-center nav-link" ><i className="fas fa-fire-smoke"></i> Fire Marshal</a>
                      <a className={userTypeName === "Building Representative" ? "flex-sm-fill text-sm-center nav-link active" : "flex-sm-fill text-sm-center nav-link"} ><i className="fas fa-building"></i> Business Representative</a>
                    </nav>
                    <div className="card-body">
                        <div className="tab-content" id="nav-tabContent">
                          <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                            <section className="section signup-height">
                                <div className="row justify-content-center">
                                  <div className="col-xl-6 col-lg-6">
                                    <div className="height100 business-signup">
                                      <div className="card-body">
                                        <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5"> Basic </span> Details </h3>
                                        <div className="signup-frm">
                                          <div className="form-row">
                                            <div className="form-group w-100">
                                              <input value={firstName} name="firstName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} className="form-control" onChange={(e) => this.setState({ firstName: e.target.value })} type="text" placeholder="First Name*" />
                                              {firstNameErr ? <span className="errorClsRegister"> {firstNameErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input value={lastName} name="lastName" className="form-control" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} onChange={(e) => this.setState({ lastName: e.target.value })} type="text" placeholder="Last Name*" />
                                              {lastNameErr ? <span className="errorClsRegister"> {lastNameErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100 text-left">
                                              <input value={email} name="email" className="form-control" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'email') }} onChange={(e) => this.setState({ email: e.target.value, emailAvaibility: '' })} type="email" placeholder="Email*" />
                                              <span className="check-availibility d-inline-block mr-2" onClick={() => this.checkEmailExist()}>Check Availability </span><span className="check_icons">
                                                {emailAvaibility !== '' ? (emailAvaibility ? <i className="fa fa-check text-success"></i> : <i className="fa fa-times text-danger"></i>) : ' '}
                                              </span>
                                              {emailErr ? <span className="errorClsRegister"> {emailErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input value={password} name="password" className="form-control" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'password') }} onChange={(e) => this.setState({ password: e.target.value })} type="password" placeholder="Password*" />
                                              {passwordErr ? <span className="errorClsRegister"> {passwordErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input name="confirmPassword" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value) }} value={confirmPassword} className="form-control" onChange={(e) => this.setState({ confirmPassword: e.target.value })} type="password" placeholder="Confirm Password*" />
                                              {confirmPasswordErr ? <span className="errorClsRegister"> {confirmPasswordErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input value={phone} name="phone" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'phone') }} className="form-control" onChange={(e) => this.setState({ phone: e.target.value })} type="text" placeholder="Phone*" />
                                              {phoneErr ? <span className="errorClsRegister"> {phoneErr}</span> : ''}
                                            </div>
                                          </div>
                                          <div className="form-row form-height">
                                            <div className="form-group w-100">
                                              <textarea name="address" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} className="form-control" placeholder="Address Line*" onChange={(e) => this.setState({ address: e.target.value })} value={address}></textarea>
                                              {addressErr ? <span className="errorClsRegister"> {addressErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <textarea name="apt" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} className="form-control" placeholder="Apt/Suite*" onChange={(e) => this.setState({ apt: e.target.value })} value={apt}></textarea>
                                              {aptErr ? <span className="errorClsRegister"> {aptErr}</span> : ''}
                                            </div>
                                          </div>
                                          <div className="form-row">
                                            <div className="form-group w-100">
                                              <input name="city" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" value={city} placeholder="City*" onChange={(e) => this.setState({ city: e.target.value })} />
                                              {cityErr ? <span className="errorClsRegister"> {cityErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input name="state" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" value={state} placeholder="State*" onChange={(e) => this.setState({ state: e.target.value })} />
                                              {stateErr ? <span className="errorClsRegister"> {stateErr}</span> : ''}
                                            </div>
                                            <div className="form-group w-100">
                                              <input type="text" name="zipcode" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string', 6) }} className="form-control" value={zipcode} placeholder="Zip Code*" onChange={(e) => this.setState({ zipcode: e.target.value })} />
                                              {zipcodeErr ? <span className="errorClsRegister"> {zipcodeErr}</span> : ''}
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  {userTypeName === 'Contractor' ?
                                    <div className="col-xl-6 col-lg-6">
                                      <div className="height100 business-signup">
                                        <div className="card-body">
                                          <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5"> Company  </span>Details </h3>
                                          <div className="signup-frm">
                                            <div className="form-row">
                                              <div className="form-group col-md-12">
                                                <input type="text" name="companyName" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} value={companyName} className="form-control" id="inputEmail4" placeholder="Company Name*" onChange={(e) => this.setState({ companyName: e.target.value })} />
                                                {companyNameErr ? <span className="errorClsRegister"> {companyNameErr}</span> : ''}
                                              </div>

                                            </div>
                                            <div className="form-row">
                                              <div className="form-group col-md-12">
                                                <ReCAPTCHA sitekey="6LdiE7gUAAAAAGEXKunwZgcuWRDb2H7h404oDpL7" onChange={(e) => this.captachChange(e)} />
                                                {captchaErr ? <span className="errorClsRegister"> {captchaErr}</span> : ''}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    :
                                    <div className="col-xl-6 col-lg-6">
                                      <div className="height100 business-signup">
                                        <div className="card-body">
                                          <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Prefered  </span> Contractor Detail </h3>
                                          <div className="signup-frm">
                                            <div className="form-row">
                                              <label htmlFor="Alarm Contractor"> Alarm Contractor* </label>
                                              <div className="form-group w-100">
                                                <input name="alarmContractor" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" value={alarmContractor} onChange={(e) => this.setState({ alarmContractor: e.target.value })} placeholder="Contractor Name" />
                                                {alarmContractorErr ? <span className="errorClsRegister"> {alarmContractorErr}</span> : ''}
                                              </div>
                                            </div>
                                            <div className="form-row">
                                              <label htmlFor="Alarm Contractor"> Sprinkler Contractor* </label>
                                              <div className="form-group w-100">
                                                <input name="sprinklerContractor" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" value={sprinklerContractor} onChange={(e) => this.setState({ sprinklerContractor: e.target.value })} placeholder="Contractor Name" />
                                                {sprinklerContractorErr ? <span className="errorClsRegister"> {sprinklerContractorErr}</span> : ''}
                                              </div>
                                            </div>
                                            <div className="form-row">
                                              <label htmlFor="Alarm Contractor"> Extinguisher Contractor* </label>
                                              <div className="form-group w-100">
                                                <input name="extinguisherContractor" onKeyUp={(e) => { this.checkValidation(e.target.name, e.target.value, 'string') }} type="text" className="form-control" value={extinguisherContractor} onChange={(e) => this.setState({ extinguisherContractor: e.target.value })} placeholder="Contractor Name" />
                                                {extinguisherContractorErr ? <span className="errorClsRegister"> {extinguisherContractorErr}</span> : ''}
                                              </div>
                                            </div>
                                            <div className="form-row">
                                              <div className="form-group col-md-12">
                                                <ReCAPTCHA sitekey="6LdiE7gUAAAAAGEXKunwZgcuWRDb2H7h404oDpL7" onChange={(e) => this.captachChange(e)} />
                                                {captchaErr ? <span className="errorClsRegister"> {captchaErr}</span> : ''}
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  }
                                  <div className="buttons col-md-12 text-center">
                                    {serviceMessage ? <div className="errorCls mt-2 mb-2">{serviceMessage}</div> : ''}
                                    <div className="btn-wrapper">
                                      <Link to={HOME} className="default-rounded-btn btn-dark mb-3 mb-sm-0">
                                        <span className="btn-inner--text">Cancel</span>
                                      </Link>
                                      <span id="addRegisterButton" onClick={(e) => this.register()} className="default-rounded-btn btn-primary mb-3 mb-sm-0 ">
                                        <span className="btn-inner--text"> Send Request </span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                            </section>
                          </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  userData: getRegisteredUser(state),
  emailExistData: emailExistData(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  registration: registration,
  emailExist: emailExist
}, dispatch);

export const Register = connect(mapStateToprops, mapDispatchToProps)(RegisterPage); 