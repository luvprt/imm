import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BANNER_IMG from '../../../assets/images/theme/benifits-banner.jpg';
import THANKSPAGE2 from '../../../assets/images/thankspage2.png';
import { Link } from "react-router-dom";
import {HOME} from "../../../routing/routeContants"

class ThankYou extends Component {

  constructor(props) {
    super(props)
    this.state = {

    }
  }

  render() {

    return (
      <div>
        <Header />
        <main>
          <section className="thank_u_page my-5 d-sm-flex flex-wrap justify-content-around align-items-center">
            {/* <img className="img-fluid bg-img-cloud floatingHorizontal" src="assets/images/thankspage1.png" alt="cloud" /> */}
            <div className="content_inner_sec text-md-left text-center">
              <div className="text-white w-100"><span className="display-1 font-weight-bold text-style-gradient">Thank You!!</span></div>
              <p className="text-md-left text-center h5 mb-0">Your Subscription Plan Activated Successfully. </p>
              <Link to={HOME} className="btn btn-primary btn-lg mt-4 mx-auto">Back to Home</Link>
            </div>
            <div className="bg_right_side floating">
              <img className="img-fluid bg-img-handshake" src={THANKSPAGE2} alt="swing" />
            </div>
          </section>
        </main>
        <Footer />
      </div>
    );
  }
}

export default ThankYou;