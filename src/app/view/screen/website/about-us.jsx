import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BENEFITS_BANNER from '../../../assets/images/theme/benifits-banner.jpg';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getCmsPageDataAction } from '../../../duck/website/website.action';
import { getCmsPageData } from '../../../duck/website/website.selector';
import _ from 'lodash';
import ReactHtmlParser from 'react-html-parser';

class AboutUsPage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            CmsPageData:'',
            loader: false
        }
    }

    componentDidMount(){
        this.setState({loader:true});
        let title = ((this.props.location.pathname).replace("/","")).replace(/[_\W]+/g, " ");
        this.props.getCmsPageDataAction(title);
    }

    componentWillReceiveProps(props) {
        if (props.getCmsPageData && _.has(props.getCmsPageData, 'data')) {
            this.setState({
              CmsPageData: (props.getCmsPageData.data && props.getCmsPageData.data.pageContent),
              loader: false,
            })
        }
    }

  render() {
    const {CmsPageData, loader} = this.state;

    return (
        <div> 
            <Header loader={loader}/>
            <main>
              <div className="inner-banner">
                  <div className="inner-banner-image">
                      <img alt='' className="img-fluid" src={BENEFITS_BANNER} />
                  </div>
                  <div className="inner-banner-content">
                      <div className="container">
                          <div className="row d-flex text-center align-items-center">
                              <h1 className="col-sm-12 text-white">About Us</h1>
                              <h3 className="col-sm-12 text-white">Designed with a love for service and people
                              </h3>
                          </div>
                      </div>
                  </div>
              </div>
              {ReactHtmlParser(CmsPageData)}
              {/* <section className="section divider-height-btm">
                  <div className="container">
                      <div className="row justify-content-center">
                          <div className="col-xl-9">
                              <h3 className="text-center font-weight-bold heading_title">Time to Innovate the service industry</h3>
                              <h4 className="section--sub-title text-center mb-5 mt-4">ITM Suite is comprised of Contractors, Software Developers, and innovators. Our goal is to create the most streamlined SaaS product to help people run their businesses.</h4>
                          </div>
                      </div>
                      <div className="row justify-content-center">
                          <div className="col-xl-3 col-lg-4 mb-5 mb-lg-1">
                              <div className="card about-card height100">
                                  <div className="card-body">
                                      <i className="far fa-shield-alt"></i>
                                      <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Our</span> Mission</h3>
                                      <p>Creating uniform solutions in the life safety industry, helping establish an all code compliant city! We strive to protect lives through creating an informative and uniformed life area. </p>
                                  </div>
                              </div>
                          </div>
                          <div className="col-xl-3 col-lg-4 mb-5 mb-lg-1">
                              <div className="card about-card height100">
                                  <div className="card-body">
                                      <i className="far fa-history"></i>
                                      <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Our</span> History</h3>
                                      <p>We have a combined 20+ years in the fire protection industry. Our background includes licensed contracting, NICET Certifications & backflow certifications. We know exactly what is needing improvement. </p>
                                  </div>
                              </div>
                          </div>
                          <div className="col-xl-3 col-lg-4 mb-5 mb-lg-1">
                              <div className="card about-card height100">
                                  <div className="card-body">
                                      <i className="far fa-fingerprint"></i>
                                      <h3 className="mb-3"><span className="font-weight-bold text-primary mt-5">Our</span> Background</h3>
                                      <p>In this industry and exactly how to become an organized, more uniformed life safety industry. We continue to strive for solutions and creative intellect within our life safety reporting process. </p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </section> */}
            </main>
            <Footer/>     
        </div>
    );
  }
}

const mapStateToprops = (state) => ({
    getCmsPageData: getCmsPageData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
    getCmsPageDataAction : getCmsPageDataAction
}, dispatch);

export const AboutUs = connect(mapStateToprops, mapDispatchToProps)(AboutUsPage);