import React, { Component } from 'react';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import Slider from "react-slick";
import { Link } from "react-router-dom";

import BANNER_TRAINING from '../../../assets/images/banner-training.jpg';
import BANNER_ICON1 from '../../../assets/images/bnr-icon1.png';
import BANNER_ICON2 from '../../../assets/images/bnr-icon2.png';
import BANNER_ICON3 from '../../../assets/images/bnr-icon3.png';
import COURSE_IMG1 from '../../../assets/images/courses-img1.jpg';
import COURSE_IMG2 from '../../../assets/images/courses-img2.jpg';
import COURSE_IMG3 from '../../../assets/images/courses-img3.jpg';
import FEATURED_IMG from '../../../assets/images/featured-block-img.jpg';
import FEATURED_ICON1 from '../../../assets/images/featured-icon1.png'
import FEATURED_ICON2 from '../../../assets/images/featured-icon2.png'
import FEATURED_ICON3 from '../../../assets/images/featured-icon3.png'
import FEATURED_ICON4 from '../../../assets/images/featured-icon4.png'
import FEATURED_ICON5 from '../../../assets/images/featured-icon5.png'
import TESTIMONIAL_USER from '../../../assets/images/testimonial-user1.jpg';

export class TrainingPrograms extends Component {
    constructor(props) {
        super(props);
        this.state = {
            settings: {
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 2,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            dots: true,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            initialSlide: 2,
                            dots: true,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            dots: true,
                            arrows: false
                        }
                    }
                ]
            }
        };

    }

    render() {
        const { settings } = this.state

        return (
            <div>
                <Header />
                <main>
                    <div id="inner-sec-banner" className="inner-banner banner-img-training p-0">
                        <div className="inner_banner_image">
                            <img className="img-fluid" src={BANNER_TRAINING} alt="banner img" />
                        </div>
                        <div className="inner-banner-content banner-sec">
                            <div className="container">
                                <div className="row d-flex text-center align-items-center">
                                    <h1 className="col-sm-12 text-white">Training Programs</h1>
                                    <h3 className="col-sm-12 text-white">Thanks for learning and growing with us. Celebrate with courses from ₹640. Ends 1/31.</h3>
                                    <form>
                                        <div className="form-group">
                                            <input id="search-field" type="search" className="form-control" placeholder="Search course here" />
                                            <button id="search-submit" type="submit" className="submit text-uppercase" value="search">Search</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <section className="banner-btm-bar bg-gradient-primary py-3">
                        <div className="container">
                            <div className="row">
                                <div className="col-12 col-sm-4 d-flex flex-wrap text-white justify-content-sm-center align-content-lg-center">
                                    <div className="icon-section mr-2"><img className="img-fluid mt-1" src={BANNER_ICON1} alt="icon1" /></div>
                                    <div className="cnt-section ml-1">
                                        <p className="my-0"><b>100,000</b> online courses</p>
                                        <small>Explore a variety of fresh topics</small>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-4 d-flex flex-wrap text-white justify-content-sm-center align-content-lg-center">
                                    <div className="icon-section mr-2"><img className="img-fluid mt-1" src={BANNER_ICON2} alt="icon1" /></div>
                                    <div className="cnt-section ml-1">
                                        <p className="my-0"><b>Expert</b> instruction</p>
                                        <small>Expert Find the right instructor or you</small>
                                    </div>
                                </div>
                                <div className="col-12 col-sm-4 d-flex flex-wrap text-white justify-content-sm-center align-content-lg-center">
                                    <div className="icon-section mr-2"><img className="img-fluid mt-1" src={BANNER_ICON3} alt="icon1" /></div>
                                    <div className="cnt-section ml-1">
                                        <p className="my-0"><b>Lifetime</b> access</p>
                                        <small>Learn on your schedule</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="featured-courses section">
                        <div className="container">
                            <div className="row justify-content-center mb-4">
                                <div className="col-xl-9">
                                    <h3 className="text-center font-weight-bold heading_title">Featured Courses</h3>
                                </div>
                            </div>
                            <div className="multiple-courses">
                                <Slider {...settings}>
                                    <div className="">
                                        <div className="card">
                                            <Link to="" onClick={e => e.preventDefault()} className="courses-links">
                                                <img className="card-img-top" src={COURSE_IMG1} alt="Card cap" />
                                                <div className="card-tag text-white">Featured</div>
                                            </Link>
                                            <div className="card-body">
                                                <Link to="" onClick={e => e.preventDefault()} className="font-weight-bold">Sed do eiusmod tempor incididunt</Link>
                                                <div className="rating-icons">
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star-half-alt mr-1"></span>
                                                    <span className="fal fa-star mr-1"></span>
                                                    <small>(10,2192)</small>
                                                </div>
                                                <del>$12,198</del><strong className="ml-2">$1000</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="card">
                                            <a className="courses-links" href="#!">
                                                <img className="card-img-top" src={COURSE_IMG2} alt="Card cap" />
                                            </a>
                                            <div className="card-body">
                                                <Link to="" onClick={e => e.preventDefault()} className="font-weight-bold">Sed do eiusmod tempor incididunt</Link>
                                                <div className="rating-icons">
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star-half-alt mr-1"></span>
                                                    <span className="fal fa-star mr-1"></span>
                                                    <small>(10,2192)</small>
                                                </div>
                                                <del>$12,198</del><strong className="ml-2">$1000</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="card">
                                            <a className="courses-links" href="#!">
                                                <img className="card-img-top" src={COURSE_IMG3} alt="Card cap" />
                                            </a>
                                            <div className="card-body">
                                                <Link to="" onClick={e => e.preventDefault()} className="font-weight-bold">Sed do eiusmod tempor incididunt</Link>
                                                <div className="rating-icons">
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star-half-alt mr-1"></span>
                                                    <span className="fal fa-star mr-1"></span>
                                                    <small>(10,2192)</small>
                                                </div>
                                                <del>$12,198</del><strong className="ml-2">$1000</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="card">
                                            <a className="courses-links" href="#!">
                                                <img className="card-img-top" src={COURSE_IMG2} alt="Card cap" />
                                            </a>
                                            <div className="card-body">
                                                <Link to="" onClick={e => e.preventDefault()} className="font-weight-bold">Sed do eiusmod tempor incididunt</Link>
                                                <div className="rating-icons">
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star-half-alt  mr-1"></span>
                                                    <span className="fal fa-star  mr-1"></span>
                                                    <small>(10,2192)</small>
                                                </div>
                                                <del>$12,198</del><strong className="ml-2">$1000</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="">
                                        <div className="card">
                                            <a className="courses-links" href="#!">
                                                <img className="card-img-top" src={COURSE_IMG2} alt="Card image cap" />
                                            </a>
                                            <div className="card-body">
                                                <a href="#" className="font-weight-bold">Sed do eiusmod tempor incididunt</a>
                                                <div className="rating-icons">
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star mr-1"></span>
                                                    <span className="fas fa-star-half-alt  mr-1"></span>
                                                    <span className="fal fa-star  mr-1"></span>
                                                    <small>(10,2192)</small>
                                                </div>
                                                <del>$12,198</del><strong className="ml-2">$1000</strong>
                                            </div>
                                        </div>
                                    </div>
                                </Slider>
                            </div>
                        </div>
                    </section>
                    <section className="categories-section mb-5">
                        <div className="container">
                            <div className="row justify-content-center mb-4">
                                <div className="col-xl-9">
                                    <h3 className="text-center font-weight-bold heading_title">Top Categories</h3>
                                </div>
                            </div>
                            <div className="grid-container">
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <a href="training-course-categories.html">
                                            <img className="img-fluid" src={FEATURED_ICON1} alt="featured-img1" />
                                            <p className="font-weight-bold">Development</p>
                                        </a>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON2} alt="featured-img2" />
                                        <p className="font-weight-bold">Business</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON3} alt="featured-img3" />
                                        <p className="font-weight-bold">IT & Software</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON4} alt="featured-img4" />
                                        <p className="font-weight-bold">Design</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON5} alt="featured-img5" />
                                        <p className="font-weight-bold">Marketing</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON1} alt="featured-img1" />
                                        <p className="font-weight-bold">Development</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON2} alt="featured-img2" />
                                        <p className="font-weight-bold">Business</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON3} alt="featured-img3" />
                                        <p className="font-weight-bold">IT & Software</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON4} alt="featured-img4" />
                                        <p className="font-weight-bold">Design</p>
                                    </div>
                                </div>
                                <div className="grid-item">
                                    <img className="img-fluid" src={FEATURED_IMG} alt="featured-bg-img" />
                                    <div className="grid-cnt">
                                        <img className="img-fluid" src={FEATURED_ICON5} alt="featured-img5" />
                                        <p className="font-weight-bold">Marketing</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className="testimonials mb-5 pb-5">
                        <div className="container">
                            <div className="row justify-content-center mb-4">
                                <div className="col-xl-9">
                                    <h3 className="text-center font-weight-bold heading_title">What our students have to say</h3>
                                </div>
                            </div>
                            <div id="carouseltestimonial" className="carousel slide pb-5" data-ride="carousel">
                                <div className="carousel-inner" role="listbox">
                                    <div className="carousel-item text-center active">
                                        <div className="user-icon mb-1">
                                            <img className="img-fluid shadow mb-1" src={TESTIMONIAL_USER} alt="user1" />
                                            <p className="mb-0"><b>Jason Aljets</b></p>
                                            <small>Aljets Home Solutions</small>
                                        </div>
                                        <p className="mt-3">The software to so easy to setup and use. The data that you get out of it is fantastic. I also love the integration to
                                            QuickBooks which makes it even easier to manage the business. Fabulous and versatile product...
                                        EXCEPTIONAL CUSTOMER SERVICE!</p>
                                    </div>
                                    <div className="carousel-item text-center">
                                        <div className="user-icon  mb-1">
                                            <img className="img-fluid shadow mb-1" src={TESTIMONIAL_USER} alt="user1" />
                                            <p className="mb-0"><b>Jason Aljets</b></p>
                                            <small>Aljets Home Solutions</small>
                                        </div>
                                        <p className="mt-3">The software to so easy to setup and use. The data that you get out of it is fantastic. I also love the integration to
                                            QuickBooks which makes it even easier to manage the business. Fabulous and versatile product...
                                        EXCEPTIONAL CUSTOMER SERVICE!</p>
                                    </div>
                                    <div className="carousel-item text-center">
                                        <div className="user-icon">
                                            <img className="img-fluid shadow mb-1" src={TESTIMONIAL_USER} alt="user1" />
                                            <p className="mb-0"><b>Jason Aljets</b></p>
                                            <small>Aljets Home Solutions</small>
                                        </div>
                                        <p className="mt-3">The software to so easy to setup and use. The data that you get out of it is fantastic. I also love the integration to
                                            QuickBooks which makes it even easier to manage the business. Fabulous and versatile product...
                                        EXCEPTIONAL CUSTOMER SERVICE!</p>
                                    </div>
                                </div>
                                <ol className="carousel-indicators">
                                    <li data-target="#carouseltestimonial" data-slide-to="0" className="active"></li>
                                    <li data-target="#carouseltestimonial" data-slide-to="1"></li>
                                    <li data-target="#carouseltestimonial" data-slide-to="2"></li>
                                </ol>
                            </div>
                        </div>
                    </section>
                </main>
                <Footer />
            </div>
        );
    }
}