import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Header } from '../../component/website/header/header';
import { Footer } from '../../component/website/footer/footer';
import BANNER_IMG from '../../../assets/images/theme/benifits-banner.jpg';
import _ from 'lodash';
import { Tokens, User } from '../../../storage/index'
import { validateInputs } from '../../../common/validation';
import { getmembershipData } from '../../../duck/subscription-plan/subscription.selector'
import { addMembership } from '../../../duck/subscription-plan/subscription.action'
import history from '../../../routing/history'
import { CardElement, injectStripe } from 'react-stripe-elements';
import { ADMIN_LOGIN } from '../../../routing/routeContants';
import SimpleCrypto from "simple-crypto-js"

class CreateMembership extends Component {

  constructor(props) {
    super(props)
    this.state = {
      planName: '',
      planDuration: '',
      planPrice: '',
      name: '',
      exp_year: '',
      exp_month: '',
      cvc: '',
      card_number: '',
      agree: false,
      nameErrMessage: '',
      agreeErrMessage: '',
      error: '',
      cardErrorMessage: '',
      loader:false,
      allAddOn: [],
    }
  }

  componentDidMount() {
    let planData = JSON.parse(localStorage.getItem('planDetail'));
    let name = planData.planName;
    let price = planData.price;
    let duration = planData.duration;
    let addonPrice = planData.addonPrice;
    let allAddOn = planData.AllAddOn;
    this.setState({ planName: name, planDuration: duration, planPrice: price, addonPrice:addonPrice, allAddOn:allAddOn })
  }

  async componentWillReceiveProps(props) {
    if (props.getMembershipData && _.has(props.getMembershipData, 'data')) {
      if(props.getMembershipData !== this.props.getMembershipData){
        if (props.getMembershipData.statusCode === 200) {
          localStorage.removeItem('planDetail');
          await Tokens.setToken(props.getMembershipData.data.token);
          const decryptUserType = await new SimpleCrypto("123654*/4514545454");
          let decryptedUserType = await decryptUserType.encrypt(props.getMembershipData.data.userInfo.userTypeId.name);
          await User.setUserType(decryptedUserType);
          User.setUserDetails({name:props.getMembershipData.data.userInfo.firstName , email: props.getMembershipData.data.email});
          this.setState({loader:false})
          history.push(ADMIN_LOGIN)
        }else{
          this.setState({loader:false})
        }
      }
    }
  }

  startMembership = (ev) => {
    ev.preventDefault();
    let stripePlanId = this.props.match.params.stripePlanId;
    let subscriptionPlanId = this.props.match.params.subscriptionPlanId;
    let userId = this.props.match.params.userId;
    const { name, agree } = this.state;

    let nameErrMessage = '', agreeErrMessage = '', cardErrorMessage = '', getError = false;

    if (validateInputs('string', name) === 'empty') {
      nameErrMessage = 'Please enter name.';
      getError = true;
    } else if (validateInputs('string', name) === false) {
      nameErrMessage = 'Please enter valid name.';
      getError = true;
    }

    if (agree === false) {
      agreeErrMessage = 'Please select terms & Conditions.';
      getError = true;
    }

    if (this.props.stripe) {
      this.props.stripe
        .createToken()
        .then((payload) => {

          if (payload.error) {
            cardErrorMessage = payload.error.message;
            getError = true;
          }

          this.setState({ nameErrMessage, agreeErrMessage, cardErrorMessage, error: getError });

          if (getError === false && nameErrMessage === '' && stripePlanId !== '' && userId !== '' && subscriptionPlanId !== '' && agreeErrMessage === '' && cardErrorMessage === '') {
            this.setState({loader:true})
            let planData = JSON.parse(localStorage.getItem('planDetail'));
            //console.log({stripePlanId, subscriptionPlanId, userId, name, token:payload.token})
            this.props.addMembership({ stripePlanId, subscriptionPlanId, userId, name, token: payload.token, addOns:planData.addOn});
          }
        });
    } else {
      console.log("Stripe.js hasn't loaded yet.");
    }
  }

  render() {
    const { name, planDuration, planPrice, planName, agree, nameErrMessage, agreeErrMessage, cardErrorMessage, loader,addonPrice, allAddOn } = this.state;
    
    let AllAddonsDetail = _.map(allAddOn, (al, uu) => {
      return <span key={uu} className="mr-2 liColor"><small className="fa fa-check mr-1"></small>{al.name+' - $'+al.price}</span>
    })
    
    return (
      <div>
        <Header loader={loader}/>
        <main>
          <div className="inner-banner">
            <div className="inner-banner-image">
              <img alt="" className="img-fluid" src={BANNER_IMG} />
            </div>
            <div className="inner-banner-content">
              <div className="container">
                <div className="row d-flex text-center align-items-center">
                  <h1 className="col-sm-12 text-white"> Checkout Steps </h1>
                  <h3 className="col-sm-12 text-white">ITM Tracking is building uniform expectations and requirements for all contractor</h3>
                </div>
              </div>
            </div>
          </div>
          <section className="section divider-height-btm">
            <form onSubmit={this.startMembership}>

              <div className="container">
                <div className="row justify-content-center">
                  <div className="col-xl-6">
                    <h3 className="mb-3 text-center"><span className="font-weight-bold text-primary mt-5"> Payment  </span> Details </h3>
                    <div className="form-row pdt checkout_payment">
                      <div className="col-md-12">
                        <label> Name On The Card<span className="errorCls">*</span> : </label>
                        <input value={name} className="form-control" onChange={(e) => this.setState({ name: e.target.value })} type="text" placeholder="Name" />
                        {nameErrMessage ? <span className="errorCls"> {nameErrMessage}</span> : ''}
                      </div>
                      <div className="col-md-12 mt-3">
                        <label> Card Deatils<span className="errorCls">*</span> : </label>
                        <div className="form-control"><CardElement /></div>
                      </div>
                      {cardErrorMessage ? <span className="errorCls"> {cardErrorMessage}</span> : ''}
                    </div>
                  </div>

                </div>
                <div className="row justify-content-center">
                  <div className="col-xl-8 col-lg-10">
                    <div className="card about-card mb-0 border-radius">
                      <div className="card-body ">
                        <div className="payment-detail">
                          <div className="header d-flex justify-content-between align-item-center mb-3">
                            <h3 className="mb-0"> Your Plan  </h3>
                            {/* <i className="far fa-edit text-right"> <span className="change text-left"> Change </span> </i> */}
                          </div>

                          <span> {planName + ' - ' + planDuration + ' -  $' + planPrice} </span>
                          {AllAddonsDetail.length>0 ? 
                            <div className="plans-tabs">
                              <ul className="list-content p-0">
                                <h5>AddOns - <span className="font-weight-normal"> </span></h5>
                                {AllAddonsDetail}
                              </ul>
                            </div> : ''
                          }
                          <p className="yourplan pt-3"> LLC is a third-party web data base company that records, tracks, and sends notifications of various life safety equipment. We strive to protect lives through creating an informative and uniformed life safety reporting process within a regional area.</p>
                          <p className="text-light"> By ticking the tickbox below, you agree to our Terms of Use, Privacy Statement, and that you are over 18. You may cancel at any time during your free trial and will not be charged. Netflix will automatically continue your membership at the end of your free trial and charge the membership fee (currently ₹ 500) to your payment method on a monthly basis until you cancel. </p>


                          <div className="form-group text-light mb-0">
                            <div className="custom-control custom-checkbox">
                              <input type="checkbox" checked={agree} onChange={(e) => this.setState({ agree: e.target.checked })} className="custom-control-input" id="invalidCheck2" />
                              <label className="custom-control-label font-weight-bold" htmlFor="invalidCheck2">I agree, our term & conditions.</label>                             
                            </div>
                            {agreeErrMessage ? <span className="errorCls h5 ml-1 mb-0 d-block d-sm-inline-block"> {agreeErrMessage}</span> : ''}
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row justify-content-center">
                  {/* <a href="javascript:;" onClick={()=> this.startMembership()} className="btn btn-primary btn-lg mp mb-4">
                        <span className="btn-inner--text text-uppercase"> Pay </span>  */}
                  <button className="btn-primary default-rounded-btn mt-5 mb-4">{'Pay - $' + (parseInt(planPrice)+parseInt(addonPrice))}</button>
                </div>
              </div>
            </form>
          </section>

        </main>
        <Footer />
      </div>
    );
  }
}

const mapStateToprops = (state) => ({
  getMembershipData: getmembershipData(state)
});

const mapDispatchToProps = (dispatch) => bindActionCreators({
  addMembership: addMembership,
}, dispatch);

export const Membership = connect(mapStateToprops, mapDispatchToProps)(injectStripe(CreateMembership));


