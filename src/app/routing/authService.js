import history from './history';
import {
  Tokens, User
} from '../storage';
import {
  ADMIN_LOGIN
} from './routeContants';
import {
  successNotification
} from '../common/notification-alert';

export const onLogout = () => {
  Tokens.removeLocalData();
  successNotification('Logout successfully!!')
  history.push(ADMIN_LOGIN);
  return true;
};

export const getToken = () => Tokens.getToken();
export const getType = () => User.getUserType();

export const isLoggedIn = () => getToken() && getType() && User.getUserDetails();