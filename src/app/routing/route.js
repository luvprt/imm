import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import {
  ADMIN_DASHBOARD, ADMIN_LOGIN, RESET_PASSWORD, HOME,
  BENEFITS, HOW_IT_WORKS, LIFE_SAFETY, ABOUT_US,
  CONTACT_US, TRAINING_PROGRAMS, ADD_CMS_PAGE, EDIT_CMS_PAGE, CMS_PAGES_LIST, REGISTRATION,
  ADD_SUBSCRIPTION_PLAN, LIST_SUBSCRIPTION_PLANS, EDIT_SUBSCRIPTION_PLAN,
  ADD_SUBSCRIPTION_PLAN_HIGHLIGHTS, LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS,
  EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS, CHECKOUT, MEMBERSHIP, THANKYOU, LOCATE_CONTRATORS,
  VIEW_CUSTOMERS, EDIT_CUSTOMER, ADD_CUSTOMER, VIEW_EMPLOYEES, ADD_EMPLOYEE,
  ADD_ONS_LIST, EDIT_ADD_ON, ADD_ADD_ON,  LOCATION_ADDRESS, BILL_TO_ADDRESS, SERVICE, WEBSITE_CMS_PAGE
} from './routeContants';
import { isLoggedIn } from './authService';
import AdminDashboard from '../view/screen/admin/dashboard/admin.dashboard';
import { AdminLogin } from '../view/screen/admin/login/admin.login';
import { AddCmsPage } from '../view/screen/admin/cms/admin.addCmsPage';
import { WebsitePage } from '../view/screen/admin/cms/admin.websiteCmsPage';
import ListCmsPages from '../view/screen/admin/cms/admin.listCmsPages';
import { UserResetPassword } from '../view/screen/admin/login/admin.resetpassword';
import { Home } from '../view/screen/website/home';
import { Checkout } from '../view/screen/website/checkout';
import { Membership } from '../view/screen/website/membership';
import THANKYOUCOMPONENT from '../view/screen/website/thank-you';
import { Benefits } from '../view/screen/website/benefits-of-itm';
import { HowItWorks } from '../view/screen/website/how-it-works';
import { LifeSafety } from '../view/screen/website/life-safety';
import { AboutUs } from '../view/screen/website/about-us';
import ContactUs from '../view/screen/website/contact-us';
import { Register } from '../view/screen/website/register';
import { TrainingPrograms } from '../view/screen/website/training-programs';
import { AddSubscriptionPlan } from '../view/screen/admin/subscription-plans/admin.addSubscriptionPlan'
import ListSubscriptionPlans from '../view/screen/admin/subscription-plans/admin.listSubscriptionPlan'
import { AddSubscriptionPlanHighlight } from '../view/screen/admin/subscription-plans-highlights/admin.addSubscriptionPlanHighlight'
import ListSubscriptionPlansHighlights from '../view/screen/admin/subscription-plans-highlights/admin.listSubscriptionPlanHighlights'
import { LocateContractors } from '../view/screen/website/locate-contractors'
import { ListCustomers } from '../view/screen/admin/customers/admin.listCustomers'
import { AddCustomer } from '../view/screen/admin/customers/admin.addCustomers'
import { ListEmployees } from '../view/screen/admin/employees/admin.listEmployees';
import { AddEmployee } from '../view/screen/admin/employees/admin.addEmployee';
import { LocationAddress } from '../view/screen/admin/location/admin.locationAddress'
import { BillToAddress } from '../view/screen/admin/location/admin.addBillToAddress'
import { AddAddOns } from '../view/screen/admin/add-ons/admin.addAddOns';
import ListAddOns from '../view/screen/admin/add-ons/admin.listAddOns';
import { Service } from '../view/screen/website/service'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      isLoggedIn() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{
            pathname: ADMIN_LOGIN,
            state: { from: props.location },
          }}
          />
        )
    )}
  />
);

const PublicRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      !isLoggedIn() || isLoggedIn() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{
            pathname: ADMIN_DASHBOARD,
            state: { from: props.location },
          }}
          />
        )
    )}
  />
);

const LoginRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      !isLoggedIn() ? (
        <Component {...props} />
      ) : (
          <Redirect to={{
            pathname: ADMIN_DASHBOARD,
            state: { from: props.location },
          }}
          />
        )
    )}
  />
);

const router = (
  <Switch>
    <LoginRoute exact path={ADMIN_LOGIN} component={AdminLogin} />
    <PublicRoute exact path={LOCATE_CONTRATORS} component={LocateContractors} />
    <PublicRoute exact path={RESET_PASSWORD} component={UserResetPassword} />
    <PublicRoute exact path={HOME} component={Home} />
    <PublicRoute exact path={BENEFITS} component={Benefits} />
    <PublicRoute exact path={HOW_IT_WORKS} component={HowItWorks} />
    <PublicRoute exact path={LIFE_SAFETY} component={LifeSafety} />
    <PublicRoute exact path={ABOUT_US} component={AboutUs} />
    <PublicRoute exact path={CONTACT_US} component={ContactUs} />
    <PublicRoute exact path={TRAINING_PROGRAMS} component={TrainingPrograms} />
    <PublicRoute exact path={REGISTRATION} component={Register} />
    <PublicRoute exact path={CHECKOUT} component={Checkout} />
    <PublicRoute exact path={SERVICE} component={Service} />
    <PublicRoute exact path={MEMBERSHIP} component={Membership} />
    <PublicRoute exact path={THANKYOU} component={THANKYOUCOMPONENT} />
    <PrivateRoute exact path={ADMIN_DASHBOARD} component={AdminDashboard} />
    <PrivateRoute exact path={ADD_SUBSCRIPTION_PLAN} component={AddSubscriptionPlan} />
    <PrivateRoute exact path={LIST_SUBSCRIPTION_PLANS} component={ListSubscriptionPlans} />
    <PrivateRoute exact path={EDIT_SUBSCRIPTION_PLAN} component={AddSubscriptionPlan} />
    <PrivateRoute exact path={ADD_SUBSCRIPTION_PLAN_HIGHLIGHTS} component={AddSubscriptionPlanHighlight} />
    <PrivateRoute exact path={LIST_SUBSCRIPTION_PLANS_HIGHLIGHTS} component={ListSubscriptionPlansHighlights} />
    <PrivateRoute exact path={EDIT_SUBSCRIPTION_PLAN_HIGHLIGHTS} component={AddSubscriptionPlanHighlight} />
    <PrivateRoute exact path={ADD_CMS_PAGE} component={AddCmsPage} />
    <PrivateRoute exact path={WEBSITE_CMS_PAGE} component={WebsitePage} />
    <PrivateRoute exact path={CMS_PAGES_LIST} component={ListCmsPages} />
    <PrivateRoute exact path={EDIT_CMS_PAGE} component={AddCmsPage} />
    <PrivateRoute exact path={VIEW_CUSTOMERS} component={ListCustomers} />
    <PrivateRoute exact path={ADD_CUSTOMER} component={AddCustomer} />
    <PrivateRoute exact path={EDIT_CUSTOMER} component={AddCustomer} />
    <PrivateRoute exact path={ADD_EMPLOYEE} component={AddEmployee} />
    <PrivateRoute exact path={VIEW_EMPLOYEES} component={ListEmployees} />
    <PrivateRoute exact path={LOCATION_ADDRESS} component={LocationAddress} />
    <PrivateRoute exact path={BILL_TO_ADDRESS} component={BillToAddress} />
    <PrivateRoute exact path={ADD_ADD_ON} component={AddAddOns} />
    <PrivateRoute exact path={ADD_ONS_LIST} component={ListAddOns} />
    <PrivateRoute exact path={EDIT_ADD_ON} component={AddAddOns} />
    <Redirect from="/*" to={HOME} />
  </Switch>
);

export default router;