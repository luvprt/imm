import { combineReducers } from 'redux';
import { authReducer } from '../duck/auth/auth.reducer';
import { subscriptionReducer } from '../duck/subscription-plan/subscription.reducer';
import { addOnReducer } from '../duck/add-on/addOn.reducer';
import { planHighlightReducer } from '../duck/subscription-plan-highlight/highlight.reducer';
import { cmsReducer } from '../duck/cms/cms.reducer'
import { websiteReducer } from '../duck/website/website.reducer'
import { locationReducer } from '../duck/location/location.reducer'
import { customerReducer } from '../duck/customer/customer.reducer'
import { employeeReducer } from '../duck/employees/employees.reducer'


export const rootReducer = combineReducers({
  auth: authReducer,
  subscription: subscriptionReducer,
  addOn: addOnReducer,
  planHighlight: planHighlightReducer,
  cms: cmsReducer,
  website: websiteReducer,
  location: locationReducer,
  customer: customerReducer,
  employee: employeeReducer
});