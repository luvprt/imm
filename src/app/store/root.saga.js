import { all } from 'redux-saga/effects';
import {
  loginWatcher, forgotPasswordWatcher, resetPasswordWatcher,
  verifyTokenWatcher, registrationWatcher, emailExistWatcher
} from '../duck/auth/auth.saga';
import {
  addSubscriptionWatcher, listSubscriptionWatcher, showSubscriptionWatcher,
  editSubscriptionWatcher, getSubscriptionWithHighlightWatcher,
  getSubscriptionWithHighlightByIdWatcher, addMembershipWatcher,
  deleteSubscriptionWatcher, updateUserWatcher
} from '../duck/subscription-plan/subscription.saga';
import {
  addSubscriptionHighlightWatcher, listSubscriptionHighlightWatcher,
  showSubscriptionHighlightWatcher, editSubscriptionHighlightWatcher,
  AllUserPlanTypesWatcher, deleteSubscriptionHighlightWatcher
} from '../duck/subscription-plan-highlight/highlight.saga';
import {
  addCmsPageWatcher, editCmsPageWatcher,
  viewCmsPageWatcher, listCmsPagesWatcher,
  deleteCmsPagesWatcher, getAllCmsPagesWatcher
} from '../duck/cms/cms.saga';
import {
  getAddOnsWatcher, addAddOnWatcher, viewAddOnWatcher,
  deleteAddOnWatcher, editAddOnWatcher, listAddOnsWatcher
} from '../duck/add-on/addOn.saga';
import {
  contactUsWatcher, getCmsPageDataWatcher, cmsPageBytypeWatcher,
  cmsPageDetailBySlugWatcher
} from '../duck/website/website.saga';
import {
  addLocationPageWatcher, listLocationWatcher, deleteLocationWatcher,
  getLocationByIdWatcher, editLocationPageWatcher, viewLocationWatcher, listCustomersByRoleWatcher, locationStatusWatcher
} from '../duck/location/location.saga';
import { addCustomerWatcher, emailCheckClearBitWatcher } from '../duck/customer/customer.saga';

import { addEmployeeWatcher, listEmployeesWatcher } from '../duck/employees/employees.saga'

export function* rootSaga() {
  yield all([
    loginWatcher(), forgotPasswordWatcher(), resetPasswordWatcher(),
    verifyTokenWatcher(), addSubscriptionWatcher(), listSubscriptionWatcher(),
    getAddOnsWatcher(), registrationWatcher(), showSubscriptionWatcher(),
    editSubscriptionWatcher(), addSubscriptionHighlightWatcher(), listSubscriptionHighlightWatcher(),
    showSubscriptionHighlightWatcher(), editSubscriptionHighlightWatcher(),
    addCmsPageWatcher(), editCmsPageWatcher(), viewCmsPageWatcher(),
    listCmsPagesWatcher(), AllUserPlanTypesWatcher(), getSubscriptionWithHighlightWatcher(),
    getSubscriptionWithHighlightByIdWatcher(), addMembershipWatcher(),
    deleteSubscriptionWatcher(), deleteCmsPagesWatcher(),
    deleteSubscriptionHighlightWatcher(), updateUserWatcher(),
    getAllCmsPagesWatcher(), contactUsWatcher(),
    getCmsPageDataWatcher(), addLocationPageWatcher(),
    emailCheckClearBitWatcher(), listLocationWatcher(), addCustomerWatcher(),
    addEmployeeWatcher(), deleteLocationWatcher(), getLocationByIdWatcher(),
    listEmployeesWatcher(), addAddOnWatcher(), viewAddOnWatcher(),
    deleteAddOnWatcher(), editAddOnWatcher(), listAddOnsWatcher(),
    editLocationPageWatcher(), viewLocationWatcher(), emailExistWatcher(),
    cmsPageBytypeWatcher(), cmsPageDetailBySlugWatcher(), listCustomersByRoleWatcher(), locationStatusWatcher()
  ])
}